﻿
#include "Msnhnet/core/cuda/MsnhGemmGPU.h"
namespace Msnhnet
{

//__global__ void im2col_gpu_kernel_ext(const int n, const float* data_im,
//    const int height, const int width, const int kernel_h, const int kernel_w,
//    const int pad_h, const int pad_w,
//    const int stride_h, const int stride_w,
//    const int dilation_h, const int dilation_w,
//    const int height_col, const int width_col,
//    float* data_col) {
//    CUDA_KERNEL_LOOP(index, n) {
//        const int h_index = index / width_col;
//        const int h_col = h_index % height_col;
//        const int w_col = index % width_col;
//        const int c_im = h_index / height_col;
//        const int c_col = c_im * kernel_h * kernel_w;
//        const int h_offset = h_col * stride_h - pad_h;
//        const int w_offset = w_col * stride_w - pad_w;
//        float* data_col_ptr = data_col;
//        data_col_ptr += (c_col * height_col + h_col) * width_col + w_col;
//        const float* data_im_ptr = data_im;
//        data_im_ptr += (c_im * height + h_offset) * width + w_offset;
//        for (int i = 0; i < kernel_h; ++i) {
//            for (int j = 0; j < kernel_w; ++j) {
//                int h_im = h_offset + i * dilation_h;
//                int w_im = w_offset + j * dilation_w;
//                *data_col_ptr =
//                    (h_im >= 0 && w_im >= 0 && h_im < height && w_im < width) ?
//                    data_im_ptr[i * dilation_h * width + j * dilation_w] : 0;
//                data_col_ptr += height_col * width_col;
//            }
//        }
//    }
//}

__global__ void im2ColExKernel(const int n, float *input,
                                  const int height, const int width,
                                  const int kernelH, const int kernelW,
                                  const int padH, const int padW,
                                  const int strideH,const int strideW,
                                  const int dilationH, const int dilationW,
                                  const int outColH, const int outColW,
                                  float *output)
{
    int index = blockIdx.x * blockDim.x + threadIdx.x;

    for (; index < n; index += blockDim.x * gridDim.x)
    {
        const int hIndex    =   index / outColW;
        const int hCol      =   hIndex % outColH;
        const int wCol      =   index % outColW;
        const int cIm       =   hIndex / outColH;


        const int cCol      =   cIm * kernelH *kernelW;
        const int hOff      =   hCol * strideH - padH;
        const int wOff      =   wCol * strideW - padW;

        float* dataColPtr   =   output;
        dataColPtr         +=   (cCol * outColH + hCol)*outColW + wCol;

        float* dataInPtr    =   input;
        dataInPtr          +=   (cIm * height + hOff) * width + wOff;

        for (int i = 0; i < kernelH; ++i)
        {
            for (int j = 0; j < kernelW; ++j)
            {
                int hIm     =   hOff + i * dilationH;
                int wIm     =   wOff + j * dilationW;

                if(hIm >= 0 && wIm >= 0 && hIm < height && wIm < width)
                {
                    *dataColPtr =  dataInPtr[i * dilationH * width + j * dilationW ];
                }
                else
                {
                    *dataColPtr = 0;
                }

                dataColPtr  += outColH*outColW;
            }
        }

    }
}

void GemmGPU::gpuIm2ColEx(float *input, const int &channelNum,
                  const int &height, const int &width,
                  const int &kernelH, const int &kernelW,
                  const int &padH, const int &padW,
                  const int &strideH,  const int &strideW,
                  const int &dilationH, const int &dilationW,
                  float *output)
{
//    // We are going to launch channels * height_col * width_col kernels, each
//    // kernel responsible for copying a single-channel grid.
//    int height_col = (height + 2 * pad_h -
//        (dilation_h * (kernel_h - 1) + 1)) / stride_h + 1;
//    int width_col = (width + 2 * pad_w -
//        (dilation_w * (kernel_w - 1) + 1)) / stride_w + 1;
//    int num_kernels = channels * height_col * width_col;
//    // NOLINT_NEXT_LINE(whitespace/operators)
//    im2col_gpu_kernel_ext << <CAFFE_GET_BLOCKS(num_kernels),
//        CAFFE_CUDA_NUM_THREADS >> >(
//            num_kernels, data_im, height, width, kernel_h, kernel_w, pad_h,
//            pad_w, stride_h, stride_w, dilation_h, dilation_w, height_col,
//            width_col, data_col);
//    // CUDA: use 512 threads per block
//    const int CAFFE_CUDA_NUM_THREADS = 512;

//    // CUDA: number of blocks for threads. <<N==numKernel>>
//    inline int CAFFE_GET_BLOCKS(const int N) {
//        return (N + CAFFE_CUDA_NUM_THREADS - 1) / CAFFE_CUDA_NUM_THREADS;
//    }



    int outColH     =  (height + 2*padH - (dilationH * (kernelH - 1) + 1)) / strideH + 1;
    int outColW     =  (width  + 2*padW - (dilationW * (kernelW - 1) + 1)) / strideW + 1;
    int numKernel   =  channelNum * outColH * outColW;
    int blocks      =  (numKernel + Cuda::blockThread - 1)/Cuda::blockThread; //计算block, 不足512ye也分配1个block

    im2ColExKernel<<<blocks,Cuda::blockThread,0,Cuda::getCudaStream()>>>(numKernel,input,height,width,kernelH,kernelW,padH,padW,strideH,strideW,dilationH,dilationW,outColH,outColW,output);
    CUDA_CHECK(cudaPeekAtLastError());
}


//__global__ void im2col_gpu_kernel(const int n, const float* data_im,
//        const int height, const int width, const int ksize,
//        const int pad,
//        const int stride,
//        const int height_col, const int width_col,
//        float *data_col) {
//    int index = blockIdx.x*blockDim.x+threadIdx.x;
//    for(; index < n; index += blockDim.x*gridDim.x){
//        int w_out = index % width_col;
//        int h_index = index / width_col;
//        int h_out = h_index % height_col;
//        int channel_in = h_index / height_col;
//        int channel_out = channel_in * ksize * ksize;
//        int h_in = h_out * stride - pad;
//        int w_in = w_out * stride - pad;
//        float* data_col_ptr = data_col;
//        data_col_ptr += (channel_out * height_col + h_out) * width_col + w_out;
//        const float* data_im_ptr = data_im;
//        data_im_ptr += (channel_in * height + h_in) * width + w_in;
//        for (int i = 0; i < ksize; ++i) {
//            for (int j = 0; j < ksize; ++j) {
//                int h = h_in + i;
//                int w = w_in + j;

//                *data_col_ptr = (h >= 0 && w >= 0 && h < height && w < width) ?
//                    data_im_ptr[i * width + j] : 0;

//                //data_im[(channel_in * height + h_in) * width + w_in + i * width + j];
//                //*data_col_ptr = data_im_ptr[ii * width + jj];

//                data_col_ptr += height_col * width_col;
//            }
//        }
//    }
//}

__global__ void im2colKernel(const int n, float *const input,
                              const int height, const int width,
                              const int kSize,
                              const int stride,
                              const int padding,
                              const int outColH, const int outColW,
                              float *const output)
{
    int index = blockIdx.x*blockDim.x+threadIdx.x;
    for(; index < n; index += blockDim.x*gridDim.x)
    {
        int wOut    = index % outColW;
        int hIndex  = index / outColW;
        int hOut    = hIndex % outColH;
        int chIn    = hIndex / outColH;
        int chOut   = chIn * kSize * kSize;
        int hIn     = hOut * stride - padding;
        int wIn     = wOut * stride - padding;
        float* dataColPtr       =   output;
        dataColPtr += (chOut * outColH + hOut) * outColW + wOut;
        const float* dataInPtr  =   input;
        dataInPtr  += (chIn * height + hIn) * width + wIn;
        for (int i = 0; i < kSize; ++i)
        {
            for (int j = 0; j < kSize; ++j)
            {
                int h = hIn + i;
                int w = hIn + j;

                if(h >= 0 && w >= 0 && h < height && w < width)
                {
                    *dataColPtr =  dataInPtr[i * width + j];
                }
                else
                {
                    *dataColPtr = 0;
                }

                dataColPtr  += outColH*outColW;
            }
        }
    }


}

//void im2col_ongpu(float *im, int channels, int height, int width,
//         int ksize, int stride, int pad, float *data_col){
//    // We are going to launch channels * height_col * width_col kernels, each
//    // kernel responsible for copying a single-channel grid.
//    int height_col = (height + 2 * pad - ksize) / stride + 1;
//    int width_col = (width + 2 * pad - ksize) / stride + 1;
//    int num_kernels = channels * height_col * width_col;
//    im2col_gpu_kernel<<<(num_kernels+BLOCK-1)/BLOCK,
//        BLOCK, 0, get_cuda_stream()>>>(
//                num_kernels, im, height, width, ksize, pad,
//                stride, height_col,
//                width_col, data_col);

//    CHECK_CUDA(cudaPeekAtLastError());
//}

void GemmGPU::gpuIm2col(float *const &input, const int &channelNum,
                     const int &height, const int &width,
                     const int &kSize,
                     const int &stride,
                     const int &padding,
                     float *const &output)
{
    int outColH     =   (height + 2 * padding - kSize) / stride + 1;
    int outColW     =   (width  + 2 * padding - kSize) / stride + 1;
    int numKernel   =   channelNum * outColH * outColW;
    int blocks      =   (numKernel + Cuda::blockThread - 1)/Cuda::blockThread; //计算block, 不足512ye也分配1个block

    im2colKernel<<<blocks,Cuda::blockThread,0,Cuda::getCudaStream()>>>(numKernel,input,height,width,kSize,stride,padding,outColH,outColW,output);
    CUDA_CHECK(cudaPeekAtLastError());
}


void GemmGPU::gpuGemm(const int &TA, const int &TB, const int &M, const int &N, const int &K, const float &ALPHA,
                   float * const &A, const int &lda,
                   float * const &B, const int &ldb,
                   const float &BETA,
                   float * const &C, const int &ldc)
{
    cublasHandle_t handle = Cuda::getBlasHandle();
    CUBLAS_CHECK(cublasSetStream(handle, Cuda::getCudaStream()));
    CUBLAS_CHECK(cublasSgemm(handle, (TB ? CUBLAS_OP_T : CUBLAS_OP_N),(TA ? CUBLAS_OP_T : CUBLAS_OP_N), N, M, K, &ALPHA, B, ldb, A, lda, &BETA, C, ldc));
}

}










﻿#include "Msnhnet/utils/MsnhMathUtils.h"
namespace Msnhnet
{
float MathUtils::sumArray(float * const &x, const int &xNum)
{
    float sum = 0;
    for (int i = 0; i < xNum; ++i)
    {
        sum = sum + x[i];
    }
    return sum;
}

float MathUtils::meanArray(float * const &x, const int &xNum)
{
    return sumArray(x,xNum)/xNum;
}

float MathUtils::randUniform(float min, float max)
{
    if(max < min)
    {
        float swap = min;
        min = max;
        max = swap;
    }

#if (RAND_MAX < 65536)
    int rnd = rand()*(RAND_MAX + 1) + rand();
    return ((float)rnd / (RAND_MAX*RAND_MAX) * (max - min)) + min;
#else
    return ((float)rand() / RAND_MAX * (max - min)) + min;
#endif
}

float MathUtils::randNorm()
{
    static int haveSapre    = 0;
    static double  rand1    = 0;
    static double  rand2    = 0;

    if(haveSapre)
    {
        haveSapre = 0;
        return static_cast<float>(sqrt(rand1) * sin(rand2));
    }

    haveSapre   = 1;

    rand1       =   randomGen() / ((double)RAND_MAX);
    if(rand1 < 1e-100)
    {
        rand1   =  1e-100;
    }
    rand1       =   -2 * log(rand1);
    rand2       =   (randomGen() / ((double)RAND_MAX)) * 2.0 *M_PI;

    return static_cast<float>(sqrt(rand1) * cos(rand2));

}

unsigned int MathUtils::randomGen()
{
    unsigned int rnd = 0;
    rnd = rand();
#if (RAND_MAX < 65536)
    rnd = rand()*(RAND_MAX + 1) + rnd;
#endif  //(RAND_MAX < 65536)
    return rnd;
}

double MathUtils::sign(double val)
{
    return (val<0)?(-1):1;
}

void MathUtils::threePoint2Circle(const Frame &frame1, const Frame &frame2, const Frame &frame3,TranslationDS& center, double& angle, double& radius, bool& neg)
{
    TranslationDS p1 = frame1.trans;
    TranslationDS p2 = frame2.trans;
    TranslationDS p3 = frame3.trans;

    TranslationDS pa = p1-p2;
    TranslationDS pb = p3-p2;

    TranslationDS pc = TranslationDS::crossProduct(pa,pb);

    if(pa.length()<MSNH_F32_EPS || pb.length()<MSNH_F32_EPS || pc.length()<MSNH_F32_EPS)
    {
        throw Exception(1,"No circle plane!",__FILE__,__LINE__,__FUNCTION__);
    }
    double x1 = p1[0];
    double y1 = p1[1];
    double z1 = p1[2];

    double x2 = p2[0];
    double y2 = p2[1];
    double z2 = p2[2];

    double x3 = p3[0];
    double y3 = p3[1];
    double z3 = p3[2];

    double l1 = x1*x1 + y1*y1 + z1*z1;
    double l2 = x2*x2 + y2*y2 + z2*z2;
    double l3 = x3*x3 + y3*y3 + z3*z3;

    double m = y1*(z2 - z3) + y2*(z3 - z1) + y3*(z1 - z2);
    double n = -(x1*(z2 - z3) + x2*(z3 - z1) + x3*(z1 - z2));
    double p = x1*(y2 - y3) - x2*(y1 - y3) - x3*(y2 - y1);
    double q = -(x1*(y2*z3 - y3*z2) - x2*y1*z3 + x2*y3*z1 + x3*(y1*z2 - y2*z1));


    MatSDS mat(4,4);

    mat.setVal({
                   x1,y1,z1,1,
                   x2,y2,z2,1,
                   x3,y3,z3,1,
                   -m,-n,-p,0
               });

    double A = mat.det();

    mat.setVal({
                   l1,y1,z1,1,
                   l2,y2,z2,1,
                   l3,y3,z3,1,
                   2*q,-n,-p,0
               });

    double B= -mat.det();

    mat.setVal({
                   l1,x1,z1,1,
                   l2,x2,z2,1,
                   l3,x3,z3,1,
                   2*q,-m,-p,0
               });

    double C= mat.det();

    mat.setVal({
                   l1,x1,y1,1,
                   l2,x2,y2,1,
                   l3,x3,y3,1,
                   2*q,-m,-n,0
               });

    double D= -mat.det();

    center.setval(-0.5*B/A,-0.5*C/A,-0.5*D/A);

    TranslationDS OA = frame1.trans - center;
    TranslationDS OB = frame2.trans - center;
    TranslationDS OC = frame3.trans - center;

    radius = OA.length();

    double angAOB = acos((OA[0]*OB[0]+ OA[1]*OB[1]+OA[2]*OB[2])/(OA.length()*OB.length()));
    double angBOC = acos((OB[0]*OC[0]+ OB[1]*OC[1]+OB[2]*OC[2])/(OB.length()*OC.length()));
    double angAOC = acos((OA[0]*OC[0]+ OA[1]*OC[1]+OA[2]*OC[2])/(OA.length()*OC.length()));

    if(angAOB + angBOC > MSNH_PI)
    {
        angle   = MSNH_2_PI - angAOC; //取圆另一半
        neg     = true;
    }
    else
    {
        angle   = angAOC;
        neg     = false;
    }
}
}

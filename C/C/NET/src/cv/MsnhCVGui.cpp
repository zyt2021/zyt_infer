﻿#ifdef USE_MSNHCV_GUI
#include "Msnhnet/cv/MsnhCVGui.h"
//#define STB_IMAGE_IMPLEMENTATION
#include "../3rdparty/stb/stb_image.h"
#include "Msnhnet/cv/MsnhCVMatOp.h"

// About Desktop OpenGL function loaders:
//  Modern desktop OpenGL doesn't have a standard portable header file to load OpenGL function pointers.
//  Helper libraries are often used for this purpose! Here we are supporting a few common ones (gl3w, glew, glad).
//  You may use another loader/header of your choice (glext, glLoadGen, etc.), or chose to manually implement your own.
#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
#include <GL/gl3w.h>           // Initialize with gl3wInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
#include <GL/glew.h>            // Initialize with glewInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
#include <glad/glad.h>          // Initialize with gladLoadGL()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD2)
#include <glad/gl.h>            // Initialize with gladLoadGL(...) or gladLoaderLoadGL()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLBINDING2)
#define GLFW_INCLUDE_NONE       // GLFW including OpenGL headers causes ambiguity or multiple definition errors.
#include <glbinding/Binding.h>  // Initialize with glbinding::Binding::initialize()
#include <glbinding/gl/gl.h>
using namespace gl;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLBINDING3)
#define GLFW_INCLUDE_NONE       // GLFW including OpenGL headers causes ambiguity or multiple definition errors.
#include <glbinding/glbinding.h>// Initialize with glbinding::initialize()
#include <glbinding/gl/gl.h>
using namespace gl;
#else
#include IMGUI_IMPL_OPENGL_LOADER_CUSTOM
#endif

// Include glfw3.h after our OpenGL definitions
#include <GLFW/glfw3.h>

// [Win32] Our example includes a copy of glfw3.lib pre-compiled with VS2010 to maximize ease of testing and compatibility with old VS compilers.
// To link with VS2010-era libraries, VS2015+ requires linking with legacy_stdio_definitions.lib, which we do using this pragma.
// Your own project should not be affected, as you are likely to link with a newer binary of GLFW that is adequate for your version of Visual Studio.
#if defined(_MSC_VER) && (_MSC_VER >= 1900) && !defined(IMGUI_DISABLE_WIN32_FUNCTIONS)
#pragma comment(lib, "legacy_stdio_definitions")
#endif

namespace Msnhnet
{

#ifndef _WIN32
int _kbhit(void)
{
    struct termios oldt, newt;
    int ch;
    int oldf;

    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
    fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

    ch = getchar();

    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    fcntl(STDIN_FILENO, F_SETFL, oldf);

    if(ch != EOF)
    {
        ungetc(ch, stdin);
        return 1;
    }

    return 0;
}
#endif

static void glfw_error_callback(int error, const char* description)
{
    fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}

void GLErrorMessageCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam)
{
    (void)source;
    (void)id;
    (void)length;
    (void)userParam;
    std::cerr <<
                 "GL CALLBACK:" <<
                 ( type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "") <<
                 " type:" << type << " severity:" << severity << " message:" <<  message << "\n";
}

std::mutex Gui::mutex;
bool Gui::isRunning = false;
bool Gui::started   = false;
std::thread Gui::th;
std::map<std::string,Mat> Gui::mats;
std::map<std::string,bool> Gui::matInited;
std::map<std::string,unsigned int> Gui::matTextures;
std::map< std::string, std::map< std::string, Plot > > Gui::xyDatas;
std::string Gui::fontPath = "";
float Gui::fontSize = 12.f;

#ifdef _WIN32
BOOL exitWinGui( DWORD fdwCtrlType )
{
    if(fdwCtrlType == CTRL_CLOSE_EVENT)
    {
        Gui::stopIt();
    }
    return true;
}
#else
void exitUnixGui(int signo)
{
    (void)signo;
    Gui::stopIt();
}
#endif

void Gui::startIt()
{
#ifdef _WIN32
    SetConsoleCtrlHandler((PHANDLER_ROUTINE)exitWinGui,true);
#else
    signal(SIGINT, exitUnixGui);
#endif
    Gui::isRunning = true;
    Gui::th = std::thread(&Gui::run);
}

void Gui::imShow(const std::string &title, Mat &mat)
{
    std::string tmpTitle = title + "_img";
    if(!Gui::started)
    {
        Gui::startIt();
        Gui::started = true;
    }
    Mat tmpMat;
    mat.convertTo(tmpMat,CVT_DATA_TO_U8);
    if(tmpMat.getChannel()==1)
    {
        MatOp::cvtColor(tmpMat,tmpMat,CVT_GRAY2RGBA);
    }
    else if(tmpMat.getChannel()==3)
    {
        MatOp::cvtColor(tmpMat,tmpMat,CVT_RGB2RGBA);
    }

    mutex.lock();
    //    if(matTextures.find(tmpTitle)!=matTextures.end())
    //    {
    //        glDeleteTextures(1,&matTextures[tmpTitle]);
    //    }
    mats[tmpTitle] = tmpMat;
    matInited[tmpTitle] = false;
    matTextures[tmpTitle] = -1;
    mutex.unlock();
}

void Gui::plotXYData(const std::string &title, const std::string &plotName, const Plot &data, const std::string &xLabel, const std::string &yLabel)
{
    if(!Gui::started)
    {
        Gui::startIt();
        Gui::started = true;
    }

    std::string tmpTitle = title + "_plot"+u8"¤"+xLabel+u8"¤"+yLabel;
    mutex.lock();
    xyDatas[tmpTitle][plotName] = data;
    mutex.unlock();
}

void Gui::plotLine(const std::string &title, const std::string &lineName, const std::vector<Vec2F32> &data)
{
    Plot p1;
    p1.plotType = PLOT_LINE;
    p1.marker   = ImPlotMarker_Circle;
    p1.withPoint = true;
    p1.data     = data;
    plotXYData(title, lineName, p1);
}

void Gui::plotPoints(const std::string &title, const std::string &pointsName, const std::vector<Vec2F32> &data)
{
    Plot p2;
    p2.plotType = PLOT_POINTS;
    p2.marker   = ImPlotMarker_Circle;
    p2.data     = data;
    plotXYData(title, pointsName, p2);
}

void Gui::stopIt()
{
    isRunning = false;
    Gui::th.join();
}


#ifdef linux
int getch(void)
{
    struct termios oldattr, newattr;
    int ch;
    tcgetattr( STDIN_FILENO, &oldattr );
    newattr = oldattr;
    newattr.c_lflag &= ~( ICANON | ECHO );
    tcsetattr( STDIN_FILENO, TCSANOW, &newattr );
    ch = getchar();
    tcsetattr( STDIN_FILENO, TCSANOW, &oldattr );
    return ch;
}
#endif

void Gui::wait(int i)
{
    if(i==0)
    {
        int ch;
        while (1)
        {
            if (_kbhit())
            {//如果有按键按下，则_kbhit()函数返回真
                ch = getch();//使用_getch()函数获取按下的键值
                if (ch == 27){ break; }//当按下ESC时循环，ESC键的键值时27.
            }
        }
        Gui::stopIt();
    }
    else
    {
#ifdef _WIN32
        _sleep(i);
#else
        sleep(i);
#endif
    }
}

bool Gui::waitEnterKey()
{
    if (_kbhit())
    {//如果有按键按下，则_kbhit()函数返回真

        int ch = getchar();//使用_getch()函数获取按下的键值
        if (ch == 10)
        {
            return true;
        }
        else
        {
            return false;
        }//当按下ESC时循环，ESC键的键值时27.
    }
    return false;
}

void Gui::setFont(const std::string &fontPath, const float &size)
{
    Gui::fontPath = fontPath;
    Gui::fontSize = size;
}

void Gui::run()
{

    // Setup window
    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
        return;

    // Decide GL+GLSL versions
#ifdef __APPLE__
    // GL 3.2 + GLSL 150
    const char* glsl_version = "#version 150";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // Required on Mac
#else
    // GL 3.0 + GLSL 130
    const char* glsl_version = "#version 130";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // 3.0+ only
#endif

    // Create window with graphics context
    GLFWwindow* window = glfwCreateWindow(1280, 720, "MsnhCV GUI (Press esc in terminal to exit!)", NULL, NULL);
    if (window == NULL)
        return;
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Enable vsync

    // Initialize OpenGL loader
#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
    bool err = gl3wInit() != 0;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
    bool err = glewInit() != GLEW_OK;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
    bool err = gladLoadGL() == 0;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD2)
    bool err = gladLoadGL(glfwGetProcAddress) == 0; // glad2 recommend using the windowing library loader instead of the (optionally) bundled one.
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLBINDING2)
    bool err = false;
    glbinding::Binding::initialize();
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLBINDING3)
    bool err = false;
    glbinding::initialize([](const char* name) { return (glbinding::ProcAddress)glfwGetProcAddress(name); });
#else
    bool err = false; // If you use IMGUI_IMPL_OPENGL_LOADER_CUSTOM, your loader is likely to requires some form of initialization.
#endif
    if (err)
    {
        fprintf(stderr, "Failed to initialize OpenGL loader!\n");
        return;
    }

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImPlot::CreateContext();

    ImGuiIO& io = ImGui::GetIO(); (void)io;
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
    if(fontPath!="")
        io.Fonts->AddFontFromFileTTF(fontPath.c_str(), fontSize, NULL, io.Fonts->GetGlyphRangesChineseFull());
    //IM_ASSERT(font != NULL);

    // Setup Dear ImGui style
    //ImGui::StyleColorsDark();
    ImGui::StyleColorsClassic();
    //ImGui::StyleColorsLight();

    // Setup Platform/Renderer bindings
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);


    // Load Fonts
    // - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them.
    // - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple.
    // - If the file cannot be loaded, the function will return NULL. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
    // - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
    // - Read 'docs/FONTS.md' for more instructions and details.
    // - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
    //io.Fonts->AddFontDefault();
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/Roboto-Medium.ttf", 16.0f);
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/Cousine-Regular.ttf", 15.0f);
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/DroidSans.ttf", 16.0f);
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/ProggyTiny.ttf", 10.0f);
    //ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());
    //IM_ASSERT(font != NULL);
    //io.Fonts->AddFontFromFileTTF("c:/windows/fonts/simhei.ttf", 13.0f, NULL, io.Fonts->GetGlyphRangesChineseSimplifiedCommon());

    // Our state

    while(isRunning)
    {
        // Poll and handle events (inputs, window resize, etc.)
        // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
        // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
        // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
        // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
        glfwPollEvents();

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        mutex.lock();

        //ImPlot::ShowDemoWindow();

        for (auto &line : xyDatas)
        {
            std::string tmp = line.first;
            std::vector<std::string> listStr;
            ExString::split(listStr, tmp, u8"¤");

            ImGui::Begin(listStr[0].c_str());
            ImGui::SetWindowSize(ImVec2(600, 350));

            std::map< std::string, Plot > lineTmp = line.second;

            if (ImPlot::BeginPlot(listStr[0].c_str(),listStr[1].c_str(),listStr[2].c_str()))
            {
                ImPlot::GetStyle().AntiAliasedLines = true;
                for(auto &l : lineTmp)
                {
                    Plot p = l.second;
                    std::vector<Vec2F32> data = p.data;

                    std::vector<float> x;
                    std::vector<float> y;

                    for (int i = 0; i < data.size(); ++i)
                    {
                        x.push_back(data[i].x1);
                        y.push_back(data[i].x2);
                    }

                    if(p.plotType == PLOT_LINE)
                    {
                        if(p.withPoint)
                        {
                            ImPlot::SetNextMarkerStyle(p.marker);
                        }

                        ImPlot::PlotLine(l.first.c_str(), x.data(), y.data(), x.size());
                    }
                    else if(p.plotType == PLOT_POINTS)
                    {
                        ImPlot::PushStyleVar(ImPlotStyleVar_FillAlpha, 0.25f);
                        ImPlot::SetNextMarkerStyle(p.marker, 2, ImVec4(0,1,0,0.5f), IMPLOT_AUTO, ImVec4(0,1,0,1));
                        ImPlot::PlotScatter(l.first.c_str(), x.data(), y.data(), x.size());
                    }

                }
                ImPlot::EndPlot();
            }
            ImGui::End();
        }

        for (auto &init : matInited)
        {
            if(!init.second)
            {
                GLuint texture;
                glGenTextures(1,&texture);
                glBindTexture(GL_TEXTURE_2D, texture);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
                Mat tmpMat = mats[init.first];

                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,tmpMat.getWidth(), tmpMat.getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, tmpMat.getData().u8);
                matTextures[init.first] = texture;
                init.second = true;
            }

            if(init.second)
            {
                int width  = mats[init.first].getWidth();
                int height = mats[init.first].getHeight();

                ImGui::Begin(init.first.c_str());   // Pass a pointer to our bool variable (the window will have a closing button that will clear the bool when clicked)
                ImGui::SetWindowSize(ImVec2(width+20, height+50));
                ImGui::Image((void*)(intptr_t)matTextures[init.first], ImVec2(width, height));
                ImGui::End();
            }
        }

        mutex.unlock();


        // Rendering
        ImGui::Render();
        int display_w, display_h;
        glfwGetFramebufferSize(window, &display_w, &display_h);
        glViewport(0, 0, display_w, display_h);
        // glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        glfwSwapBuffers(window);
    }

    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImPlot::DestroyContext();
    ImGui::DestroyContext();

    glfwDestroyWindow(window);
    glfwTerminate();
}

}
#endif

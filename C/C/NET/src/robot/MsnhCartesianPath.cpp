﻿#include <Msnhnet/robot/MsnhCartesianPath.h>
#include <Msnhnet/utils/MsnhMathUtils.h>


namespace Msnhnet
{

/// KDL里方法是这样的，它通过一个等效半径将角度与其想乘转化为等效长度，表示姿态的变换的幅度，而质点的位移的路径长度（线段加圆弧长）
/// 表示位移的变化的幅度。通过比较出这两者的大小，来确定将谁作为pathlength（程序里的s）。具体的处理方式如下，
///
/// alpha*eqradius > L ---> S = alpha*eqradius ----> L(t) = S(t)*scaleLinear = S(t)*L/S
///                                                  Alpha(t) = S(t)*scaleRotation = S(t)*1/eqradius
///
/// alpha*eqradius < L ---> S = L  ----> L(t) = S(t)*scaleLinear = S(t)*1
///                                      Alpha(t) = S(t)*scaleRotation = S(t)*alpha/eqradius
///
CartesianPathLine::CartesianPathLine(const Frame &FBaseStart, const Frame &FBaseEnd, RotationInterp *orient, double eqradius, bool aggregate)
    :_orient(orient),
    _VBase2Start(FBaseStart.trans),
    _VBase2End(FBaseEnd.trans),
    _eqradius(eqradius),
    _aggregate(aggregate)
{
    _VStart2End     =   _VBase2End - _VBase2Start;
    double distance =   _VStart2End.length();
    _VStart2End.normalize();
    _orient->setStartEnd(FBaseStart.rotMat, FBaseEnd.rotMat);
    double alpha    =   _orient->getAngle();

    if(alpha!=0 && alpha*eqradius>distance )
    {
        _pathLength     = alpha * _eqradius;
        _scaleRotation  = 1/_eqradius;
        _scaleLinear    = distance/_pathLength;
    }
    else if( distance != 0)
    {
        _pathLength     = distance;
        _scaleRotation  = alpha/_pathLength;
        _scaleLinear    = 1;
    }
    else
    {
        _pathLength     = 0;
        _scaleRotation  = 1;
        _scaleLinear    = 1;
    }
}

CartesianPathLine::~CartesianPathLine()
{
    if(_aggregate)
    {
        delete _orient;
    }
}

double CartesianPathLine::getLengthToScale(double length)
{
    return length / _scaleLinear;
}

double CartesianPathLine::getPathLength()
{
    return _pathLength;
}

Frame CartesianPathLine::getPos(double s) const
{
    return Frame(_orient->getPos(s * _scaleRotation), _VBase2Start + _VStart2End * s *_scaleLinear);
}

Twist CartesianPathLine::getVel(double s, double sd) const
{
    return Twist(_VStart2End*sd*_scaleLinear, _orient->getVel(s*_scaleRotation, sd*_scaleRotation));
}

Twist CartesianPathLine::getAcc(double s, double sd, double sdd) const
{
    return Twist(_VStart2End*sdd*_scaleLinear, _orient->getAcc(s*_scaleRotation, sd*_scaleRotation, sdd*_scaleRotation));
}

CartesianPathCircle::CartesianPathCircle(const Frame &fBase2Start, const Vector3DS &vBase2Center, const Frame &fBase2End, double alpha, RotationInterp *orient, double eqradius, bool negWay, bool aggregate)
    :_orient(orient),_negWay(negWay),_eqradius(eqradius),_aggregate(aggregate)
{
    _fBase2Center.trans     = vBase2Center;
    _orient->setStartEnd(fBase2Start.rotMat, fBase2End.rotMat);

    double oAlpha = _orient->getAngle();

    Vector3DS x(fBase2Start.trans - _fBase2Center.trans);

    _radius     = x.length();
    x.normalize();

    if(_radius < MSNH_F32_EPS)
    {
        throw Exception(1,"Circle is too small !",__FILE__,__LINE__,__FUNCTION__);
    }

    Vector3DS tmpVec(fBase2End.trans - _fBase2Center.trans);
    tmpVec.normalize();

    Vector3DS z = Vector3DS::crossProduct(x, tmpVec);
    double n = z.length();
    z.normalize();

    if(n < MSNH_F32_EPS)
    {
        throw Exception(1,"No circle plane!",__FILE__,__LINE__,__FUNCTION__);
    }

    _fBase2Center.rotMat = RotationMatDS(x, Vector3DS::crossProduct(z, x), z);

    double distance = alpha * _radius;

    if(oAlpha*_eqradius > distance)
    {
        //旋转插值作为极限
        _pathLength    = oAlpha * _eqradius;
        _scaleRotation = 1/_eqradius;
        _scaleLinear   = distance/_pathLength;
    }
    else
    {
        //位移作为极限
        _pathLength    = distance;
        _scaleRotation = oAlpha/_pathLength;
        _scaleLinear   = 1;
    }

}

CartesianPathCircle::CartesianPathCircle(const Frame &frame1, const Frame &frame2, const Frame &frame3, RotationInterp *orient, double eqradius, bool fullCircle, bool aggregate)
    :_orient(orient),_eqradius(eqradius),_aggregate(aggregate)
{
    TranslationDS center;
    double radius = 0.0;
    double angle  = 0.0;
    bool   neg    = false;
    MathUtils::threePoint2Circle(frame1, frame2, frame3, center, angle, radius, neg);

    _negWay = neg;

    _fBase2Center.trans     = center;
    _orient->setStartEnd(frame1.rotMat, frame3.rotMat);

    double oAlpha = _orient->getAngle();

    Vector3DS x(frame1.trans - _fBase2Center.trans);

    _radius     = x.length();
    x.normalize();

    if(_radius < MSNH_F32_EPS)
    {
        throw Exception(1,"Circle is too small !",__FILE__,__LINE__,__FUNCTION__);
    }

    Vector3DS tmpVec(frame3.trans - _fBase2Center.trans);
    tmpVec.normalize();

    Vector3DS z = Vector3DS::crossProduct(x, tmpVec);
    double n = z.length();
    if(n < MSNH_F32_EPS)
    {
        Vector3DS tmpVec(frame2.trans - _fBase2Center.trans);
        tmpVec.normalize();

        z = Vector3DS::crossProduct(x, tmpVec);
        double n = z.length();
        if(n < MSNH_F32_EPS)
        {
            throw Exception(1,"No circle plane!",__FILE__,__LINE__,__FUNCTION__);
        }
    }

    z.normalize();
    _fBase2Center.rotMat = RotationMatDS(x, Vector3DS::crossProduct(z, x), z);

    if(fullCircle)
    {
        angle = MSNH_2_PI;
    }

    double distance = angle * _radius;

    if(oAlpha*_eqradius > distance)
    {
        //旋转插值作为极限
        _pathLength    = oAlpha * _eqradius;
        _scaleRotation = 1/_eqradius;
        _scaleLinear   = distance/_pathLength;
    }
    else
    {
        //位移作为极限
        _pathLength    = distance;
        _scaleRotation = oAlpha/_pathLength;
        _scaleLinear   = 1;
    }
}

CartesianPathCircle::~CartesianPathCircle()
{
    if(_aggregate)
    {
        delete _orient;
    }
}

double CartesianPathCircle::getLengthToScale(double length)
{
    return length/_scaleLinear;
}

double CartesianPathCircle::getPathLength()
{
    return _pathLength;
}

Frame CartesianPathCircle::getPos(double s) const
{
    double pos      = s*_scaleLinear / _radius;
    if(_negWay)     // 旋转反向
        pos = -pos; // 反向
    return Frame(_orient->getPos(s*_scaleRotation),_fBase2Center*Vector3DS(_radius*cos(pos), _radius*sin(pos),0));
}

Twist CartesianPathCircle::getVel(double s, double sd) const
{
    double pos = s*_scaleLinear / _radius;
    double vel = sd*_scaleLinear / _radius;

    return Twist(_fBase2Center.rotMat*Vector3DS(-vel*_radius*sin(pos), vel*_radius*cos(pos),0),
        _orient->getVel(s*_scaleRotation, sd*_scaleRotation)
        );
}

Twist CartesianPathCircle::getAcc(double s, double sd, double sdd) const
{
    double pos  = s*_scaleLinear / _radius;
    double cPos = cos(pos);
    double sPos = sin(pos);
    double vel  = sd*_scaleLinear / _radius;
    double acc  = sdd*_scaleLinear / _radius;
    return Twist( _fBase2Center.rotMat*Vector3DS(
                     -_radius*cPos*vel*vel  -  _radius*sPos*acc,
                     -_radius*sPos*vel*vel  +  _radius*cPos*acc,
                     0
                     ),
        _orient->getAcc(s*_scaleRotation,sd*_scaleRotation,sdd*_scaleRotation)
        );
}




CartesianPathComposite::CartesianPathComposite()
{
    _pathLength     = 0;
    _cachedStarts   = 0;
    _cachedEnds     = 0;
    _cachedIndex    = 0;
}

CartesianPathComposite::~CartesianPathComposite()
{
    auto iter = _geoVec.begin();

    while (iter!=_geoVec.end())
    {
        if(iter->second)
        {
            delete iter->first;
        }
        iter++;
    }
}

void CartesianPathComposite::add(CartesianPath *geom, bool aggregate)
{
    _pathLength += geom->getPathLength();
    _dVec.push_back(_pathLength);
    _geoVec.push_back(std::make_pair(geom,aggregate));
}

double CartesianPathComposite::getLengthToScale(double length)
{
    (void)length;
    throw Exception(1,"Function can not be called here!", __FILE__, __LINE__, __FUNCTION__);
}

double CartesianPathComposite::getPathLength()
{
    return _pathLength;
}

Frame CartesianPathComposite::getPos(double s) const
{
    s = lookUp(s);
    return _geoVec[_cachedIndex].first->getPos(s);
}

Twist CartesianPathComposite::getVel(double s, double sd) const
{
    s = lookUp(s);
    return _geoVec[_cachedIndex].first->getVel(s,sd);
}

Twist CartesianPathComposite::getAcc(double s, double sd, double sdd) const
{
    s = lookUp(s);
    return _geoVec[_cachedIndex].first->getAcc(s,sd,sdd);
}

double CartesianPathComposite::lookUp(double s) const
{
    assert(s>=-1e-12);
    assert(s<=(_pathLength+1e-12));

    if((_cachedStarts<=s)&&(s<=_cachedEnds))
    {
        return s-_cachedStarts;
    }

    double prevS = 0;

    for (int i = 0; i < _dVec.size(); ++i)
    {
        if((s<=_dVec[i]) || (i==_dVec.size()-1) )
        {
            _cachedIndex    = i;
            _cachedStarts   = prevS;
            _cachedEnds     = _dVec[i];

            return s - prevS;
        }

        prevS = _dVec[i];
    }

    return 0;
}

CartesianPathRoundComposite::CartesianPathRoundComposite(double radius, double eqradius, RotationInterp *orient, bool aggregate)
    : _radius(radius),_eqradius(eqradius),
    _orient(orient),_aggregate(aggregate)
{
    _comp = new CartesianPathComposite();
    _numOfPoints = 0;
    assert(_eqradius>0);
}

CartesianPathRoundComposite::~CartesianPathRoundComposite()
{
    delete _comp;
    _comp = nullptr;
}

void CartesianPathRoundComposite::add(const Frame &fBase2Point)
{
    if(_numOfPoints == 0)
    {
        _fBase2Start = fBase2Point;
    }
    else if( _numOfPoints == 1)
    {
        _fBase2Via   = fBase2Point;
    }
    else
    {

        Vector3DS AB = _fBase2Via.trans - _fBase2Start.trans;
        Vector3DS BC = fBase2Point.trans - _fBase2Via.trans;

        double ABDistance = AB.length();
        double BCDistance = BC.length();

        if(ABDistance < MSNH_F32_EPS)
        {
            throw Exception(1,"AB points too close!",__FILE__,__LINE__,__FUNCTION__);
        }

        if(BCDistance < MSNH_F32_EPS)
        {
            throw Exception(1,"BC points too close!",__FILE__,__LINE__,__FUNCTION__);
        }

        double alpha = std::acos(max(-1.0, min(Vector3DS::dotProduct(AB,BC)/ABDistance/BCDistance, 1.0)));

        //如果第一段和第二段之间的角度接近PI(意思是各部分相互重叠）
        if((MSNH_PI- alpha) < MSNH_F32_EPS)
        {
            throw Exception(1,"Lines are on top of each other!",__FILE__,__LINE__,__FUNCTION__);
        }

        /// -----------x x--------- 直线平行

        if(alpha < MSNH_F32_EPS)
        {
            _comp->add(new CartesianPathLine(_fBase2Start, _fBase2Via, _orient->clone(), _eqradius));
            _fBase2Start = _fBase2Via; //上一个via为下一个start
            _fBase2Via   = fBase2Point;//上一个base2Point为下一个via
        }
        else //不平行
        {
            double d = _radius / tan((MSNH_PI - alpha)/2);//保证不返回0
            //如果圆角所需的距离大于第一段。
            if(d >= ABDistance)
            {
                throw Exception(1,"The distance needed for the rounding is larger than the first segment !",__FILE__,__LINE__,__FUNCTION__);
            }

            //如果圆角所需的距离大于第二段。
            if(d >= BCDistance)
            {
                throw Exception(1,"The distance needed for the rounding is larger than the second segment !",__FILE__,__LINE__,__FUNCTION__);
            }


            ///
            /// A                    C
            ///   \                 /
            ///     \             /
            ///       x         x
            ///       d \     / d
            ///           \ /
            ///            B
            ///
            ///           D
            ///           |
            ///           |
            ///           |
            ///         B | ----------C
            ///         /
            ///       /
            ///     / A

            CartesianPath* line1 = new CartesianPathLine(_fBase2Start, _fBase2Via, _orient->clone(), _eqradius);
            CartesianPath* line2 = new CartesianPathLine(_fBase2Via, fBase2Point,  _orient->clone(), _eqradius);

            Frame fBase2CircleStart = line1->getPos(line1->getLengthToScale(ABDistance - d));
            Frame fBase2CircleEnd   = line2->getPos(line2->getLengthToScale(d));


            Vector3DS vBase2T = Vector3DS::crossProduct(AB,Vector3DS::crossProduct(AB,BC));

            vBase2T.normalize();

            _comp->add(new CartesianPathLine(_fBase2Start, fBase2CircleStart, _orient->clone(), _eqradius));

            _comp->add(new CartesianPathCircle(fBase2CircleStart,
                fBase2CircleStart.trans - vBase2T * _radius,
                fBase2CircleEnd,
                alpha,
                _orient->clone(),
                _eqradius
                ));

            _fBase2Start = fBase2CircleEnd;
            _fBase2Via   = fBase2Point;

            delete line1;
            delete line2;

        }

    }

    _numOfPoints++;
}

void CartesianPathRoundComposite::finish()
{
    if(_numOfPoints >=1)
    {
        _comp->add(new CartesianPathLine(_fBase2Start,_fBase2Via,_orient->clone(),_eqradius));
    }
}

double CartesianPathRoundComposite::getLengthToScale(double length)
{
    return _comp->getLengthToScale(length);
}

double CartesianPathRoundComposite::getPathLength()
{
    return _comp->getPathLength();
}

Frame CartesianPathRoundComposite::getPos(double s) const
{
    return _comp->getPos(s);
}

Twist CartesianPathRoundComposite::getVel(double s, double sd) const
{
    return _comp->getVel(s,sd);
}

Twist CartesianPathRoundComposite::getAcc(double s, double sd, double sdd) const
{
    return _comp->getAcc(s, sd, sdd);
}


}

﻿#include <Msnhnet/robot/MsnhURDF.h>

namespace Msnhnet
{

Vector3DS ParseFromXML::parseVec3(const string &str)
{
    std::vector<std::string> list;
    ExString::split(list, str, " ");

    if(list.size()!=3)
    {
        throw Exception(1,"[URDF] Parse Vector3DS failed, size error!", __FILE__, __LINE__, __FUNCTION__);
    }

    Vector3DS vec;
    try
    {
        vec[0] = lexical_cast<double>(list[0]);
        vec[1] = lexical_cast<double>(list[1]);
        vec[2] = lexical_cast<double>(list[2]);
    }
    catch (Exception &e)
    {
        throw Exception(1,"[URDF] Error not able to parse component  to a double (while parsing a vector value)", __FILE__, __LINE__, __FUNCTION__);
    }


    return vec;
}

RotationMatDS ParseFromXML::parseRotMatFromRpy(const string &str)
{
    std::vector<std::string> list;
    ExString::split(list, str, " ");

    if(list.size()!=3)
    {
        throw Exception(1,"Parse Rotation Mat failed, size error!", __FILE__, __LINE__, __FUNCTION__);
    }

    EulerDS euler;
    try
    {
        euler[0] = lexical_cast<double>(list[0]);
        euler[1] = lexical_cast<double>(list[1]);
        euler[2] = lexical_cast<double>(list[2]);
    }
    catch (Exception &e)
    {
        throw Exception(1,"[URDF] Error not able to parse component  to a double (while parsing a vector value)", __FILE__, __LINE__, __FUNCTION__);
    }

    return GeometryS::euler2RotMat(euler, ROT_ZYX);
}

Color ParseFromXML::parseColor(const string &str)
{
    std::vector<std::string> list;
    ExString::split(list, str, " ");

    if(list.size()!=4)
    {
        throw Exception(1,"Parse Color failed, size error!", __FILE__, __LINE__, __FUNCTION__);
    }

    Color color;

    try
    {
        color.r = lexical_cast<float>(list[0]);
        color.g = lexical_cast<float>(list[1]);
        color.b = lexical_cast<float>(list[2]);
        color.a = lexical_cast<float>(list[3]);
    }
    catch (Exception &e)
    {
        throw Exception(1,"[URDF] Error not able to parse component  to a double (while parsing a vector value)", __FILE__, __LINE__, __FUNCTION__);
    }
    return color;
}

Frame ParseFromXML::parseTransFormFromXML(tinyxml2::XMLElement *xml)
{
    Frame t;

    if(xml)
    {
        const char* xyzStr = xml->Attribute("xyz");

        if(xyzStr)
        {
            t.trans = ParseFromXML::parseVec3(xyzStr);
        }

        const char* rpyStr = xml->Attribute("rpy");


        if(rpyStr)
        {
            t.rotMat = ParseFromXML::parseRotMatFromRpy(rpyStr);
        }
    }

    return t;
}

std::shared_ptr<URDFSphere> ParseFromXML::parseSphereFromXML(tinyxml2::XMLElement *xml)
{
    std::shared_ptr<URDFSphere> sphere = std::make_shared<URDFSphere>();

    if(xml->Attribute("radius"))
    {
        try
        {
            sphere->radius = lexical_cast<double>(xml->Attribute("radius"));

        }
        catch (Exception &e)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing link '" << getParentLinkName(xml)
                   << "': sphere radius [" << xml->Attribute("radius")
                   << "] is not a valid float: " << e.what() << "!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
    }
    else
    {
        std::ostringstream errMsg;
        errMsg << "[URDF] Error while parsing link '" << getParentLinkName(xml)
               << "': Sphere shape must have a radius attribute";

        throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
    }

    return sphere;
}

std::shared_ptr<URDFBox> ParseFromXML::parseBoxFromXML(tinyxml2::XMLElement *xml)
{

    std::shared_ptr<URDFBox> box = std::make_shared<URDFBox>();

    if(xml->Attribute("size"))
    {
        try
        {
            box->dim = ParseFromXML::parseVec3(xml->Attribute("size"));
        }
        catch (Exception &e)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing link '" << getParentLinkName(xml)
                   << "': box size [" << xml->Attribute("size")
                   << "] is not a valid: " << e.what() << "!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
    }
    else
    {
        std::ostringstream errMsg;
        errMsg << "[URDF] Error while parsing link '" << getParentLinkName(xml)
               << "': Sphere shape must have a size attribute";

        throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
    }

    return box;
}

std::shared_ptr<URDFCylinder> ParseFromXML::parseCylinderFromXML(tinyxml2::XMLElement *xml)
{
    std::shared_ptr<URDFCylinder> cylinder = std::make_shared<URDFCylinder>();

    if (xml->Attribute("length") && xml->Attribute("radius"))
    {

        try
        {
            cylinder->length = lexical_cast<double>(xml->Attribute("length"));
        }
        catch (Exception &e)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing link '" << getParentLinkName(xml)
                   << "': cylinder length [" << xml->Attribute("length")
                   << "] is not a valid float: " << e.what() << "!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }

        try
        {
            cylinder->radius = lexical_cast<double>(xml->Attribute("radius"));
        }
        catch (Exception &e)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing link '" << getParentLinkName(xml)
                   << "': cylinder radius [" << xml->Attribute("radius")
                   << "] is not a valid float: " << e.what() << "!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
    }
    else
    {
        std::ostringstream errMsg;
        errMsg << "[URDF] Error while parsing link '" << getParentLinkName(xml)
               << "': Cylinder shape must have both length and radius attributes!";

        throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
    }
    return cylinder;
}

std::shared_ptr<URDFCone> ParseFromXML::parseConeFromXML(tinyxml2::XMLElement *xml)
{
    std::shared_ptr<URDFCone> cone = std::make_shared<URDFCone>();

    if (xml->Attribute("length") && xml->Attribute("radius"))
    {
        try
        {
            cone->length = lexical_cast<double>(xml->Attribute("length"));
        }
        catch (Exception &e)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing link '" << getParentLinkName(xml)
                   << "': cone length [" << xml->Attribute("length")
                   << "] is not a valid float: " << e.what() << "!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }

        try
        {
            cone->radius = lexical_cast<double>(xml->Attribute("radius"));
        }
        catch (Exception &e)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing link '" << getParentLinkName(xml)
                   << "': cone radius [" << xml->Attribute("radius")
                   << "] is not a valid float: " << e.what() << "!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
    }
    else
    {
        std::ostringstream errMsg;
        errMsg << "[URDF] Error while parsing link '" << getParentLinkName(xml)
               << "': cone shape must have both length and radius attributes!";

        throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
    }
    return cone;
}

std::shared_ptr<URDFMesh> ParseFromXML::parseMeshFromXML(tinyxml2::XMLElement *xml)
{
    std::shared_ptr<URDFMesh> mesh = std::make_shared<URDFMesh>();

    if (xml->Attribute("filename"))
    {
        mesh->filename = xml->Attribute("filename");
    }
    else
    {
        std::ostringstream errMsg;
        errMsg << "[URDF] Error while parsing link '" << getParentLinkName(xml)
               << "Mesh must contain a filename attribute!";

        throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
    }

    if (xml->Attribute("scale"))
    {
        try
        {
            mesh->scale = ParseFromXML::parseVec3(xml->Attribute("scale"));
        }
        catch (Exception &e)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing link '" << getParentLinkName(xml)
                   << "': mesh scale [" << xml->Attribute("scale")
                   << "] is not a valid: " << e.what() << "!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
    }
    return mesh;
}

std::shared_ptr<URDFGeometry> ParseFromXML::parseGeomFromXML(tinyxml2::XMLElement *xml)
{
    if (xml == nullptr)
    {
        std::ostringstream errMsg;
        errMsg << "[URDF] Error while parsing link '" << getParentLinkName(xml)
               << "' geometry structure pointer is null nothing to parse!";
        throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
    }

    tinyxml2::XMLElement *shape = xml->FirstChildElement();
    if (shape == nullptr)
    {
        std::ostringstream errMsg;
        errMsg << "[URDF] Error while parsing link '" << getParentLinkName(xml)
               << "' geometry does not contain any shape information!";
        throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
    }

    const std::string typeName = shape->Value();
    if (typeName == "sphere")
    {
        return parseSphereFromXML(shape);
    }
    else if (typeName == "box")
    {
        return parseBoxFromXML(shape);
    }
    else if (typeName == "cylinder")
    {
        return parseCylinderFromXML(shape);
    }
    else if (typeName == "mesh")
    {
        return parseMeshFromXML(shape);
    }
    else
    {
        std::ostringstream errMsg;
        errMsg << "[URDF] Error while parsing link '" << getParentLinkName(xml)
               << "' unknown shape type '" << typeName << "'!";
        throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
    }
}

std::shared_ptr<URDFJointDynamics> ParseFromXML::parseJointDynamicsFromXML(tinyxml2::XMLElement *xml)
{
    std::shared_ptr<URDFJointDynamics> jointDynamic = std::make_shared<URDFJointDynamics>();

    const char* dampingStr = xml->Attribute("damping");
    const char* frictionStr = xml->Attribute("friction");

    if (dampingStr == nullptr && frictionStr == nullptr)
    {
        std::ostringstream errMsg;
        errMsg << "[URDF] Error while parsing joint '" << getParentJointName(xml)
               << "': joint dynamics element specified with no damping and no friction!";
        throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
    }

    if (dampingStr)
    {
        try
        {
            jointDynamic->damping = lexical_cast<double>(dampingStr);
        }
        catch (Exception &e)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing joint '" << getParentJointName(xml)
                   << "': dynamics damping value (" << dampingStr
                   << ") is not a float: " << e.what() << "!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
    }


    if (frictionStr)
    {
        try
        {
            jointDynamic->friction = lexical_cast<double>(frictionStr);
        }
        catch (Exception &e)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing joint '" << getParentJointName(xml)
                   << "': dynamics friction value (" << frictionStr
                   << ") is not a float: " << e.what() << "!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
    }

    return jointDynamic;

}

std::shared_ptr<URDFJointLimits> ParseFromXML::parseJointLimitsFromXML(tinyxml2::XMLElement *xml)
{
    std::shared_ptr<URDFJointLimits> jointLimit = std::make_shared<URDFJointLimits>();

    const char* effortStr = xml->Attribute("effort");
    if (effortStr )
    {
        try
        {
            jointLimit->effort = lexical_cast<double>(effortStr);
        }
        catch (Exception &e)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing joint '" << getParentJointName(xml)
                   << "' limits effort value (" << effortStr
                   << ") is not a float: " << e.what() << "!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
    }
    else
    {
        std::ostringstream errMsg;
        errMsg << "[URDF] Error while parsing joint '" << getParentJointName(xml)
               << "' joint limit: no effort specified!";
        throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
    }

    const char* velocityStr = xml->Attribute("velocity");
    if (velocityStr )
    {
        try
        {
            jointLimit->velocity = lexical_cast<double>(velocityStr);
        }
        catch (Exception &e)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing joint '" << getParentJointName(xml)
                   << "' limits velocity value (" << velocityStr
                   << ") is not a float: " << e.what() << "!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
    }
    else
    {
        std::ostringstream errMsg;
        errMsg << "[URDF] Error while parsing joint '" << getParentJointName(xml)
               << "' joint limit: no velocity specified!";
        throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
    }


    const char* lowerStr = xml->Attribute("lower");
    if (lowerStr )
    {
        try
        {
            jointLimit->lower = lexical_cast<double>(lowerStr);
        }
        catch (Exception &e)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing joint '" << getParentJointName(xml)
                   << "': limits lower value (" << lowerStr
                   << ") is not a float: " << e.what() << "!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
    }

    const char* upperStr = xml->Attribute("upper");
    if (upperStr )
    {
        try
        {
            jointLimit->upper = lexical_cast<double>(upperStr);
        }
        catch (Exception &e)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing joint '" << getParentJointName(xml)
                   << "': limits upper value (" << upperStr
                   << ") is not a float: " << e.what() << "!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
    }

    return jointLimit;
}

std::shared_ptr<URDFJointSafety> ParseFromXML::parseJointSafetyFromXML(tinyxml2::XMLElement *xml)
{
    std::shared_ptr<URDFJointSafety> jointSafty = std::make_shared<URDFJointSafety>();

    const char* kVelocityStr = xml->Attribute("k_velocity");
    if (kVelocityStr)
    {
        try
        {
            jointSafty->kVelocity = lexical_cast<double>(kVelocityStr);
        }
        catch (Exception &e)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing joint '" << getParentJointName(xml)
                   << "' safety k_velocity value (" << kVelocityStr
                   << ") is not a float: " << e.what() << "!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
    }
    else
    {
        std::ostringstream errMsg;
        errMsg << "[URDF] Error while parsing joint '" << getParentJointName(xml)
               << "' joint safety no k_velocity!";
        throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
    }

    const char* lowerLimitStr = xml->Attribute("lower_limit");
    if (lowerLimitStr)
    {
        try
        {
            jointSafty->lowerLimit = lexical_cast<double>(lowerLimitStr);
        }
        catch (Exception &e)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing joint '" << getParentJointName(xml)
                   << "' safety lower_limit value (" << lowerLimitStr
                   << ") is not a float: " << e.what() << "!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
    }

    const char* upperLimitStr = xml->Attribute("upper_limit");
    if (upperLimitStr)
    {
        try
        {
            jointSafty->upperLimit = lexical_cast<double>(upperLimitStr);
        }
        catch (Exception &e)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing joint '" << getParentJointName(xml)
                   << "' safety upper_limit value (" << upperLimitStr
                   << ") is not a float: " << e.what() << "!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
    }

    const char* kPositionStr = xml->Attribute("k_position");
    if (kPositionStr)
    {
        try
        {
            jointSafty->kPosition = lexical_cast<double>(kPositionStr);
        }
        catch (Exception &e)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing joint '" << getParentJointName(xml)
                   << "' safety k_position value (" << kPositionStr
                   << ") is not a float: " << e.what() << "!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
    }

    return jointSafty;
}

std::shared_ptr<URDFJointCalibration> ParseFromXML::parseJointCalibrationFromXML(tinyxml2::XMLElement *xml)
{
    std::shared_ptr<URDFJointCalibration> jointCalibration = std::make_shared<URDFJointCalibration>();

    const char* risingStr = xml->Attribute("rising");
    if (risingStr)
    {
        try
        {
            jointCalibration->rising = lexical_cast<double>(risingStr);
        }
        catch (Exception &e)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing joint '" << getParentJointName(xml)
                   << "' calibration rising_position value (" << risingStr
                   << ") is not a float: " << e.what() << "!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
    }

    const char* fallingStr = xml->Attribute("falling");
    if (fallingStr)
    {
        try
        {
            jointCalibration->falling = lexical_cast<double>(fallingStr);
        }
        catch (Exception &e)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing joint '" << getParentJointName(xml)
                   << "' calibration falling_position value (" << fallingStr
                   << ") is not a float: " << e.what() << "!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
    }

    return jointCalibration;
}

std::shared_ptr<URDFJointMimic> ParseFromXML::parseJointMimicFromXML(tinyxml2::XMLElement *xml)
{

    std::shared_ptr<URDFJointMimic> jointMimic = std::make_shared<URDFJointMimic>();

    const char* jointNameStr = xml->Attribute("joint");
    if (jointNameStr)
    {
        jointMimic->jointName = jointNameStr;
    }
    else
    {
        std::ostringstream errMsg;
        errMsg << "[URDF] Error while parsing joint '" << getParentJointName(xml)
               << "joint mimic: no mimic joint specified!";
        throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
    }

    const char* multiplierStr = xml->Attribute("multiplier");
    if (multiplierStr)
    {
        try
        {
            jointMimic->multiplier = lexical_cast<double>(multiplierStr);
        }
        catch (Exception &e)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing joint '" << getParentJointName(xml)
                   << "' mimic multiplier value (" << multiplierStr
                   << ") is not a float: " << e.what() << "!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
    }

    const char* offsetStr = xml->Attribute("offset");
    if (offsetStr)
    {
        try
        {
            jointMimic->offset = lexical_cast<double>(offsetStr);
        }
        catch (Exception &e)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing joint '" << getParentJointName(xml)
                   << "' mimic offset value (" << offsetStr
                   << ") is not a float: " << e.what() << "!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
    }

    return jointMimic;
}

std::shared_ptr<URDFJoint> ParseFromXML::parseJointFromXML(tinyxml2::XMLElement *xml)
{
    std::shared_ptr<URDFJoint> joint = std::make_shared<URDFJoint>();

    const char *name = xml->Attribute("name");
    if (name)
    {
        joint->name = name;
    }
    else
    {
        std::ostringstream errMsg;
        errMsg << "[URDF] Error while parsing model: unnamed joint found!";
        throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
    }

    tinyxml2::XMLElement  *originXml = xml->FirstChildElement("origin");
    if (originXml)
    {
        try
        {
            joint->parentToJointTransform =  parseTransFormFromXML(originXml);
        }
        catch (Exception& e)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error! Malformed parent origin element for joint '" << joint->name<< "': " << e.what() << "!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
    }

    tinyxml2::XMLElement  *parentXml = xml->FirstChildElement("parent");
    if (parentXml)
    {
        const char *pname = parentXml->Attribute("link");
        if (pname)
        {
            joint->parentLinkName = std::string(pname);
        }
        // if no parent link name specified. this might be the root node
    }

    tinyxml2::XMLElement  *childXml = xml->FirstChildElement("child");
    if (childXml)
    {
        const char *pname = childXml->Attribute("link");
        if (pname)
        {
            joint->childLinkName = std::string(pname);
        }
    }

    const char* typeChar = xml->Attribute("type");
    if (typeChar == nullptr)
    {
        std::ostringstream errMsg;
        errMsg << "[URDF] Error! Joint " << joint->name
               <<" has no type, check to see if it's a reference.";
        throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
    }

    std::string type_str = typeChar;
    if (type_str == "planar")
        joint->type = URDFJointType::URDF_PLANAR;
    else if (type_str == "floating")
        joint->type = URDFJointType::URDF_FLOATING;
    else if (type_str == "revolute")
        joint->type = URDFJointType::URDF_REVOLUTE;
    else if (type_str == "continuous")
        joint->type = URDFJointType::URDF_CONTINUOUS;
    else if (type_str == "prismatic")
        joint->type = URDFJointType::URDF_PRISMATIC;
    else if (type_str == "fixed")
        joint->type = URDFJointType::URDF_FIXED;
    else {
        std::ostringstream errMsg;
        errMsg << "[URDF] Error! Joint '" << joint->name
               <<"' has unknown type (" << type_str << ")!";
        throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
    }

    if (joint->type != URDFJointType::URDF_FLOATING && joint->type != URDFJointType::URDF_FIXED)
    {
        tinyxml2::XMLElement  *axisXml = xml->FirstChildElement("axis");
        if (axisXml == nullptr)
        {
            joint->axis = Vector3DS(1.0, 0.0, 0.0);
        }
        else
        {
            if (axisXml->Attribute("xyz"))
            {
                try
                {
                    joint->axis = parseVec3(axisXml->Attribute("xyz"));
                }
                catch (Exception &e)
                {
                    std::ostringstream errMsg;
                    errMsg << "[URDF] Error! Malformed axis element for joint ["<< joint->name
                           << "]: " << e.what();
                    throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
                }
            }
        }
    }

    tinyxml2::XMLElement  *propXml = xml->FirstChildElement("dynamics");
    if (propXml)
    {
        joint->dynamics = parseJointDynamicsFromXML(propXml);
    }

    tinyxml2::XMLElement  *limitXml = xml->FirstChildElement("limit");
    if (limitXml)
    {
        joint->limits = parseJointLimitsFromXML(limitXml);
    }

    tinyxml2::XMLElement  *safetyXml = xml->FirstChildElement("safety_controller");
    if (safetyXml) {
        joint->safety = parseJointSafetyFromXML(safetyXml);
    }

    tinyxml2::XMLElement  *calibration_xml = xml->FirstChildElement("calibration");
    if (calibration_xml)
    {
        joint->calibration = parseJointCalibrationFromXML(calibration_xml);
    }

    tinyxml2::XMLElement  *mimicXml = xml->FirstChildElement("mimic");
    if (mimicXml)
    {
        joint->mimic = parseJointMimicFromXML(mimicXml);
    }

    return joint;
}

std::shared_ptr<URDFMaterial> ParseFromXML::parseMaterialFromXML(tinyxml2::XMLElement *xml, bool onlyNameIsOk)
{
    bool hasRgb = false;
    bool hasFilename = false;

    std::shared_ptr<URDFMaterial> material = std::make_shared<URDFMaterial>();

    auto nameStr = xml->Attribute("name");
    if (nameStr)
    {
        material->name = nameStr;
    }
    else
    {
        std::ostringstream errMsg;
        errMsg << "[URDF] Error! Material without a name attribute detected!";
        throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
    }


    auto t = xml->FirstChildElement("texture");
    if (t)
    {
        if (t->Attribute("filename"))
        {
            material->textureFilename = t->Attribute("filename");
            hasFilename = true;
        }
    }

    auto c = xml->FirstChildElement("color");
    if (c)
    {
        if (c->Attribute("rgba"))
        {
            try
            {
                material->color = parseColor(c->Attribute("rgba"));
                hasRgb = true;
            }
            catch (Exception &e)
            {
                std::ostringstream errMsg;
                errMsg << "[URDF] Material [" << material->name
                       << "] has malformed color rgba values: "
                       << e.what() << "!";
                throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
            }
        }
    }

    if (!hasRgb && !hasFilename)
    {
        if (!onlyNameIsOk) // no need for an error if only name is ok
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Material [" << material->name
                   << "] has neither a texture nor a color defined!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
    }

    return material;
}

URDFInertial ParseFromXML::parseInertialFromXML(tinyxml2::XMLElement *xml)
{
    URDFInertial i;

    tinyxml2::XMLElement *o = xml->FirstChildElement("origin");
    if (o)
    {
        i.origin = parseTransFormFromXML(o);
    }

    tinyxml2::XMLElement *massXml = xml->FirstChildElement("mass");
    if (massXml)
    {
        if (massXml->Attribute("value"))
        {
            try
            {
                i.mass = lexical_cast<double>(massXml->Attribute("value"));
            }
            catch (Exception &e)
            {
                std::ostringstream errMsg;
                errMsg << "[URDF] Error while parsing link '" << getParentLinkName(xml)
                       << "': inertial mass [" << massXml->Attribute("value")
                       << "] is not a valid double: " << e.what() << "!";
                throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
            }
        }
        else
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing link '" << getParentLinkName(xml)
                   << "' <mass> element must have a value attribute!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
    }
    else
    {
        std::ostringstream errMsg;
        errMsg << "[URDF] Error while parsing link '" << getParentLinkName(xml)
               << "' inertial element must have a <mass> element!";
        throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
    }


    tinyxml2::XMLElement *inertia_xml = xml->FirstChildElement("inertia");
    if (inertia_xml)
    {
        if (inertia_xml->Attribute("ixx") && inertia_xml->Attribute("ixy") && inertia_xml->Attribute("ixz") &&
                inertia_xml->Attribute("iyy") && inertia_xml->Attribute("iyz") &&
                inertia_xml->Attribute("izz")) {
            try {
                i.ixx	= lexical_cast<double>(inertia_xml->Attribute("ixx"));
                i.ixy	= lexical_cast<double>(inertia_xml->Attribute("ixy"));
                i.ixz	= lexical_cast<double>(inertia_xml->Attribute("ixz"));
                i.iyy	= lexical_cast<double>(inertia_xml->Attribute("iyy"));
                i.iyz	= lexical_cast<double>(inertia_xml->Attribute("iyz"));
                i.izz	= lexical_cast<double>(inertia_xml->Attribute("izz"));
            }
            catch (Exception &e)
            {
                std::ostringstream errMsg;
                errMsg << "[URDF] Error while parsing link '" << getParentLinkName(xml)
                       << "Inertial: one of the inertia elements is not a valid double:"
                       << " ixx [" << inertia_xml->Attribute("ixx") << "]"
                       << " ixy [" << inertia_xml->Attribute("ixy") << "]"
                       << " ixz [" << inertia_xml->Attribute("ixz") << "]"
                       << " iyy [" << inertia_xml->Attribute("iyy") << "]"
                       << " iyz [" << inertia_xml->Attribute("iyz") << "]"
                       << " izz [" << inertia_xml->Attribute("izz") << "]\n\n"
                       << e.what();
                throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
            }
        }
        else
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing link '" << getParentLinkName(xml)
                   << "' <inertia> element must have ixx,ixy,ixz,iyy,iyz,izz attributes!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
    }
    else
    {
        std::ostringstream errMsg;
        errMsg << "[URDF] Error while parsing link '" << getParentLinkName(xml)
               << "' inertial element must have a <inertia> element!";
        throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
    }

    return i;
}

std::shared_ptr<URDFVisual> ParseFromXML::parseVisualFromXML(tinyxml2::XMLElement *xml)
{
    std::shared_ptr<URDFVisual> vis = std::make_shared<URDFVisual>();

    tinyxml2::XMLElement *o = xml->FirstChildElement("origin");
    if (o != nullptr)
    {
        try
        {
            vis->origin = parseTransFormFromXML(o);
        }
        catch (Exception& e)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing link '" << getParentLinkName(xml)
                   << "': visual origin is not valid: " << e.what() << "!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
    }

    tinyxml2::XMLElement *geom = xml->FirstChildElement("geometry");
    if (geom != nullptr)
    {
        vis->geometry =  parseGeomFromXML(geom);
    }

    const char *name_char = xml->Attribute("name");
    if (name_char != nullptr)
    {
        vis->name = name_char;
    }

    tinyxml2::XMLElement *mat = xml->FirstChildElement("material");
    if (mat != nullptr)
    {
        if (mat->Attribute("name") != nullptr)
        {
            vis->materialName = mat->Attribute("name");
        }
        else
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing link '" << getParentLinkName(xml)
                   << "': visual material must contain a name attribute!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }

        vis->material = parseMaterialFromXML(mat, true);
    }

    return vis;
}

std::shared_ptr<URDFCollision> ParseFromXML::parseCollisionFromXML(tinyxml2::XMLElement *xml)
{
    std::shared_ptr<URDFCollision> col = std::make_shared<URDFCollision>();

    tinyxml2::XMLElement *o = xml->FirstChildElement("origin");
    if (o != nullptr)
    {
        try
        {
            col->origin = parseTransFormFromXML(o);
        }
        catch (Exception& e)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error while parsing link '" << getParentLinkName(xml)
                   << "': collision origin is not a valid: " << e.what() << "!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
    }

    tinyxml2::XMLElement *geom = xml->FirstChildElement("geometry");
    if (geom != nullptr)
    {
        col->geometry = parseGeomFromXML(geom);
    }

    const char *nameChar = xml->Attribute("name");

    if (nameChar != nullptr)
    {
        col->name = nameChar;
    }

    return col;
}

std::shared_ptr<URDFLink> ParseFromXML::parseLinkFromXML(tinyxml2::XMLElement *xml)
{
    std::shared_ptr<URDFLink> link = std::make_shared<URDFLink>();

    const char *nameChar = xml->Attribute("name");
    if (nameChar != nullptr)
    {
        link->name = std::string(nameChar);
    }
    else
    {
        std::ostringstream errMsg;
        errMsg << "[URDF] Error! Link without a name attribute detected!";
        throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
    }

    tinyxml2::XMLElement *i = xml->FirstChildElement("inertial");
    if (i != nullptr)
    {
        link->inertial = parseInertialFromXML(i);
    }

    for (tinyxml2::XMLElement* visXml = xml->FirstChildElement("visual"); visXml != nullptr; visXml = visXml->NextSiblingElement("visual"))
    {
        auto vis = parseVisualFromXML(visXml);
        link->visuals.push_back(vis);
    }

    for (tinyxml2::XMLElement* colXml = xml->FirstChildElement("collision"); colXml != nullptr; colXml = colXml->NextSiblingElement("collision"))
    {
        auto col = parseCollisionFromXML(colXml);
        link->collisions.push_back(col);
    }

    return link;
}

#ifdef WIN32

std::wstring ToUtf16(std::string str)
{
    std::wstring ret;
    int len = MultiByteToWideChar(CP_UTF8, 0, str.c_str(), str.length(), NULL, 0);
    if (len > 0)
    {
        ret.resize(len);
        MultiByteToWideChar(CP_UTF8, 0, str.c_str(), str.length(), &ret[0], len);
    }
    return ret;
}
#endif

std::shared_ptr<URDFModel> URDF::parseURDF(const string &msg, bool isPath, const string &rootPath)
{
    std::shared_ptr<URDFModel> model = std::make_shared<URDFModel>();

    tinyxml2::XMLDocument xmlDoc;

    if(!isPath)
    {
        model->rootPath = rootPath;
        xmlDoc.Parse(msg.c_str());
    }
    else
    {
        std::string path = msg;
#ifdef WIN32
        ifstream modelFile(ToUtf16(path.c_str()));
#else
        ifstream modelFile(path.c_str());
#endif

        if (!modelFile)
        {
            std::string errMsg = "Error opening file <" + msg + ">.";
            throw Exception(1, errMsg, __FILE__,__LINE__,__FUNCTION__);
        }
        string modelXmlString;
        modelFile.seekg(0, std::ios::end);
        modelXmlString.reserve(modelFile.tellg());
        modelFile.seekg(0, std::ios::beg);
        modelXmlString.assign((std::istreambuf_iterator<char>(modelFile)),
                              std::istreambuf_iterator<char>());
        modelFile.close();

        xmlDoc.Parse(modelXmlString.c_str());

        /// =====================

        ExString::replace(path,"\\","/");

        int id = path.find_last_of("/");

        model->rootPath = ExString::mid(path,0,id);


        //xmlDoc.LoadFile(msg.c_str());
    }

    if (xmlDoc.Error())
    {
        std::string errMsg = xmlDoc.ErrorStr();
        xmlDoc.ClearError();
        throw Exception(1, errMsg, __FILE__,__LINE__,__FUNCTION__);
    }


    tinyxml2::XMLElement *robotXml = xmlDoc.RootElement();
    if (robotXml == nullptr || std::string(robotXml->Value()) != "robot")
    {
        std::string errMsg = "[URDF] SError! Could not find the <robot> element in the xml file";
        throw Exception(1, errMsg, __FILE__,__LINE__,__FUNCTION__);
    }

    const char *name = robotXml->Attribute("name");
    if (name != nullptr)
    {
        model->name = std::string(name);
    }
    else
    {
        std::string errMsg = "[URDF] No name given for the robot. Please add a name attribute to the robot element!";
        throw Exception(1, errMsg, __FILE__,__LINE__,__FUNCTION__);
    }

    for (tinyxml2::XMLElement* material_xml = robotXml->FirstChildElement("material"); material_xml != nullptr; material_xml = material_xml->NextSiblingElement("material")) {
        auto material = ParseFromXML::parseMaterialFromXML(material_xml, false); // material needs to be fully defined here
        if (model->getMaterial(material->name) != nullptr)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Duplicate materials '" << material->name << "' found!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
        else
        {
            model->materialMap[material->name] = material;
        }
    }

    for (tinyxml2::XMLElement* link_xml = robotXml->FirstChildElement("link"); link_xml != nullptr; link_xml = link_xml->NextSiblingElement("link"))
    {
        auto link = ParseFromXML::parseLinkFromXML(link_xml);

        if (model->getLink(link->name) != nullptr)
        {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error! Duplicate links '" << link->name << "' found!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
        else
        {
            // loop over link visual to find the materials
            if (!link->visuals.empty())
            {
                for ( auto visual : link->visuals )
                {
                    if (!visual->materialName.empty())
                    {
                        if (model->getMaterial(visual->materialName) != nullptr)
                        {
                            visual->material.emplace(model->getMaterial( visual->materialName.c_str() ));
                        }
                        else
                        {
                            // if no model matrial found use the one defined in the visual
                            if (visual->material.has_value())
                            {
                                model->materialMap[visual->materialName] = visual->material.value();
                            }
                            else
                            {
                                // no matrial information available for this visual -> error
                                std::ostringstream errMsg;
                                errMsg << "[URDF] Error! Link '" << link->name
                                       << "' material '" << visual->materialName
                                       <<" ' undefined!";
                                throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
                            }
                        }
                    }
                }
            }
            model->linkMap[link->name] = link;
        }
    }

    if (model->linkMap.size() == 0)
    {
        std::string errMsg = "[URDF] Error! No link elements found in the urdf file.";
        throw Exception(1, errMsg, __FILE__,__LINE__,__FUNCTION__);
    }

    for (tinyxml2::XMLElement* joint_xml = robotXml->FirstChildElement("joint"); joint_xml != nullptr; joint_xml = joint_xml->NextSiblingElement("joint"))
    {
        auto joint = ParseFromXML::parseJointFromXML(joint_xml);

        if (model->getJoint(joint->name) != nullptr) {
            std::ostringstream errMsg;
            errMsg << "[URDF] Error! Duplicate joints '" << joint->name << "' found!";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
        else
        {
            model->jointMap[joint->name] = joint;
        }
    }

    std::map<std::string, std::string> parentLinkTree;

    model->initLinkTree(parentLinkTree);
    model->findRoot(parentLinkTree);

    return model;
}

std::shared_ptr<URDFModel> URDF::parseFromUrdfStr(const string &xmlString, const string &rootPath)
{
    return parseURDF(xmlString, false);
}

std::shared_ptr<URDFModel> URDF::parseFromUrdfFile(const string &path)
{
    return parseURDF(path, true);
}

string URDF::getURDFStr(const string &msg)
{
    std::string path = msg;
#ifdef WIN32
    ifstream modelFile(ToUtf16(path.c_str()));
#else
    ifstream modelFile(path.c_str());
#endif

    if (!modelFile)
    {
        std::string errMsg = "Error opening file <" + msg + ">.";
        throw Exception(1, errMsg, __FILE__,__LINE__,__FUNCTION__);
    }
    string modelXmlString;
    modelFile.seekg(0, std::ios::end);
    modelXmlString.reserve(modelFile.tellg());
    modelFile.seekg(0, std::ios::beg);
    modelXmlString.assign((std::istreambuf_iterator<char>(modelFile)),
                          std::istreambuf_iterator<char>());
    modelFile.close();

    return modelXmlString;
}

const char *getParentLinkName(tinyxml2::XMLElement *xml)
{
    tinyxml2::XMLElement* e = xml->Parent()->ToElement();
    while(e->Parent())
    {
        if (std::string(e->Value()) == "link")
        {
            break;
        }
        e = e->Parent()->ToElement();
    }
    return e->Attribute("name");
}

const char *getParentJointName(tinyxml2::XMLElement *xml)
{
    // this should always be set since we check for the joint name in parseJoint already
    return ((tinyxml2::XMLElement*)xml->Parent())->Attribute("name");
}

std::shared_ptr<URDFLink> URDFModel::getLink(const string &name)
{
    if (linkMap.find(name) == linkMap.end())
    {
        return nullptr;
    }
    else
    {
        return linkMap.find(name)->second;
    }
}

std::shared_ptr<URDFJoint> URDFModel::getJoint(const string &name)
{
    if (jointMap.find(name) == jointMap.end())
    {
        return nullptr;
    }
    else
    {
        return jointMap.find(name)->second;
    }
}

std::shared_ptr<URDFMaterial> URDFModel::getMaterial(const string &name)
{
    if (materialMap.find(name) == materialMap.end())
    {
        return nullptr;
    }
    else
    {
        return materialMap.find(name)->second;
    }
}

void URDFModel::getLinks(vector<std::shared_ptr<URDFLink> > &linklist) const
{
    for (auto link = linkMap.begin(); link != linkMap.end(); link++)
    {
        linklist.push_back(link->second);
    }
}


void URDFModel::initLinkTree(map<string, string> &parentLinkTree)
{
    for (auto joint = jointMap.begin(); joint != jointMap.end(); joint++)
    {
        string parentLinkName = joint->second->parentLinkName;
        string childLinkName = joint->second->childLinkName;

        if (parentLinkName.empty())
        {
            ostringstream errMsg;
            errMsg << "[URDF] Error while constructing model! Joint [" << joint->first
                   << "] is missing a parent link specification.";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }
        if (childLinkName.empty())
        {
            ostringstream errMsg;
            errMsg << "[URDF] Error while constructing model! Joint [" << joint->first
                   << "] is missing a child link specification.";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }

        auto childLink = getLink(childLinkName);
        if (childLink == nullptr)
        {
            ostringstream errMsg;
            errMsg << "[URDF] Error while constructing model! Child link [" << childLinkName
                   << "] of joint [" <<  joint->first << "] not found";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }

        auto parentLink = getLink(parentLinkName);
        if (parentLink == nullptr)
        {
            ostringstream errMsg;
            errMsg << "[URDF] Error while constructing model! Parent link [" << parentLinkName
                   << "] of joint [" <<  joint->first << "] not found";
            throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
        }

        childLink->setParentLink(parentLink);
        childLink->setParentJoint(joint->second);

        parentLink->childJoints.push_back(joint->second);
        parentLink->childLinks.push_back(childLink);

        parentLinkTree[childLink->name] = parentLinkName;

    }
}

void URDFModel::findRoot(const map<string, string> &parentLinkTree)
{
    for (auto l=linkMap.begin(); l!=linkMap.end(); l++)
    {
        auto parent = parentLinkTree.find(l->first);
        if (parent == parentLinkTree.end())
        {
            if (rootLink == nullptr)
            {
                rootLink = getLink(l->first);
            }
            else
            {
                ostringstream errMsg;
                errMsg << "[URDF] Error! Multiple root links found: (" << rootLink->name
                       << ") and (" + l->first + ")!";
                throw Exception(1, errMsg.str(), __FILE__,__LINE__,__FUNCTION__);
            }
        }
    }
    if (rootLink == nullptr)
    {
        throw Exception(1,"[URDF] Error! No root link found. The urdf does not contain a valid link tree.", __FILE__,__LINE__,__FUNCTION__);
    }
}

std::shared_ptr<URDFLinkTree> URDF::getLinkTree(const std::shared_ptr<URDFLink> &link)
{
    std::shared_ptr<URDFLinkTree> linkTree = std::make_shared<URDFLinkTree>();

    if(link->parentJoint)
    {
        linkTree->joint   =  link->parentJoint;
        linkTree->visuals  =  link->visuals;
        linkTree->collisions  =  link->collisions;

        // 访问内容
        //        for(int i =0 ;i<link->visuals.size();i++)
        //        {
        //            if(link->visuals[i]->geometry->get()->type == URDFGeometryType::URDF_MESH)
        //            {
        //               std::cout<<((URDFMesh*)link->visuals[i]->geometry->get())->filename<<std::endl;
        //            }
        //        }

    }
    else
    {
        std::shared_ptr<URDFJoint> joint = std::make_shared<URDFJoint>();
        joint->name = "WorldFix";
        joint->type = URDFJointType::URDF_FIXED;

        linkTree->joint = joint;
        linkTree->collisions = link->collisions;
        linkTree->visuals = link->visuals;
    }

    for(size_t i=0; i<link->childLinks.size(); i++)
    {
        linkTree->nextLink.push_back(getLinkTree(link->childLinks[i]));
    }

    return linkTree;
}

std::shared_ptr<URDFLinkTree> URDF::getLinkTree(const string &msg, bool isPath, const string &rootPath)
{
    std::shared_ptr<URDFModel> Model = parseURDF(msg, isPath,rootPath);

    return getLinkTree(Model->rootLink);
}

std::shared_ptr<StringTree> URDF::getStringTree(std::shared_ptr<URDFLinkTree> &link)
{
    std::shared_ptr<StringTree> strTree = std::make_shared<StringTree>();


    strTree->name = link->joint.value()->name;

    if(link->nextLink.size()>0)
    {
        for (size_t i = 0; i < link->nextLink.size(); ++i)
        {
            strTree->addChild(getStringTree(link->nextLink[i]));
        }
    }

    return strTree;
}

Chain URDF::getChain(const std::shared_ptr<URDFModel> &model, const std::shared_ptr<StringTree> &strTree, const string &start, const string &end)
{
    std::vector<std::string> ways;
    bool res =StringTree::getShortestPath(strTree,start,end,ways);

    if(!res)
    {
        throw Exception(1,"[URDF] Get a chain between 2 nodes failed",__FILE__, __LINE__, __FUNCTION__);
    }

    Chain chain;

    for(size_t i=0; i<ways.size(); i++)
    {
        auto joint = model->jointMap[ways[i]];
        auto jointType = joint->type;
        auto axis  = joint->axis;
        double min = -DBL_MAX;
        double max = -DBL_MIN;
        JointType chainType;
        std::string name = joint->name;
        Frame frame = joint->parentToJointTransform;

        if(jointType == URDFJointType::URDF_FIXED || jointType == URDFJointType::URDF_UNKNOWN)
        {
            chainType = (JointType::JOINT_FIXED);
            chain.addSegments(Segment(name, Joint(chainType), frame, min, max));
            continue;
        }
        else if(jointType == URDFJointType::URDF_CONTINUOUS)
        {
            if(axis == Vector3DS(0,0,0))
            {
                throw Exception(1,"[URDF] Continuous axis type is not supported!",__FILE__, __LINE__, __FUNCTION__);
            }
            else
            {
                chainType = (JOINT_ROT_AXIS);
            }
        }
        else if(jointType == URDFJointType::URDF_REVOLUTE)
        {
            if(axis == Vector3DS(0,0,0))
            {
                throw Exception(1,"[URDF] Revolute axis type is not supported!",__FILE__, __LINE__, __FUNCTION__);
            }
            else
            {
                chainType = (JOINT_ROT_AXIS);
            }

            min =joint->limits.value()->lower;
            max =joint->limits.value()->upper;
        }
        else if(jointType == URDFJointType::URDF_PRISMATIC)
        {
            if(axis == Vector3DS(0,0,0))
            {
                throw Exception(1,"[URDF] Prismatic axis type is not supported!",__FILE__, __LINE__, __FUNCTION__);
            }
            else
            {
                chainType =(JOINT_TRANS_AXIS);
            }
        }
        else
        {
            throw Exception(1,"[URDF] Type is not supported!",__FILE__, __LINE__, __FUNCTION__);
        }
        //frame.trans.print();
        chain.addSegments(Segment(name, Joint(frame.trans, frame.rotMat*axis, chainType), frame, min, max));
    }

    return chain;
}

std::vector<string> URDF::getChainNames(const std::shared_ptr<StringTree> &strTree, const string &start, const string &end)
{
    //TODO: std::move
    std::vector<std::string> ways;
    bool res =StringTree::getShortestPath(strTree,start,end,ways);

    if(!res)
    {
        throw Exception(1,"[URDF] Get a chain between 2 nodes failed",__FILE__, __LINE__, __FUNCTION__);
    }

    return ways;
}

}

﻿#include <Msnhnet/robot/MsnhRotationInterp.h>
namespace Msnhnet
{

void RotationInterpSingleAxis::setStartEnd(const RotationMatDS &start, const RotationMatDS &end)
{
    _rotBase2Start      =   start;
    _rotBase2End        =   end;

    RotationMatDS rotStart2End    =   _rotBase2Start.inverse()*_rotBase2End;

    _angle = rotStart2End.getRotAngle(_rotStart2EndAxis,MSNH_F32_EPS);
}

double RotationInterpSingleAxis::getAngle()
{
    return _angle;
}

RotationMatDS RotationInterpSingleAxis::getPos(double theta) const
{
    auto rotVec = _rotStart2EndAxis*theta;
    return _rotBase2Start * Msnhnet::GeometryS::rotVec2RotMat(rotVec);
}

Vector3DS RotationInterpSingleAxis::getVel(double theta, double thetad) const
{
    (void)theta;
    return _rotBase2Start * _rotStart2EndAxis * thetad;
}

Vector3DS RotationInterpSingleAxis::getAcc(double theta, double thetad, double thetadd) const
{
    (void)theta;
    (void)thetad;
    return _rotBase2Start * _rotStart2EndAxis * thetadd;
}

RotationInterpSingleAxis *RotationInterpSingleAxis::clone()
{
    return new RotationInterpSingleAxis();
}

void RotationInterpQuat::setStartEnd(const RotationMatDS &start, const RotationMatDS &end)
{
    _rotBase2Start      =   start;
    _rotBase2End        =   end;

    _quatBase2Start = GeometryS::rotMat2Quaternion(_rotBase2Start);
    _quatBase2End   = GeometryS::rotMat2Quaternion(_rotBase2End);

    //        B   C
    //     A         D
    //     H         E
    //        G   F
    //
    //  A -> C, 有两种方式
    //虽然 𝑞 与 −𝑞 对向量变换的最终效果是完全相同的，但是它们作为向量相差了 𝜋 弧度：
    //我们需要先检测𝑞0 与 𝑞1 之间是否是钝角，即检测它们点积的结果 𝑞0 · 𝑞1 是否为负数;
    //如果𝑞0 · 𝑞1 < 0，那么我们就反转其中的一个四元数,这样才能保证插值的路径是最短的

    _cosHalfTheta = _quatBase2Start.q0*_quatBase2End.q0 +
            _quatBase2Start.q1*_quatBase2End.q1 +
            _quatBase2Start.q2*_quatBase2End.q2 +
            _quatBase2Start.q3*_quatBase2End.q3;

    if(_cosHalfTheta < 0)
    {
        _quatBase2End.q0 = -_quatBase2End.q0;
        _quatBase2End.q1 = -_quatBase2End.q1;
        _quatBase2End.q2 = -_quatBase2End.q2;
        _quatBase2End.q3 = -_quatBase2End.q3;

        _cosHalfTheta = -_cosHalfTheta;
    }


    _sinHalfTheta = sqrt(1 - _cosHalfTheta*_cosHalfTheta);
    _halfTheta    = atan2(_sinHalfTheta, _cosHalfTheta);
    _angle        = _halfTheta * 2;

    _halfThetaDivSinHalfTheta   = _halfTheta/_sinHalfTheta;
    _dHalfThetaDivSinHalfTheta  = _halfTheta*_halfTheta/_sinHalfTheta;
}

double RotationInterpQuat::getAngle()
{
    return _angle;
}

RotationMatDS RotationInterpQuat::getPos(double theta) const
{
    if(theta <= 0)
    {
        return _rotBase2Start;
    }

    if(fabs(theta - _angle)<MSNH_F32_EPS)
    {
        return _rotBase2End;
    }

    if(_cosHalfTheta >= 1.0)
    {
        return  GeometryS::quaternion2RotMat(_quatBase2End);
    }

    const double t = theta / _angle;

    //如果θ=180度，则结果未完全定义,我们可以绕着垂直于a或b的任何轴旋转
    if(_sinHalfTheta <= 0.001)
    {
        QuaternionDS dQ = (1-t)*_quatBase2Start + t*_quatBase2End;
        return  GeometryS::quaternion2RotMat(dQ);
    }


    // slerp 球面路径最短
    //
    const double ratio1 = sin( (1-t)*_halfTheta) / _sinHalfTheta;
    const double ratio2 = sin(t*_halfTheta) / _sinHalfTheta;

    QuaternionDS dQ = _quatBase2Start*ratio1 + _quatBase2End*ratio2;
    return  GeometryS::quaternion2RotMat(dQ);
}

Vector3DS RotationInterpQuat::getVel(double theta, double thetad) const
{
    (void)theta;

    if(_cosHalfTheta >= 1.0)
    {
        return Vector3DS();
    }

    const double t = thetad / _angle;

    //如果θ=180度，则结果未完全定义,我们可以绕着垂直于a或b的任何轴旋转
    if(_sinHalfTheta <= 0.001)
    {
        //((1-t)*_quatBase2Start + t*_quatBase2End)'= _quatBase2End - _quatBase2Start
        QuaternionDS dQ = _quatBase2End - _quatBase2Start;
        return  GeometryS::quaternion2Euler(dQ,ROT_ZYX);
    }

    // [sin((1-x)*θ)/sin(θ)]' = -θcos((1-x)*θ)/sin(θ)
    const double ratio1 = -1*cos( (1-t)*_halfTheta) * _halfThetaDivSinHalfTheta;
    // [sin(x*θ)/sin(θ)]' = θcos(x*θ)/sin(θ)
    const double ratio2 = cos(t*_halfTheta) * _halfThetaDivSinHalfTheta;

    QuaternionDS dQ = _quatBase2Start*ratio1 + _quatBase2End*ratio2;
    return GeometryS::quaternion2Euler(dQ,ROT_ZYX);
}

Vector3DS RotationInterpQuat::getAcc(double theta, double thetad, double thetadd) const
{
    (void)theta;
    (void)thetad;

    if(_cosHalfTheta >= 1.0)
    {
        return Vector3DS();
    }

    const double t = thetadd / _angle;

    //如果θ=180度，则结果未完全定义,我们可以绕着垂直于a或b的任何轴旋转
    if(_sinHalfTheta <= 0.001)
    {
        //((1-t)*_quatBase2Start + t*_quatBase2End)''= 0
        return  Vector3DS();
    }

    // [sin((1-x)*θ)/sin(θ)]' = -θ^2*sin((1-x)*θ)/sin(θ)
    const double ratio1 = -1*sin( (1-t)*_halfTheta) * _dHalfThetaDivSinHalfTheta;
    // [sin(x*θ)/sin(θ)]' = θ^2*sin(x*θ)/sin(θ)
    const double ratio2 = sin(t*_halfTheta) * _dHalfThetaDivSinHalfTheta;

    QuaternionDS dQ = _quatBase2Start*ratio1 + _quatBase2End*ratio2;
    return GeometryS::quaternion2Euler(dQ,ROT_ZYX);
}

RotationInterpQuat *RotationInterpQuat::clone()
{
    return new RotationInterpQuat();
}


}


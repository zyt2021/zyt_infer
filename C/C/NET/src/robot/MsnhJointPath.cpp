﻿#include  <Msnhnet/robot/MsnhJointPath.h>

namespace Msnhnet
{

SingleJointPath::SingleJointPath(const VectorXSDS &startJoints, const VectorXSDS &endJoints)
    :_startJoints(startJoints),_endJoints(endJoints)
{
    assert(startJoints.mN == endJoints.mN);
    _jointsRatio.resize(startJoints.mN);

    _start2EndJoints = endJoints - startJoints;

    double length = 0;

    for (int i = 0; i < _start2EndJoints.mN; ++i)
    {
        double len = std::fabs(_start2EndJoints[i]);
        if( len > length)
        {
            length = len;
        }
    }

    _length = length;

    for (int i = 0; i < _start2EndJoints.mN; ++i)
    {
        _jointsRatio[i] = _start2EndJoints[i] / _length;
    }
}

double SingleJointPath::getPathLength()
{
    return _length;
}

VectorXSDS SingleJointPath::getPos(double s) const
{
    VectorXSDS pos(_start2EndJoints.mN);

    for (int i = 0; i < _start2EndJoints.mN; ++i)
    {
        pos[i] = _startJoints[i] + _jointsRatio[i] * s;
    }

    return pos;
}

VectorXSDS SingleJointPath::getVel(double s, double sd) const
{

    (void) s;

    VectorXSDS vec(_start2EndJoints.mN);

    for (int i = 0; i < _start2EndJoints.mN; ++i)
    {
        vec[i] = _jointsRatio[i]*sd;
    }

    return vec;
}

VectorXSDS SingleJointPath::getAcc(double s, double sd, double sdd) const
{
    (void)s;
    (void)sd;

    VectorXSDS acc(_start2EndJoints.mN);

    for (int i = 0; i < _start2EndJoints.mN; ++i)
    {
        acc[i] = _jointsRatio[i]*sdd;
    }

    return acc;
}

JointPathComposite::JointPathComposite()
{
    _pathLength     = 0;
    _cachedStarts   = 0;
    _cachedEnds     = 0;
    _cachedIndex    = 0;
}

JointPathComposite::~JointPathComposite()
{
    auto iter = _geoVec.begin();

    while (iter!=_geoVec.end())
    {
        if(iter->second)
        {
            delete iter->first;
        }
    }
}

void JointPathComposite::add(JointPath *geom, bool aggregate)
{
    _pathLength += geom->getPathLength();
    _dVec.push_back(_pathLength);
    _geoVec.push_back(std::make_pair(geom,aggregate));
}

double JointPathComposite::getPathLength()
{
    return _pathLength;
}

VectorXSDS JointPathComposite::getPos(double s) const
{
    s = lookUp(s);
    return _geoVec[_cachedIndex].first->getPos(s);
}

VectorXSDS JointPathComposite::getVel(double s, double sd) const
{
    s = lookUp(s);
    return _geoVec[_cachedIndex].first->getVel(s,sd);
}

VectorXSDS JointPathComposite::getAcc(double s, double sd, double sdd) const
{
    s = lookUp(s);
    return _geoVec[_cachedIndex].first->getAcc(s,sd,sdd);
}

double JointPathComposite::lookUp(double s) const
{
    assert(s>=-1e-12);
    assert(s<=(_pathLength+1e-12));

    if((_cachedStarts<=s)&&(s<=_cachedEnds))
    {
        return s-_cachedStarts;
    }

    double prevS = 0;

    for (int i = 0; i < _dVec.size(); ++i)
    {
        if((s<=_dVec[i]) || (i==_dVec.size()-1) )
        {
            _cachedIndex    = i;
            _cachedStarts   = prevS;
            _cachedEnds     = _dVec[i];

            return s - prevS;
        }

        prevS = _dVec[i];
    }

    return 0;
}

}

﻿#include <Msnhnet/robot/MsnhVelocityProfile.h>

namespace Msnhnet
{

VelocityProfileTrap::VelocityProfileTrap(double maxVel, double maxAcc)
    :_a1(0),_a2(0),_a3(0),_b1(0),_b2(0),_b3(0),_c1(0),_c2(0),_c3(0),
     _duration(0),_t1(0),_t2(0),_maxVel(maxVel),_maxAcc(maxAcc),
     _startPos(0),_endPos(0)
{

}

void VelocityProfileTrap::setProfile(double pos1, double pos2)
{
    _startPos   = pos1;
    _endPos     = pos2;

    /// ^
    /// |      ------------
    /// |    / |          | \
    /// |   /  |          |  \
    /// |  /dS |          |   \
    /// | /    |          |    \
    ///  ------------------------ >
    /// 0      t1         t2     t
    /// |<-----   duration   ----->
    ///
    /// ^
    /// |
    /// |    / \
    /// |   / | \
    /// |  /  |  \
    /// | /   |   \
    ///  ------------------------>
    /// 0     t1  t2
    ///|<- duration ->
    ///
    ///
    ///         | a1 + a2*t + a3*t^2   t < t1
    /// L(t) = <  b1 + b2*t + b3*t^2   t1 < t < t2
    ///         | c1 + c2*t + c3*t^2   t2 < t < duration
    ///
    /// 加速阶段:
    /// L(t)    = a1 + a2*t + a3*t      (t=0, L(t) = L(0)
    /// L'(t)   = a2 + 2*a3*t           = 0
    /// L''(t)  = 2*a3                  = a
    ///
    /// a1 =  L(0)
    /// a2 =  0
    /// a3 =  1/2*a
    ///
    /// 匀速阶段:
    ///
    /// L(t)    = b1 + b2*t             b1 = a1 + (a2+a3*1)*t1 - (b2+b3*t1)*t1
    /// L'(t)   = b2                    b2 = a2 + 2*a3*t1 -2*b3*t1
    /// L''(t)  = 0                     b3 = 0
    ///
    /// 减速阶段:
    ///
    /// L(t)    = c1 + c2*t + c3*t      c1 = b1 + (b2 + b3*t2)*t2 - (c2 + c3*t2)*t2
    /// L'(t)   = c2 + 2*c3*t           c2 = b2 + 2*b3*t2 - 2*c3*t2
    /// L''(t)  = 2*c3                  c3 = -a/2
    ///

    _t1 = _maxVel / _maxAcc; //加速到最大速度所需时间

    double s    = MathUtils::sign( _endPos - _startPos ); //正负标记
    double dX1  = s * _maxAcc * _t1 * _t1 /2;             //dS = 1/2*a*t^2
    double dt   = (_endPos - _startPos -2.0*dX1) /(s*_maxVel);// (t2 - t1)段运行时间

    // 如果有匀速段(t2-t1)
    if(dt > 0.0)
    {
        _duration = 2*_t1 + dt; //整个持续时间为两倍t1+匀速时间
        _t2       = _duration - _t1;//(t-t2)=t1
    }
    else //没有匀速段
    {
        _t1         = sqrt(s*(_endPos - _startPos)/_maxAcc); //2*1/2*a*t^2 = S;  t = sqrt(S/a)
        _duration   = _t1*2; //持续时间
        _t2         = _t1;
    }

    _a3         = s * _maxAcc / 2.0;
    _a2         = 0;
    _a1         = _startPos;

    _b3         = 0;
    _b2         = _a2 + 2*_a3*_t1 - 2.0*_b3*_t1;
    _b1         = _a1 + (_a2+_a3*_t1)*_t1 - (_b2+_b3*_t1)*_t1;

    _c3         = -s * _maxAcc / 2.0;
    _c2         = _b2 + 2*_b3*_t2 - 2.0*_c3*_t2;
    _c1         = _b1+(_b2+_b3*_t2)*_t2 - (_c2+_c3*_t2)*_t2;

}

void VelocityProfileTrap::setProfileDuration(double pos1, double pos2, double duration)
{

    //最快的方式
    setProfile(pos1, pos2);

    //设置持续时间必须大于最快方式所需时间，即必须比最快的方式慢一点
    double factor = _duration / duration;

    if(factor >1)
    {
        return;
    }

    _duration = duration;

    //受时间影响项进行拉伸
    _a2 *= factor;
    _a3 *= (factor*factor);
    _b2 *= factor;
    _b3 *= (factor*factor);
    _c2 *= factor;
    _c3 *= (factor*factor);

    _t1 /= factor;
    _t2 /= factor;
}

void VelocityProfileTrap::setMax(double maxVel, double maxAcc)
{
    _maxVel = maxVel;
    _maxAcc = maxAcc;
}

double VelocityProfileTrap::getDuration() const
{
    return _duration;
}

double VelocityProfileTrap::getPos(double time) const
{
    if(time < 0)
    {
        return _startPos;
    }
    else if(time < _t1)
    {
        return _a1 + time*(_a2 + time*_a3);
    }
    else if(time < _t2)
    {
        return _b1 + time*(_b2 + time*_b3);
    }
    else if(time <= _duration)
    {
        return _c1 + time*(_c2 + time*_c3);
    }
    else
    {
        return _endPos;
    }
}

double VelocityProfileTrap::getVel(double time) const
{
    if(time < 0)
    {
        return 0;
    }
    else if(time < _t1)
    {
        return (_a2 + 2*time*_a3);
    }
    else if(time < _t2)
    {
        return (_b2 + 2*time*_b3);
    }
    else if(time <= _duration)
    {
        return (_c2 + 2*time*_c3);
    }
    else
    {
        return 0;
    }
}

double VelocityProfileTrap::getAcc(double time) const
{
    if(time < 0)
    {
        return 0;
    }
    else if(time < _t1)
    {
        return 2*_a3;
    }
    else if(time < _t2)
    {
        return 2*_b3;
    }
    else if(time <= _duration)
    {
        return 2*_c3;
    }
    else
    {
        return 0;
    }
}

VelocityProfileConst::VelocityProfileConst(double maxVel):_maxVel(maxVel)
{

}

void VelocityProfileConst::setProfile(double pos1, double pos2)
{
    double difference = (pos2 > pos1); //增量/s
    if(abs(difference) > MSNH_F64_EPS)
    {
        _velocity = (difference>0)?_maxVel:-_maxVel;
        _posStart      = pos1; //起始点
        _duration = difference/_velocity;
    }
    else
    {
        _velocity = 0;
        _posStart      = pos1; //起始点
        _duration = 0;
    }
}

void VelocityProfileConst::setProfileDuration(double pos1, double pos2, double duration)
{
    double difference = (pos2 > pos1); //增量/s
    if(abs(difference) > MSNH_F64_EPS)
    {
        _velocity = difference/duration;

        if(_velocity > _maxVel || duration == 0)
            _velocity = _maxVel; //防止最大速度越界

        _posStart      = pos1; //起始点
        _duration = difference/_velocity;
    }
    else
    {
        _velocity = 0;
        _posStart      = pos1; //起始点
        _duration = duration;
    }
}

void VelocityProfileConst::setMax(double maxVel)
{
    _maxVel = maxVel;
}

double VelocityProfileConst::getDuration() const
{
    return _duration;
}

double VelocityProfileConst::getPos(double time) const
{
    if(time < 0)
    {
        return _posStart;
    }
    else if(time > _duration)
    {
        return _velocity * _duration + _posStart;
    }
    else
    {
        return _velocity * time + _posStart;
    }
}

double VelocityProfileConst::getVel(double time) const
{
    if(time < 0)
    {
        return 0;
    }
    else if(time > _duration)
    {
        return 0;
    }
    else
    {
        return _velocity;
    }
}

double VelocityProfileConst::getAcc(double time) const
{
    return 0;
}

VelocityProfileTrapHalf::VelocityProfileTrapHalf(double maxVel, double maxAcc, bool isStartPart)
    :_maxVel(maxVel),_maxAcc(maxAcc),_isStartPart(isStartPart)
{

}

void VelocityProfileTrapHalf::setProfile(double pos1, double pos2)
{
    _startPos       = pos1;
    _endPos         = pos2;
    double s        = ((_endPos-_startPos)<0)?-1:1;

    // check that the maxvel is obtainable
    double vel = std::min(_maxVel, sqrt(2.0*s*(_endPos-_startPos)*_maxAcc));
    _duration       = s*(_endPos-_startPos)/vel + vel/_maxAcc/2.0;
    if (_isStartPart)
    {
        _t1 = 0;
        _t2 = vel/_maxAcc;
        planProfile1(vel*s,_maxAcc*s);
    }
    else
    {
        _t1 = _duration-vel/_maxAcc;
        _t2 = _duration;
        planProfile2(s*vel,s*_maxAcc);
    }
}

void VelocityProfileTrapHalf::setProfileDuration(double pos1, double pos2, double newDuration)
{
    setProfile(pos1,pos2);
    double factor = _duration/newDuration;

    if ( factor > 1 )
        return;

    double s        = ((_endPos-_startPos)<0)?-1:1;
    double tmp      = 2.0*s*(_endPos-_startPos)/_maxVel;
    double v        = s*_maxVel;
    _duration       = newDuration;
    if (_isStartPart)
    {
        if (tmp > newDuration)
        {
            _t1 = 0;
            double a = v*v/2.0/(v*newDuration-(_endPos-_startPos));
            _t2 = v/a;
            planProfile1(v,a);
        } else
        {
            _t2 = newDuration;
            double a = v*v/2.0/(_endPos-_startPos);
            _t1 = _t2-v/a;
            planProfile1(v,a);
        }
    }
    else
    {
        if (tmp > newDuration)
        {
            _t2 = newDuration;
            double a = v*v/2.0/(v*newDuration-(_endPos-_startPos));
            _t1 = _t2-v/a;
            planProfile2(v,a);
        }
        else
        {
            double a = v*v/2.0/(_endPos-_startPos);
            _t1 = 0;
            _t2 = v/a;
            planProfile2(v,a);
        }
    }
}

void VelocityProfileTrapHalf::setMax(double maxVel, double maxAcc, bool isStartPart)
{
    _maxVel = maxVel;
    _maxAcc = maxAcc;
    _isStartPart = isStartPart;
}

double VelocityProfileTrapHalf::getDuration() const
{
    return _duration;
}

double VelocityProfileTrapHalf::getPos(double time) const
{
    if (time < 0)
    {
        return _startPos;
    }
    else if (time<_t1)
    {
        return _a1+time*(_a2+_a3*time);
    }
    else if (time<_t2)
    {
        return _b1+time*(_b2+_b3*time);
    }
    else if (time<=_duration)
    {
        return _c1+time*(_c2+_c3*time);
    }
    else
    {
        return _endPos;
    }
}

double VelocityProfileTrapHalf::getVel(double time) const
{
    if (time < 0)
    {
        return 0;
    }
    else if (time<_t1)
    {
        return _a2+2*_a3*time;
    }
    else if (time<_t2)
    {
        return _b2+2*_b3*time;
    }
    else if (time<=_duration)
    {
        return _c2+2*_c3*time;
    }
    else
    {
        return 0;
    }
}

double VelocityProfileTrapHalf::getAcc(double time) const
{
    if (time < 0)
    {
        return 0;
    }
    else if (time<_t1)
    {
        return 2*_a3;
    }
    else if (time<_t2)
    {
        return 2*_b3;
    }
    else if (time<=_duration)
    {
        return 2*_c3;
    } else {
        return 0;
    }
}

void VelocityProfileTrapHalf::planProfile1(double velocity, double acc)
{
    _a3 = 0;
    _a2 = 0;
    _a1 = _startPos;
    _b3 = acc/2.0;
    _b2 = -acc*_t1;
    _b1 = _startPos + acc*_t1*_t1/2.0;
    _c3 = 0;
    _c2 = velocity;
    _c1 = _endPos - velocity*_duration;
}

void VelocityProfileTrapHalf::planProfile2(double velocity, double acc)
{
    _a3 = 0;
    _a2 = velocity;
    _a1 = _startPos;
    _b3 = -acc/2.0;
    _b2 = acc*_t2;
    _b1 = _endPos - acc*_t2*_t2/2.0;
    _c3 = 0;
    _c2 = 0;
    _c1 = _endPos;
}

}

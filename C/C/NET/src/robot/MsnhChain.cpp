﻿#include "Msnhnet/robot/MsnhChain.h"

namespace Msnhnet
{

inline static double fRand(double min, double max)
{
    double f = (double)rand() / RAND_MAX;
    return min + f * (max - min);
}

//辅助功能以最小化（请求配置的关节角度误差平方和）。
//因为我们想要一个没有静态成员的类，但是NLOpt库不支持传递类的方法，所以我们使用这些辅助函数。
double minfuncSumSquared(const std::vector<double>& x, std::vector<double>& grad, void* data)
{


    Chain *chain = (Chain *) data;

    std::vector<double> vals(x);

    double jump = 0.000001;
    double result[1] = {0};


    chain->cartSumSquaredErr(vals, result);


    if (!grad.empty())
    {
        double v1[1];
        for (unsigned int i = 0; i < x.size(); i++)
        {
            double original = vals[i];

            vals[i] = original + jump;

            chain->cartSumSquaredErr(vals, v1);
            vals[i] = original;
            grad[i] = (v1[0] - result[0]) / (2.0 * jump);
        }
    }


    return result[0];
}


Chain::Chain():
    segments(0),
    _numOfJoints(0),
    _numOfSegments(0)
{

}

Chain::Chain(const Chain &chain):
    segments(0),
    _numOfJoints(0),
    _numOfSegments(0)

{
    _minJoints.clear();
    _maxJoints.clear();
    _jointMoveTypes.clear();
    for(uint32_t i=0; i<chain.getNumOfSegments();i++)
    {
        addSegments(chain.getSegment(i));
    }
}

Chain &Chain::operator=(const Chain &chain)
{
    _numOfJoints    = 0;
    _numOfSegments  = 0;

    segments.clear();
    _minJoints.clear();
    _maxJoints.clear();
    _jointMoveTypes.clear();

    for(uint32_t i=0; i<chain.getNumOfSegments();i++)
    {
        addSegments(chain.getSegment(i));
    }
    return *this;
}

void Chain::initOpt()
{
    _opt = nlopt::opt(nlopt::LD_SLSQP, _numOfJoints);
    _opt.set_xtol_abs(0.000001);
    _opt.set_min_objective(minfuncSumSquared, this);
    _optInited = true;
}

void Chain::addSegments(const Segment &segment)
{
    segments.push_back(segment);
    _numOfSegments++;
    if(segment.getJoint().getType() != JOINT_FIXED)
    {
        _numOfJoints++;

        _minJoints.push_back(segment.getJointMin());
        _maxJoints.push_back(segment.getJointMax());
        _jointMoveTypes.push_back(segment.getMoveType());
    }
}

uint32_t Chain::getNumOfJoints() const
{
    return _numOfJoints;
}

uint32_t Chain::getNumOfSegments() const
{
    return _numOfSegments;
}

const Segment &Chain::getSegment(uint32_t idx) const
{
    return segments[idx];
}

void Chain::jacobi(MatSDS &jac, const VectorXSDS &joints, int segNum) const
{
    int segmentNum = 0;

    if(segNum<0)
    {
        segmentNum = _numOfSegments;
    }
    else
    {
        segmentNum = segNum;
    }

    if(joints.mN!=_numOfJoints)
    {
        throw Exception(1,"[Robot Chain] input joints num != chain's joints", __FILE__, __LINE__, __FUNCTION__);
    }

    if(segmentNum > _numOfSegments)
    {
        throw Exception(1,"[Robot Chain] input segments num > chain's segments", __FILE__, __LINE__, __FUNCTION__);
    }

    Twist twist;

    Frame fk;
    Frame tTmp;


    int jointCnt = 0;

    int jacCnt   = 0;

    for (int i = 0; i < segmentNum; ++i)
    {

        if(segments[i].getJoint().getType() != JOINT_FIXED)
        {
            //FK
            fk = tTmp * segments[i].getPos(joints[jointCnt]);
            twist = tTmp.rotMat * segments[i].getTwist(joints[jointCnt],1.0);
            jointCnt++;
        }
        else
        {
            fk = tTmp * segments[i].getPos(0.0);
        }
        changeRefPoint(jac, fk.trans - tTmp.trans);

        if(segments[i].getJoint().getType() != JOINT_FIXED)
        {
            jac.setCol(jacCnt++, twist.toMat());
        }

        tTmp = fk;
    }
}

void Chain::jacobi(MatSDS &jac, const VectorXSDS &joints, const std::vector<Frame> &jointRoot,                                //NONFREE
                   const std::vector<Frame> &jointTip, const Frame &finalFrame, int segNum) const                              //NONFREE
{                                                                                                                              //NONFREE
    int segmentNum = 0;                                                                                                        //NONFREE
    //NONFREE
    if(segNum<0)                                                                                                               //NONFREE
    {                                                                                                                          //NONFREE
        segmentNum = _numOfSegments;                                                                                           //NONFREE
    }                                                                                                                          //NONFREE
    else                                                                                                                       //NONFREE
    {                                                                                                                          //NONFREE
        segmentNum = segNum;                                                                                                   //NONFREE
    }                                                                                                                          //NONFREE
    //NONFREE
    if(joints.mN != _numOfJoints)                                                                                              //NONFREE
    {                                                                                                                          //NONFREE
        throw Exception(1,"[Robot Chain] input joints num != chain's joints", __FILE__, __LINE__, __FUNCTION__);               //NONFREE
    }                                                                                                                          //NONFREE
    //NONFREE
    if(segmentNum > _numOfSegments)                                                                                            //NONFREE
    {                                                                                                                          //NONFREE
        throw Exception(1,"[Robot Chain] input segments num > chain's segments", __FILE__, __LINE__, __FUNCTION__);            //NONFREE
    }                                                                                                                          //NONFREE
    //NONFREE
    unsigned int jointndx=0;                                                                                                   //NONFREE
    for (int i = 0; i < segmentNum; ++i)                                                                                       //NONFREE
    {                                                                                                                          //NONFREE
        if(segments[i].getJoint().getType() != JOINT_FIXED)                                                             //NONFREE
        {                                                                                                                      //NONFREE
            Twist t = (jointRoot[jointndx].rotMat) * (segments[i].getTwist(joints[jointndx],1.0));                             //NONFREE
            t = t.refPoint( finalFrame.trans - jointTip[jointndx].trans);                                                      //NONFREE
            jac(jointndx,0)=t[0];                                                                                              //NONFREE
            jac(jointndx,1)=t[1];                                                                                              //NONFREE
            jac(jointndx,2)=t[2];                                                                                              //NONFREE
            jac(jointndx,3)=t[3];                                                                                              //NONFREE
            jac(jointndx,4)=t[4];                                                                                              //NONFREE
            jac(jointndx,5)=t[5];                                                                                              //NONFREE
            jointndx++;                                                                                                        //NONFREE
        }                                                                                                                      //NONFREE
    }                                                                                                                          //NONFREE
}                                                                                                                              //NONFREE

Frame Chain::fk(const VectorXSDS &joints, int segNum)
{
    int segmentNum = 0;

    if(segNum<0)
    {
        segmentNum = _numOfSegments;
    }
    else
    {
        segmentNum = segNum;
    }

    if(joints.mN!= _numOfJoints)
    {
        throw Exception(1,"[RobotFK] input joints num != chain's joints", __FILE__, __LINE__, __FUNCTION__);
    }

    if(segmentNum > _numOfSegments)
    {
        throw Exception(1,"[RobotFK] input segments num > chain's segments", __FILE__, __LINE__, __FUNCTION__);
    }

    Frame frame;

    int jointCnt = 0;

    for (int i = 0; i < segmentNum; ++i)
    {
        if(segments[i].getJoint().getType() != JOINT_FIXED)
        {
            frame = frame * segments[i].getPos(joints[jointCnt]);
            //            std::cout<<"=============\n";
            //            segments[i].getPos(joints[jointCnt]).print();
            jointCnt++;
        }
        else
        {
            frame = frame * segments[i].getPos(0.0);
        }
    }


    return frame;
}


Frame Chain::fk(const VectorXSDS &joints, std::vector<Frame> &jointRoot, std::vector<Frame> &jointTip, int segNum)         //NONFREE
{                                                                                                                          //NONFREE
    int segmentNum = 0;                                                                                                    //NONFREE
    //NONFREE
    if(segNum<0)                                                                                                           //NONFREE
    {                                                                                                                      //NONFREE
        segmentNum = _numOfSegments;                                                                                       //NONFREE
    }                                                                                                                      //NONFREE
    else                                                                                                                   //NONFREE
    {                                                                                                                      //NONFREE
        segmentNum = segNum;                                                                                               //NONFREE
    }                                                                                                                      //NONFREE
    //NONFREE
    if(joints.mN!= _numOfJoints)                                                                                           //NONFREE
    {                                                                                                                      //NONFREE
        throw Exception(1,"[RobotFK] input joints num != chain's joints", __FILE__, __LINE__, __FUNCTION__);               //NONFREE
    }                                                                                                                      //NONFREE
    //NONFREE
    if(segmentNum > _numOfSegments)                                                                                        //NONFREE
    {                                                                                                                      //NONFREE
        throw Exception(1,"[RobotFK] input segments num > chain's segments", __FILE__, __LINE__, __FUNCTION__);            //NONFREE
    }                                                                                                                      //NONFREE
    //NONFREE
    Frame frame;                                                                                                           //NONFREE
    //NONFREE
    int jointCnt = 0;                                                                                                      //NONFREE
    //NONFREE
    for (int i = 0; i < segmentNum; ++i)                                                                                   //NONFREE
    {                                                                                                                      //NONFREE
        if(segments[i].getJoint().getType() != JOINT_FIXED)                                                         //NONFREE
        {                                                                                                                  //NONFREE
            jointRoot[jointCnt]=frame;                                                                                     //NONFREE
            frame = frame * segments[i].getPos(joints[jointCnt]);                                                          //NONFREE
            jointTip[jointCnt]=frame;                                                                                      //NONFREE
            jointCnt++;                                                                                                    //NONFREE
        }                                                                                                                  //NONFREE
        else                                                                                                               //NONFREE
        {                                                                                                                  //NONFREE
            frame = frame * segments[i].getPos(0.0);                                                                       //NONFREE
        }                                                                                                                  //NONFREE
    }                                                                                                                      //NONFREE
    return frame;                                                                                                          //NONFREE
}                                                                                                                          //NONFREE

int Chain::ikNewton(const Frame &desireFrame, VectorXSDS &outJoints, int maxIter, double epsilon, double maxTime)
{

    _stopImmediately = false;//NONFREE

    if(outJoints.mN != _numOfJoints)
    {
        return -2;
    }

    Frame frame;
    Twist dtTwist;
    MatSDS jac(_numOfJoints,6);

    double timeM    = maxTime * 1000.0;
    double timeNow  = 0;
    auto joints     = outJoints;

#ifdef USE_NOT_FREE                                                                //NONFREE
    std::vector<Frame> jointRoot(_numOfJoints);                                    //NONFREE
    std::vector<Frame> jointTip(_numOfJoints);                                     //NONFREE
#endif                                                                             //NONFREE

    auto st = std::chrono::high_resolution_clock::now();

    for (int i = 0; i < maxIter; ++i)
    {

        auto so = std::chrono::high_resolution_clock::now();

        timeNow = std::chrono::duration <double,std::milli> ((so-st)).count();

        if(timeNow > timeM)
        {
            return -6;
        }

        if(_stopImmediately)                                                       //NONFREE
            return -7;                                                             //NONFREE

#ifdef USE_NOT_FREE                                                                //NONFREE
        frame = fk(joints, jointRoot, jointTip);                                   //NONFREE
#else                                                                              //NONFREE
        frame = fk(joints);//0.009ms
#endif                                                                             //NONFREE

        dtTwist = Frame::diff(frame, desireFrame);  // 0.002ms

        if(dtTwist.closeToEps(epsilon))
        {
            outJoints = joints;
            return i;
        }

#ifdef USE_NOT_FREE                                                                //NONFREE
        jacobi(jac,joints, jointRoot, jointTip, frame);                            //NONFREE
#else                                                                              //NONFREE
        jacobi(jac,joints);
#endif                                                                             //NONFREE

        MatSDS dtJoints = jac.pseudoInvert()*dtTwist.toMat(); //0.008ms

        for (int i = 0; i < joints.mN; ++i)
        {
            joints[i] = joints[i] + dtJoints[i];

            int time = (int)(joints[i]/MSNH_2_PI);
            joints[i] -= time * MSNH_2_PI;
        }

    }
    return -1;
}

int Chain::ikNewtonJL(const Frame &desireFrame, VectorXSDS &outJoints, int maxIter, double epsilon)
{
    _stopImmediately = false;//NONFREE

    if(outJoints.mN != _numOfJoints)
    {
        return -2;
    }

    Frame frame;
    Twist dtTwist;
    auto joints = outJoints;

#ifdef USE_NOT_FREE                                                                //NONFREE
    std::vector<Frame> jointRoot(_numOfJoints);                                    //NONFREE
    std::vector<Frame> jointTip(_numOfJoints);                                     //NONFREE
#endif                                                                             //NONFREE

    MatSDS jac(_numOfJoints,6);

    for (int i = 0; i < maxIter; ++i)
    {

#ifdef USE_NOT_FREE                                                                //NONFREE
        frame = fk(joints, jointRoot, jointTip);                                   //NONFREE
#else                                                                              //NONFREE
        frame = fk(joints);//0.009ms
#endif                                                                             //NONFREE

        dtTwist = Frame::diff(frame, desireFrame);  // 0.002ms

        if(dtTwist.closeToEps(epsilon))
        {
            outJoints = joints;
            return i;
        }

#ifdef USE_NOT_FREE                                                                //NONFREE
        jacobi(jac,joints, jointRoot, jointTip, frame);                             //NONFREE
#else                                                                              //NONFREE
        jacobi(jac,joints);
#endif                                                                             //NONFREE

        MatSDS dtJoints = jac.pseudoInvert()*dtTwist.toMat(); //0.008ms

        for (int i = 0; i < joints.mN; ++i)
        {
            joints[i] = joints[i] + dtJoints[i];

            if(_jointMoveTypes[i] != MOVE_TRANS && _jointMoveTypes[i] != MOVE_ROT_CONTINUOUS)
            {
                int time = (int)(joints[i]/MSNH_2_PI);
                joints[i] -= time * MSNH_2_PI;
            }

            if(joints[i] < _minJoints[i])
            {
                joints[i] = _minJoints[i];
            }
            else if(joints[i] > _maxJoints[i])
            {
                joints[i] = _maxJoints[i];
            }
        }

    }
    return -1;
}

int Chain::ikNewtonRR(const Frame &desireFrame, VectorXSDS &outJoints, const Twist &bounds, const bool &randomStart, const bool &wrap, int maxIter, double epsilon, double maxTime)
{
    _stopImmediately = false;//NONFREE
    if(outJoints.mN != _numOfJoints)
    {
        return -2;
    }

    Frame frame;
    Twist dtTwist;
    auto joints     = outJoints;

    double timeM = maxTime * 1000.0;
    double timeNow = 0;

#ifdef USE_NOT_FREE                                                                //NONFREE
    std::vector<Frame> jointRoot(_numOfJoints);                                    //NONFREE
    std::vector<Frame> jointTip(_numOfJoints);                                     //NONFREE
#endif                                                                             //NONFREE

    MatSDS jac(_numOfJoints,6);

    auto st = std::chrono::high_resolution_clock::now();

    for (int i = 0; i < maxIter; ++i)
    {

        auto so = std::chrono::high_resolution_clock::now();

        timeNow = std::chrono::duration <double,std::milli> ((so-st)).count();

        if(timeNow > timeM)
        {
            return -6;
        }

        if(_stopImmediately)                                                       //NONFREE
            return -7;                                                             //NONFREE

#ifdef USE_NOT_FREE                                                                //NONFREE
        frame = fk(joints, jointRoot, jointTip);                                   //NONFREE
#else                                                                              //NONFREE
        frame = fk(joints);//0.009ms
#endif                                                                             //NONFREE
        dtTwist = Frame::diffRelative(desireFrame,frame);  // 0.002ms

        if (std::abs(dtTwist.v[0]) <= std::abs(bounds.v[0]))
            dtTwist.v[0] = 0;

        if (std::abs(dtTwist.v[1]) <= std::abs(bounds.v[1]))
            dtTwist.v[1] = 0;

        if (std::abs(dtTwist.v[2]) <= std::abs(bounds.v[2]))
            dtTwist.v[2] = 0;

        if (std::abs(dtTwist.omg[0]) <= std::abs(bounds.omg[0]))
            dtTwist.omg[0] = 0;

        if (std::abs(dtTwist.omg[1]) <= std::abs(bounds.omg[1]))
            dtTwist.omg[1] = 0;

        if (std::abs(dtTwist.omg[2]) <= std::abs(bounds.omg[2]))
            dtTwist.omg[2] = 0;


        if(dtTwist.closeToEps(epsilon))
        {
            outJoints = joints;
            return i;
        }

        dtTwist = Frame::diff(frame,desireFrame);

#ifdef USE_NOT_FREE                                                                //NONFREE
        jacobi(jac,joints, jointRoot, jointTip, frame);                             //NONFREE
#else                                                                              //NONFREE
        jacobi(jac,joints);
#endif                                                                             //NONFREE


        //        std::vector<VectorXSDS> U(6,VectorXSDS(jac.mWidth));
        //        VectorXSDS D(jac.mWidth);
        //        std::vector<VectorXSDS> V(jac.mWidth,VectorXSDS(jac.mWidth));
        //        jac.householderSVD(U,D,V,150);

        //        double sum = 0;
        //        VectorXSDS tmp(jac.mWidth);

        //        VectorXSDS dtJoints(jac.mWidth);

        //        for (int i = 0; i < jac.mWidth; ++i)
        //        {
        //            sum = 0;
        //            for (int j = 0; j < jac.mHeight; ++j)
        //            {
        //                sum +=  U[j](i)*dtTwist[j];
        //            }

        //            if(fabs(D[i])<epsilon)
        //            {
        //                tmp[i] = 0;
        //            }
        //            else
        //            {
        //                tmp[i] = sum/D[i];
        //            }
        //        }

        //        for (int i = 0; i < jac.mWidth; ++i)
        //        {
        //            sum = 0;
        //            for (int j=0;j<jac.mWidth;j++)
        //            {
        //                sum+=V[i](j)*tmp[j];
        //            }

        //            dtJoints[i] = sum;
        //        }

        //6*7 7*7 7*7 eigen
        //
        //        auto UDVt = jac.svd();
        //        double sum = 0;
        //        VectorXSDS tmp(jac.mWidth);

        //        VectorXSDS dtJoints(jac.mWidth);

        //        for (int i = 0; i < jac.mWidth; ++i)
        //        {
        //            sum = 0;
        //            for (int j = 0; j < jac.mHeight; ++j)
        //            {
        //                sum +=  UDVt[0](i,j)*dtTwist[j];
        //            }

        //            if(fabs(UDVt[1][i])<epsilon)
        //            {
        //                tmp[i] = 0;
        //            }
        //            else
        //            {
        //                tmp[i] = sum/UDVt[1][i];
        //            }
        //        }

        //        for (int i = 0; i < jac.mWidth; ++i)
        //        {
        //            sum = 0;
        //            for (int j=0;j<jac.mWidth;j++)
        //            {
        //                sum+=UDVt[2](i,j)*tmp[j];
        //            }

        //            dtJoints[i] = sum;
        //        }


        MatSDS dtJoints = jac.pseudoInvert()*dtTwist.toMat(); //0.008ms



        VectorXSDS currJoints(joints.mN);

        for (int i = 0; i < joints.mN; ++i)
        {
            currJoints[i] = joints[i] + dtJoints[i];

            auto mtype      = _jointMoveTypes[i];
            auto minJoint   = _minJoints[i];
            auto maxJoint   = _maxJoints[i];

            if( mtype == MOVE_ROT_CONTINUOUS)
            {
                continue;
            }

            if(currJoints[i] < minJoint)
            {
                if(!wrap ||  mtype == MOVE_TRANS)
                {
                    currJoints[i] = minJoint;
                }
                else
                {
                    double diffAngle = fmod(minJoint - currJoints[i], MSNH_2_PI);
                    double currAngle = minJoint - diffAngle + MSNH_2_PI;
                    if (currAngle > maxJoint)
                        currJoints[i] = minJoint;
                    else
                        currJoints[i] = currAngle;
                }
            }

            if(currJoints[i] > maxJoint)
            {
                if(!wrap ||  mtype == MOVE_TRANS)
                {
                    currJoints[i] = maxJoint;
                }
                else
                {
                    double diffAngle = fmod(currJoints[i] - maxJoint, MSNH_2_PI);
                    double currAngle = maxJoint + diffAngle - MSNH_2_PI;
                    if (currAngle < minJoint)
                        currJoints[i] = maxJoint;
                    else
                        currJoints[i] = currAngle;
                }
            }

            joints[i] = joints[i] - currJoints[i];
        }

        if(joints.isFuzzyNull(MSNH_F32_EPS))
        {
            if(randomStart)
            {
                for (int j = 0; j < currJoints.mN; j++)
                {
                    if (_jointMoveTypes[j] == MOVE_ROT_CONTINUOUS)
                        currJoints[j] = fRand(currJoints[j] - MSNH_2_PI, currJoints[j] + MSNH_2_PI);
                    else
                        currJoints[j] = fRand(_minJoints[j], _maxJoints[j]);
                }
            }
        }

        joints = currJoints;
    }
    return -1;
}

void Chain::cartSumSquaredErr(const std::vector<double> &x, double error[])
{

    if(_optStatus != -3)
    {
        _opt.force_stop();
        return;
    }

    Frame frame = fk(x);

    if(std::isnan(frame.trans[0]))
    {
        error[0] = std::numeric_limits<float>::max();
        _optStatus = -1;
        return;
    }

    Twist dtTwist = Frame::diffRelative(_desireFrameSQP, frame);

    if (std::abs(dtTwist.v[0]) <= std::abs(_boundsSQP.v[0]))
        dtTwist.v[0] = 0;

    if (std::abs(dtTwist.v[1]) <= std::abs(_boundsSQP.v[1]))
        dtTwist.v[1] = 0;

    if (std::abs(dtTwist.v[2]) <= std::abs(_boundsSQP.v[2]))
        dtTwist.v[2] = 0;

    if (std::abs(dtTwist.omg[0]) <= std::abs(_boundsSQP.omg[0]))
        dtTwist.omg[0] = 0;

    if (std::abs(dtTwist.omg[1]) <= std::abs(_boundsSQP.omg[1]))
        dtTwist.omg[1] = 0;

    if (std::abs(dtTwist.omg[2]) <= std::abs(_boundsSQP.omg[2]))
        dtTwist.omg[2] = 0;

    error[0] = Vector3DS::dotProduct(dtTwist.v,dtTwist.v) + Vector3DS::dotProduct(dtTwist.omg,dtTwist.omg);

    if(dtTwist.closeToEps(_epsSQP))
    {
        _optStatus = 1;
        _bestXSQP  = x;
        return;
    }

}

int Chain::ikSQPSumSqr(const Frame &desireFrame, VectorXSDS &outJoints, const Twist &bounds, int maxIter, double epsilon, double maxTime)
{
    _stopImmediately = false;//NONFREE
    if(outJoints.mN != _numOfJoints)
    {
        return -2;
    }

    _boundsSQP = bounds;

    _epsSQP    = epsilon;

    _opt.set_maxtime(maxTime);

    double minf; /* the minimum objective value, upon return */

    _desireFrameSQP = desireFrame;

    std::vector<double> x(_numOfJoints);

    for (unsigned int i = 0; i < x.size(); i++)
    {
        x[i] = outJoints[i];

        if (_jointMoveTypes[i] == MOVE_ROT_CONTINUOUS)
            continue;

        if (_jointMoveTypes[i] == MOVE_TRANS)
        {
            x[i] = std::min(x[i], _maxJoints[i]);
            x[i] = std::max(x[i], _minJoints[i]);
        }
        else
        {

            // Below is to handle bad seeds outside of limits

            if (x[i] > _maxJoints[i])
            {
                //Find actual angle offset
                double diffangle = fmod(x[i] - _maxJoints[i], MSNH_2_PI);
                // Add that to upper bound and go back a full rotation
                x[i] = _maxJoints[i] + diffangle - MSNH_2_PI;
            }

            if (x[i] < _minJoints[i])
            {
                //Find actual angle offset
                double diffangle = fmod(_minJoints[i] - x[i], MSNH_2_PI);
                // Subtract that from lower bound and go forward a full rotation
                x[i] = _minJoints[i] - diffangle + MSNH_2_PI;
            }

            if (x[i] > _maxJoints[i])
                x[i] = (_maxJoints[i] + _minJoints[i]) / 2.0;
        }
    }

    _bestXSQP   = x;
    _optStatus  = -3;

    std::vector<double> artificialLowerLimits(_minJoints.size());

    for (unsigned int i = 0; i < _minJoints.size(); i++)
    {
        if (_jointMoveTypes[i] == MOVE_ROT_CONTINUOUS)
        {
            artificialLowerLimits[i] = _bestXSQP[i] - MSNH_2_PI;
        }
        else if (_jointMoveTypes[i] == MOVE_TRANS)
        {
            artificialLowerLimits[i] = _minJoints[i];
        }
        else
        {
            artificialLowerLimits[i] = std::max(_minJoints[i], _bestXSQP[i] - MSNH_2_PI);
        }
    }

    _opt.set_lower_bounds(artificialLowerLimits);

    std::vector<double> artificialUpperLimits(_minJoints.size());

    for (unsigned int i = 0; i < _maxJoints.size(); i++)
    {
        if (_jointMoveTypes[i] == MOVE_ROT_CONTINUOUS)
        {
            artificialUpperLimits[i] = _bestXSQP[i] + MSNH_2_PI;
        }
        else if (_jointMoveTypes[i] == MOVE_TRANS)
        {
            artificialUpperLimits[i] = _maxJoints[i];
        }
        else
        {
            artificialUpperLimits[i] = std::min(_maxJoints[i], _bestXSQP[i] + MSNH_2_PI);
        }
    }

    _opt.set_upper_bounds(artificialUpperLimits);

    //auto st = std::chrono::high_resolution_clock::now();                           //NONFREE
    try
    {
        _opt.optimize(x, minf);
    }
    catch (...)
    {
    }

    if (_optStatus == -1) // Got NaNs
    {
        _optStatus = -3;
    }

    //    auto so = std::chrono::high_resolution_clock::now();
    //    std::cout<<std::chrono::duration <double,std::milli> ((so-st)).count()<< "------Msnhnet" << std::endl;

    int q = 0;

    if (!(_optStatus < 0)) //_stopImmediately ||
    {

        //double time_left;

        for (int z = 0; z < maxIter; ++z)
        {

            q = z;
            if(!(_optStatus < 0)) //!_stopImmediately &&
            {
                break;
            }

            for (unsigned int i = 0; i < x.size(); i++)
            {
                x[i] = fRand(artificialLowerLimits[i], artificialUpperLimits[i]);
            }

            //_opt.set_maxtime(time_left);

            try
            {
                _opt.optimize(x, minf);
            }
            catch (...) {}

            if (_optStatus == -1) // Got NaNs
            {
                _optStatus = -3;
            }

        }
    }

    if(q == (maxIter-1))
    {
        return -1;
    }

    for (unsigned int i = 0; i < x.size(); i++)
    {
        outJoints[i] = _bestXSQP[i];
    }

    return q;
}

int Chain::ikLM(const Frame &desireFrame, VectorXSDS &outJoints,                   //NONFREE
                int maxIter, double epsilon, double epsJoint, double maxTime)       //NONFREE
{                                                                                  //NONFREE
    _stopImmediately = false;                                                      //NONFREE
    //NONFREE
    if(outJoints.mN != _numOfJoints)                                               //NONFREE
    {                                                                              //NONFREE
        return -2;                                                                 //NONFREE
    }                                                                              //NONFREE
    //NONFREE
    Frame frame;                                                                   //NONFREE
    std::vector<Frame> jointRoot(_numOfJoints);                                    //NONFREE
    std::vector<Frame> jointTip(_numOfJoints);                                     //NONFREE
    auto joints          =  outJoints;                                             //NONFREE
    VectorXSDS jointsNew = outJoints;                                              //NONFREE
    //NONFREE
    MatSDS jac(_numOfJoints,6);                                                    //NONFREE
    //NONFREE
    double v    =   2;                                                             //NONFREE
    double rho;                                                                    //NONFREE
    double miu  =   10;                                                            //NONFREE
    double deltaPosNorm = 0;                                                       //NONFREE
    Twist  deltaPos;                                                               //NONFREE
    Twist  deltaPosNew;                                                            //NONFREE
    Twist  L(LinearVelDS(1,1,1), AngularVelDS(0.01,0.01,0.01));                    //NONFREE
    MatSDS orgAii;                                                                 //NONFREE
    MatSDS tmp;                                                                    //NONFREE
    MatSDS diffJoints;                                                             //NONFREE
    MatSDS grad;                                                                   //NONFREE

    double timeM = maxTime * 1000.0;                                               //NONFREE
    double timeNow = 0;                                                            //NONFREE
    auto st = std::chrono::high_resolution_clock::now();                           //NONFREE
    frame = fk(joints, jointRoot, jointTip);                                    //NONFREE

    deltaPos    = Frame::diff(frame, desireFrame);                                 //NONFREE

    deltaPos    = L*deltaPos;                                                      //NONFREE
    deltaPosNorm =  deltaPos.length();                                             //NONFREE


    if(deltaPosNorm < epsilon)                                                         //NONFREE
    {                                                                              //NONFREE
        return 1;                                                                  //NONFREE
    }                                                                              //NONFREE

    jacobi(jac, joints, jointRoot, jointTip, frame);                            //NONFREE
    for (int i = 0; i < joints.mN; ++i)                                         //NONFREE
    {                                                                              //NONFREE
        jac(i,0)=jac(i,0)*L[0];                                                    //NONFREE
        jac(i,1)=jac(i,1)*L[1];                                                    //NONFREE
        jac(i,2)=jac(i,2)*L[2];                                                    //NONFREE
        jac(i,3)=jac(i,3)*L[3];                                                    //NONFREE
        jac(i,4)=jac(i,4)*L[4];                                                    //NONFREE
        jac(i,5)=jac(i,5)*L[5];                                                    //NONFREE
    }                                                                              //NONFREE

    for (int i = 0; i < maxIter; ++i)                                              //NONFREE
    {                                                                              //NONFREE
        auto so = std::chrono::high_resolution_clock::now();                       //NONFREE
        timeNow = std::chrono::duration <double,std::milli> ((so-st)).count();     //NONFREE
        if(timeNow > timeM*1000)                                                        //NONFREE
        {                                                                          //NONFREE
            return -6;                                                             //NONFREE
        }                                                                          //NONFREE

        if(_stopImmediately)                                                       //NONFREE
            return -7;                                                             //NONFREE


        auto UDVT = jac.svd();                                                     //NONFREE

        orgAii = UDVT[1];                                                          //NONFREE
        for (int j = 0; j < orgAii.mHeight; ++j)                                   //NONFREE
        {                                                                          //NONFREE
            orgAii[j] = orgAii[j]/(orgAii[j]*orgAii[j] + miu);                     //NONFREE
        }                                                                          //NONFREE

        if(UDVT[1].mHeight < 6)                                                          //NONFREE
        {                                                                               //NONFREE
            MatSDS mat(6,UDVT[1].mHeight);                                               //NONFREE
            for(int i=0; i<UDVT[1].mHeight; i++)                                         //NONFREE
            {                                                                           //NONFREE
                for(int j=0; j<6; j++)                                                  //NONFREE
                {                                                                       //NONFREE
                    mat(j,i) = UDVT[0](i,j);                                            //NONFREE
                }                                                                       //NONFREE
            }                                                                           //NONFREE
            tmp = mat * deltaPos.toMat();                                               //NONFREE
        }                                                                               //NONFREE
        else                                                                            //NONFREE
        {                                                                               //NONFREE
            tmp = UDVT[0].transpose() * deltaPos.toMat();                               //NONFREE
        }                                                                               //NONFREE


        tmp = MatSDS::eleWiseMul(orgAii,tmp);                                      //NONFREE

        //        auto sst = std::chrono::high_resolution_clock::now();
        //        for (int i = 0; i < 1000; ++i)
        //        {
        //            auto ccc = UDVT[2].transpose();
        //        }
        //        auto sso = std::chrono::high_resolution_clock::now();
        //        std::cout<<std::chrono::duration <double,std::milli> ((sso-sst)).count()/1000<< "------LMA" << std::endl;


        if(UDVT[2].mWidth > 6)                                                          //NONFREE
        {                                                                               //NONFREE
            MatSDS mat(6,UDVT[2].mWidth);                                               //NONFREE
            for(int i=0; i<UDVT[2].mWidth; i++)                                         //NONFREE
            {                                                                           //NONFREE
                for(int j=0; j<6; j++)                                                  //NONFREE
                {                                                                       //NONFREE
                    mat(j,i) = UDVT[2](i,j);                                            //NONFREE
                }                                                                       //NONFREE
            }                                                                           //NONFREE
            diffJoints = mat * tmp;                                                     //NONFREE
        }                                                                               //NONFREE
        else                                                                            //NONFREE
        {                                                                               //NONFREE
            diffJoints = UDVT[2].transpose() * tmp;                                    //NONFREE
        }                                                                              //NONFREE


        grad = jac.transpose()*deltaPos.toMat();                                   //NONFREE



        double dNorm = diffJoints.LInf();                                          //NONFREE

        if(dNorm < epsJoint)                                                       //NONFREE
        {                                                                          //NONFREE
            return -4;                                                             //NONFREE
        }                                                                          //NONFREE

        if((grad.transpose()*grad)(0,0) < epsJoint*epsJoint)                       //NONFREE
        {                                                                          //NONFREE
            return -5;                                                             //NONFREE
        }                                                                          //NONFREE

        for (int i = 0; i < joints.mN; ++i)                                     //NONFREE
        {                                                                          //NONFREE
            jointsNew[i] = joints[i] + diffJoints[i];                           //NONFREE
        }                                                                          //NONFREE
        frame = fk(jointsNew, jointRoot, jointTip);                                 //NONFREE

        deltaPosNew = Frame::diff(frame, desireFrame);                             //NONFREE
        deltaPosNew = L*deltaPosNew;                                               //NONFREE

        double deltaPosNewNorm =  deltaPosNew.length();                            //NONFREE
        //rho = (F(x)-F(x_new))/(L(0) - L(h)) L(0)=f(x)                            //NONFREE
        rho         = deltaPosNorm*deltaPosNorm - deltaPosNewNorm*deltaPosNewNorm; //NONFREE
        rho        /= (diffJoints.transpose()*(miu*diffJoints + grad))(0,0);       //NONFREE

        if(rho > 0)                                                                //NONFREE
        {                                                                          //NONFREE
            joints       = jointsNew;                                           //NONFREE
            deltaPos        = deltaPosNew;                                         //NONFREE
            deltaPosNorm    = deltaPosNewNorm;                                     //NONFREE
            if( deltaPosNorm < epsilon )                                               //NONFREE
            {                                                                      //NONFREE
                outJoints = joints;                                                //NONFREE
                return i;                                                          //NONFREE
            }                                                                      //NONFREE
            jacobi(jac, joints, jointRoot, jointTip, frame);                    //NONFREE
            for (int i = 0; i < joints.mN; ++i)                                 //NONFREE
            {                                                                      //NONFREE
                jac(i,0)=jac(i,0)*L[0];                                            //NONFREE
                jac(i,1)=jac(i,1)*L[1];                                            //NONFREE
                jac(i,2)=jac(i,2)*L[2];                                            //NONFREE
                jac(i,3)=jac(i,3)*L[3];                                            //NONFREE
                jac(i,4)=jac(i,4)*L[4];                                            //NONFREE
                jac(i,5)=jac(i,5)*L[5];                                            //NONFREE
            }                                                                      //NONFREE

            double t = 2*rho - 1;                                                  //NONFREE
            miu = miu*max(1/3.0,1-t*t*t);                                          //NONFREE
            v = 2;                                                                 //NONFREE
        }                                                                          //NONFREE
        else                                                                       //NONFREE
        {                                                                          //NONFREE
            miu = miu * v;                                                         //NONFREE
            v     = 2*v;                                                           //NONFREE
        }                                                                          //NONFREE
    }                                                                              //NONFREE
    return -1;                                                                     //NONFREE
}                                                                                  //NONFREE

void Chain::initIKFast(int maxIter, const Twist &bounds, const bool &randomStart, const bool &wrap, double epsilon, double epsJoint, double maxTime)                       //NONFREE
{                                                                                                                                                                          //NONFREE
    _th1Running = true;                                                                                                                                                    //NONFREE
    _th2Running = true;                                                                                                                                                    //NONFREE
    _th3Running = true;                                                                                                                                                    //NONFREE
    //NONFREE
    _outJoints = VectorXSDS((int)_maxJoints.size());                                                                                                                            //NONFREE
    //NONFREE
    //NONFREE
#ifdef __WIN32__                                                                                                                                                           //NONFREE
    th1 = std::thread(&Chain::runIKNetwon, this, std::ref(maxIter), std::ref(eps), std::ref(maxTime));                                                                     //NONFREE
    th2 = std::thread(&Chain::runIKNetwonRR, this,std::ref(bounds), std::ref(randomStart), std::ref(wrap), std::ref(maxIter), std::ref(eps), std::ref(maxTime));           //NONFREE
    th3 = std::thread(&Chain::runIKLM, this, std::ref(maxIter), std::ref(eps), std::ref(epsJoint),std::ref(maxTime));                                                      //NONFREE
#else                                                                                                                                                                      //NONFREE
    th1 = std::thread(&Chain::runIKNetwon, this, maxIter, epsilon, maxTime);                                                                                                   //NONFREE
    th2 = std::thread(&Chain::runIKNetwonRR, this,bounds, randomStart, wrap, maxIter, epsilon, maxTime);                                                                       //NONFREE
    th3 = std::thread(&Chain::runIKLM, this, maxIter, epsilon/10.0, epsJoint,maxTime);                                                                                              //NONFREE
#endif                                                                                                                                                                     //NONFREE
    _ikFastInited = true;                                                                                                                                                  //NONFREE
}                                                                                                                                                                          //NONFREE

void Chain::stopIKFast()           //NONFREE
{                                   //NONFREE
    _th1Running = false;            //NONFREE
    _th2Running = false;            //NONFREE
    _th3Running = false;            //NONFREE
    th1.join();                     //NONFREE
    th2.join();                     //NONFREE
    th3.join();                     //NONFREE
}                                   //NONFREE

int Chain::ikFast(const Frame &desireFrame, VectorXSDS &outJoints)             //NONFREE
{                                                                              //NONFREE
    _stopImmediately = false;                                                  //NONFREE
    _desireFrame = desireFrame;                                                //NONFREE
    _outJoints   = outJoints;                                                  //NONFREE
    //NONFREE
    _th1Working  = true;                                                       //NONFREE
    _th2Working  = true;                                                       //NONFREE
    _th3Working  = true;                                                       //NONFREE
    _res         = -1;                                                         //NONFREE
    //NONFREE
    while(1)                                                                   //NONFREE
    {                                                                          //NONFREE
        if( (!_th1Working) && (!_th2Working) && (!_th3Working))                //NONFREE
        {                                                                      //NONFREE
            break;                                                             //NONFREE
        }                                                                      //NONFREE
    }                                                                          //NONFREE
    //NONFREE
    outJoints = this->_outJoints;                                              //NONFREE
    return this->_res;                                                         //NONFREE
}                                                                              //NONFREE

void Chain::runIKNetwon(Chain * const &data, int maxIter, double epsilon, double maxTime)     //NONFREE
{                                                                                          //NONFREE
    while(data->_th1Running)                                                               //NONFREE
    {                                                                                      //NONFREE
        if(data->_th1Working)                                                              //NONFREE
        {                                                                                  //NONFREE
            auto joints = data->_outJoints;                                                //NONFREE
            int res = data->ikNewton(data->_desireFrame, joints, maxIter, epsilon, maxTime);   //NONFREE
            if(data->_stopImmediately == false)                                            //NONFREE
            {                                                                              //NONFREE
                data->_outJoints = joints;                                                 //NONFREE
                data->_res  = res;                                                         //NONFREE
                data->_stopImmediately = true;                                             //NONFREE
            }                                                                              //NONFREE
            //NONFREE
            data->_th1Working = false;                                                     //NONFREE
            //NONFREE
        }                                                                                  //NONFREE
    }                                                                                      //NONFREE
    //NONFREE
}                                                                                          //NONFREE

void Chain::runIKNetwonRR(Chain * const &data, const Twist &bounds, const bool &randomStart, const bool &wrap, int maxIter, double epsilon, double maxTime)   //NONFREE
{                                                                                                                                                           //NONFREE
    while(data->_th2Running)                                                                                                                                //NONFREE
    {                                                                                                                                                       //NONFREE
        if(data->_th2Working)                                                                                                                               //NONFREE
        {                                                                                                                                                   //NONFREE
            auto joints = data->_outJoints;                                                                                                                  //NONFREE
            int res = data->ikNewtonRR(data->_desireFrame, joints, bounds, randomStart, wrap, maxIter, epsilon, maxTime);                                       //NONFREE
            if(data->_stopImmediately == false)                                                                                                             //NONFREE
            {                                                                                                                                               //NONFREE
                data->_outJoints = joints;                                                                                                                  //NONFREE
                data->_res  = res;                                                                                                                          //NONFREE
                data->_stopImmediately = true;                                                                                                              //NONFREE
            }                                                                                                                                               //NONFREE
            data->_th2Working = false;                                                                                                                      //NONFREE
        }                                                                                                                                                   //NONFREE
    }                                                                                                                                                       //NONFREE
}                                                                                                                                                           //NONFREE

void Chain::runIKLM(Chain * const &data, int maxIter, double epsilon, double epsJoint, double maxTime)                                                         //NONFREE
{                                                                                                                                                           //NONFREE
    while(data->_th2Running)                                                                                                                                //NONFREE
    {                                                                                                                                                       //NONFREE
        if(data->_th3Working)                                                                                                                               //NONFREE
        {                                                                                                                                                   //NONFREE
            auto joints = data->_outJoints;                                                                                                                 //NONFREE
            int res = data->ikLM(data->_desireFrame, joints, maxIter, epsilon, epsJoint, maxTime);                                                              //NONFREE
            if(data->_stopImmediately == false)                                                                                                             //NONFREE
            {                                                                                                                                               //NONFREE
                data->_outJoints = joints;                                                                                                                  //NONFREE
                data->_res  = res;                                                                                                                          //NONFREE
                data->_stopImmediately = true;                                                                                                              //NONFREE
            }                                                                                                                                               //NONFREE
            data->_th3Working = false;                                                                                                                      //NONFREE
            //NONFREE
        }                                                                                                                                                   //NONFREE
    }                                                                                                                                                       //NONFREE
}

bool Chain::getIkFastInited() const    //NONFREE
{                                      //NONFREE
    return _ikFastInited;              //NONFREE
}                                      //NONFREE

bool Chain::getSQPInited() const
{
    return _optInited;
}                                                                                                                                                       //NONFREE


void Chain::changeRefPoint(MatSDS &src, const Vector3DS &baseAB) const
{
    for (int i = 0; i < src.mHeight; ++i)
    {
        MatSDS col   = src.getCol(i);

        Twist J  = Twist(LinearVelDS(col[0], col[1], col[2]),
                AngularVelDS(col[3], col[4], col[5]));

        src.setCol(i, J.refPoint(baseAB).toMat());
    }

}

std::vector<double> Chain::getMinJoints() const
{
    return _minJoints;
}

std::vector<double> Chain::getMaxJoints() const
{
    return _maxJoints;
}

std::vector<MoveType> Chain::getJointMoveTypes() const
{
    return _jointMoveTypes;
}


}


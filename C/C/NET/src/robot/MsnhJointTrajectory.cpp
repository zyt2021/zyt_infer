﻿#include  <Msnhnet/robot/MsnhJointTrajectory.h>

namespace Msnhnet
{

JointTrajectorySegment::JointTrajectorySegment(JointPath *geom, VelocityProfile *velProfile, bool aggregate)
    :_geom(geom),_velProfile(velProfile),_aggregate(aggregate)
{

}

JointTrajectorySegment::JointTrajectorySegment(JointPath *geom, VelocityProfile *velProfile, double duration, bool aggregate)
    :_geom(geom),_velProfile(velProfile),_aggregate(aggregate)
{
    _velProfile->setProfileDuration(0, geom->getPathLength(), duration);
}

JointTrajectorySegment::~JointTrajectorySegment()
{
    if(_aggregate)
    {
        delete _geom;
        delete _velProfile;
    }
}

double JointTrajectorySegment::getDuration() const
{
    return _velProfile->getDuration();
}

VectorXSDS JointTrajectorySegment::getPos(double time) const
{
    return _geom->getPos(_velProfile->getPos(time));
}

VectorXSDS JointTrajectorySegment::getVel(double time) const
{
    return _geom->getVel(_velProfile->getPos(time), _velProfile->getVel(time));
}

VectorXSDS JointTrajectorySegment::getAcc(double time) const
{
    return _geom->getAcc(_velProfile->getPos(time), _velProfile->getVel(time), _velProfile->getAcc(time));
}

JointTrajectoryComposite::~JointTrajectoryComposite()
{
    auto iter = _trajVec.begin();

    while (iter!=_trajVec.end())
    {
        delete *iter;
        iter++;
    }
}

double JointTrajectoryComposite::getDuration() const
{
    return _duration;
}

VectorXSDS JointTrajectoryComposite::getPos(double time) const
{
    JointTrajectory *traj;
    double prevTime;

    if(time < 0)
    {
        return _trajVec[0]->getPos(0);
    }

    prevTime = 0;

    for(size_t i =0; i<_trajVec.size(); ++i)
    {
        if(time < _dVec[i])
        {
            return _trajVec[i]->getPos(time - prevTime);
        }

        prevTime = _dVec[i];
    }

    traj = _trajVec[_trajVec.size() - 1];

    return traj->getPos(traj->getDuration());
}

VectorXSDS JointTrajectoryComposite::getVel(double time) const
{
    JointTrajectory *traj;
    double prevTime;

    if(time < 0)
    {
        return _trajVec[0]->getVel(0);
    }

    prevTime = 0;

    for(size_t i =0; i<_trajVec.size(); ++i)
    {
        if(time < _dVec[i])
        {
            return _trajVec[i]->getVel(time - prevTime);
        }

        prevTime = _dVec[i];
    }

    traj = _trajVec[_trajVec.size() - 1];

    return traj->getVel(traj->getDuration());
}

VectorXSDS JointTrajectoryComposite::getAcc(double time) const
{
    JointTrajectory *traj;
    double prevTime;

    if(time < 0)
    {
        return _trajVec[0]->getAcc(0);
    }

    prevTime = 0;

    for(size_t i =0; i<_trajVec.size(); ++i)
    {
        if(time < _dVec[i])
        {
            return _trajVec[i]->getAcc(time - prevTime);
        }

        prevTime = _dVec[i];
    }

    traj = _trajVec[_trajVec.size() - 1];

    return traj->getAcc(traj->getDuration());
}

void JointTrajectoryComposite::add(JointTrajectory *traj)
{
    _trajVec.push_back(traj);
    _duration += traj->getDuration();
    _dVec.push_back(_duration);
}

}

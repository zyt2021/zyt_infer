﻿#include <Msnhnet/robot/MsnhCartesianTrajectory.h>

namespace Msnhnet
{

CartesianTrajectorySegment::CartesianTrajectorySegment(CartesianPath *geom, VelocityProfile *velProfile, bool aggregate)
    :_geom(geom),_velProfile(velProfile),_aggregate(aggregate)
{

}

CartesianTrajectorySegment::CartesianTrajectorySegment(CartesianPath *geom, VelocityProfile *velProfile, double duration, bool aggregate)
    :_geom(geom),_velProfile(velProfile),_aggregate(aggregate)
{
    _velProfile->setProfileDuration(0, geom->getPathLength(), duration);
}

CartesianTrajectorySegment::~CartesianTrajectorySegment()
{
    if(_aggregate)
    {
        delete _geom;
        delete _velProfile;
    }
}

double CartesianTrajectorySegment::getDuration() const
{
    return _velProfile->getDuration();
}

Frame CartesianTrajectorySegment::getPos(double time) const
{
    return _geom->getPos(_velProfile->getPos(time));
}

Twist CartesianTrajectorySegment::getVel(double time) const
{
    return _geom->getVel(_velProfile->getPos(time), _velProfile->getVel(time));
}

Twist CartesianTrajectorySegment::getAcc(double time) const
{
    return _geom->getAcc(_velProfile->getPos(time), _velProfile->getVel(time), _velProfile->getAcc(time));
}

CartesianTrajectoryComposite::~CartesianTrajectoryComposite()
{
    auto iter = _trajVec.begin();

    while (iter!=_trajVec.end())
    {
        delete *iter;
        iter++;
    }
}

double CartesianTrajectoryComposite::getDuration() const
{
    return _duration;
}

Frame CartesianTrajectoryComposite::getPos(double time) const
{
    CartesianTrajectory *traj;
    double prevTime;

    if(time < 0)
    {
        return _trajVec[0]->getPos(0);
    }

    prevTime = 0;

    for(size_t i =0; i<_trajVec.size(); ++i)
    {
        if(time < _dVec[i])
        {
            return _trajVec[i]->getPos(time - prevTime);
        }

        prevTime = _dVec[i];
    }

    traj = _trajVec[_trajVec.size() - 1];

    return traj->getPos(traj->getDuration());
}

Twist CartesianTrajectoryComposite::getVel(double time) const
{
    CartesianTrajectory *traj;
    double prevTime;

    if(time < 0)
    {
        return _trajVec[0]->getVel(0);
    }

    prevTime = 0;

    for(size_t i =0; i<_trajVec.size(); ++i)
    {
        if(time < _dVec[i])
        {
            return _trajVec[i]->getVel(time - prevTime);
        }

        prevTime = _dVec[i];
    }

    traj = _trajVec[_trajVec.size() - 1];

    return traj->getVel(traj->getDuration());
}

Twist CartesianTrajectoryComposite::getAcc(double time) const
{
    CartesianTrajectory *traj;
    double prevTime;

    if(time < 0)
    {
        return _trajVec[0]->getAcc(0);
    }

    prevTime = 0;

    for(size_t i =0; i<_trajVec.size(); ++i)
    {
        if(time < _dVec[i])
        {
            return _trajVec[i]->getAcc(time - prevTime);
        }

        prevTime = _dVec[i];
    }

    traj = _trajVec[_trajVec.size() - 1];

    return traj->getAcc(traj->getDuration());
}

void CartesianTrajectoryComposite::add(CartesianTrajectory *traj)
{
    _trajVec.push_back(traj);
    _duration += traj->getDuration();
    _dVec.push_back(_duration);
}

}


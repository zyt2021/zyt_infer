﻿#include <Msnhnet/robot/MsnhSpatialMath.h>


namespace Msnhnet
{

bool SO3D::forceCheckSO3 = true;
bool SO3F::forceCheckSO3 = true;
bool SE3D::forceCheckSE3 = true;
bool SE3F::forceCheckSE3 = true;

SO3D::SO3D(const Mat &mat)
{
    if(mat.getWidth()!=3 || mat.getHeight()!=3 || mat.getChannel()!=1 || mat.getStep()!=8 || mat.getMatType()!= MatType::MAT_GRAY_F64)//类型确定
    {
        throw Exception(1, "[SO3D] mat should be: wxh==3x3 channel==1 step==8 matType==MAT_GRAY_F64", __FILE__, __LINE__,__FUNCTION__);
    }

    if(forceCheckSO3)
    {
        if(!mat.isRotMat())
        {
            throw Exception(1, "[SO3D] not a SO3 mat", __FILE__, __LINE__,__FUNCTION__);
        }
    }

    release();
    this->_channel  = mat.getChannel();
    this->_width    = mat.getWidth();
    this->_height   = mat.getHeight();
    this->_step     = mat.getStep();
    this->_matType  = mat.getMatType();

    if(mat.getBytes()!=nullptr)
    {
        uint8_t *u8Ptr =  new uint8_t[this->_width*this->_height*this->_step]();
        memcpy(u8Ptr, mat.getBytes(), this->_width*this->_height*this->_step);
        this->_data.u8 =u8Ptr;
    }
}

SO3D::SO3D(Mat &&mat)
{
    if(mat.getWidth()!=3 || mat.getHeight()!=3 || mat.getChannel()!=1 || mat.getStep()!=8 || mat.getMatType()!= MatType::MAT_GRAY_F64)//类型确定
    {
        throw Exception(1, "[SO3D] mat should be: wxh==3x3 channel==1 step==8 matType==MAT_GRAY_F64", __FILE__, __LINE__,__FUNCTION__);
    }


    if(forceCheckSO3)
    {
        if(!mat.isRotMat())
        {
            throw Exception(1, "[SO3D] not a SO3 mat", __FILE__, __LINE__,__FUNCTION__);
        }
    }

    release();
    this->_channel  = mat.getChannel();
    this->_width    = mat.getWidth();
    this->_height   = mat.getHeight();
    this->_step     = mat.getStep();
    this->_matType  = mat.getMatType();
    this->_data.u8  = mat.getBytes();
    mat.setDataNull();
}

SO3D::SO3D(const SO3D &mat)
{
    release();
    this->_channel  = mat.getChannel();
    this->_width    = mat.getWidth();
    this->_height   = mat.getHeight();
    this->_step     = mat.getStep();
    this->_matType  = mat.getMatType();

    if(mat.getBytes()!=nullptr)
    {
        uint8_t *u8Ptr =  new uint8_t[this->_width*this->_height*this->_step]();
        memcpy(u8Ptr, mat.getBytes(), this->_width*this->_height*this->_step);
        this->_data.u8 =u8Ptr;
    }
}

SO3D::SO3D(SO3D &&mat)
{
    release();
    this->_channel  = mat.getChannel();
    this->_width    = mat.getWidth();
    this->_height   = mat.getHeight();
    this->_step     = mat.getStep();
    this->_matType  = mat.getMatType();
    this->_data.u8  = mat.getBytes();
    mat.setDataNull();
}

SO3D &SO3D::operator=(const Mat &mat)
{
    if(mat.getWidth()!=3 || mat.getHeight()!=3 || mat.getChannel()!=1 || mat.getStep()!=8 || mat.getMatType()!= MatType::MAT_GRAY_F64)//类型确定
    {
        throw Exception(1, "[SO3D] mat should be: wxh==3x3 channel==1 step==8 matType==MAT_GRAY_F64", __FILE__, __LINE__,__FUNCTION__);
    }

    if(forceCheckSO3)
    {
        if(!mat.isRotMat())
        {
            throw Exception(1, "[SO3D] not a SO3 mat", __FILE__, __LINE__,__FUNCTION__);
        }
    }

    if(this!=&mat)
    {
        release();
        this->_channel  = mat.getChannel();
        this->_width    = mat.getWidth();
        this->_height   = mat.getHeight();
        this->_step     = mat.getStep();
        this->_matType  = mat.getMatType();

        if(mat.getBytes()!=nullptr)
        {
            uint8_t *u8Ptr =  new uint8_t[this->_width*this->_height*this->_step]();
            memcpy(u8Ptr, mat.getBytes(), this->_width*this->_height*this->_step);
            this->_data.u8 =u8Ptr;
        }
    }
    return *this;
}

SO3D &SO3D::operator=(Mat&& mat)
{
    if(mat.getWidth()!=3 || mat.getHeight()!=3 || mat.getChannel()!=1 || mat.getStep()!=8 || mat.getMatType()!= MatType::MAT_GRAY_F64)//类型确定
    {
        throw Exception(1, "[SO3D] mat should be: wxh==3x3 channel==1 step==8 matType==MAT_GRAY_F64", __FILE__, __LINE__,__FUNCTION__);
    }

    if(forceCheckSO3)
    {
        if(!mat.isRotMat())
        {
            throw Exception(1, "[SO3D] not a SO3 mat", __FILE__, __LINE__,__FUNCTION__);
        }
    }

    if(this!=&mat)
    {
        release();
        this->_channel  = mat.getChannel();
        this->_width    = mat.getWidth();
        this->_height   = mat.getHeight();
        this->_step     = mat.getStep();
        this->_matType  = mat.getMatType();
        this->_data.u8  = mat.getBytes();
        mat.setDataNull();
    }
    return *this;
}

SO3D &SO3D::operator=(const SO3D &mat)
{
    if(this!=&mat)
    {
        release();
        this->_channel  = mat._channel;
        this->_width    = mat._width;
        this->_height   = mat._height;
        this->_step     = mat._step;
        this->_matType  = mat._matType;

        if(mat._data.u8!=nullptr)
        {
            uint8_t *u8Ptr =  new uint8_t[this->_width*this->_height*this->_step]();
            memcpy(u8Ptr, mat._data.u8, this->_width*this->_height*this->_step);
            this->_data.u8 =u8Ptr;
        }
    }
    return *this;
}

SO3D &SO3D::operator=(SO3D&& mat)
{
    if(this!=&mat)
    {
        release();
        this->_channel  = mat._channel;
        this->_width    = mat._width;
        this->_height   = mat._height;
        this->_step     = mat._step;
        this->_matType  = mat._matType;
        this->_data.u8  = mat.getBytes();
        mat.setDataNull();
    }
    return *this;
}

RotationMatD &SO3D::toRotMat()
{
    return *this;
}

QuaternionD SO3D::toQuaternion()
{
    return Geometry::rotMat2Quaternion(*this);
}

EulerD SO3D::toEuler(const RotSequence &rotSeq)
{
    return Geometry::rotMat2Euler(*this,rotSeq);
}

RotationVecD SO3D::toRotVector()
{
    return Geometry::rotMat2RotVec(*this);
}

bool SO3D::isSO3(const Mat &mat)
{
    return mat.isRotMat();
}

SO3D SO3D::adjoint()
{
    return *this;
}

SO3D SO3D::rotX(double angleInRad)
{
    return  Geometry::rotX(angleInRad);
}

SO3D SO3D::rotY(double angleInRad)
{
    return  Geometry::rotY(angleInRad);
}

SO3D SO3D::rotZ(double angleInRad)
{
    return  Geometry::rotZ(angleInRad);
}

SO3D SO3D::fromRotMat(const RotationMatD &rotMat)
{
    return rotMat;
}

SO3D SO3D::fromQuaternion(const QuaternionD &quat)
{
    return Geometry::quaternion2RotMat(quat);
}

SO3D SO3D::fromEuler(const EulerD &euler, const RotSequence &rotSeq)
{
    return Geometry::euler2RotMat(euler,rotSeq);
}

SO3D SO3D::fromRotVec(const RotationVecD &rotVec)
{
    return Geometry::rotVec2RotMat(rotVec);
}

SO3D SO3D::fastInvert()
{
    return this->transpose();
}

Matrix3x3D SO3D::wedge(const Vector3D &omg, bool needCalUnit)
{
    Vector3D tmp = omg;

    if(needCalUnit)
    {
        if(closeToZeroD(tmp.length()))
        {
            return Mat(3,3,MAT_GRAY_F64);
        }

        tmp = tmp / tmp.length();
    }

    Matrix3x3D mat3x3;
    mat3x3.setVal({
                      0,  -tmp[2],   tmp[1],
                      tmp[2],       0 ,  -tmp[0],
                      -tmp[1],   tmp[0],    0
                  });
    return mat3x3;
}

Vector3D SO3D::vee(const Matrix3x3D &mat3x3, bool needCalUnit)
{
    Vector3D omg;
    omg[0] = 0.5*(mat3x3.getValAtRowCol(2,1) - mat3x3.getValAtRowCol(1,2));
    omg[1] = 0.5*(mat3x3.getValAtRowCol(0,2) - mat3x3.getValAtRowCol(2,0));
    omg[2] = 0.5*(mat3x3.getValAtRowCol(1,0) - mat3x3.getValAtRowCol(0,1));

    if(needCalUnit)
    {
        if(closeToZeroD(omg.length()))
        {
            return Vector3D({0,0,0});
        }

        omg = omg / omg.length();
    }

    return omg;
}

SO3D SO3D::exp(const Vector3D &omg)
{
    double angle = omg.length();

    if(closeToZeroD(angle))//趋近0
    {
        return Mat::eye(3,MAT_GRAY_F64) + wedge(omg);
    }

    Vector3D axis = omg/angle;

    double s = sin(angle);
    double c = cos(angle);

    Matrix3x3D skew = wedge(axis);

    return Mat::eye(3,MAT_GRAY_F64) + s*skew + (1-c)*skew*skew;

}

SO3D SO3D::exp(const Vector3D &omg, double theta)
{
    //不为单位向量                         且         不为0
    if(!closeToZeroD(omg.length()-1) && !closeToZeroD(omg.length()))
    {
        throw Exception(1, "[SO3D] given theta, OMG must be a unit vector", __FILE__, __LINE__,__FUNCTION__);
    }

    return SO3D::exp(omg*theta);
}

Vector3D SO3D::log()
{
    if(*this == Mat::eye(3,MAT_GRAY_F64))
    {
        return Vector3D({0,0,0});
    }
    else if(closeToZeroD(this->trace()+1))
    {
        double m00 = this->getValAtRowCol(0,0);
        double m01 = this->getValAtRowCol(0,1);
        double m02 = this->getValAtRowCol(0,2);

        double m10 = this->getValAtRowCol(1,0);
        double m11 = this->getValAtRowCol(1,1);
        double m12 = this->getValAtRowCol(1,2);

        double m20 = this->getValAtRowCol(2,0);
        double m21 = this->getValAtRowCol(2,1);
        double m22 = this->getValAtRowCol(2,2);

        if(m00>m11 && m00>m22)
        {
            return 1/(sqrt(1+m00))*Vector3D({m00+1, m10, m20});
        }
        else if(m11>m00 && m11>m22)
        {
            return 1/(sqrt(1+m11))*Vector3D({m01+1, m11, m21});
        }
        else if(m22>m00 && m22>m11)
        {
            return 1/(sqrt(1+m22))*Vector3D({m02+1, m12, m22});
        }
    }
    else
    {
        double theta = acos(0.5*(this->trace()-1));
        return SO3D::vee(1.0/(2*sin(theta))*(*this - (*this).transpose()))*theta;
    }
}


SO3F::SO3F(const Mat &mat)
{
    if(mat.getWidth()!=3 || mat.getHeight()!=3 || mat.getChannel()!=1 || mat.getStep()!=4 || mat.getMatType()!= MatType::MAT_GRAY_F32)//类型确定
    {
        throw Exception(1, "[SO3F] mat should be: wxh==3x3 channel==1 step==4 matType==MAT_GRAY_F32", __FILE__, __LINE__,__FUNCTION__);
    }


    if(forceCheckSO3)
    {
        if(!mat.isRotMat())
        {
            throw Exception(1, "[SO3F] not a SO3 mat", __FILE__, __LINE__,__FUNCTION__);
        }
    }

    release();
    this->_channel  = mat.getChannel();
    this->_width    = mat.getWidth();
    this->_height   = mat.getHeight();
    this->_step     = mat.getStep();
    this->_matType  = mat.getMatType();

    if(mat.getBytes()!=nullptr)
    {
        uint8_t *u8Ptr =  new uint8_t[this->_width*this->_height*this->_step]();
        memcpy(u8Ptr, mat.getBytes(), this->_width*this->_height*this->_step);
        this->_data.u8 =u8Ptr;
    }
}

SO3F::SO3F(Mat &&mat)
{
    if(mat.getWidth()!=3 || mat.getHeight()!=3 || mat.getChannel()!=1 || mat.getStep()!=4 || mat.getMatType()!= MatType::MAT_GRAY_F32)//类型确定
    {
        throw Exception(1, "[SO3F] mat should be: wxh==3x3 channel==1 step==4 matType==MAT_GRAY_F32", __FILE__, __LINE__,__FUNCTION__);
    }


    if(forceCheckSO3)
    {
        if(!mat.isRotMat())
        {
            throw Exception(1, "[SO3F] not a SO3 mat", __FILE__, __LINE__,__FUNCTION__);
        }
    }

    release();
    this->_channel  = mat.getChannel();
    this->_width    = mat.getWidth();
    this->_height   = mat.getHeight();
    this->_step     = mat.getStep();
    this->_matType  = mat.getMatType();
    this->_data.u8  = mat.getBytes();
    mat.setDataNull();
}

SO3F::SO3F(const SO3F &mat)
{
    release();
    this->_channel  = mat.getChannel();
    this->_width    = mat.getWidth();
    this->_height   = mat.getHeight();
    this->_step     = mat.getStep();
    this->_matType  = mat.getMatType();

    if(mat.getBytes()!=nullptr)
    {
        uint8_t *u8Ptr =  new uint8_t[this->_width*this->_height*this->_step]();
        memcpy(u8Ptr, mat.getBytes(), this->_width*this->_height*this->_step);
        this->_data.u8 =u8Ptr;
    }
}

SO3F::SO3F(SO3F &&mat)
{
    release();
    this->_channel  = mat.getChannel();
    this->_width    = mat.getWidth();
    this->_height   = mat.getHeight();
    this->_step     = mat.getStep();
    this->_matType  = mat.getMatType();
    this->_data.u8  = mat.getBytes();
    mat.setDataNull();
}

SO3F &SO3F::operator=(const Mat &mat)
{
    if(mat.getWidth()!=3 || mat.getHeight()!=3 || mat.getChannel()!=1 || mat.getStep()!=4 || mat.getMatType()!= MatType::MAT_GRAY_F32)//类型确定
    {
        throw Exception(1, "[SO3F] mat should be: wxh==3x3 channel==1 step==4 matType==MAT_GRAY_F32", __FILE__, __LINE__,__FUNCTION__);
    }

    if(forceCheckSO3)
    {
        if(!mat.isRotMat())
        {
            throw Exception(1, "[SO3F] not a SO3 mat", __FILE__, __LINE__,__FUNCTION__);
        }
    }

    if(this!=&mat)
    {
        release();
        this->_channel  = mat.getChannel();
        this->_width    = mat.getWidth();
        this->_height   = mat.getHeight();
        this->_step     = mat.getStep();
        this->_matType  = mat.getMatType();

        if(mat.getBytes()!=nullptr)
        {
            uint8_t *u8Ptr =  new uint8_t[this->_width*this->_height*this->_step]();
            memcpy(u8Ptr, mat.getBytes(), this->_width*this->_height*this->_step);
            this->_data.u8 =u8Ptr;
        }
    }
    return *this;
}

SO3F &SO3F::operator=(Mat&& mat)
{
    if(mat.getWidth()!=3 || mat.getHeight()!=3 || mat.getChannel()!=1 || mat.getStep()!=4 || mat.getMatType()!= MatType::MAT_GRAY_F32)//类型确定
    {
        throw Exception(1, "[SO3F] mat should be: wxh==3x3 channel==1 step==4 matType==MAT_GRAY_F32", __FILE__, __LINE__,__FUNCTION__);
    }

    if(forceCheckSO3)
    {
        if(!mat.isRotMat())
        {
            throw Exception(1, "[SO3F] not a SO3 mat", __FILE__, __LINE__,__FUNCTION__);
        }
    }

    if(this!=&mat)
    {
        release();
        this->_channel  = mat.getChannel();
        this->_width    = mat.getWidth();
        this->_height   = mat.getHeight();
        this->_step     = mat.getStep();
        this->_matType  = mat.getMatType();
        this->_data.u8  = mat.getBytes();
        mat.setDataNull();
    }
    return *this;
}

SO3F &SO3F::operator=(const SO3F &mat)
{
    if(this!=&mat)
    {
        release();
        this->_channel  = mat._channel;
        this->_width    = mat._width;
        this->_height   = mat._height;
        this->_step     = mat._step;
        this->_matType  = mat._matType;

        if(mat._data.u8!=nullptr)
        {
            uint8_t *u8Ptr =  new uint8_t[this->_width*this->_height*this->_step]();
            memcpy(u8Ptr, mat._data.u8, this->_width*this->_height*this->_step);
            this->_data.u8 =u8Ptr;
        }
    }
    return *this;
}

SO3F &SO3F::operator=(SO3F&& mat)
{
    if(this!=&mat)
    {
        release();
        this->_channel  = mat._channel;
        this->_width    = mat._width;
        this->_height   = mat._height;
        this->_step     = mat._step;
        this->_matType  = mat._matType;
        this->_data.u8  = mat.getBytes();
        mat.setDataNull();
    }
    return *this;
}

RotationMatF &SO3F::toRotMat()
{
    return *this;
}

QuaternionF SO3F::toQuaternion()
{
    return Geometry::rotMat2Quaternion(*this);
}

EulerF SO3F::toEuler(const RotSequence &rotSeq)
{
    return Geometry::rotMat2Euler(*this,rotSeq);
}

RotationVecF SO3F::toRotVector()
{
    return Geometry::rotMat2RotVec(*this);
}

bool SO3F::isSO3(const Mat &mat)
{
    return mat.isRotMat();
}

SO3F SO3F::adjoint()
{
    return *this;
}

SO3F SO3F::rotX(float angleInRad)
{
    return  Geometry::rotX(angleInRad);
}

SO3F SO3F::rotY(float angleInRad)
{
    return  Geometry::rotY(angleInRad);
}

SO3F SO3F::rotZ(float angleInRad)
{
    return  Geometry::rotZ(angleInRad);
}

SO3F SO3F::fromRotMat(const RotationMatF &rotMat)
{
    return rotMat;
}

SO3F SO3F::fromQuaternion(const QuaternionF &quat)
{
    return Geometry::quaternion2RotMat(quat);
}

SO3F SO3F::fromEuler(const EulerF &euler, const RotSequence &rotSeq)
{
    return Geometry::euler2RotMat(euler,rotSeq);
}

SO3F SO3F::fromRotVec(const RotationVecF &rotVec)
{
    return Geometry::rotVec2RotMat(rotVec);
}

SO3F SO3F::fastInvert()
{
    return this->transpose();
}

Matrix3x3F SO3F::wedge(const Vector3F &omg, bool needCalUnit)
{

    Vector3F tmp = omg;

    if(needCalUnit)
    {
        if(closeToZeroF((float)tmp.length()))
        {
            return Mat(3,3,MAT_GRAY_F32);
        }

        tmp = tmp / (float)tmp.length();
    }

    Matrix3x3F mat3x3;
    mat3x3.setVal({
                      0,   -tmp[2],    tmp[1],
                      tmp[2],          0,    -tmp[0],
                      -tmp[1],   tmp[0],    0
                  });
    return mat3x3;
}

Vector3F SO3F::vee(const Matrix3x3F &mat3x3, bool needCalUnit)
{
    Vector3F omg;
    omg[0] = 0.5f*(mat3x3.getValAtRowCol(2,1) - mat3x3.getValAtRowCol(1,2));
    omg[1] = 0.5f*(mat3x3.getValAtRowCol(0,2) - mat3x3.getValAtRowCol(2,0));
    omg[2] = 0.5f*(mat3x3.getValAtRowCol(1,0) - mat3x3.getValAtRowCol(0,1));

    if(needCalUnit)
    {
        if(closeToZeroF((float)omg.length()))
        {
            return Vector3F({0,0,0});
        }

        omg = omg / (float)omg.length();
    }
    return omg;
}

SO3F SO3F::exp(const Vector3F &omg)
{
    float angle = (float)omg.length();

    if(closeToZeroF(angle))//趋近0
    {
        return Mat::eye(3,MAT_GRAY_F32) + wedge(omg);
    }

    Vector3F axis = omg/angle;

    float s = sinf(angle);
    float c = cosf(angle);

    Matrix3x3F skew = wedge(axis);

    return Mat::eye(3,MAT_GRAY_F32) + s*skew + (1-c)*skew*skew;

}

SO3F SO3F::exp(const Vector3F &omg, float theta)
{
    //不为单位向量                         且         不为0
    if(!closeToZeroF((float)omg.length()-1) && !closeToZeroF((float)omg.length()))
    {
        throw Exception(1, "[SO3F] given theta, OMG must be a unit vector", __FILE__, __LINE__,__FUNCTION__);
    }
    return SO3F::exp(omg*theta);
}

Vector3F SO3F::log()
{
    if(*this == Mat::eye(3,MAT_GRAY_F32))
    {
        return Vector3F({0,0,0});
    }
    else if(closeToZeroF((float)this->trace()+1))
    {
        float m00 = this->getValAtRowCol(0,0);
        float m01 = this->getValAtRowCol(0,1);
        float m02 = this->getValAtRowCol(0,2);

        float m10 = this->getValAtRowCol(1,0);
        float m11 = this->getValAtRowCol(1,1);
        float m12 = this->getValAtRowCol(1,2);

        float m20 = this->getValAtRowCol(2,0);
        float m21 = this->getValAtRowCol(2,1);
        float m22 = this->getValAtRowCol(2,2);

        if(m00>m11 && m00>m22)
        {
            return 1/(sqrtf(1+m00))*Vector3F({m00+1, m10, m20});
        }
        else if(m11>m00 && m11>m22)
        {
            return 1/(sqrtf(1+m11))*Vector3F({m01+1, m11, m21});
        }
        else if(m22>m00 && m22>m11)
        {
            return 1/(sqrtf(1+m22))*Vector3F({m02+1, m12, m22});
        }
    }
    else
    {
        float theta = acosf(0.5f*((float)this->trace()-1));
        return SO3F::vee(1/(2*sin(theta))*(*this - (*this).transpose()))*theta;
    }
}


SE3D::SE3D(const Mat &mat)
{
    if(mat.getWidth()!=4 || mat.getHeight()!=4 || mat.getChannel()!=1 || mat.getStep()!=8 || mat.getMatType()!= MatType::MAT_GRAY_F64)//类型确定
    {
        throw Exception(1, "[SE3D] mat should be: wxh==4x4 channel==1 step==8 matType==MAT_GRAY_F64", __FILE__, __LINE__,__FUNCTION__);
    }


    if(forceCheckSE3)
    {
        if(!mat.isHomTransMatrix())
        {
            throw Exception(1, "[SE3D] not a SE3 mat", __FILE__, __LINE__,__FUNCTION__);
        }
    }

    release();
    this->_channel  = mat.getChannel();
    this->_width    = mat.getWidth();
    this->_height   = mat.getHeight();
    this->_step     = mat.getStep();
    this->_matType  = mat.getMatType();

    if(mat.getBytes()!=nullptr)
    {
        uint8_t *u8Ptr =  new uint8_t[this->_width*this->_height*this->_step]();
        memcpy(u8Ptr, mat.getBytes(), this->_width*this->_height*this->_step);
        this->_data.u8 =u8Ptr;
    }
}

SE3D::SE3D(Mat &&mat)
{
    if(mat.getWidth()!=4 || mat.getHeight()!=4 || mat.getChannel()!=1 || mat.getStep()!=8 || mat.getMatType()!= MatType::MAT_GRAY_F64)//类型确定
    {
        throw Exception(1, "[SE3D] mat should be: wxh==4x4 channel==1 step==8 matType==MAT_GRAY_F64", __FILE__, __LINE__,__FUNCTION__);
    }


    if(forceCheckSE3)
    {
        if(!mat.isHomTransMatrix())
        {
            throw Exception(1, "[SE3D] not a SE3 mat", __FILE__, __LINE__,__FUNCTION__);
        }
    }

    release();
    this->_channel  = mat.getChannel();
    this->_width    = mat.getWidth();
    this->_height   = mat.getHeight();
    this->_step     = mat.getStep();
    this->_matType  = mat.getMatType();
    this->_data.u8  = mat.getBytes();
    mat.setDataNull();
}

SE3D::SE3D(const SE3D &mat)
{
    release();
    this->_channel  = mat.getChannel();
    this->_width    = mat.getWidth();
    this->_height   = mat.getHeight();
    this->_step     = mat.getStep();
    this->_matType  = mat.getMatType();

    if(mat.getBytes()!=nullptr)
    {
        uint8_t *u8Ptr =  new uint8_t[this->_width*this->_height*this->_step]();
        memcpy(u8Ptr, mat.getBytes(), this->_width*this->_height*this->_step);
        this->_data.u8 =u8Ptr;
    }
}

SE3D::SE3D(SE3D &&mat)
{
    release();
    this->_channel  = mat.getChannel();
    this->_width    = mat.getWidth();
    this->_height   = mat.getHeight();
    this->_step     = mat.getStep();
    this->_matType  = mat.getMatType();
    this->_data.u8  = mat.getBytes();
    mat.setDataNull();
}

SE3D &SE3D::operator=(const Mat &mat)
{
    if(mat.getWidth()!=4 || mat.getHeight()!=4 || mat.getChannel()!=1 || mat.getStep()!=8 || mat.getMatType()!= MatType::MAT_GRAY_F64)//类型确定
    {
        throw Exception(1, "[SE3D] mat should be: wxh==4x4 channel==1 step==8 matType==MAT_GRAY_F64", __FILE__, __LINE__,__FUNCTION__);
    }

    if(forceCheckSE3)
    {
        if(!mat.isHomTransMatrix())
        {
            throw Exception(1, "[SE3D] not a SE3 mat", __FILE__, __LINE__,__FUNCTION__);
        }
    }

    if(this!=&mat)
    {
        release();
        this->_channel  = mat.getChannel();
        this->_width    = mat.getWidth();
        this->_height   = mat.getHeight();
        this->_step     = mat.getStep();
        this->_matType  = mat.getMatType();

        if(mat.getBytes()!=nullptr)
        {
            uint8_t *u8Ptr =  new uint8_t[this->_width*this->_height*this->_step]();
            memcpy(u8Ptr, mat.getBytes(), this->_width*this->_height*this->_step);
            this->_data.u8 =u8Ptr;
        }
    }
    return *this;
}

SE3D &SE3D::operator=(Mat&& mat)
{
    if(mat.getWidth()!=4 || mat.getHeight()!=4 || mat.getChannel()!=1 || mat.getStep()!=8 || mat.getMatType()!= MatType::MAT_GRAY_F64)//类型确定
    {
        throw Exception(1, "[SE3D] mat should be: wxh==4x4 channel==1 step==8 matType==MAT_GRAY_F64", __FILE__, __LINE__,__FUNCTION__);
    }

    if(forceCheckSE3)
    {
        if(!mat.isHomTransMatrix())
        {
            throw Exception(1, "[SE3D] not a SE3 mat", __FILE__, __LINE__,__FUNCTION__);
        }
    }

    if(this!=&mat)
    {
        release();
        this->_channel  = mat.getChannel();
        this->_width    = mat.getWidth();
        this->_height   = mat.getHeight();
        this->_step     = mat.getStep();
        this->_matType  = mat.getMatType();
        this->_data.u8  = mat.getBytes();
        mat.setDataNull();
    }
    return *this;
}

SE3D &SE3D::operator=(const SE3D &mat)
{
    if(this!=&mat)
    {
        release();
        this->_channel  = mat._channel;
        this->_width    = mat._width;
        this->_height   = mat._height;
        this->_step     = mat._step;
        this->_matType  = mat._matType;

        if(mat._data.u8!=nullptr)
        {
            uint8_t *u8Ptr =  new uint8_t[this->_width*this->_height*this->_step]();
            memcpy(u8Ptr, mat._data.u8, this->_width*this->_height*this->_step);
            this->_data.u8 =u8Ptr;
        }
    }
    return *this;
}

SE3D &SE3D::operator=(SE3D&& mat)
{
    if(this!=&mat)
    {
        release();
        this->_channel  = mat._channel;
        this->_width    = mat._width;
        this->_height   = mat._height;
        this->_step     = mat._step;
        this->_matType  = mat._matType;
        this->_data.u8  = mat.getBytes();
        mat.setDataNull();
    }
    return *this;
}

SE3D::SE3D(const SO3D &rotMat, const Vector3D &trans)
{
    setRotationMat(rotMat);
    setTranslation(trans);
}


Matrix4x4D &SE3D::toMatrix4x4()
{
    return *this;
}

Mat SE3D::adjoint()
{
    RotationMatD R = getRotationMat();
    TranslationD p = getTranslation();

    Mat rightUp = SO3D::wedge(p)*R;
    Mat zeros3x3= Mat(3,3,MAT_GRAY_F64);

    Mat up      = MatOp::vContact(R,rightUp);
    Mat down    = MatOp::vContact(zeros3x3,R);

    return MatOp::hContact(up,down);
}

SE3D SE3D::fastInvert()
{
    RotationMatD rotMat = getRotationMat();
    TranslationD trans  = getTranslation();
    trans = trans * -1;

    rotMat = rotMat.transpose();
    trans = rotMat.mulVec(trans);

    return  SE3D(rotMat,trans);
}

Matrix4x4D SE3D::wedge(const ScrewD &screw, bool needCalUnit)
{
    Matrix4x4D mat = Mat(4,4,MAT_GRAY_F64);

    if(closeToZeroD(screw.w.length()))
    {
        if(needCalUnit)
        {
            if(closeToZeroD(screw.v.length()))
            {
                return mat;
            }

            mat.setTranslation(screw.v/screw.v.length());
            return mat;
        }
    }

    if(needCalUnit)
    {
        Vector3D omg   = screw.w / screw.w.length();
        Matrix3x3D so3 = SO3D::wedge(omg);

        mat.setRotationMat(so3);
        mat.setTranslation(screw.v);
        return mat;
    }

    Matrix3x3D so3     = SO3D::wedge(screw.w);
    mat.setRotationMat(so3);
    mat.setTranslation(screw.v);
    return mat;
}

ScrewD SE3D::vee(const Matrix4x4D &wed, bool needCalUnit)
{
    Vector3D w = SO3D::vee(wed.getRotationMat());
    Vector3D v = wed.getTranslation();

    if(needCalUnit)
    {
        if(closeToZeroD(w.length()))
        {
            return ScrewD(v/v.length(),Vector3D({0,0,0}));
        }
        else
        {
            return ScrewD(v,w/w.length());
        }
    }

    return ScrewD(v,w);
}

ScrewD SE3D::log()
{

    if((*this) == Mat::eye(4,MAT_GRAY_F64))
    {
        return ScrewD();
    }

    SO3D R         = getRotationMat();
    TranslationD p = getTranslation();

    if(R == Mat::eye(3,MAT_GRAY_F64))
    {          //     v    w
        return ScrewD(p, Vector3D({0,0,0}));
    }

    Vector3D w        = R.log();

    Matrix3x3D wWedge = SO3D::wedge(w);

    double theta      = w.length();

    Matrix3x3D GInv   = Mat::eye(3,MAT_GRAY_F64) - 0.5*wWedge + wWedge*wWedge*(1/theta - 0.5*1/tan(0.5*theta))/theta;

    Vector3D v        = GInv.mulVec(p);

    return ScrewD(v,w);
}

SE3D SE3D::exp(const ScrewD &screw, double theta)
{
    if(std::abs(screw.w.length()-1)>MSNH_F64_EPS && std::abs(screw.w.length())>MSNH_F64_EPS)
    {
        throw Exception(1, "[SE3D] given theta, OMG must be a unit vector ", __FILE__, __LINE__,__FUNCTION__);
    }

    return SE3D::exp(ScrewD(screw.v, screw.w*theta));
}

SE3D SE3D::exp(const ScrewD &screw)
{
    Vector3D v   = screw.v;
    Vector3D omg = screw.w;

    if(closeToZeroD(omg.length()))
    {
        SE3D se3;
        se3.setTranslation(v);
        return se3;
    }
    else
    {
        double theta  = omg.length();

        SO3D so3      = SO3D::exp(omg);

        Vector3D axis = omg/theta;
        v             = v/theta;

        Matrix3x3D wedge = SO3D::wedge(axis);

        Matrix3x3D G    = Mat::eye(3,MAT_GRAY_F64)*theta + (1-cos(theta))*wedge + wedge*wedge*(theta - sin(theta));
        Vector3D p      = G.mulVec(v);

        SE3D se3;
        se3.setRotationMat(so3);
        se3.setTranslation(p);
        return se3;
    }
}

bool SE3D::isSE3(const Mat &mat)
{
    return mat.isHomTransMatrix();
}

SE3F::SE3F(const Mat &mat)
{
    if(mat.getWidth()!=4 || mat.getHeight()!=4 || mat.getChannel()!=1 || mat.getStep()!=4 || mat.getMatType()!= MatType::MAT_GRAY_F32)

    {
        throw Exception(1, "[SE3F] mat should be: wxh==4x4 channel==1 step==4 matType==MAT_GRAY_F32", __FILE__, __LINE__,__FUNCTION__);
    }

    if(forceCheckSE3)
    {
        if(!mat.isHomTransMatrix())
        {
            throw Exception(1, "[SE3F] not a SE3 mat", __FILE__, __LINE__,__FUNCTION__);
        }
    }

    release();
    this->_channel  = mat.getChannel();
    this->_width    = mat.getWidth();
    this->_height   = mat.getHeight();
    this->_step     = mat.getStep();
    this->_matType  = mat.getMatType();

    if(mat.getBytes()!=nullptr)
    {
        uint8_t *u8Ptr =  new uint8_t[this->_width*this->_height*this->_step]();
        memcpy(u8Ptr, mat.getBytes(), this->_width*this->_height*this->_step);
        this->_data.u8 =u8Ptr;
    }
}

SE3F::SE3F(Mat &&mat)
{
    if(mat.getWidth()!=4 || mat.getHeight()!=4 || mat.getChannel()!=1 || mat.getStep()!=4 || mat.getMatType()!= MatType::MAT_GRAY_F32)

    {
        throw Exception(1, "[SE3F] mat should be: wxh==4x4 channel==1 step==4 matType==MAT_GRAY_F32", __FILE__, __LINE__,__FUNCTION__);
    }

    if(forceCheckSE3)
    {
        if(!mat.isHomTransMatrix())
        {
            throw Exception(1, "[SE3F] not a SE3 mat", __FILE__, __LINE__,__FUNCTION__);
        }
    }

    release();
    this->_channel  = mat.getChannel();
    this->_width    = mat.getWidth();
    this->_height   = mat.getHeight();
    this->_step     = mat.getStep();
    this->_matType  = mat.getMatType();
    this->_data.u8  = mat.getBytes();
    mat.setDataNull();
}

SE3F::SE3F(const SE3F &mat)
{
    release();
    this->_channel  = mat.getChannel();
    this->_width    = mat.getWidth();
    this->_height   = mat.getHeight();
    this->_step     = mat.getStep();
    this->_matType  = mat.getMatType();

    if(mat.getBytes()!=nullptr)
    {
        uint8_t *u8Ptr =  new uint8_t[this->_width*this->_height*this->_step]();
        memcpy(u8Ptr, mat.getBytes(), this->_width*this->_height*this->_step);
        this->_data.u8 =u8Ptr;
    }
}

SE3F::SE3F(SE3F &&mat)
{
    release();
    this->_channel  = mat.getChannel();
    this->_width    = mat.getWidth();
    this->_height   = mat.getHeight();
    this->_step     = mat.getStep();
    this->_matType  = mat.getMatType();
    this->_data.u8  = mat.getBytes();
    mat.setDataNull();
}

SE3F &SE3F::operator=(const Mat &mat)
{
    if(mat.getWidth()!=4 || mat.getHeight()!=4 || mat.getChannel()!=1 || mat.getStep()!=4 || mat.getMatType()!= MatType::MAT_GRAY_F32)

    {
        throw Exception(1, "[SE3F] mat should be: wxh==4x4 channel==1 step==4 matType==MAT_GRAY_F32", __FILE__, __LINE__,__FUNCTION__);
    }

    if(forceCheckSE3)
    {
        if(!mat.isHomTransMatrix())
        {
            throw Exception(1, "[SE3F] not a SE3 mat", __FILE__, __LINE__,__FUNCTION__);
        }
    }

    if(this!=&mat)
    {
        release();
        this->_channel  = mat.getChannel();
        this->_width    = mat.getWidth();
        this->_height   = mat.getHeight();
        this->_step     = mat.getStep();
        this->_matType  = mat.getMatType();

        if(mat.getBytes()!=nullptr)
        {
            uint8_t *u8Ptr =  new uint8_t[this->_width*this->_height*this->_step]();
            memcpy(u8Ptr, mat.getBytes(), this->_width*this->_height*this->_step);
            this->_data.u8 =u8Ptr;
        }
    }
    return *this;
}

SE3F &SE3F::operator=(Mat&& mat)
{
    if(mat.getWidth()!=4 || mat.getHeight()!=4 || mat.getChannel()!=1 || mat.getStep()!=4 || mat.getMatType()!= MatType::MAT_GRAY_F32)

    {
        throw Exception(1, "[SE3F] mat should be: wxh==4x4 channel==1 step==4 matType==MAT_GRAY_F32", __FILE__, __LINE__,__FUNCTION__);
    }

    if(forceCheckSE3)
    {
        if(!mat.isHomTransMatrix())
        {
            throw Exception(1, "[SE3F] not a SE3 mat", __FILE__, __LINE__,__FUNCTION__);
        }
    }

    if(this!=&mat)
    {
        release();
        this->_channel  = mat.getChannel();
        this->_width    = mat.getWidth();
        this->_height   = mat.getHeight();
        this->_step     = mat.getStep();
        this->_matType  = mat.getMatType();
        this->_data.u8  = mat.getBytes();
        mat.setDataNull();
    }
    return *this;
}

SE3F &SE3F::operator=(const SE3F &mat)
{
    if(this!=&mat)
    {
        release();
        this->_channel  = mat._channel;
        this->_width    = mat._width;
        this->_height   = mat._height;
        this->_step     = mat._step;
        this->_matType  = mat._matType;

        if(mat._data.u8!=nullptr)
        {
            uint8_t *u8Ptr =  new uint8_t[this->_width*this->_height*this->_step]();
            memcpy(u8Ptr, mat._data.u8, this->_width*this->_height*this->_step);
            this->_data.u8 =u8Ptr;
        }
    }
    return *this;
}

SE3F &SE3F::operator=(SE3F&& mat)
{
    if(this!=&mat)
    {
        release();
        this->_channel  = mat._channel;
        this->_width    = mat._width;
        this->_height   = mat._height;
        this->_step     = mat._step;
        this->_matType  = mat._matType;
        this->_data.u8  = mat.getBytes();
        mat.setDataNull();
    }
    return *this;
}

SE3F::SE3F(const SO3F &rotMat, const Vector3F &trans)
{
    setRotationMat(rotMat);
    setTranslation(trans);
}

Matrix4x4F &SE3F::toMatrix4x4()
{
    return *this;
}

Mat SE3F::adjoint()
{
    RotationMatF R = getRotationMat();
    TranslationF p = getTranslation();

    Mat rightUp = SO3F::wedge(p)*R;
    Mat zeros3x3= Mat(3,3,MAT_GRAY_F32);

    Mat up      = MatOp::vContact(R,rightUp);
    Mat down    = MatOp::vContact(zeros3x3,R);

    return MatOp::hContact(up,down);
}

SE3F SE3F::fastInvert()
{
    RotationMatF rotMat = getRotationMat();
    TranslationF trans  = getTranslation();
    trans = trans * -1;

    rotMat = rotMat.transpose();
    trans = rotMat.mulVec(trans);

    return  SE3F(rotMat,trans);
}

Matrix4x4F SE3F::wedge(const ScrewF &screw, bool needCalUnit)
{
    Matrix4x4F mat = Mat(4,4,MAT_GRAY_F32);

    if(closeToZeroF((float)(screw.w.length())))
    {
        if(needCalUnit)
        {
            if(closeToZeroF((float)(screw.v.length())))
            {
                return mat;
            }

            mat.setTranslation(screw.v/(float)(screw.v.length()));
            return mat;
        }
    }

    if(needCalUnit)
    {
        Vector3F omg   = screw.w / (float)(screw.w.length());
        Matrix3x3F so3 = SO3F::wedge(omg);

        mat.setRotationMat(so3);
        mat.setTranslation(screw.v);
        return mat;
    }

    Matrix3x3F so3     = SO3F::wedge(screw.w);
    mat.setRotationMat(so3);
    mat.setTranslation(screw.v);
    return mat;
}

ScrewF SE3F::vee(const Matrix4x4F &wed, bool needCalUnit)
{
    Vector3F w = SO3F::vee(wed.getRotationMat());
    Vector3F v = wed.getTranslation();

    if(needCalUnit)
    {
        if(closeToZeroF((float)(w.length())))
        {
            return ScrewF(v/(float)(v.length()),Vector3F({0,0,0}));
        }
        else
        {
            return ScrewF(v,w/(float)(w.length()));
        }
    }

    return ScrewF(v,w);
}

ScrewF SE3F::log()
{

    if((*this) == Mat::eye(4,MAT_GRAY_F32))
    {
        return ScrewF();
    }

    SO3F R         = getRotationMat();
    TranslationF p = getTranslation();

    if(R == Mat::eye(3,MAT_GRAY_F32))
    {

        return ScrewF(p, Vector3F({0,0,0}));
    }
    Vector3F w        = R.log();

    Matrix3x3F wWedge = SO3F::wedge(w);

    float theta      = (float)(w.length());

    Matrix3x3F GInv   = Mat::eye(3,MAT_GRAY_F32) - 0.5f*wWedge + wWedge*wWedge*(1/theta - 0.5f*1/tanf(0.5f*theta))/theta;

    Vector3F v        = GInv.mulVec(p);

    return ScrewF(v,w);
}

SE3F SE3F::exp(const ScrewF &screw, float theta)
{
    if(!closeToZeroF((float)(screw.w.length()-1)) && !closeToZeroF((float)(screw.w.length())))
    {
        throw Exception(1, "[SE3F] given theta, OMG must be a unit vector ", __FILE__, __LINE__,__FUNCTION__);
    }

    return SE3F::exp(ScrewF(screw.v, screw.w*theta));
}

SE3F SE3F::exp(const ScrewF &screw)
{
    Vector3F v   = screw.v;
    Vector3F omg = screw.w;

    if(closeToZeroF((float)(omg.length())))
    {
        SE3F se3;
        se3.setTranslation(v);
        return se3;
    }
    else
    {
        float theta  = (float)(omg.length());

        SO3F so3      = SO3F::exp(omg);

        Vector3F axis = omg/theta;
        v             = v/theta;

        Matrix3x3F wedge = SO3F::wedge(axis);

        Matrix3x3F G    = Mat::eye(3,MAT_GRAY_F32)*theta + (1-cosf(theta))*wedge + wedge*wedge*(theta - sinf(theta));
        Vector3F p      = G.mulVec(v);

        SE3F se3;
        se3.setRotationMat(so3);
        se3.setTranslation(p);
        return se3;
    }
}

bool SE3F::isSE3(const Mat &mat)
{
    return mat.isHomTransMatrix();
}

SO3DS::SO3DS(const SO3DS &so3ds)
{
    this->val[0] = so3ds.val[0];
    this->val[1] = so3ds.val[1];
    this->val[2] = so3ds.val[2];
    this->val[3] = so3ds.val[3];
    this->val[4] = so3ds.val[4];
    this->val[5] = so3ds.val[5];
    this->val[6] = so3ds.val[6];
    this->val[7] = so3ds.val[7];
    this->val[8] = so3ds.val[8];
}

SO3DS::SO3DS(const RotationMatDS &rotMat)
{
    this->val[0] = rotMat.val[0];
    this->val[1] = rotMat.val[1];
    this->val[2] = rotMat.val[2];
    this->val[3] = rotMat.val[3];
    this->val[4] = rotMat.val[4];
    this->val[5] = rotMat.val[5];
    this->val[6] = rotMat.val[6];
    this->val[7] = rotMat.val[7];
    this->val[8] = rotMat.val[8];
}

SO3DS &SO3DS::operator=(const SO3DS &so3ds)
{
    if(this!=&so3ds)
    {
        this->val[0] = so3ds.val[0];
        this->val[1] = so3ds.val[1];
        this->val[2] = so3ds.val[2];
        this->val[3] = so3ds.val[3];
        this->val[4] = so3ds.val[4];
        this->val[5] = so3ds.val[5];
        this->val[6] = so3ds.val[6];
        this->val[7] = so3ds.val[7];
        this->val[8] = so3ds.val[8];
    }

    return *this;
}

SO3DS &SO3DS::operator=(const RotationMatDS &so3ds)
{
    this->val[0] = so3ds.val[0];
    this->val[1] = so3ds.val[1];
    this->val[2] = so3ds.val[2];
    this->val[3] = so3ds.val[3];
    this->val[4] = so3ds.val[4];
    this->val[5] = so3ds.val[5];
    this->val[6] = so3ds.val[6];
    this->val[7] = so3ds.val[7];
    this->val[8] = so3ds.val[8];

    return *this;
}

QuaternionDS SO3DS::toQuaternion()
{
    return GeometryS::rotMat2Quaternion(*this);
}

EulerDS SO3DS::toEuler(const RotSequence &rotSeq)
{
    return GeometryS::rotMat2Euler(*this, rotSeq);
}

RotationVecDS SO3DS::toRotVector()
{
    return GeometryS::rotMat2RotVec(*this);
}

SO3DS SO3DS::adjoint()
{
    return *this;
}

SO3DS SO3DS::rotX(double angleInRad)
{
    return GeometryS::rotX(angleInRad);
}

SO3DS SO3DS::rotY(double angleInRad)
{
    return GeometryS::rotX(angleInRad);
}

SO3DS SO3DS::rotZ(double angleInRad)
{
    return GeometryS::rotX(angleInRad);
}

SO3DS SO3DS::fromRotMat(const RotationMatDS &rotMat)
{
    return rotMat;
}

SO3DS SO3DS::fromQuaternion(const QuaternionDS &quat)
{
    return GeometryS::quaternion2RotMat(quat);
}

SO3DS SO3DS::fromEuler(const EulerDS &euler, const RotSequence &rotSeq)
{
    return GeometryS::euler2RotMat(euler, rotSeq);
}

SO3DS SO3DS::fromRotVec(const RotationVecDS &rotVec)
{
    return GeometryS::rotVec2RotMat(rotVec);
}

SO3DS SO3DS::wedge(const Vector3DS &omg, bool needCalUnit)
{
    Vector3DS tmp = omg;

    if(needCalUnit)
    {
        if(closeToZeroD(tmp.length()))
        {
            return SO3DS();
        }

        tmp = tmp / tmp.length();
    }

    SO3DS so3ds;
    so3ds.setVal({
                     0,  -tmp[2],   tmp[1],
                     tmp[2],       0 ,  -tmp[0],
                     -tmp[1],   tmp[0],    0
                 });
    return so3ds;
}

Vector3DS SO3DS::vee(const SO3DS &so3ds, bool needCalUnit)
{
    ///   | 0 1 2
    /// --|-------
    /// 0 | 1 2 3
    /// 1 | 4 5 6
    /// 2 | 7 8 9

    Vector3DS omg;
    omg[0] = 0.5*(so3ds.val[8] - so3ds.val[6]);
    omg[1] = 0.5*(so3ds.val[2] - so3ds.val[7]);
    omg[2] = 0.5*(so3ds.val[4] - so3ds.val[1]);

    if(needCalUnit)
    {
        if(closeToZeroD(omg.length()))
        {
            return Vector3DS(0,0,0);
        }

        omg = omg / omg.length();
    }

    return omg;
}

SO3DS SO3DS::exp(const Vector3DS &omg)
{
    double angle = omg.length();

    if(closeToZeroD(angle))//趋近0
    {
        return SO3DS() + wedge(omg);
    }

    Vector3DS axis = omg/angle;

    double s = sin(angle);
    double c = cos(angle);

    SO3DS skew = wedge(axis);

    return SO3DS() + s*skew + (1-c)*skew*skew;
}

SO3DS SO3DS::exp(const Vector3DS &omg, double theta)
{
    //不为单位向量                         且         不为0
    if(!closeToZeroD(omg.length()-1) && !closeToZeroD(omg.length()))
    {
        throw Exception(1, "[SO3DS] given theta, OMG must be a unit vector", __FILE__, __LINE__,__FUNCTION__);
    }

    return SO3DS::exp(omg*theta);
}

Vector3DS SO3DS::log()
{
    if(*this == SO3DS())
    {
        return Vector3DS(0,0,0);
    }
    else if(closeToZeroD(this->trace()+1))
    {
        ///   | 0 1 2
        /// --|-------
        /// 0 | 1 2 3
        /// 1 | 4 5 6
        /// 2 | 7 8 9

        double m00 =val[0];
        double m01 =val[1];
        double m02 =val[2];

        double m10 =val[3];
        double m11 =val[4];
        double m12 =val[5];

        double m20 =val[6];
        double m21 =val[7];
        double m22 =val[8];

        if(m00>m11 && m00>m22)
        {
            return 1/(sqrt(1+m00))*Vector3DS(m00+1, m10, m20);
        }
        else if(m11>m00 && m11>m22)
        {
            return 1/(sqrt(1+m11))*Vector3DS(m01+1, m11, m21);
        }
        else if(m22>m00 && m22>m11)
        {
            return 1/(sqrt(1+m22))*Vector3DS(m02+1, m12, m22);
        }
        else
        {
            throw Exception(1, "Log error!", __FILE__, __LINE__,__FUNCTION__);
        }
    }
    else
    {
        double theta = acos(0.5*(this->trace()-1));
        return SO3DS::vee(1.0/(2*sin(theta))*(*this - (*this).transpose()))*theta;
    }
}

SO3FS::SO3FS(const SO3FS &so3ds)
{
    this->val[0] = so3ds.val[0];
    this->val[1] = so3ds.val[1];
    this->val[2] = so3ds.val[2];
    this->val[3] = so3ds.val[3];
    this->val[4] = so3ds.val[4];
    this->val[5] = so3ds.val[5];
    this->val[6] = so3ds.val[6];
    this->val[7] = so3ds.val[7];
    this->val[8] = so3ds.val[8];
}

SO3FS::SO3FS(const RotationMatFS &rotMat)
{
    this->val[0] = rotMat.val[0];
    this->val[1] = rotMat.val[1];
    this->val[2] = rotMat.val[2];
    this->val[3] = rotMat.val[3];
    this->val[4] = rotMat.val[4];
    this->val[5] = rotMat.val[5];
    this->val[6] = rotMat.val[6];
    this->val[7] = rotMat.val[7];
    this->val[8] = rotMat.val[8];
}

SO3FS &SO3FS::operator=(const SO3FS &so3fs)
{
    if(this!=&so3fs)
    {
        this->val[0] = so3fs.val[0];
        this->val[1] = so3fs.val[1];
        this->val[2] = so3fs.val[2];
        this->val[3] = so3fs.val[3];
        this->val[4] = so3fs.val[4];
        this->val[5] = so3fs.val[5];
        this->val[6] = so3fs.val[6];
        this->val[7] = so3fs.val[7];
        this->val[8] = so3fs.val[8];
    }

    return *this;
}

SO3FS &SO3FS::operator=(const RotationMatFS &so3fs)
{
    this->val[0] = so3fs.val[0];
    this->val[1] = so3fs.val[1];
    this->val[2] = so3fs.val[2];
    this->val[3] = so3fs.val[3];
    this->val[4] = so3fs.val[4];
    this->val[5] = so3fs.val[5];
    this->val[6] = so3fs.val[6];
    this->val[7] = so3fs.val[7];
    this->val[8] = so3fs.val[8];

    return *this;
}

QuaternionFS SO3FS::toQuaternion()
{
    return GeometryS::rotMat2Quaternion(*this);
}

EulerFS SO3FS::toEuler(const RotSequence &rotSeq)
{
    return GeometryS::rotMat2Euler(*this, rotSeq);
}

RotationVecFS SO3FS::toRotVector()
{
    return GeometryS::rotMat2RotVec(*this);
}

SO3FS SO3FS::adjoint()
{
    return *this;
}

SO3FS SO3FS::rotX(float angleInRad)
{
    return GeometryS::rotX(angleInRad);
}

SO3FS SO3FS::rotY(float angleInRad)
{
    return GeometryS::rotX(angleInRad);
}

SO3FS SO3FS::rotZ(float angleInRad)
{
    return GeometryS::rotX(angleInRad);
}

SO3FS SO3FS::fromRotMat(const RotationMatFS &rotMat)
{
    return rotMat;
}

SO3FS SO3FS::fromQuaternion(const QuaternionFS &quat)
{
    return GeometryS::quaternion2RotMat(quat);
}

SO3FS SO3FS::fromEuler(const EulerFS &euler, const RotSequence &rotSeq)
{
    return GeometryS::euler2RotMat(euler, rotSeq);
}

SO3FS SO3FS::fromRotVec(const RotationVecFS &rotVec)
{
    return GeometryS::rotVec2RotMat(rotVec);
}

SO3FS SO3FS::wedge(const Vector3FS &omg, bool needCalUnit)
{
    Vector3FS tmp = omg;

    if(needCalUnit)
    {
        if(closeToZeroF(tmp.length()))
        {
            return SO3FS();
        }

        tmp = tmp / tmp.length();
    }

    SO3FS so3ds;
    so3ds.setVal({
                     0,  -tmp[2],   tmp[1],
                     tmp[2],       0 ,  -tmp[0],
                     -tmp[1],   tmp[0],    0
                 });
    return so3ds;
}

Vector3FS SO3FS::vee(const SO3FS &so3ds, bool needCalUnit)
{
    ///   | 0 1 2
    /// --|-------
    /// 0 | 1 2 3
    /// 1 | 4 5 6
    /// 2 | 7 8 9

    Vector3FS omg;
    omg[0] = 0.5*(so3ds.val[8] - so3ds.val[6]);
    omg[1] = 0.5*(so3ds.val[2] - so3ds.val[7]);
    omg[2] = 0.5*(so3ds.val[4] - so3ds.val[1]);

    if(needCalUnit)
    {
        if(closeToZeroF(omg.length()))
        {
            return Vector3FS(0,0,0);
        }

        omg = omg / omg.length();
    }

    return omg;
}

SO3FS SO3FS::exp(const Vector3FS &omg)
{
    float angle = omg.length();

    if(closeToZeroF(angle))//趋近0
    {
        return SO3FS() + wedge(omg);
    }

    Vector3FS axis = omg/angle;

    float s = sinf(angle);
    float c = cosf(angle);

    SO3FS skew = wedge(axis);

    return SO3FS() + s*skew + (1-c)*skew*skew;
}

SO3FS SO3FS::exp(const Vector3FS &omg, float theta)
{
    //不为单位向量                         且         不为0
    if(!closeToZeroF(omg.length()-1) && !closeToZeroF(omg.length()))
    {
        throw Exception(1, "[SO3FS] given theta, OMG must be a unit vector", __FILE__, __LINE__,__FUNCTION__);
    }

    return SO3FS::exp(omg*theta);
}

Vector3FS SO3FS::log()
{
    if(*this == SO3FS())
    {
        return Vector3FS(0,0,0);
    }
    else if(closeToZeroF(this->trace()+1))
    {
        ///   | 0 1 2
        /// --|-------
        /// 0 | 1 2 3
        /// 1 | 4 5 6
        /// 2 | 7 8 9

        float m00 =val[0];
        float m01 =val[1];
        float m02 =val[2];

        float m10 =val[3];
        float m11 =val[4];
        float m12 =val[5];

        float m20 =val[6];
        float m21 =val[7];
        float m22 =val[8];

        if(m00>m11 && m00>m22)
        {
            return 1/(sqrtf(1+m00))*Vector3FS(m00+1, m10, m20);
        }
        else if(m11>m00 && m11>m22)
        {
            return 1/(sqrtf(1+m11))*Vector3FS(m01+1, m11, m21);
        }
        else if(m22>m00 && m22>m11)
        {
            return 1/(sqrtf(1+m22))*Vector3FS(m02+1, m12, m22);
        }
        else
        {
            throw Exception(1, "Log error!", __FILE__, __LINE__,__FUNCTION__);
        }
    }
    else
    {
        float theta = acosf(0.5*(this->trace()-1));
        return SO3FS::vee(1.0/(2*sin(theta))*(*this - (*this).transpose()))*theta;
    }
}

SE3DS::SE3DS(const SE3DS &se3d)
{
    this->trans     = se3d.trans;
    this->rotMat    = se3d.rotMat;
}

SE3DS::SE3DS(const HomTransMatDS &homDS)
{
    this->trans     = homDS.trans;
    this->rotMat    = homDS.rotMat;
}

SE3DS &SE3DS::operator=(const SE3DS &se3d)
{
    if(*this != se3d)
    {
        this->trans     = se3d.trans;
        this->rotMat    = se3d.rotMat;
    }
    return *this;
}

SE3DS &SE3DS::operator=(const HomTransMatDS &homDS)
{
    this->trans     = homDS.trans;
    this->rotMat    = homDS.rotMat;
    return *this;
}

SE3DS::SE3DS(const SO3DS &rotMat, const Vector3DS &trans)
{
    this->trans     = trans;
    this->rotMat    = rotMat;
}

Mat SE3DS::adjoint()
{
//    RotationMatDS R = getRotationMat();
//    TranslationDS p = getTranslation();

    RotationMatDS right = SO3DS::wedge(trans)*rotMat;

    RotationMatD R ;
    R.setVal({rotMat.val[0],rotMat.val[1],rotMat.val[2],
              rotMat.val[3],rotMat.val[4],rotMat.val[5],
              rotMat.val[6],rotMat.val[7],rotMat.val[8]}
            );

    RotationMatD rightUp= Mat(3,3,MAT_GRAY_F64);
    rightUp.setVal({right.val[0],right.val[1],right.val[2],
                    right.val[3],right.val[4],right.val[5],
                    right.val[6],right.val[7],right.val[8]}
                  );

    Mat zeros3x3= Mat(3,3,MAT_GRAY_F64);

    Mat up      = MatOp::vContact(R,rightUp);
    Mat down    = MatOp::vContact(zeros3x3,R);

    return MatOp::hContact(up,down);
}

HomTransMatDS SE3DS::wedge(const ScrewDS &screw, bool needCalUnit)
{
    HomTransMatDS homDS;
    if(closeToZeroD(screw.w.length()))
    {
        if(needCalUnit)
        {
            if(closeToZeroD(screw.v.length()))
            {
                return homDS;
            }

            homDS.trans = screw.v/screw.v.length();
            return homDS;
        }
    }

    if(needCalUnit)
    {
        Vector3DS omg   = screw.w / screw.w.length();
        SO3DS so3 = SO3DS::wedge(omg);

        homDS.rotMat = so3;
        homDS.trans  = omg;
        return homDS;
    }

    SO3DS so3     = SO3DS::wedge(screw.w);
    homDS.rotMat  = so3;
    homDS.trans   = screw.v;
    return homDS;
}

ScrewDS SE3DS::vee(const HomTransMatDS &wed, bool needCalUnit)
{
    Vector3DS w = SO3DS::vee(wed.rotMat);
    Vector3DS v = wed.trans;

    if(needCalUnit)
    {
        if(closeToZeroD(w.length()))
        {
            return ScrewDS(v/v.length(),Vector3DS(0,0,0));
        }
        else
        {
            return ScrewDS(v,w/w.length());
        }
    }

    return ScrewDS(v,w);
}

ScrewDS SE3DS::log()
{
    if((*this) == HomTransMatDS())
    {
        return ScrewDS();
    }

    SO3DS R         = this->rotMat;
    TranslationDS p = this->trans;

    if(R == SO3DS())
    {          //     v    w
        return ScrewDS(p, Vector3DS(0,0,0));
    }

    Vector3DS w         = R.log();

    RotationMatDS wWedge = SO3DS::wedge(w);

    double theta        = w.length();

    RotationMatDS GInv  = RotationMatDS() - 0.5*wWedge + (1/theta - 0.5*1/tan(0.5*theta))/theta*wWedge*wWedge;

    Vector3DS v          = GInv*p;

    return ScrewDS(v,w);
}

SE3DS SE3DS::exp(const ScrewDS &screw, double theta)
{
    if(std::abs(screw.w.length()-1)>MSNH_F64_EPS && std::abs(screw.w.length())>MSNH_F64_EPS)
    {
        throw Exception(1, "[SE3DS] given theta, OMG must be a unit vector ", __FILE__, __LINE__,__FUNCTION__);
    }

    return SE3DS::exp(ScrewDS(screw.v, screw.w*theta));
}

SE3DS SE3DS::exp(const ScrewDS &screw)
{
    Vector3DS v   = screw.v;
    Vector3DS omg = screw.w;

    if(closeToZeroD(omg.length()))
    {
        SE3DS se3;
        se3.trans = v;
        return se3;
    }
    else
    {
        double theta        = omg.length();

        SO3DS so3           = SO3DS::exp(omg);

        Vector3DS axis      = omg/theta;
        v                   = v/theta;

        RotationMatDS wedge = SO3DS::wedge(axis);
        RotationMatDS eye;

        RotationMatDS G     = eye*theta + (1-cos(theta))*wedge + wedge*wedge*(theta - sin(theta));
        Vector3DS p         = G*v;

        SE3DS se3(so3, p);
        return se3;
    }
}

SE3FS::SE3FS(const SE3FS &se3d)
{
    this->trans     = se3d.trans;
    this->rotMat    = se3d.rotMat;
}

SE3FS::SE3FS(const HomTransMatFS &homFS)
{
    this->trans     = homFS.trans;
    this->rotMat    = homFS.rotMat;
}

SE3FS &SE3FS::operator=(const SE3FS &se3d)
{
    if(*this != se3d)
    {
        this->trans     = se3d.trans;
        this->rotMat    = se3d.rotMat;
    }
    return *this;
}

SE3FS &SE3FS::operator=(const HomTransMatFS &homFS)
{
    this->trans     = homFS.trans;
    this->rotMat    = homFS.rotMat;
    return *this;
}

SE3FS::SE3FS(const SO3FS &rotMat, const Vector3FS &trans)
{
    this->trans     = trans;
    this->rotMat    = rotMat;
}

Mat SE3FS::adjoint()
{
//    RotationMatFS R = getRotationMat();
//    TranslationFS p = getTranslation();

    RotationMatFS right = SO3FS::wedge(trans)*rotMat;

    RotationMatD R ;
    R.setVal({rotMat.val[0],rotMat.val[1],rotMat.val[2],
              rotMat.val[3],rotMat.val[4],rotMat.val[5],
              rotMat.val[6],rotMat.val[7],rotMat.val[8]}
            );

    RotationMatD rightUp= Mat(3,3,MAT_GRAY_F32);
    rightUp.setVal({right.val[0],right.val[1],right.val[2],
                    right.val[3],right.val[4],right.val[5],
                    right.val[6],right.val[7],right.val[8]}
                  );

    Mat zeros3x3= Mat(3,3,MAT_GRAY_F32);

    Mat up      = MatOp::vContact(R,rightUp);
    Mat down    = MatOp::vContact(zeros3x3,R);

    return MatOp::hContact(up,down);
}

HomTransMatFS SE3FS::wedge(const ScrewFS &screw, bool needCalUnit)
{
    HomTransMatFS homFS;
    if(closeToZeroF(screw.w.length()))
    {
        if(needCalUnit)
        {
            if(closeToZeroF(screw.v.length()))
            {
                return homFS;
            }

            homFS.trans = screw.v/screw.v.length();
            return homFS;
        }
    }

    if(needCalUnit)
    {
        Vector3FS omg   = screw.w / screw.w.length();
        SO3FS so3 = SO3FS::wedge(omg);

        homFS.rotMat = so3;
        homFS.trans  = omg;
        return homFS;
    }

    SO3FS so3     = SO3FS::wedge(screw.w);
    homFS.rotMat  = so3;
    homFS.trans   = screw.v;
    return homFS;
}

ScrewFS SE3FS::vee(const HomTransMatFS &wed, bool needCalUnit)
{
    Vector3FS w = SO3FS::vee(wed.rotMat);
    Vector3FS v = wed.trans;

    if(needCalUnit)
    {
        if(closeToZeroF(w.length()))
        {
            return ScrewFS(v/v.length(),Vector3FS(0,0,0));
        }
        else
        {
            return ScrewFS(v,w/w.length());
        }
    }

    return ScrewFS(v,w);
}

ScrewFS SE3FS::log()
{
    if((*this) == HomTransMatFS())
    {
        return ScrewFS();
    }

    SO3FS R         = this->rotMat;
    TranslationFS p = this->trans;

    if(R == SO3FS())
    {          //     v    w
        return ScrewFS(p, Vector3FS(0,0,0));
    }

    Vector3FS w         = R.log();

    RotationMatFS wWedge = SO3FS::wedge(w);

    double theta        = w.length();

    RotationMatFS GInv  = RotationMatFS() - 0.5f*wWedge + (1/theta - 0.5f*1/tan(0.5f*theta))/theta*wWedge*wWedge;

    Vector3FS v          = GInv*p;

    return ScrewFS(v,w);
}

SE3FS SE3FS::exp(const ScrewFS &screw, double theta)
{
    if(std::abs(screw.w.length()-1)>MSNH_F32_EPS && std::abs(screw.w.length())>MSNH_F32_EPS)
    {
        throw Exception(1, "[SE3FS] given theta, OMG must be a unit vector ", __FILE__, __LINE__,__FUNCTION__);
    }

    return SE3FS::exp(ScrewFS(screw.v, screw.w*theta));
}

SE3FS SE3FS::exp(const ScrewFS &screw)
{
    Vector3FS v   = screw.v;
    Vector3FS omg = screw.w;

    if(closeToZeroF(omg.length()))
    {
        SE3FS se3;
        se3.trans = v;
        return se3;
    }
    else
    {
        double theta        = omg.length();

        SO3FS so3           = SO3FS::exp(omg);

        Vector3FS axis      = omg/theta;
        v                   = v/theta;

        RotationMatFS wedge = SO3FS::wedge(axis);
        RotationMatFS eye;

        RotationMatFS G     = eye*theta + (1-cosf(theta))*wedge + wedge*wedge*(theta - sinf(theta));
        Vector3FS p         = G*v;

        SE3FS se3(so3, p);
        return se3;
    }
}

}

﻿#include "Msnhnet/robot/MsnhSegment.h"

namespace Msnhnet
{

Segment::Segment(const std::string &name, const Joint &joint, const Frame &endToTip, const double jointMin , const double jointMax)
    :_name(name),
     _joint(joint),
     _endToTip(_joint.getPos(0).invert()*endToTip),
     _jointMin(jointMin),
     _jointMax(jointMax)
{
    if(joint.getType() == JOINT_FIXED)
    {
        _moveType = MOVE_CAN_NOT;
    }
    else if( joint.getType()>=JOINT_ROT_AXIS && joint.getType()<JOINT_TRANS_AXIS)
    {

        if(std::abs(_jointMin+DBL_MAX) < MSNH_F64_EPS && std::abs(_jointMin-DBL_MAX) < MSNH_F64_EPS)
        {
            _moveType = MOVE_ROT_CONTINUOUS;
        }
        else
        {
            _moveType = MOVE_ROT_LIMIT;
        }
    }
    else
    {
        _moveType = MOVE_TRANS;
    }
}

Segment::Segment(const Joint &joint, const Frame &endToTip,  const double jointMin , const double jointMax)
    :_name("untitled segment"),
      _joint(joint),
      _endToTip(_joint.getPos(0).invert()*endToTip),
      _jointMin(jointMin),
      _jointMax(jointMax)
{
    if(joint.getType() == JOINT_FIXED)
    {
        _moveType = MOVE_CAN_NOT;
    }
    else if( joint.getType()>=JOINT_ROT_AXIS && joint.getType()<JOINT_TRANS_AXIS)
    {

        if(std::abs(_jointMin+DBL_MAX) < MSNH_F64_EPS && std::abs(_jointMin-DBL_MAX) < MSNH_F64_EPS)
        {
            _moveType = MOVE_ROT_CONTINUOUS;
        }
        else
        {
            _moveType = MOVE_ROT_LIMIT;
        }
    }
    else
    {
        _moveType = MOVE_TRANS;
    }
}

Segment::Segment(const Segment &in):
    _name(in._name),
    _joint(in._joint),
    _endToTip(in._endToTip),
    _jointMin(in._jointMin),
    _jointMax(in._jointMax),
    _moveType(in._moveType)
{

}

Segment &Segment::operator=(const Segment &in)
{
    _name       = in._name;
    _joint      = in._joint;
    _endToTip   = in._endToTip;
    _jointMin   = in._jointMin;
    _jointMax   = in._jointMax;
    _moveType   = in._moveType;
    return *this;
}

std::string Segment::getName() const
{
    return _name;
}

Joint Segment::getJoint() const
{
    return _joint;
}

Frame Segment::getEndToTip() const
{
    return _joint.getPos(0)*_endToTip;
}

Frame Segment::getPos(const double &q) const
{
//    std::cout<<__FILE__<<"f_tip\n";
//    auto tmp = _endToTip;
//    tmp.print();
    return  _joint.getPos(q)*_endToTip;
}

Twist Segment::getTwist(const double &q, const double &qdot) const
{
    return _joint.getTwist(qdot).refPoint(_joint.getPos(q).rotMat*_endToTip.trans);
}

double Segment::getJointMin() const
{
    return _jointMin;
}

double Segment::getJointMax() const
{
    return _jointMax;
}

MoveType Segment::getMoveType() const
{
    return _moveType;
}

}


﻿#include "Msnhnet/layers/cuda/MsnhPixelShuffleLayerGPU.h"

namespace Msnhnet
{

__global__ void pixShuffleKernel(const int num, const int outHeight, const int outWidth, const int outChannel,
                                const int height, const int width, const int channel,
                                const int factor, float *const input, float *const output)
{

//    for (int b = 0; b < this->_batch; ++b)
//    {
//#ifdef USE_OMP
//#pragma omp parallel for num_threads(OMP_THREAD)
//#endif
//        for (int c = 0; c < this->_outChannel; ++c)
//        {
//            for (int h = 0; h < this->_outHeight; ++h)
//            {
//                for (int w = 0; w < this->_outWidth; ++w)
//                {
//                    layerOutput[b*this->_outChannel*this->_outWidth*this->_outHeight + c*this->_outWidth*this->_outHeight + h*this->_outWidth + w]
//                            =
//                    layerInput[b*this->_channel*this->_width*this->_height +
//                               c*this->_factor*this->_width*this->_height +
//                               h*this->_width +
//                               w%(this->_factor)*this->_width*this->_height +
//                               w/this->_factor ];
//                }
//            }
//        }
//    }

    int index = (blockIdx.x + blockIdx.y*gridDim.x) * blockDim.x + threadIdx.x;
    if(index < num)
    {
        int w = index % outWidth;
        index = index / outWidth;
        int h = index % outHeight;
        index = index / outHeight;
        int c = index % outChannel;
        index = index / outChannel;
        int b = index;

        output[b*outChannel*outWidth*outHeight + c*outWidth*outHeight + h*outWidth + w]
        =
        input[b*channel*width*height + c*factor*width*height + h*width + w%factor*width*height + w/factor];

       // printf("%f",input[b*channel*width*height + cc*width*height + hh*width + ww]);

    }
}

void PixelShuffleLayerGPU::forwardNormalGPU(const int &batch, const int &outChannel, const int &outHeight, const int &outWidth,
                                        const int &height, const int &width, const int &channel,
                                        const int &factor,float *const &input, float *const &output)
{
    size_t n = outHeight * outWidth * outChannel * batch;
    pixShuffleKernel<<<Cuda::getGrid(n), Cuda::blockThread, 0, Cuda::getCudaStream()>>>(n, outHeight, outWidth, outChannel, height, width, channel, factor, input, output);
    CUDA_CHECK(cudaPeekAtLastError());
}

}

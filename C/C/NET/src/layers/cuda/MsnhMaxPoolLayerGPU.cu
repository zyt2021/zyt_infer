﻿#include "Msnhnet/layers/cuda/MsnhMaxPoolLayerGPU.h"
namespace Msnhnet
{

__global__ void maxpoolDepthKernel(const int n, const int width, const int height, const int channel,
                                     const int outChannel, const int batch, float *const input, float *const output)
{

//    for(int b=0; b<this->_batch; ++b)                    // bacth
//    {                                                    // cache miss
//        for(int i=0; i<this->_height; i++)               // height
//        {
//            for(int j=0; j<this->_width; ++j)            // width
//            {
//                for(int g=0; g<this->_outChannel; ++g)   // channel
//                {
//                    int outIndex = j + this->_width*(i + this->_height*(g + this->_outChannel*b));
//                    float max    = -FLT_MAX;
//                    //每次间隔outChannl进行一次池化
//                    for(int k=g; k<this->_channel; k+=this->_outChannel)
//                    {
//                        int inIndex = j + this->_width*(i + this->_height*(k + this->_channel*b));
//                        float val   = netState.input[inIndex];

//                        //跟新最大值的索引
//                        max         = (val > max)? val:max;
//                    }

//                    this->_output[outIndex] = max;
//                }

//            }

//        }
//    }

    int index = (blockIdx.x + blockIdx.y*gridDim.x) * blockDim.x + threadIdx.x;
    if(index < n)
    {
        int j   = index % width;
        index   = index / width;
        int i   = index % height;
        index   = index / height;

        int b   = index % batch;

        for (int g = 0; g < outChannel; ++g)
        {
            int outIndex    = j + width*(i + height*(g + outChannel*b));
            float max       = -FLT_MAX;

            for (int k = g; k < channel; k+=outChannel)
            {
                int inIndex = j + width*(i + height*(k + channel*b));
                float val   = input[inIndex];
                max         = (val > max)?val:max;
            }
            output[outIndex] = max;
        }
    }
}


__global__ void maxpoolNormalKernel(const int n,
                                     const int width, const int height,
                                     const int channel,
                                     const int outWidth, const int outHeight,
                                     const int strideX, const int strideY,
                                     const int kSizeX, const int kSizeY,
                                     const int paddingX, const int paddingY,
                                     float *const input, float *const output)
{


//    int widthOffset  =     -(this->_paddingX + 1)/2;
//    int heightOffset =     -(this->_paddingY + 1)/2;

//    // 获取该层的输出width和height
//    int mHeight         =   this->_outHeight;
//    int mWidth          =   this->_outWidth;
//    // 获取该层的输入通道数, 此处输入和输出相同
//    int mChannel        =   this->_channel;

//    for(int b=0; b<this->_batch; ++b)                // batch
//    {
//        for(int k=0; k<mChannel; ++k)               // channel
//        {
//            for(int i=0; i<mHeight; ++i)            // height
//            {
//                for(int j=0; j<mWidth; ++j)         // width
//                {
//                    // 图片的索引
//                    int outIndex = j + mWidth*(i + mHeight*(k + _channel*b));
//                    float max    = -FLT_MAX;
//                    int maxIndex = -1;

//                    for(int n=0; n<this->_kSizeY; ++n)
//                    {
//                        for(int m=0; m<this->_kSizeX; ++m)
//                        {
//                            // 当前层的索引h
//                            int curHeight =  heightOffset + i*this->_strideY + n;
//                            // 当前层的索引w
//                            int curWidth  =  widthOffset  + j*this->_strideX + m;
//                            // 总索引
//                            int index     =  curWidth + this->_width*(curHeight + this->_height*(k + b*this->_channel));

//                            // 超界判定
//                            bool valid    =  (curHeight >=0 && curHeight < this->_height &&
//                                              curWidth  >=0 && curWidth  < this->_width);
//                            // 超界部分补负无穷
//                            float value   =  (valid)? netState.input[index] : -FLT_MAX;

//                            // 最大值
//                            max           =  (value > max) ? value : max;
//                        }
//                    }

//                    // 得到值
//                    this->_output[outIndex] = max;

//                }
//            }
//        }

    int index = (blockIdx.x + blockIdx.y*gridDim.x) * blockDim.x + threadIdx.x;
    if(index < n)
    {
        int j = index % outWidth; // outWIndex;
        index = index / outWidth;
        int i = index % outHeight;// outHIndex;
        index = index / outHeight;

        int k = index % channel;  // chIndex
        index = index / channel;  //  此处输入和输出相同

        int b = index;//batchIndex

        int widthOffset     =   -paddingX;
        int heightOffset    =   -paddingY;

        int outIndex        =   j + outWidth*(i + outHeight*(k + channel*b));
        float max           =   -INFINITY;

        for (int l = 0; l < kSizeY; ++l)
        {
            for (int m = 0; m < kSizeX; ++m)
            {
                // 当前层的索引h
                int curHeight   =   heightOffset + i*strideY + l;
                // 当前层的索引w
                int curWidth    =   widthOffset  + j*strideX + m;

                int idx         =   curWidth + width*(curHeight + height*(k + b*channel));

                bool valid      =  (curHeight >=0 && curHeight < height &&
                                    curWidth  >=0 && curWidth  < width);

                // 超界部分补负无穷
                float value     =  (valid != 0)? input[idx] : -INFINITY;

                // 最大值
                max             =  (value > max) ? value : max;
            }
        }

        output[outIndex]        =   max;

    }


}

void MaxPoolLayerGPU::forwardDepthGPU(const int &width, const int &height, const int &channel, const int &outWidth, const int &outHeight,
                                    const int &outChannel, const int &batch, float *const &input, float *const &output)
{
    size_t n = outHeight * outWidth * 1 * batch;
    maxpoolDepthKernel<<<Cuda::getGrid(n), Cuda::blockThread, 0, Cuda::getCudaStream()>>>(n, width, height, channel, outChannel, batch, input, output);
    CUDA_CHECK(cudaPeekAtLastError());
}

void MaxPoolLayerGPU::forwardNormalGPU(const int &width, const int &height, const int &channel, const int &outWidth, const int &outHeight,
                                  const int &outChannel, const int &strideX, const int &strideY, const int &kSizeX, const int kSizeY,
                                  const int &paddingX, const int &paddingY, const int &batch, float *const &input, float *const &output)
{


    //printf("%d %d %d %d\n",width, outWidth, height, outHeight);
    size_t n    =   outHeight * outWidth * outChannel * batch;
    maxpoolNormalKernel<<<Cuda::getGrid(n), Cuda::blockThread, 0, Cuda::getCudaStream()>>>(n,width,height,
                                                                                           channel,
                                                                                           outHeight,outWidth,
                                                                                           strideX,strideY,
                                                                                           kSizeX,kSizeY,
                                                                                           paddingX,paddingY,
                                                                                           input,output);
    CUDA_CHECK(cudaPeekAtLastError());
}
}

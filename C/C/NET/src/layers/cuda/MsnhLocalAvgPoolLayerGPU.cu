﻿#include "Msnhnet/layers/cuda/MsnhLocalAvgPoolLayerGPU.h"
namespace Msnhnet
{

__global__ void localAvgPoolNormalKernel(const int n,
                                         const int width, const int height,
                                         const int channel,
                                         const int outWidth, const int outHeight,
                                         const int strideX, const int strideY,
                                         const int kSizeX, const int kSizeY,
                                         const int paddingX, const int paddingY,
                                         float *const input, float *const output)
{

//    int widthOffset  =     -(this->_paddingX + 1) / 2;
//    int heightOffset =     -(this->_paddingY + 1) / 2;

//    // 获取该层的输出width和height
//    int mHeight         =   this->_outHeight;
//    int mWidth          =   this->_outWidth;
//    // 获取该层的输入通道数, 此处输入和输出相同
//    int mChannel        =   this->_channel;

//    for(int b=0; b<this->_batch; ++b)                // batch
//    {
//        for(int k=0; k<mChannel; ++k)               // channel
//        {
//            for(int i=0; i<mHeight; ++i)            // height
//            {
//                for(int j=0; j<mWidth; ++j)         // width
//                {
//                    // 图片的索引
//                    int outIndex = j + mWidth*(i + mHeight*(k + _channel*b));

//                    float avg    = 0;

//                    int counter  = 0;

//                    for(int n=0; n<this->_kSizeY; ++n)
//                    {
//                        for(int m=0; m<this->_kSizeX; ++m)
//                        {
//                            // 当前层的索引h
//                            int curHeight =  heightOffset + i*this->_strideY + n;
//                            // 当前层的索引w
//                            int curWidth  =  widthOffset  + j*this->_strideX + m;
//                            // 总索引 (即输入的索引)
//                            int index     =  curWidth + this->_width*(curHeight + this->_height*(k + b*this->_channel));

//                            // 超界判定
//                            bool valid    =  (curHeight >=0 && curHeight < this->_height &&
//                                              curWidth  >=0 && curWidth  < this->_width);

//                            // 在一个kernel范围,把所有的值进行累加
//                            if(valid)
//                            {
//                                counter++;
//                                avg += netState.input[index];
//                            }
//                        }
//                    }

//                    this->_output[outIndex] = avg / counter;  //取平均
//                }

//            }
//        }
//    }

    int index = (blockIdx.x + blockIdx.y*gridDim.x) * blockDim.x + threadIdx.x;
    if(index < n)
    {
        int j = index % outWidth; // outWIndex;
        index = index / outWidth;
        int i = index % outHeight;// outHIndex;
        index = index / outHeight;

        int k = index % channel;  // chIndex
        index = index / channel;  //  此处输入和输出相同

        int b = index;//batchIndex

        int widthOffset     =   -paddingX;
        int heightOffset    =   -paddingY;

        int outIndex        =   j + outWidth*(i + outHeight*(k + channel*b));

        float avg           = 0;

        int counter         = 0;

        for (int l = 0; l < kSizeY; ++l)
        {
            for (int m = 0; m < kSizeX; ++m)
            {
                // 当前层的索引h
                int curHeight   =   heightOffset + i*strideY + l;
                // 当前层的索引w
                int curWidth    =   widthOffset  + j*strideX + m;

                int index       =   curWidth + width*(curHeight + height*(k + b*channel));

                bool valid      =  (curHeight >=0 && curHeight < height &&
                                    curWidth  >=0 && curWidth  < width);

                if(valid)
                {
                    counter ++;
                    avg         += input[index];
                }
            }
        }
        output[outIndex]        =   avg / counter;
    }
}


void LocalAvgPoolLayerGPU::forwardNormalGPU(const int &width, const int &height, const int &channel, const int &outWidth, const int &outHeight,
                                  const int &outChannel, const int &strideX, const int &strideY, const int &kSizeX, const int kSizeY,
                                  const int &paddingX, const int &paddingY, const int &batch, float *const &input, float *const &output)
{
    size_t n    =   outHeight * outWidth * outChannel * batch;
    localAvgPoolNormalKernel<<<Cuda::getGrid(n), Cuda::blockThread, 0, Cuda::getCudaStream()>>>(n,width,height,
                                                                                           channel,
                                                                                           outHeight,outWidth,
                                                                                           strideX,strideY,
                                                                                           kSizeX,kSizeY,
                                                                                           paddingX,paddingY,
                                                                                           input,output);

    CUDA_CHECK(cudaPeekAtLastError());
}
}

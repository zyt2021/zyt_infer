﻿#include "Msnhnet/layers/cuda/MsnhClipLayerGPU.h"
namespace Msnhnet
{

__global__ void clipKernel(const int num, const int outHeight, const int outWidth, const int outChannel,
                                const float min, const float max,
                                float *const input, float *const output)
{

//    for (int b = 0; b < this->_batch; ++b)
//    {
//#ifdef USE_OMP
//#pragma omp parallel for num_threads(OMP_THREAD)
//#endif
//        for (int c = 0; c < this->_outChannel; ++c)
//        {
//            for (int h = 0; h < this->_outHeight; ++h)
//            {
//                for (int w = 0; w < this->_outWidth; ++w)
//                {
//                    int idx = b*this->_outChannel*this->_outHeight*this->_outWidth + c*this->_outHeight*this->_outWidth + h*this->_outWidth + w;
//                    if(layerInput[idx]>this->_max)
//                        layerOutput[idx] = this->_max;
//                    else if(layerInput[idx]<this->_min)
//                        layerOutput[idx] = this->_min;
//                    else
//                        layerOutput[idx] = layerInput[idx];
//                }
//            }
//        }
//    }

    int idx = (blockIdx.x + blockIdx.y*gridDim.x) * blockDim.x + threadIdx.x;
    if(idx < num)
    {
        if(input[idx]>max)
            output[idx] = max;
        else if(input[idx]<min)
            output[idx] = min;
        else
            output[idx] = input[idx];
       // printf("%f",input[b*channel*width*height + cc*width*height + hh*width + ww]);

    }
}
void ClipLayerGPU::forwardNormalGPU(const int &batch,  const int &outChannel, const int &outHeight, const int &outWidth,
                               const float &min,  const float &max,
                               float *const &input, float * const &output)
{
    size_t n = outHeight * outWidth * outChannel * batch;
    clipKernel<<<Cuda::getGrid(n), Cuda::blockThread, 0, Cuda::getCudaStream()>>>(n, outHeight, outWidth, outChannel, min, max, input, output);
    CUDA_CHECK(cudaPeekAtLastError());
}

}

﻿#include "Msnhnet/layers/cuda/MsnhPaddingLayerGPU.h"
namespace Msnhnet
{

__global__ void paddingKernel(const int num, const int outHeight, const int outWidth, const int outChannel,
                              const int height, const int width, const int channel,
                              const int top, const int left,
                              const float paddingVal,
                              float *const input, float *const output)
{

    //for (int i = 0; i < this->_batch; ++i)
    //{
    //    for (int j = 0; j < this->_outChannel; ++j)
    //    {
    //        for (int m = 0; m < this->_outHeight; ++m)
    //        {
    //            for (int n = 0; n < this->_outWidth; ++n)
    //            {
    //                float val = 0;
    //                if(m < this->_top || (m >= (this->_height + this->_top)))
    //                {
    //                    val     =   this->_paddingVal;
    //                }
    //                else
    //                {
    //                    if(n < this->_left || (n >= (this->_width + this->_left)))
    //                    {
    //                        val     =   this->_paddingVal;
    //                    }
    //                    else
    //                    {
    //                        val     =   netState.input[ i*this->_channel*this->_height*this->_width + j*this->_height*this->_width + (m-this->_top)*this->_width + (n - this->_left)];
    //                    }
    //                }
    //                this->_output[i*this->_outChannel*this->_outHeight*this->_outHeight + j*this->_outHeight*this->_outWidth + m*this->_outWidth + n] = val;
    //            }
    //        }
    //    }
    //}

    int index = (blockIdx.x + blockIdx.y*gridDim.x) * blockDim.x + threadIdx.x;
    if(index < num)
    {
        int n = index % outWidth;
        index = index / outWidth;
        int m = index % outHeight;
        index = index / outHeight;
        int j = index % outChannel;
        index = index / outChannel;
        int i = index;

        float val = 0;

        if(m < top || m>=(height+top))
        {
            val = paddingVal;
        }
        else
        {
            if(n < left || n >=(width+left))
            {
                val = paddingVal;
            }
            else
            {
                val = input[i*channel*height*width + j*height*width + (m - top)*width + (n-left)];
            }
        }
        output[i*outChannel*outHeight*outWidth + j*outHeight*outWidth + m*outWidth + n] = val;
    }
}



void PaddingLayerGPU::forwardNormalGPU(const int &batch, const int &outChannel, const int &outHeight, const int &outWidth,
                                    const int &height, const int &width, const int &channel,
                                    const int &top, const int &left,
                                    const float &paddingVal,
                                    float * const &input, float * const &output)
{
    size_t n = outHeight * outWidth * outChannel * batch;
    paddingKernel<<<Cuda::getGrid(n), Cuda::blockThread, 0, Cuda::getCudaStream()>>>(n, outHeight, outWidth, outChannel, height, width, channel, top, left, paddingVal, input, output);
    CUDA_CHECK(cudaPeekAtLastError());
}

}

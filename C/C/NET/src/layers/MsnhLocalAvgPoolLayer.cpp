﻿#include "Msnhnet/layers/MsnhLocalAvgPoolLayer.h"

namespace Msnhnet
{
LocalAvgPoolLayer::LocalAvgPoolLayer(const int &batch, const int &height, const int &width, const int &channel,
                                     const int &kSizeX, const int &kSizeY, const int &strideX, const int &strideY, const int &paddingX, const int &paddingY, const int &ceilMode,
                                     const int &antialiasing)
{

    this->_type              = LayerType::LOCAL_AVGPOOL;
    this->_layerName         = "LocalAvgPool    ";

    this->_batch             = batch;
    this->_height            = height;
    this->_width             = width;
    this->_channel           = channel;
    this->_paddingX          = paddingX;
    this->_paddingY          = paddingY;

    this->_kSizeX            = kSizeX;  //kerner尺寸
    this->_kSizeY            = kSizeY;

    this->_ceilMode          = ceilMode;


    //    const int blurStrideX   = strideX;
    //    const int blurStrideY   = strideY;

    this->_antialiasing      = antialiasing;

    if(antialiasing)
    {
        this->_strideX       = 1;
        this->_strideY       = 1;
    }
    else
    {
        this->_strideX       = strideX;//X
        this->_strideY       = strideY;//Y
    }

    if(this->_ceilMode == 1)
    {
        int tmpW = (width  + paddingX*2 - kSizeX) % strideX; //输出图片宽度
        int tmpH = (height + paddingY*2 - kSizeY) % strideY; //输出图片高度

        if(tmpW >= kSizeX)
        {
            throw Exception(1,"localavgpool padding error ", __FILE__, __LINE__, __FUNCTION__);
        }


        if(tmpH >= kSizeY)
        {
            throw Exception(1,"localavgpool padding error ", __FILE__, __LINE__, __FUNCTION__);
        }

        if(tmpW <= paddingX)
        {
            this->_outWidth   = (width  + paddingX*2 - kSizeX) / strideX + 1; //输出图片宽度
        }
        else
        {
            this->_outWidth   = (width  + paddingX*2 - kSizeX) / strideX + 2; //输出图片宽度
        }

        if(tmpH <= paddingY)
        {
            this->_outHeight  = (height + paddingY*2 - kSizeY) / strideY + 1; //输出图片高度
        }
        else
        {
            this->_outHeight  = (height + paddingY*2 - kSizeY) / strideY + 2; //输出图片高度
        }
    }
    else if(this->_ceilMode == 0)
    {
        this->_outWidth   = (width  + 2*paddingX - kSizeX) / strideX + 1; //输出图片宽度
        this->_outHeight  = (height + 2*paddingY - kSizeY) / strideY + 1; //输出图片高度
    }
    else
    {
        this->_outWidth   = (width  + paddingX - kSizeX) / strideX + 1; //输出图片宽度
        this->_outHeight  = (height + paddingY - kSizeY) / strideY + 1; //输出图片高度
    }

    this->_outChannel        = channel;                                  //输出图片通道数


    this->_outputNum         = this->_outHeight * this->_outWidth * this->_outChannel; //池化层输出元素个数
    this->_inputNum          = height * width * channel; //池化层输入元素个数


#ifdef USE_GPU
#ifdef USE_CUDNN
    //创建tensor描述符
    CUDNN_CHECK(cudnnCreateTensorDescriptor(&this->_inputDesc));
    //设置描述符参数
    CUDNN_CHECK(cudnnSetTensor4dDescriptor(this->_inputDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, this->_batch, this->_channel, this->_height, this->_width));

    CUDNN_CHECK(cudnnCreateTensorDescriptor(&this->_outputDesc));
    CUDNN_CHECK(cudnnSetTensor4dDescriptor(this->_outputDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, this->_batch, this->_outChannel, this->_outHeight, this->_outWidth));

    CUDNN_CHECK(cudnnCreatePoolingDescriptor(&this->_localAvgPoolDesc));

    CUDNN_CHECK(cudnnSetPooling2dDescriptor(this->_localAvgPoolDesc, CUDNN_POOLING_AVERAGE_COUNT_EXCLUDE_PADDING, CUDNN_NOT_PROPAGATE_NAN,  // CUDNN_PROPAGATE_NAN, CUDNN_NOT_PROPAGATE_NAN
                                            this->_kSizeY, this->_kSizeX, this->_paddingY, this->_paddingX, this->_strideY,this->_strideX));
#endif
#endif

    this->_bFlops            = (this->_kSizeX*this->_kSizeY* this->_channel*this->_outHeight*this->_outWidth)/ 1000000000.f;

    this->_maxOutputNum  = this->_batch*this->_outputNum;

    char msg[100];

    if(strideX == strideY)
    {
#ifdef WIN32
        sprintf_s(msg, "LocalAvgPool       %2dx%2d/%2d   %4d x%4d x%4d -> %4d x%4d x%4d %5.3f BF\n",
                  this->_kSizeX, this->_kSizeY, this->_strideX, this->_width, this->_height, this->_channel,
                  this->_outWidth, this->_outHeight,this->_outChannel,this->_bFlops);
#else
        sprintf(msg, "avg               %2dx%2d/%2d   %4d x%4d x%4d -> %4d x%4d x%4d %5.3f BF\n",
                this->_kSizeX, this->_kSizeY, this->_strideX, this->_width, this->_height, this->_channel,
                this->_outWidth, this->_outHeight,this->_outChannel,this->_bFlops);
#endif
    }
    else
    {
#ifdef WIN32
        sprintf_s(msg, "LocalAvgPool       %2dx%2d/%2dx%2d %4d x%4d x%4d -> %4d x%4d x%4d %5.3f BF\n",
                  this->_kSizeX, this->_kSizeY, this->_strideX,this->_strideY, this->_width, this->_height,
                  this->_channel, this->_outWidth, this->_outHeight,this->_outChannel,this->_bFlops);
#else
        sprintf(msg, "avg              %2dx%2d/%2dx%2d %4d x%4d x%4d -> %4d x%4d x%4d %5.3f BF\n",
                this->_kSizeX, this->_kSizeY, this->_strideX,this->_strideY, this->_width, this->_height,
                this->_channel, this->_outWidth, this->_outHeight,this->_outChannel,this->_bFlops);
#endif
    }

    this->_layerDetail       = msg;

}

LocalAvgPoolLayer::~LocalAvgPoolLayer()
{
#ifdef USE_GPU
#ifdef USE_CUDNN
    if(_inputDesc)
        CUDNN_CHECK(cudnnDestroyTensorDescriptor(_inputDesc));

    if(_outputDesc)
        CUDNN_CHECK(cudnnDestroyTensorDescriptor(_outputDesc));

    if(_localAvgPoolDesc)
        CUDNN_CHECK(cudnnDestroyPoolingDescriptor(_localAvgPoolDesc));
#endif
#endif
}

int LocalAvgPoolLayer::getKSizeX() const
{
    return _kSizeX;
}

int LocalAvgPoolLayer::getKSizeY() const
{
    return _kSizeY;
}

int LocalAvgPoolLayer::getStride() const
{
    return _stride;
}

int LocalAvgPoolLayer::getStrideX() const
{
    return _strideX;
}

int LocalAvgPoolLayer::getStrideY() const
{
    return _strideY;
}

int LocalAvgPoolLayer::getPaddingX() const
{
    return _paddingX;
}

int LocalAvgPoolLayer::getPaddingY() const
{
    return _paddingY;
}

int LocalAvgPoolLayer::getCeilMode() const
{
    return _ceilMode;
}

int LocalAvgPoolLayer::getAntialiasing() const
{
    return _antialiasing;
}

void LocalAvgPoolLayer::forward(NetworkState &netState)
{

    auto st = TimeUtil::startRecord();

    float* layerInput   = netState.getInput();
    float* layerOutput  = nullptr;

    /* 输入 */
    if(this->_isBranchLayer) //分支
    {
        if(this->_isFirstBranch)//分支第一个
        {
            layerInput      = netState.input;
        }
    }
    else
    {
        if(this->_layerIndex == 0) //第一层
        {
            layerInput      = netState.input;
        }
        else //不是第一层
        {
            if(netState.net->layers[this->_layerIndex - 1]->getMemReUse() == 0)//上层内存不复用
            {
                layerInput  = netState.input;
            }
        }
    }

    /* 输出 */
    if(this->_isBranchLayer) //分支,不管_memReUse
    {
        if(this->_isLastBranch)//分支第最后一个
        {
            layerOutput     = this->_output; //不复用
        }
        else //除最后一个branch层不复用外,其他层都复用
        {
            layerOutput     = netState.getOutput(); //此处的output将是下层的输入
            netState.shuffleInOut();//input改output, output改input
        }
    }
    else
    {
        if(this->_memReUse==1) //本层复用
        {
            layerOutput     = netState.getOutput(); //此处的output将是下层的输入
            netState.shuffleInOut();//input改output, output改input
        }
        else//本层不复用
        {
            layerOutput     = this->_output;
        }
    }

    int widthOffset  =     -this->_paddingX;
    int heightOffset =     -this->_paddingY;

    // 获取该层的输出width和height
    int mHeight         =   this->_outHeight;
    int mWidth          =   this->_outWidth;
    // 获取该层的输入通道数, 此处输入和输出相同
    int mChannel        =   this->_channel;

    for(int b=0; b<this->_batch; ++b)                // batch
    {
#ifdef USE_OMP
#pragma omp parallel for num_threads(OMP_THREAD)
#endif
        for(int k=0; k<mChannel; ++k)               // channel
        {
            for(int i=0; i<mHeight; ++i)            // height
            {
                for(int j=0; j<mWidth; ++j)         // width
                {
                    // 图片的索引
                    int outIndex = j + mWidth*(i + mHeight*(k + _channel*b));

                    float avg    = 0;

                    int counter  = 0;

                    for(int n=0; n<this->_kSizeY; ++n)
                    {
                        for(int m=0; m<this->_kSizeX; ++m)
                        {
                            // 当前层的索引h
                            int curHeight =  heightOffset + i*this->_strideY + n;
                            // 当前层的索引w
                            int curWidth  =  widthOffset  + j*this->_strideX + m;
                            // 总索引 (即输入的索引)
                            int index     =  curWidth + this->_width*(curHeight + this->_height*(k + b*this->_channel));

                            // 超界判定
                            bool valid    =  (curHeight >=0 && curHeight < this->_height &&
                                              curWidth  >=0 && curWidth  < this->_width);

                            // 在一个kernel范围,把所有的值进行累加
                            if(valid)
                            {
                                counter++;
                                avg += layerInput[index];
                            }
                        }
                    }

                    layerOutput[outIndex] = avg / counter;  //取平均
                }

            }
        }
    }

    this->_forwardTime =   TimeUtil::getElapsedTime(st);
    //std::vector<float> inputB{this->output, this->output + this->outputNum};
}

void LocalAvgPoolLayer::mallocMemory()
{
    if(!this->_memoryMalloced)
    {
        if(!BaseLayer::isPreviewMode)
        {
            if(!BaseLayer::onlyUseGpu) //不只是使用GPU的话,则需要分配CPU内存
            {
                //Mem this->_output         = new float[static_cast<size_t>(this->_outputNum * this->_batch)]();
                this->_output             = MemoryManager::effcientNew<float>(static_cast<size_t>(this->_outputNum * this->_batch));
            }
#ifdef USE_GPU
            if(!BaseLayer::onlyUseCpu)//不只是使用CPU的话,则需要分配GPU内存
            {
                this->_gpuOutput      = Cuda::mallocCudaArray(this->_outputNum * this->_batch);
            }
#endif
            this->_memoryMalloced  =  true;
        }
    }
    this->_memReUse         =  0;
}


#ifdef USE_GPU
void LocalAvgPoolLayer::forwardGPU(NetworkState &netState)
{
    this->recordCudaStart();

    float* layerGpuInput   = netState.getGpuInput();
    float* layerGpuOutput  = nullptr;

    /* 输入 */
    if(this->_isBranchLayer) //分支
    {
        if(this->_isFirstBranch)//分支第一个
        {
            layerGpuInput      = netState.input;
        }
    }
    else
    {
        if(this->_layerIndex == 0) //第一层
        {
            layerGpuInput      = netState.input;
        }
        else //不是第一层
        {
            if(netState.net->layers[this->_layerIndex - 1]->getMemReUse() == 0)//上层内存不复用
            {
                layerGpuInput  = netState.input;
            }
        }
    }

    /* 输出 */
    if(this->_isBranchLayer) //分支,不管_memReUse
    {
        if(this->_isLastBranch)//分支第最后一个
        {
            layerGpuOutput     = this->_gpuOutput; //不复用
        }
        else //除最后一个branch层不复用外,其他层都复用
        {
            layerGpuOutput     = netState.getGpuOutput(); //此处的output将是下层的输入
            netState.shuffleGpuInOut();//input改output, output改input
        }
    }
    else
    {
        if(this->_memReUse==1) //本层复用
        {
            layerGpuOutput     = netState.getGpuOutput(); //此处的output将是下层的输入
            netState.shuffleGpuInOut();//input改output, output改input
        }
        else//本层不复用
        {
            layerGpuOutput     = this->_gpuOutput;
        }
    }

#ifdef USE_CUDNN
    if(!onlyUseCuda)
    {
        float a = 1.f;
        float b = 0;
        CUDNN_CHECK(cudnnPoolingForward(Cuda::getCudnnHandle(), this->_localAvgPoolDesc, &a,
                                        this->_inputDesc, layerGpuInput,
                                        &b,
                                        this->_outputDesc, layerGpuOutput));
    }
    else
    {
        LocalAvgPoolLayerGPU::forwardNormalGPU(this->_width,this->_height,this->_channel,
                                               this->_outWidth, this->_outHeight, this->_outChannel,
                                               this->_strideX, this->_strideY,
                                               this->_kSizeX, this->_kSizeY,
                                               this->_paddingX, this->_paddingY,
                                               this->_batch,
                                               layerGpuInput,
                                               layerGpuOutput
                                               );
    }
#else
    LocalAvgPoolLayerGPU::forwardNormalGPU(this->_width,this->_height,this->_channel,
                                           this->_outWidth, this->_outHeight, this->_outChannel,
                                           this->_strideX, this->_strideY,
                                           this->_kSizeX, this->_kSizeY,
                                           this->_paddingX, this->_paddingY,
                                           this->_batch,
                                           layerGpuInput,
                                           layerGpuOutput
                                           );
#endif
    this->recordCudaStop();
}
#endif

}

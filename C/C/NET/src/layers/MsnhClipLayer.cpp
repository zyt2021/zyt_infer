#include "Msnhnet/layers/MsnhClipLayer.h"

namespace Msnhnet
{

ClipLayer::ClipLayer(const int &batch, const int &height, const int &width, const int &channel, const float &min, const float &max)
{
    this->_batch        = batch;
    this->_height       = height;
    this->_width        = width;
    this->_channel      = channel;

    this->_layerName    =  "Clip            ";
    this->_type         =  LayerType::CLIP;

    this->_min          = min;
    this->_max          = max;

    if(this->_min > this->_max)
    {
        throw Exception(1,"Clip min val must < max val [min:max]=[" + std::to_string(this->_min)+":"+std::to_string(this->_max)+"]\n",__FILE__, __LINE__, __FUNCTION__);
    }

    this->_outChannel   =   this->_channel;
    this->_outWidth     =   this->_width;
    this->_outHeight    =   this->_height;

    this->_inputNum     =   this->_width * this->_height * this->_channel;
    this->_outputNum    =   this->_outWidth * this->_outHeight * this->_outChannel;

    this->_maxOutputNum =   this->_batch*this->_outputNum;

    char msg[100];
#ifdef WIN32
    sprintf_s(msg, "Slice                        min = %4d, max = x%4d \n", this->_min, this->_max);
#else
    sprintf(msg, "Slice                        min = %4d, max = x%4d \n", this->_min, this->_max);
#endif
    this->_layerDetail = msg;
}

void ClipLayer::mallocMemory()
{
    if(!this->_memoryMalloced)
    {
        if(!BaseLayer::isPreviewMode)
        {
            if(!BaseLayer::onlyUseGpu) //不只是使用GPU的话,则需要分配CPU内存
            {
                //Mem this->_output        =   new float[static_cast<size_t>(this->_outputNum * this->_batch)]();
                this->_output         = MemoryManager::effcientNew<float>(static_cast<size_t>(this->_outputNum * this->_batch));
            }
#ifdef USE_GPU
            if(!BaseLayer::onlyUseCpu)//不只是使用CPU的话,则需要分配GPU内存
            {
                this->_gpuOutput =  Cuda::mallocCudaArray(this->_outputNum * this->_batch);
            }
#endif
            this->_memoryMalloced  =  true;
        }
    }
    this->_memReUse         =  0;
}

void ClipLayer::forward(NetworkState &netState)
{
    auto st = TimeUtil::startRecord();

    float* layerInput   = netState.getInput();
    float* layerOutput  = nullptr;

    if(this->_layerIndex == 0) //第一层
    {
        layerInput      = netState.input;
    }
    else //不是第一层
    {
        if(netState.net->layers[this->_layerIndex - 1]->getMemReUse() == 0)//上层内存不复用
        {
            layerInput  = netState.input;
        }
    }

    if(this->_memReUse==1) //本层复用
    {
        layerOutput     = netState.getOutput(); //此处的output将是下层的输入
        netState.shuffleInOut();//input改output, output改input
    }
    else//本层不复用
    {
        layerOutput     = this->_output;
    }

    for (int b = 0; b < this->_batch; ++b)
    {
#ifdef USE_OMP
#pragma omp parallel for num_threads(OMP_THREAD)
#endif
        for (int c = 0; c < this->_outChannel; ++c)
        {
            for (int h = 0; h < this->_outHeight; ++h)
            {
                for (int w = 0; w < this->_outWidth; ++w)
                {
                    int idx = b*this->_outChannel*this->_outHeight*this->_outWidth + c*this->_outHeight*this->_outWidth + h*this->_outWidth + w;
                    if(layerInput[idx]>this->_max)
                        layerOutput[idx] = this->_max;
                    else if(layerInput[idx]<this->_min)
                        layerOutput[idx] = this->_min;
                    else
                        layerOutput[idx] = layerInput[idx];
                }
            }
        }
    }

    this->_forwardTime = TimeUtil::getElapsedTime(st);
    return;
}

float ClipLayer::getMax() const
{
    return _max;
}

float ClipLayer::getMin() const
{
    return _min;
}

}


﻿#ifdef USE_X86

#include "Msnhnet/layers/x86/MsnhConvolution3x3LayerX86.h"

namespace Msnhnet
{

void Convolution3x3LayerX86::convolution3x3S1(float * const &src, const int &height, const int &width, const int &channel,
                                              float * &dst, const int &outHeight, const int &outWidth, const int &outChannel,
                                              float * const &kernel, const bool useFMA)
{
    int fastOutCh = (outChannel>>1)<<1;

    const int inSize        = width*height;
    const int outSize       = outWidth*outHeight;
    const int kernelSize    = 9;/* 3x3 */

    ///                                         ------
    ///                                         1 2 3 | (w3)
    ///                                      ------ 6 |                         1 2  (o3)
    ///                                      1 2 3 |9 |                         3 4
    ///                                      4 5 6 |
    ///                                      7 8 9 |
    ///
    ///         输入的channel            ------
    ///       -------------            1 2 3 | (w2)
    ///       1  2  3   4  |        ------ 6 |                            1 2  (o2)
    ///    -----------  8  |        1 2 3 |9 |                            3 4
    ///    1  2  3  4 | 12 |        4 5 6 |
    ///    5  6  7  8 | 16 |        7 8 9 |
    ///    9 10 11 12 |
    ///   13 14 15 16 |        ------
    ///                        1 2 3 | (w1)
    ///                     ------ 6 |                                1 2  (o1)
    ///                     1 2 3 |9 |                                3 4
    ///                     4 5 6 |
    ///                     7 8 9 |


    //    |---------|
    // -- x01 x02 x03 x04            --
    // |  x05 x06 x07 x08          | o1 o2
    // |  x09 x10 x11 x12  ====>>  | o3 o4
    // -- x13 x14 x15 x16            o5 o6
    //    x17 x18 x19 x20

    if(!useFMA)
    {

        //一次处理两个通道的输出, 即输入的4行
#if USE_OMP
#pragma omp parallel for num_threads(OMP_THREAD)
#endif
        for (int oc = 0; oc < fastOutCh; oc+=2)
        {
            float *dst1 = dst + oc*outSize;
            float *dst2 = dst + (oc+1)*outSize;
            float *kernel1 = kernel + oc*channel*kernelSize;
            float *kernel2 = kernel + (oc+1)*channel*kernelSize;

            //输入的channel
            for (int ic = 0; ic < channel; ++ic)
            {
                //一次处理output的两个channel
                float *dstPtr1 = dst1; //ch1的第1行
                float *dstPtr2 = dst2; //ch2的第1行

                //一次处理output的两个channel
                float *dstPtr1Next = dstPtr1 + outWidth; //ch1的第2行
                float *dstPtr2Next = dstPtr2 + outWidth; //ch2的第2行


                float* srcNow = src + ic*inSize;
                //一次src处理四行
                float* line0  = srcNow;
                float* line1  = srcNow + width;
                float* line2  = srcNow + width * 2;
                float* line3  = srcNow + width * 3;

                int oh = 0;
                for (; oh < outHeight-1; oh+=2)
                {
                    for (int ow = 0; ow < outWidth; ++ow)
                    {
                        float sum1      = 0.f; //第一个channel的第一行sum
                        float sum2      = 0.f; //下一个channel的第一行sum
                        float sum1Next  = 0.f; //第一个channel的下一行sum
                        float sum2Next  = 0.f; //下一个channel的下一行sum

                        //conv output1->chanel q output1
                        //  src      kernel1
                        // 第一行     kernel
                        sum1 += line0[0] * kernel1[0];  //   0         0     -|      ---
                        sum1 += line0[1] * kernel1[1];  //   1         1      |     |x x|
                        sum1 += line0[2] * kernel1[2];  //   2         2      |     |x x|
                        // 第二行              |      ---
                        sum1 += line1[0] * kernel1[3];  //   0         3      |    ---
                        sum1 += line1[1] * kernel1[4];  //   1         4      |-- |0 x|
                        sum1 += line1[2] * kernel1[5];  //   2         5      |   |x x|
                        // 第三行              |    ---
                        sum1 += line2[0] * kernel1[6];  //   0         6      |
                        sum1 += line2[1] * kernel1[7];  //   1         7      |
                        sum1 += line2[2] * kernel1[8];  //   2         8     -|

                        //conv output1->channel q output2
                        //  src      kernel2
                        // 第一行     kernel
                        sum2 += line0[0] * kernel2[0];  //   0         0      -|      ---
                        sum2 += line0[1] * kernel2[1];  //   1         1       |     |0 x|
                        sum2 += line0[2] * kernel2[2];  //   2         2       |     |x x|
                        // 第二行               |      ---
                        sum2 += line1[0] * kernel2[3];  //   0         3       |    ---
                        sum2 += line1[1] * kernel2[4];  //   1         4       |-- |x x|
                        sum2 += line1[2] * kernel2[5];  //   2         5       |   |x x|
                        // 第三行               |    ---
                        sum2 += line2[0] * kernel2[6];  //   0         6       |
                        sum2 += line2[1] * kernel2[7];  //   1         7       |
                        sum2 += line2[2] * kernel2[8];  //   2         8      -|

                        //conv output2->channel q output1
                        //  src      kernel1
                        // 第二行     kernel
                        sum1Next += line1[0] * kernel1[0];   //   0         0     -|      ---
                        sum1Next += line1[1] * kernel1[1];   //   1         1      |     |x x|
                        sum1Next += line1[2] * kernel1[2];   //   2         2      |     |x x|
                        // 第三行              |      ---
                        sum1Next += line2[0] * kernel1[3];   //   0         3      |    ---
                        sum1Next += line2[1] * kernel1[4];   //   1         4      |-- |x x|
                        sum1Next += line2[2] * kernel1[5];   //   2         5      |   |0 x|
                        // 第四行              |    ---
                        sum1Next += line3[0] * kernel1[6];   //   0         6      |
                        sum1Next += line3[1] * kernel1[7];   //   1         7      |
                        sum1Next += line3[2] * kernel1[8];   //   2         8     -|

                        //conv output2->channel q output2
                        //  src      kernel1
                        // 第二行     kernel
                        sum2Next += line1[0] * kernel2[0];   //   0         0     -|      ---
                        sum2Next += line1[1] * kernel2[1];   //   1         1      |     |x x|
                        sum2Next += line1[2] * kernel2[2];   //   2         2      |     |0 x|
                        // 第三行              |      ---
                        sum2Next += line2[0] * kernel2[3];   //   0         3      |    ---
                        sum2Next += line2[1] * kernel2[4];   //   1         4      |-- |x x|
                        sum2Next += line2[2] * kernel2[5];   //   2         5      |   |x x|
                        // 第四行              |    ---
                        sum2Next += line3[0] * kernel2[6];   //   0         6      |
                        sum2Next += line3[1] * kernel2[7];   //   1         7      |
                        sum2Next += line3[2] * kernel2[8];   //   2         8     -|

                        *(dstPtr1++)     += sum1;         //赋值累加
                        *(dstPtr2++)     += sum2;         //赋值累加
                        *(dstPtr1Next++) += sum1Next;     //赋值累加
                        *(dstPtr2Next++) += sum2Next;     //赋值累加

                        line0++;   //下一列
                        line1++;   //下一列
                        line2++;   //下一列
                        line3++;   //下一列
                    }

                    line0        += 2 + width; //src下一行
                    line1        += 2 + width; //src下一行
                    line2        += 2 + width; //src下一行
                    line3        += 2 + width; //src下一行

                    dstPtr1      += outWidth;
                    dstPtr2      += outWidth;
                    dstPtr1Next  += outWidth;
                    dstPtr2Next  += outWidth;
                }

                //最后一行
                for (; oh < outHeight; ++oh)
                {
                    for (int ow = 0; ow < outWidth; ++ow)
                    {
                        float sum1      = 0.f; //第一个channel的第一行sum
                        float sum2      = 0.f; //下一个channel的第一行sum

                        //conv output1->chanel q output1
                        sum1 += line0[0] * kernel1[0];
                        sum1 += line0[1] * kernel1[1];
                        sum1 += line0[2] * kernel1[2];
                        sum1 += line1[0] * kernel1[3];
                        sum1 += line1[1] * kernel1[4];
                        sum1 += line1[2] * kernel1[5];
                        sum1 += line2[0] * kernel1[6];
                        sum1 += line2[1] * kernel1[7];
                        sum1 += line2[2] * kernel1[8];

                        //conv output2->channel q output1
                        sum2 += line0[0] * kernel2[0];
                        sum2 += line0[1] * kernel2[1];
                        sum2 += line0[2] * kernel2[2];
                        sum2 += line1[0] * kernel2[3];
                        sum2 += line1[1] * kernel2[4];
                        sum2 += line1[2] * kernel2[5];
                        sum2 += line2[0] * kernel2[6];
                        sum2 += line2[1] * kernel2[7];
                        sum2 += line2[2] * kernel2[8];


                        *(dstPtr1++)     += sum1;         //赋值累加
                        *(dstPtr2++)     += sum2;         //赋值累加

                        line0++;   //下一列
                        line1++;   //下一列
                        line2++;   //下一列
                    }

                    line0        += 2;
                    line1        += 2;
                    line2        += 2;
                }

                kernel1 += kernelSize;//kernel的下一通道
                kernel2 += kernelSize;//kernel的下一通道
            }
        }
    }
    else
    {

        //一次处理两个通道的输出, 即输入的4行
#if USE_OMP
#pragma omp parallel for num_threads(OMP_THREAD)
#endif
        for (int oc = 0; oc < fastOutCh; oc+=2)
        {
            float *dst1 = dst + oc*outSize;
            float *dst2 = dst + (oc+1)*outSize;

            float *kernel1 = kernel + oc*channel*kernelSize;
            float *kernel2 = kernel + (oc+1)*channel*kernelSize;

            //输入的channel
            for (int ic = 0; ic < channel; ++ic)
            {
                M128 k012;
                M128 k345;
                M128 k678;
                M128 k012Next;
                M128 k345Next;
                M128 k678Next;

                k012.m128       = _mm_loadu_ps(kernel1);
                k345.m128       = _mm_loadu_ps(kernel1+3);
                k678.m128       = _mm_loadu_ps(kernel1+6);

                k012Next.m128   = _mm_loadu_ps(kernel2);
                k345Next.m128   = _mm_loadu_ps(kernel2+3);
                k678Next.m128   = _mm_loadu_ps(kernel2+6);

                k012.f32[3]     = 0;
                k345.f32[3]     = 0;
                k678.f32[3]     = 0;

                k012Next.f32[3] = 0;
                k345Next.f32[3] = 0;
                k678Next.f32[3] = 0;

                //一次处理output的两个channel
                float *dstPtr1 = dst1; //ch1的第1行
                float *dstPtr2 = dst2; //ch2的第1行

                //一次处理output的两个channel
                float *dstPtr1Next = dstPtr1 + outWidth; //ch1的第2行
                float *dstPtr2Next = dstPtr2 + outWidth; //ch2的第2行


                float* srcNow = src + ic*inSize;
                //一次src处理四行
                float* line0  = srcNow;
                float* line1  = srcNow + width;
                float* line2  = srcNow + width * 2;
                float* line3  = srcNow + width * 3;

                int oh = 0;
                for (; oh < outHeight-1; oh+=2)
                {
                    for (int ow = 0; ow < outWidth; ++ow)
                    {
                        float sum1      = 0.f; //第一个channel的第一行sum
                        float sum2      = 0.f; //下一个channel的第一行sum
                        float sum1Next  = 0.f; //第一个channel的下一行sum
                        float sum2Next  = 0.f; //下一个channel的下一行sum

                        M128 mmSum;
                        __m128 mmLine0  = _mm_loadu_ps(line0);
                        __m128 mmLine1  = _mm_loadu_ps(line1);
                        __m128 mmLine2  = _mm_loadu_ps(line2);
                        __m128 mmLine3  = _mm_loadu_ps(line3);


                        // xx = { xx3, xx2, xx1, xx0 }
                        // xx=_mm_hadd_ps(xx,xx);
                        // xx = {xx3+xx2, xx1+xx0, xx3+xx2, xx1+xx0}
                        // xx=_mm_hadd_ps(xx,xx);
                        // xx = {xx2+xx3+xx1+xx0, xx3+xx2+xx1+xx0, xx3+xx2+xx1+xx0, xx3+xx2+xx1+xx0}
                        //mmSum.m128 = _mm_add_ps(_mm_add_ps(_mm_mul_ps(mmLine0,k012.m128),_mm_mul_ps(mmLine1,k345.m128)),_mm_mul_ps(mmLine2,k678.m128));
                        mmSum.m128 = _mm_fmadd_ps(mmLine0,k012.m128,_mm_fmadd_ps(mmLine1,k345.m128,_mm_mul_ps(mmLine2,k678.m128)));
                        mmSum.m128 = _mm_hadd_ps(mmSum.m128,mmSum.m128);
                        mmSum.m128 = _mm_hadd_ps(mmSum.m128,mmSum.m128);
                        sum1       = mmSum.f32[0];

//                        mmSum.m128 = _mm_add_ps(_mm_add_ps(_mm_mul_ps(mmLine0,k012Next.m128),_mm_mul_ps(mmLine1,k345Next.m128)),_mm_mul_ps(mmLine2,k678Next.m128));
                        mmSum.m128 = _mm_fmadd_ps(mmLine0,k012Next.m128,_mm_fmadd_ps(mmLine1,k345Next.m128,_mm_mul_ps(mmLine2,k678Next.m128)));
                        mmSum.m128 = _mm_hadd_ps(mmSum.m128,mmSum.m128);
                        mmSum.m128 = _mm_hadd_ps(mmSum.m128,mmSum.m128);
                        sum2       = mmSum.f32[0];

                        //mmSum.m128 = _mm_add_ps(_mm_add_ps(_mm_mul_ps(mmLine1,k012.m128),_mm_mul_ps(mmLine2,k345.m128)),_mm_mul_ps(mmLine3,k678.m128));
                        mmSum.m128 = _mm_fmadd_ps(mmLine1,k012.m128,_mm_fmadd_ps(mmLine2,k345.m128,_mm_mul_ps(mmLine3,k678.m128)));
                        mmSum.m128 = _mm_hadd_ps(mmSum.m128,mmSum.m128);
                        mmSum.m128 = _mm_hadd_ps(mmSum.m128,mmSum.m128);
                        sum1Next   = mmSum.f32[0];

                        //mmSum.m128 = _mm_add_ps(_mm_add_ps(_mm_mul_ps(mmLine1,k012Next.m128),_mm_mul_ps(mmLine2,k345Next.m128)),_mm_mul_ps(mmLine3,k678Next.m128));
                        mmSum.m128 = _mm_fmadd_ps(mmLine1,k012Next.m128,_mm_fmadd_ps(mmLine2,k345Next.m128,_mm_mul_ps(mmLine3,k678Next.m128)));
                        mmSum.m128 = _mm_hadd_ps(mmSum.m128,mmSum.m128);
                        mmSum.m128 = _mm_hadd_ps(mmSum.m128,mmSum.m128);
                        sum2Next   = mmSum.f32[0];

                        *(dstPtr1++)     += sum1;         //赋值累加
                        *(dstPtr2++)     += sum2;         //赋值累加
                        *(dstPtr1Next++) += sum1Next;     //赋值累加
                        *(dstPtr2Next++) += sum2Next;     //赋值累加

                        line0++;   //下一列
                        line1++;   //下一列
                        line2++;   //下一列
                        line3++;   //下一列
                    }

                    line0        += 2 + width; //src下一行
                    line1        += 2 + width; //src下一行
                    line2        += 2 + width; //src下一行
                    line3        += 2 + width; //src下一行

                    dstPtr1      += outWidth;
                    dstPtr2      += outWidth;
                    dstPtr1Next  += outWidth;
                    dstPtr2Next  += outWidth;
                }

                //最后一行
                for (; oh < outHeight; ++oh)
                {
                    for (int ow = 0; ow < outWidth; ++ow)
                    {
                        float sum1      = 0.f; //第一个channel的第一行sum
                        float sum2      = 0.f; //下一个channel的第一行sum

                        M128 mmSum;
                        __m128 mmLine0;
                        __m128 mmLine1;
                        __m128 mmLine2;

                        mmLine0    = _mm_loadu_ps(line0);
                        mmLine1    = _mm_loadu_ps(line1);
                        mmLine2    = _mm_loadu_ps(line2);


                        //mmSum.m128 = _mm_add_ps(_mm_add_ps(_mm_mul_ps(mmLine0,k012.m128),_mm_mul_ps(mmLine1,k345.m128)),_mm_mul_ps(mmLine2,k678.m128));
                        mmSum.m128 = _mm_fmadd_ps(mmLine0,k012.m128,_mm_fmadd_ps(mmLine1,k345.m128,_mm_mul_ps(mmLine2,k678.m128)));
                        mmSum.m128 = _mm_hadd_ps(mmSum.m128,mmSum.m128);
                        mmSum.m128 = _mm_hadd_ps(mmSum.m128,mmSum.m128);
                        sum1       = mmSum.f32[0];

                        //mmSum.m128 = _mm_add_ps(_mm_add_ps(_mm_mul_ps(mmLine0,k012Next.m128),_mm_mul_ps(mmLine1,k345Next.m128)),_mm_mul_ps(mmLine2,k678Next.m128));
                        mmSum.m128 = _mm_fmadd_ps(mmLine0,k012Next.m128,_mm_fmadd_ps(mmLine1,k345Next.m128,_mm_mul_ps(mmLine2,k678Next.m128)));
                        mmSum.m128 = _mm_hadd_ps(mmSum.m128,mmSum.m128);
                        mmSum.m128 = _mm_hadd_ps(mmSum.m128,mmSum.m128);
                        sum2       = mmSum.f32[0];


                        *(dstPtr1++)     += sum1;         //赋值累加
                        *(dstPtr2++)     += sum2;         //赋值累加

                        line0++;   //下一列
                        line1++;   //下一列
                        line2++;   //下一列
                    }

                    line0        += 2;
                    line1        += 2;
                    line2        += 2;
                }

                kernel1 += kernelSize;//kernel的下一通道
                kernel2 += kernelSize;//kernel的下一通道
            }
        }
    }

}

void Convolution3x3LayerX86::convolution3x3S2(float * const &src, const int &height, const int &width, const int &channel,
                                              float *&dst, const int &outHeight, const int &outWidth, const int &outChannel,
                                              float * const &kernel, const bool useFMA)
{
    int fastOutCh = (outChannel>>1)<<1;

    const int inSize        = width*height;
    const int outSize       = outWidth*outHeight;
    const int kernelSize    = 9;/* 3x3 */

    ///                                         ------
    ///                                         1 2 3 | (w3)
    ///                                      ------ 6 |                         1 2  (o3)
    ///                                      1 2 3 |9 |                         3 4
    ///                                      4 5 6 |
    ///                                      7 8 9 |
    ///
    ///         输入的channel            ------
    ///       -------------            1 2 3 | (w2)
    ///       1  2  3   4  |        ------ 6 |                            1 2  (o2)
    ///    -----------  8  |        1 2 3 |9 |                            3 4
    ///    1  2  3  4 | 12 |        4 5 6 |
    ///    5  6  7  8 | 16 |        7 8 9 |
    ///    9 10 11 12 |
    ///   13 14 15 16 |        ------
    ///                        1 2 3 | (w1)
    ///                     ------ 6 |                                1 2  (o1)
    ///                     1 2 3 |9 |                                3 4
    ///                     4 5 6 |
    ///                     7 8 9 |


    //    |---------|
    // -- x01 x02 x03 x04            --
    // |  x05 x06 x07 x08          | o1 o2
    // |  x09 x10 x11 x12  ====>>  | o3 o4
    // -- x13 x14 x15 x16            o5 o6
    //    x17 x18 x19 x20

    if(!useFMA)
    {

        //一次处理两个通道的输出, 即输入的4行
#if USE_OMP
#pragma omp parallel for num_threads(OMP_THREAD)
#endif
        for (int oc = 0; oc < fastOutCh; oc+=2)
        {
            float *dst1 = dst + oc*outSize;
            float *dst2 = dst + (oc+1)*outSize;
            float *kernel1 = kernel + oc*channel*kernelSize;
            float *kernel2 = kernel + (oc+1)*channel*kernelSize;

            //输入的channel
            for (int ic = 0; ic < channel; ++ic)
            {
                //一次处理output的两个channel
                float *dstPtr1 = dst1; //ch1的第1行
                float *dstPtr2 = dst2; //ch2的第1行

                float* srcNow = src + ic*inSize;
                //一次src处理四行
                float* line0  = srcNow;
                float* line1  = srcNow + width;
                float* line2  = srcNow + width * 2;

                int oh = 0;
                for (; oh < outHeight; oh++)
                {
                    for (int ow = 0; ow < outWidth; ++ow)
                    {
                        float sum1      = 0.f; //第一个channel的第一行sum
                        float sum2      = 0.f; //下一个channel的第一行sum

                        //conv output1->chanel q output1
                        //  src      kernel1
                        // 第一行     kernel
                        sum1 += line0[0] * kernel1[0];  //   0         0     -|      ---
                        sum1 += line0[1] * kernel1[1];  //   1         1      |     |x x|
                        sum1 += line0[2] * kernel1[2];  //   2         2      |     |x x|
                        // 第二行              |      ---
                        sum1 += line1[0] * kernel1[3];  //   0         3      |    ---
                        sum1 += line1[1] * kernel1[4];  //   1         4      |-- |0 x|
                        sum1 += line1[2] * kernel1[5];  //   2         5      |   |x x|
                        // 第三行              |    ---
                        sum1 += line2[0] * kernel1[6];  //   0         6      |
                        sum1 += line2[1] * kernel1[7];  //   1         7      |
                        sum1 += line2[2] * kernel1[8];  //   2         8     -|

                        //conv output1->channel q output2
                        //  src      kernel2
                        // 第一行     kernel
                        sum2 += line0[0] * kernel2[0];  //   0         0      -|      ---
                        sum2 += line0[1] * kernel2[1];  //   1         1       |     |0 x|
                        sum2 += line0[2] * kernel2[2];  //   2         2       |     |x x|
                        // 第二行               |      ---
                        sum2 += line1[0] * kernel2[3];  //   0         3       |    ---
                        sum2 += line1[1] * kernel2[4];  //   1         4       |-- |x x|
                        sum2 += line1[2] * kernel2[5];  //   2         5       |   |x x|
                        // 第三行               |    ---
                        sum2 += line2[0] * kernel2[6];  //   0         6       |
                        sum2 += line2[1] * kernel2[7];  //   1         7       |
                        sum2 += line2[2] * kernel2[8];  //   2         8      -|

                        *(dstPtr1++)     += sum1;         //赋值累加
                        *(dstPtr2++)     += sum2;         //赋值累加

                        line0+=2;   //下一列
                        line1+=2;   //下一列
                        line2+=2;   //下一列
                    }

                    line0        += 2 * (width - outWidth); //src下一行
                    line1        += 2 * (width - outWidth); //src下一行
                    line2        += 2 * (width - outWidth); //src下一行
                }
                kernel1 += kernelSize;//kernel的下一通道
                kernel2 += kernelSize;//kernel的下一通道
            }
        }


#if USE_OMP
#pragma omp parallel for num_threads(OMP_THREAD)
#endif
        for (int oc = fastOutCh; oc < outChannel; oc++)
        {
            float *dst1 = dst + oc*outSize;
            float *kernel1 = kernel + oc*channel*kernelSize;

            //输入的channel
            for (int ic = 0; ic < channel; ++ic)
            {
                //一次处理output的两个channel
                float *dstPtr1 = dst1; //ch1的第1行

                float* srcNow = src + ic*inSize;
                //一次src处理四行
                float* line0  = srcNow;
                float* line1  = srcNow + width;
                float* line2  = srcNow + width * 2;

                int oh = 0;
                for (; oh < outHeight; oh++)
                {
                    for (int ow = 0; ow < outWidth; ++ow)
                    {
                        float sum1      = 0.f; //第一个channel的第一行sum

                        //conv output1->chanel q output1
                        //  src      kernel1
                        // 第一行     kernel
                        sum1 += line0[0] * kernel1[0];  //   0         0     -|      ---
                        sum1 += line0[1] * kernel1[1];  //   1         1      |     |x x|
                        sum1 += line0[2] * kernel1[2];  //   2         2      |     |x x|
                        // 第二行              |      ---
                        sum1 += line1[0] * kernel1[3];  //   0         3      |    ---
                        sum1 += line1[1] * kernel1[4];  //   1         4      |-- |0 x|
                        sum1 += line1[2] * kernel1[5];  //   2         5      |   |x x|
                        // 第三行              |    ---
                        sum1 += line2[0] * kernel1[6];  //   0         6      |
                        sum1 += line2[1] * kernel1[7];  //   1         7      |
                        sum1 += line2[2] * kernel1[8];  //   2         8     -|

                        *(dstPtr1++)     += sum1;         //赋值累加

                        line0+=2;   //下一列
                        line1+=2;   //下一列
                        line2+=2;   //下一列
                    }

                    line0        += 2 * (width - outWidth); //src下一行
                    line1        += 2 * (width - outWidth); //src下一行
                    line2        += 2 * (width - outWidth); //src下一行
                }
                kernel1 += kernelSize;//kernel的下一通道
            }
        }
    }
    else
    {
        //一次处理两个通道的输出, 即输入的4行
#if USE_OMP
#pragma omp parallel for num_threads(OMP_THREAD)
#endif
        for (int oc = 0; oc < fastOutCh; oc+=2)
        {
            float *dst1 = dst + oc*outSize;
            float *dst2 = dst + (oc+1)*outSize;
            float *kernel1 = kernel + oc*channel*kernelSize;
            float *kernel2 = kernel + (oc+1)*channel*kernelSize;

            //输入的channel
            for (int ic = 0; ic < channel; ++ic)
            {
                M128 k012;
                M128 k345;
                M128 k678;
                M128 k012Next;
                M128 k345Next;
                M128 k678Next;

                k012.m128       = _mm_loadu_ps(kernel1);
                k345.m128       = _mm_loadu_ps(kernel1+3);
                k678.m128       = _mm_loadu_ps(kernel1+6);

                k012Next.m128   = _mm_loadu_ps(kernel2);
                k345Next.m128   = _mm_loadu_ps(kernel2+3);
                k678Next.m128   = _mm_loadu_ps(kernel2+6);

                k012.f32[3]     = 0;
                k345.f32[3]     = 0;
                k678.f32[3]     = 0;

                k012Next.f32[3] = 0;
                k345Next.f32[3] = 0;
                k678Next.f32[3] = 0;

                //一次处理output的两个channel
                float *dstPtr1 = dst1; //ch1的第1行
                float *dstPtr2 = dst2; //ch2的第1行

                float* srcNow = src + ic*inSize;
                //一次src处理3行
                float* line0  = srcNow;
                float* line1  = srcNow + width;
                float* line2  = srcNow + width * 2;

                int oh = 0;
                for (; oh < outHeight; oh++)
                {
                    for (int ow = 0; ow < outWidth; ++ow)
                    {
                        float sum1      = 0.f; //第一个channel的第一行sum
                        float sum2      = 0.f; //下一个channel的第一行sum

                        M128 mmSum;
                        __m128 mmLine0  = _mm_loadu_ps(line0);
                        __m128 mmLine1  = _mm_loadu_ps(line1);
                        __m128 mmLine2  = _mm_loadu_ps(line2);

                        // xx = { xx3, xx2, xx1, xx0 }
                        // xx=_mm_hadd_ps(xx,xx);
                        // xx = {xx3+xx2, xx1+xx0, xx3+xx2, xx1+xx0}
                        // xx=_mm_hadd_ps(xx,xx);
                        // xx = {xx2+xx3+xx1+xx0, xx3+xx2+xx1+xx0, xx3+xx2+xx1+xx0, xx3+xx2+xx1+xx0}
                        //mmSum.m128 = _mm_add_ps(_mm_add_ps(_mm_mul_ps(mmLine0,k012.m128),_mm_mul_ps(mmLine1,k345.m128)),_mm_mul_ps(mmLine2,k678.m128));
                        mmSum.m128 = _mm_fmadd_ps(mmLine0,k012.m128,_mm_fmadd_ps(mmLine1,k345.m128,_mm_mul_ps(mmLine2,k678.m128)));
                        mmSum.m128 = _mm_hadd_ps(mmSum.m128,mmSum.m128);
                        mmSum.m128 = _mm_hadd_ps(mmSum.m128,mmSum.m128);
                        sum1       = mmSum.f32[0];

                        //mmSum.m128 = _mm_add_ps(_mm_add_ps(_mm_mul_ps(mmLine0,k012Next.m128),_mm_mul_ps(mmLine1,k345Next.m128)),_mm_mul_ps(mmLine2,k678Next.m128));
                        mmSum.m128 = _mm_fmadd_ps(mmLine0,k012Next.m128,_mm_fmadd_ps(mmLine1,k345Next.m128,_mm_mul_ps(mmLine2,k678Next.m128)));
                        mmSum.m128 = _mm_hadd_ps(mmSum.m128,mmSum.m128);
                        mmSum.m128 = _mm_hadd_ps(mmSum.m128,mmSum.m128);
                        sum2       = mmSum.f32[0];


                        *(dstPtr1++)     += sum1;         //赋值累加
                        *(dstPtr2++)     += sum2;         //赋值累加

                        line0+=2;   //下一列
                        line1+=2;   //下一列
                        line2+=2;   //下一列
                    }

                    line0        += 2 * (width - outWidth); //src下一行
                    line1        += 2 * (width - outWidth); //src下一行
                    line2        += 2 * (width - outWidth); //src下一行
                }
                kernel1 += kernelSize;//kernel的下一通道
                kernel2 += kernelSize;//kernel的下一通道
            }
        }


#if USE_OMP
#pragma omp parallel for num_threads(OMP_THREAD)
#endif
        for (int oc = fastOutCh; oc < outChannel; oc++)
        {
            float *dst1 = dst + oc*outSize;
            float *kernel1 = kernel + oc*channel*kernelSize;

            //输入的channel
            for (int ic = 0; ic < channel; ++ic)
            {

                M128 k012;
                M128 k345;
                M128 k678;

                k012.m128       = _mm_loadu_ps(kernel1);
                k345.m128       = _mm_loadu_ps(kernel1+3);
                k678.m128       = _mm_loadu_ps(kernel1+6);

                k012.f32[3]     = 0;
                k345.f32[3]     = 0;
                k678.f32[3]     = 0;

                //一次处理output的两个channel
                float *dstPtr1 = dst1; //ch1的第1行

                float* srcNow = src + ic*inSize;
                //一次src处理3行
                float* line0  = srcNow;
                float* line1  = srcNow + width;
                float* line2  = srcNow + width * 2;

                int oh = 0;
                for (; oh < outHeight; oh++)
                {
                    for (int ow = 0; ow < outWidth; ++ow)
                    {
                        float sum1      = 0.f; //第一个channel的第一行sum

                        M128 mmSum;
                        __m128 mmLine0  = _mm_loadu_ps(line0);
                        __m128 mmLine1  = _mm_loadu_ps(line1);
                        __m128 mmLine2  = _mm_loadu_ps(line2);

                        // xx = { xx3, xx2, xx1, xx0 }
                        // xx=_mm_hadd_ps(xx,xx);
                        // xx = {xx3+xx2, xx1+xx0, xx3+xx2, xx1+xx0}
                        // xx=_mm_hadd_ps(xx,xx);
                        // xx = {xx2+xx3+xx1+xx0, xx3+xx2+xx1+xx0, xx3+xx2+xx1+xx0, xx3+xx2+xx1+xx0}
                        //mmSum.m128 = _mm_add_ps(_mm_add_ps(_mm_mul_ps(mmLine0,k012.m128),_mm_mul_ps(mmLine1,k345.m128)),_mm_mul_ps(mmLine2,k678.m128));
                        mmSum.m128 = _mm_fmadd_ps(mmLine0,k012.m128,_mm_fmadd_ps(mmLine1,k345.m128,_mm_mul_ps(mmLine2,k678.m128)));
                        mmSum.m128 = _mm_hadd_ps(mmSum.m128,mmSum.m128);
                        mmSum.m128 = _mm_hadd_ps(mmSum.m128,mmSum.m128);
                        sum1       = mmSum.f32[0];
                        *(dstPtr1++)     += sum1;         //赋值累加

                        line0+=2;   //下一列
                        line1+=2;   //下一列
                        line2+=2;   //下一列
                    }

                    line0        += 2 * (width - outWidth); //src下一行
                    line1        += 2 * (width - outWidth); //src下一行
                    line2        += 2 * (width - outWidth); //src下一行
                }
                kernel1 += kernelSize;//kernel的下一通道
            }
        }
    }

}

}

#endif

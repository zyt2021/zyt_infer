﻿#include "Msnhnet/layers/MsnhMaxPoolLayer.h"

namespace Msnhnet
{
MaxPoolLayer::MaxPoolLayer(const int &batch,   const int &height, const int &width, const int &channel, const int &kSizeX,  const int &kSizeY,
                           const int &strideX, const int &strideY, const int &paddingX, const int &paddingY, const int &maxPoolDepth,
                           const int &outChannelsMp, const int& ceilMode,  const int &antialiasing)
{
    this->_type           = LayerType::MAXPOOL;
    this->_layerName      = "MaxPool         ";

    this->_batch          = batch;
    this->_height         = height;
    this->_width          = width;
    this->_channel        = channel;

    this->_paddingX       = paddingX;
    this->_paddingY       = paddingY;

    this->_ceilMode       = ceilMode;

    this->_maxPoolDepth   = maxPoolDepth;
    this->_outChannelsMp  = outChannelsMp;   //输出通道数
    this->_antialiasing   = antialiasing;

    this->_kSizeX         = kSizeX;  //kerner尺寸
    this->_kSizeY         = kSizeY;  //kerner尺寸

    this->_stride         = strideX; //步幅
    this->_strideX        = strideX;//X
    this->_strideY        = strideY;//Y

    if(maxPoolDepth)
    {
        this->_outChannel = outChannelsMp;
        this->_outWidth   = this->_width;
        this->_outHeight  = this->_height;
    }
    else
    {
        //涉及到padding的问题, 此处darknet采用向下取整, 同pytorch
        if(this->_ceilMode == 1)
        {
            int tmpW = (width  + paddingX*2 - kSizeX) % strideX; //输出图片宽度
            int tmpH = (height + paddingY*2 - kSizeY) % strideY; //输出图片高度

            if(tmpW >= kSizeX)
            {
                throw Exception(1,"maxpool padding error ", __FILE__, __LINE__, __FUNCTION__);
            }

            if(tmpH >= kSizeY)
            {
                throw Exception(1,"maxpool padding error ", __FILE__, __LINE__, __FUNCTION__);
            }

            if(tmpW <= paddingX)
            {
                this->_outWidth   = (width  + paddingX*2 - kSizeX) / strideX + 1; //输出图片宽度
            }
            else
            {
                this->_outWidth   = (width  + paddingX*2 - kSizeX) / strideX + 2; //输出图片宽度
            }

            if(tmpH <= paddingY)
            {
                this->_outHeight  = (height + paddingY*2 - kSizeY) / strideY + 1; //输出图片高度
            }
            else
            {
                this->_outHeight  = (height + paddingY*2 - kSizeY) / strideY + 2; //输出图片高度
            }
        }
        else if(this->_ceilMode == 0)
        {
            this->_outWidth   = (width  + paddingX*2 - kSizeX) / strideX + 1; //输出图片宽度
            this->_outHeight  = (height + paddingY*2 - kSizeY) / strideY + 1; //输出图片高度
        }
        else
        {
            this->_outWidth   = (width  + paddingX - kSizeX) / strideX + 1; //输出图片宽度
            this->_outHeight  = (height + paddingY - kSizeY) / strideY + 1; //输出图片高度
        }
        this->_outChannel = channel;                                 //输出图片通道数
    }

    this->_outputNum      =  this->_outHeight * this->_outWidth * this->_outChannel; //池化层输出元素个数
    this->_inputNum       =  height * width * channel; //池化层输入元素个数


#ifdef USE_GPU
#ifdef USE_CUDNN
    //创建tensor描述符
    CUDNN_CHECK(cudnnCreateTensorDescriptor(&this->_inputDesc));
    //设置描述符参数
    CUDNN_CHECK(cudnnSetTensor4dDescriptor(this->_inputDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, this->_batch, this->_channel, this->_height, this->_width));

    CUDNN_CHECK(cudnnCreateTensorDescriptor(&this->_outputDesc));
    CUDNN_CHECK(cudnnSetTensor4dDescriptor(this->_outputDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, this->_batch, this->_outChannel, this->_outHeight, this->_outWidth));

    CUDNN_CHECK(cudnnCreatePoolingDescriptor(&this->_maxPoolDesc));

    CUDNN_CHECK(cudnnSetPooling2dDescriptor(this->_maxPoolDesc, CUDNN_POOLING_MAX, CUDNN_NOT_PROPAGATE_NAN,  // CUDNN_PROPAGATE_NAN, CUDNN_NOT_PROPAGATE_NAN
                                            this->_kSizeY, this->_kSizeX, this->_paddingY, this->_paddingX, this->_strideY,this->_strideX));
#endif
#endif

    this->_bFlops            = (this->_kSizeX*this->_kSizeY* this->_channel*this->_outHeight*this->_outWidth)/ 1000000000.f;

    this->_maxOutputNum  = this->_batch*this->_outputNum;

    char msg[100];

    if(maxPoolDepth)
    {
#ifdef WIN32
        sprintf_s(msg, "MaxPooldepth      %2dx%2d/%2d   %4d x%4d x%4d -> %4d x%4d x%4d %5.3f BF\n",
                  this->_kSizeX, this->_kSizeY, this->_strideX, this->_width, this->_height, this->_channel,
                  this->_outWidth, this->_outHeight,this->_outChannel,this->_bFlops);
#else
        sprintf(msg, "max-depth         %2dx%2d/%2d   %4d x%4d x%4d -> %4d x%4d x%4d %5.3f BF\n",
                this->_kSizeX, this->_kSizeY, this->_strideX, this->_width, this->_height, this->_channel,
                this->_outWidth, this->_outHeight,this->_outChannel,this->_bFlops);
#endif
    }

    if(strideX == strideY)
    {
#ifdef WIN32
        sprintf_s(msg, "MaxPool           %2dx%2d/%2d   %4d x%4d x%4d -> %4d x%4d x%4d %5.3f BF\n",
                  this->_kSizeX, this->_kSizeY, this->_strideX, this->_width, this->_height, this->_channel,
                  this->_outWidth, this->_outHeight,this->_outChannel,this->_bFlops);
#else
        sprintf(msg, "max               %2dx%2d/%2d   %4d x%4d x%4d -> %4d x%4d x%4d %5.3f BF\n",
                this->_kSizeX, this->_kSizeY, this->_strideX, this->_width, this->_height, this->_channel,
                this->_outWidth, this->_outHeight,this->_outChannel,this->_bFlops);
#endif
    }
    else
    {
#ifdef WIN32
        sprintf_s(msg, "MaxPool          %2dx%2d/%2dx%2d %4d x%4d x%4d -> %4d x%4d x%4d %5.3f BF\n",
                  this->_kSizeX, this->_kSizeY, this->_strideX,this->_strideY, this->_width, this->_height,
                  this->_channel, this->_outWidth, this->_outHeight,this->_outChannel,this->_bFlops);
#else
        sprintf(msg, "max              %2dx%2d/%2dx%2d %4d x%4d x%4d -> %4d x%4d x%4d %5.3f BF\n",
                this->_kSizeX, this->_kSizeY, this->_strideX,this->_strideY, this->_width, this->_height,
                this->_channel, this->_outWidth, this->_outHeight,this->_outChannel,this->_bFlops);
#endif
    }

    this->_layerDetail       = msg;

}

void MaxPoolLayer::forward(NetworkState &netState)
{

    auto st = TimeUtil::startRecord();

    float* layerInput   = netState.getInput();
    float* layerOutput  = nullptr;

    /* 输入 */
    if(this->_isBranchLayer) //分支
    {
        if(this->_isFirstBranch)//分支第一个
        {
            layerInput      = netState.input;
        }
    }
    else
    {
        if(this->_layerIndex == 0) //第一层
        {
            layerInput      = netState.input;
        }
        else //不是第一层
        {
            if(netState.net->layers[this->_layerIndex - 1]->getMemReUse() == 0)//上层内存不复用
            {
                layerInput  = netState.input;
            }
        }
    }

    /* 输出 */
    if(this->_isBranchLayer) //分支,不管_memReUse
    {
        if(this->_isLastBranch)//分支第最后一个
        {
            layerOutput     = this->_output; //不复用
        }
        else //除最后一个branch层不复用外,其他层都复用
        {
            layerOutput     = netState.getOutput(); //此处的output将是下层的输入
            netState.shuffleInOut();//input改output, output改input
        }
    }
    else
    {
        if(this->_memReUse==1) //本层复用
        {
            layerOutput     = netState.getOutput(); //此处的output将是下层的输入
            netState.shuffleInOut();//input改output, output改input
        }
        else//本层不复用
        {
            layerOutput     = this->_output;
        }
    }


    if(this->_maxPoolDepth)
    {
        for(int b=0; b<this->_batch; ++b)                    // bacth
        {
#ifdef USE_OMP
#pragma omp parallel for num_threads(OMP_THREAD)
#endif                                                      // cache miss
            for(int i=0; i<this->_height; i++)               // height
            {
                for(int j=0; j<this->_width; ++j)            // width
                {
                    for(int g=0; g<this->_outChannel; ++g)   // channel
                    {
                        int outIndex = j + this->_width*(i + this->_height*(g + this->_outChannel*b));
                        float max    = -FLT_MAX;
                        //每次间隔outChannl进行一次池化
                        for(int k=g; k<this->_channel; k+=this->_outChannel)
                        {
                            int inIndex = j + this->_width*(i + this->_height*(k + this->_channel*b));
                            float val   = layerInput[inIndex];

                            //跟新最大值的索引
                            max         = (val > max)? val:max;
                        }

                        layerOutput[outIndex] = max;
                    }

                }

            }
        }
        this->_forwardTime = TimeUtil::getElapsedTime(st);
        return;
    }
#ifdef USE_X86
    if((this->_strideX == this->_strideY) && !supportAvx )
    {
        std::cout<<"avx"<<std::endl;
        forwardAvx(layerInput,layerOutput,this->_kSizeX, this->_kSizeY, this->_width,this->_height,this->_outWidth,
                   this->_outHeight,this->_channel,this->_paddingX, this->_paddingY,this->_stride,this->_batch);
    }
    else
#endif
    {
        std::cout<<"---avx"<<std::endl;
        int widthOffset  =     -this->_paddingX;
        int heightOffset =     -this->_paddingY;

        // 获取该层的输出width和height
        int mHeight         =   this->_outHeight;
        int mWidth          =   this->_outWidth;
        // 获取该层的输入通道数, 此处输入和输出相同
        int mChannel        =   this->_channel;

        for(int b=0; b<this->_batch; ++b)                // batch
        {
            for(int k=0; k<mChannel; ++k)               // channel
            {
#ifdef USE_OMP
#pragma omp parallel for num_threads(OMP_THREAD)
#endif
                for(int i=0; i<mHeight; ++i)            // height
                {
                    for(int j=0; j<mWidth; ++j)         // width
                    {
                        // 图片的索引
                        int outIndex = j + mWidth*(i + mHeight*(k + _channel*b));
                        float max    = -FLT_MAX;

                        for(int n=0; n<this->_kSizeY; ++n)
                        {
                            for(int m=0; m<this->_kSizeX; ++m)
                            {
                                // 当前层的索引h
                                int curHeight =  heightOffset + i*this->_strideY + n;
                                // 当前层的索引w
                                int curWidth  =  widthOffset  + j*this->_strideX + m;
                                // 总索引
                                int index     =  curWidth + this->_width*(curHeight + this->_height*(k + b*this->_channel));

                                // 超界判定
                                bool valid    =  (curHeight >=0 && curHeight < this->_height &&
                                                  curWidth  >=0 && curWidth  < this->_width);
                                // 超界部分补负无穷
                                float value   =  (valid)? layerInput[index] : -FLT_MAX;

                                // 最大值
                                max           =  (value > max) ? value : max;
                            }
                        }

                        // 得到值
                        layerOutput[outIndex] = max;
                    }
                }
            }
        }
    }
    //std::vector<float> aaa{this->output, this->output+this->outputNum};

    this->_forwardTime = TimeUtil::getElapsedTime(st);

}

void MaxPoolLayer::mallocMemory()
{
    if(!this->_memoryMalloced)
    {
        if(!BaseLayer::isPreviewMode)
        {
            //输出
            if(!BaseLayer::onlyUseGpu) //不只是使用GPU的话,则需要分配CPU内存
            {
                //Mem this->_output         = new float[static_cast<size_t>(this->_outputNum * this->_batch)]();
                this->_output             = MemoryManager::effcientNew<float>(static_cast<size_t>(this->_outputNum * this->_batch));
            }
#ifdef USE_GPU
            if(!BaseLayer::onlyUseCpu)//不只是使用CPU的话,则需要分配GPU内存
            {
                this->_gpuOutput      = Cuda::mallocCudaArray(this->_outputNum * this->_batch);
            }
#endif
            this->_memoryMalloced  =  true;
        }
    }
    this->_memReUse         =  0;
}

#ifdef USE_GPU
void MaxPoolLayer::forwardGPU(NetworkState &netState)
{
    this->recordCudaStart();

    float* layerGpuInput   = netState.getGpuInput();
    float* layerGpuOutput  = nullptr;

    /* 输入 */
    if(this->_isBranchLayer) //分支
    {
        if(this->_isFirstBranch)//分支第一个
        {
            layerGpuInput      = netState.input;
        }
    }
    else
    {
        if(this->_layerIndex == 0) //第一层
        {
            layerGpuInput      = netState.input;
        }
        else //不是第一层
        {
            if(netState.net->layers[this->_layerIndex - 1]->getMemReUse() == 0)//上层内存不复用
            {
                layerGpuInput  = netState.input;
            }
        }
    }

    /* 输出 */
    if(this->_isBranchLayer) //分支,不管_memReUse
    {
        if(this->_isLastBranch)//分支第最后一个
        {
            layerGpuOutput     = this->_gpuOutput; //不复用
        }
        else //除最后一个branch层不复用外,其他层都复用
        {
            layerGpuOutput     = netState.getGpuOutput(); //此处的output将是下层的输入
            netState.shuffleGpuInOut();//input改output, output改input
        }
    }
    else
    {
        if(this->_memReUse==1) //本层复用
        {
            layerGpuOutput     = netState.getGpuOutput(); //此处的output将是下层的输入
            netState.shuffleGpuInOut();//input改output, output改input
        }
        else//本层不复用
        {
            layerGpuOutput     = this->_gpuOutput;
        }
    }

    if(this->_maxPoolDepth)
    {
        MaxPoolLayerGPU::forwardDepthGPU(this->_width, this->_height, this->_channel, this->_outWidth, this->_outHeight, this->_outChannel, this->_batch, layerGpuInput, layerGpuOutput);
    }
    else
    {
#ifdef USE_CUDNN
        if(!onlyUseCuda)
        {
            float a = 1.f;
            float b = 0;
            CUDNN_CHECK(cudnnPoolingForward(Cuda::getCudnnHandle(), this->_maxPoolDesc, &a,
                                            this->_inputDesc, layerGpuInput,
                                            &b,
                                            this->_outputDesc, layerGpuOutput));
        }
        else
        {
            MaxPoolLayerGPU::forwardNormalGPU(this->_width,this->_height,this->_channel,
                                              this->_outWidth, this->_outHeight, this->_outChannel,
                                              this->_strideX, this->_strideY,
                                              this->_kSizeX, this->_kSizeY,
                                              this->_paddingX, this->_paddingY,
                                              this->_batch,
                                              layerGpuInput,
                                              layerGpuOutput
                                              );
        }

#else
        MaxPoolLayerGPU::forwardNormalGPU(this->_width,this->_height,this->_channel,
                                          this->_outWidth, this->_outHeight, this->_outChannel,
                                          this->_strideX, this->_strideY,
                                          this->_kSizeX, this->_kSizeY,
                                          this->_paddingX, this->_paddingY,
                                          this->_batch,
                                          layerGpuInput,
                                          layerGpuOutput
                                          );
#endif
    }

    this->recordCudaStop();
}
#endif

#ifdef USE_X86
void MaxPoolLayer::forwardAvx(float *const &src, float *const &dst, const int &kSizeX, const int &kSizeY,
                              const int &width, const int &height, const int &outWidth, const int &outHeight,
                              const int &channel,const int &paddingX,const int &paddingY,const int &stride, const int &batch)
{

    int widthOffset  =     -paddingX ;
    int heightOffset =     -paddingY ;

    for(int b=0; b<batch; ++b)
    {
#ifdef USE_OMP
#pragma omp parallel for num_threads(OMP_THREAD)
#endif
        for(int k=0; k<channel; ++k)
        {
            for(int i=0; i<outHeight; ++i)
            {
                int j = 0;
                if(stride == 1)  // strideX == strideY
                {
                    for(j=0; j<outWidth - 8 -(kSizeX - 1); j+=8)  //特殊情况 stride == 1的时候 每8个进行处理
                    {
                        int outIndex = j + outWidth*(i + outHeight*(k + channel*b));
                        __m256 max256    = _mm256_set1_ps(-FLT_MAX);
                        for(int n=0; n<kSizeY; ++n)  //bug check kSizeY
                        {
                            for(int m=0; m<kSizeX; ++m) //bug check kSizeX
                            {
                                int curHeight = heightOffset + i*stride + n;
                                int curWidth  = widthOffset  + j*stride + m;
                                int index     = curWidth +width*(curHeight + height*(k + b*channel));

                                int valid     = (curHeight >=0 && curHeight < height && curWidth>=0 && curWidth<width );

                                if(!valid) continue;

                                __m256 src256 = _mm256_loadu_ps(&src[index]);

                                max256        = _mm256_max_ps(src256,max256);
                            }
                        }

                        _mm256_storeu_ps(&dst[outIndex], max256);

                    }
                }
                else if(kSizeX == 2 && kSizeX == 2 && stride == 2)  //特殊情况 stride == 2 && kSize == 2的时候 每4个进行处理
                {
                    for(j=0; j<outWidth-4; j+=4)
                    {
                        int outIndex = j + outWidth * (i + outHeight*(k + channel*b));

                        __m128 max128 = _mm_set1_ps(-FLT_MAX);

                        for(int n=0; n<kSizeX; ++n)
                        {
                            int m = 0;
                            {
                                int curHeight  = heightOffset + i * stride + n;
                                int curWidth   = widthOffset  + j * stride + m;

                                int index      = curWidth + width*(curHeight + height*(k + b*channel));
                                int valid      = (curHeight >=0 && curHeight < height && curWidth>=0 && curWidth<width );

                                if (!valid) continue;


                                // eg:  1 2 3 4 5 6 7 8
                                //      3 4 5 6 7 8 9 2
                                //            ==  ==  ==  ==
                                // n = 0 比较 1 2 3 4 5 6 7 8
                                // n = 1 比较 3 4 5 6 7 8 9 2

                                //一次处理两个float, n来处理行

                                //   10110001         10110001
                                // =============   =============
                                // 1   2   3   4   5   6   7   8    ---
                                // |   |   |　 |   |   |   |   |       | 重排,比较, trick
                                // 2   1   4   3   6   5   8   7    <--

                                __m256 src256   = _mm256_loadu_ps(&src[index]);
                                //                                00110001;
                                __m256 src256_2 = _mm256_permute_ps(src256, 0b10110001);
                                __m256 max256   = _mm256_max_ps(src256, src256_2);

                                __m128 src128_0 = _mm256_extractf128_ps(max256, 0);
                                __m128 src128_1 = _mm256_extractf128_ps(max256, 1);

                                //               00   01   10   11
                                // float A[] = {1.f, 2.f, 3.f, 4.f};
                                // float B[] = {5.f, 6.f, 7.f, 8.f};
                                //
                                // 高======>低
                                // 10001000
                                // __m128 src256_2 = _mm_shuffle_ps(A1,B1,0b10001000);
                                // 低=======>高
                                // => 1 3 5 7

                                //间隔提取, 因为{2 2 4 4} {6 6 8 8}
                                // =>{2 4 6 8}
                                // 10001000
                                __m128 src128   = _mm_shuffle_ps(src128_0, src128_1, 0b10001000);

                                // 4个结果
                                max128 = _mm_max_ps(src128, max128);

                            }
                        }

                        _mm_storeu_ps(&dst[outIndex], max128);
                    }
                }

                for(; j<outWidth; ++j)       // 每行剩下的使用默认的方法
                {
                    int outIndex  =  j + outWidth * (i + outHeight * (k + channel * b));
                    float max     =  -FLT_MAX;
                    int maxIndex  =  -1;

                    for(int n=0; n<kSizeY; ++n)
                    {
                        for(int m=0; m<kSizeX; ++m)
                        {
                            int curHeight    = heightOffset + i*stride + n;
                            int curWidth     = widthOffset  + j*stride + m;

                            int index        = curWidth + width*(curHeight + height*(k + b*channel));
                            int valid        = (curHeight >=0 && curHeight < height && curWidth>=0 && curWidth<width );

                            float value      = (valid !=0)?src[index]: -FLT_MAX;

                            maxIndex         = (value > max)?index:maxIndex;
                            max              = (value > max)?value:max;
                        }
                    }

                    dst[outIndex]  = max;
                }
            }
        }
    }
}
#endif

MaxPoolLayer::~MaxPoolLayer()
{
#ifdef USE_GPU
#ifdef USE_CUDNN
    if(_inputDesc)
        CUDNN_CHECK(cudnnDestroyTensorDescriptor(_inputDesc));

    if(_outputDesc)
        CUDNN_CHECK(cudnnDestroyTensorDescriptor(_outputDesc));

    if(_maxPoolDesc)
        CUDNN_CHECK(cudnnDestroyPoolingDescriptor(_maxPoolDesc));
#endif
#endif
}

int MaxPoolLayer::getKSizeX() const
{
    return _kSizeX;
}

int MaxPoolLayer::getKSizeY() const
{
    return _kSizeY;
}

int MaxPoolLayer::getStride() const
{
    return _stride;
}

int MaxPoolLayer::getStrideX() const
{
    return _strideX;
}

int MaxPoolLayer::getStrideY() const
{
    return _strideY;
}

int MaxPoolLayer::getPaddingX() const
{
    return _paddingX;
}

int MaxPoolLayer::getPaddingY() const
{
    return _paddingY;
}

int MaxPoolLayer::getAntialiasing() const
{
    return _antialiasing;
}

int MaxPoolLayer::getMaxPoolDepth() const
{
    return _maxPoolDepth;
}

int MaxPoolLayer::getOutChannelsMp() const
{
    return _outChannelsMp;
}

int MaxPoolLayer::getCeilMode() const
{
    return _ceilMode;
}

}

﻿#include "Msnhnet/layers/MsnhPaddingLayer.h"
namespace Msnhnet
{

PaddingLayer::PaddingLayer(const int &batch, const int &height, const int &width, const int &channel, const int &top,
                           const int &down, const int &left, const int &right, const float &paddingVal)
{
    this->_type          =   LayerType::PADDING;
    this->_layerName     =   "Padding         ";
    this->_batch         =   batch;
    this->_height        =   height;
    this->_width         =   width;
    this->_channel       =   channel;

    this->_inputNum      =   width * height * channel;

    this->_top           =   top;
    this->_down          =   down;
    this->_left          =   left;
    this->_right         =   right;

    this->_paddingVal    =   paddingVal;

    this->_outHeight     =   this->_height + this->_top + this->_down;
    this->_outWidth      =   this->_width   + this->_left + this->_right;
    this->_outChannel    =   this->_channel;

    this->_outputNum     =   this->_outHeight * this->_outWidth * this->_outChannel;

    char msg[100];

    this->_maxOutputNum  = this->_batch*this->_outputNum;

#ifdef WIN32
    sprintf_s(msg, "Padding                      %4d x%4d x%4d -> %4d x%4d x%4d %5.3f BF\n", this->_width, this->_height, this->_channel,
              this->_outWidth, this->_outHeight, this->_outChannel, this->_bFlops);
#else
    sprintf(msg, "Padding                      %4d x%4d x%4d -> %4d x%4d x%4d %5.3f BF\n", this->_width, this->_height, this->_channel,
            this->_outWidth, this->_outHeight, this->_outChannel, this->_bFlops);
#endif
    this->_layerDetail = msg;
}

void PaddingLayer::mallocMemory()
{
    if(!this->_memoryMalloced)
    {
        if(!BaseLayer::isPreviewMode)
        {
            if(!BaseLayer::onlyUseGpu) //不只是使用GPU的话,则需要分配CPU内存
            {
                //Mem this->_output        =   new float[static_cast<size_t>(this->_outputNum * this->_batch)]();
                this->_output         = MemoryManager::effcientNew<float>(static_cast<size_t>(this->_outputNum * this->_batch));
            }
#ifdef USE_GPU
            if(!BaseLayer::onlyUseCpu)//不只是使用CPU的话,则需要分配GPU内存
            {
                this->_gpuOutput     =   Cuda::mallocCudaArray(this->_outputNum * this->_batch);
            }
#endif
            this->_memoryMalloced  =  true;
        }
    }
    this->_memReUse         =  0;
}

void PaddingLayer::forward(NetworkState &netState)
{
    auto st = TimeUtil::startRecord();

    float* layerInput   = netState.getInput();
    float* layerOutput  = nullptr;

    /* 输入 */
    if(this->_isBranchLayer) //分支
    {
        if(this->_isFirstBranch)//分支第一个
        {
            layerInput      = netState.input;
        }
    }
    else
    {
        if(this->_layerIndex == 0) //第一层
        {
            layerInput      = netState.input;
        }
        else //不是第一层
        {
            if(netState.net->layers[this->_layerIndex - 1]->getMemReUse() == 0)//上层内存不复用
            {
                layerInput  = netState.input;
            }
        }
    }

    /* 输出 */
    if(this->_isBranchLayer) //分支,不管_memReUse
    {
        if(this->_isLastBranch)//分支第最后一个
        {
            layerOutput     = this->_output; //不复用
        }
        else //除最后一个branch层不复用外,其他层都复用
        {
            layerOutput     = netState.getOutput(); //此处的output将是下层的输入
            netState.shuffleInOut();//input改output, output改input
        }
    }
    else
    {
        if(this->_memReUse==1) //本层复用
        {
            layerOutput     = netState.getOutput(); //此处的output将是下层的输入
            netState.shuffleInOut();//input改output, output改input
        }
        else//本层不复用
        {
            layerOutput     = this->_output;
        }
    }

#ifdef USE_ARM1/*TODO*/
    PaddingLayerArm::padding(layerInput, this->_width, this->_height, this->_channel, layerOutput, this->_top, this->_down, this->_left, this->_right, this->_paddingVal);
#else
    for (int i = 0; i < this->_batch; ++i)
    {
#ifdef USE_OMP
#pragma omp parallel for num_threads(OMP_THREAD)
#endif
        for (int j = 0; j < this->_outChannel; ++j)
        {
            for (int m = 0; m < this->_outHeight; ++m)
            {
                for (int n = 0; n < this->_outWidth; ++n)
                {
                    float val = 0;

                    //   00000
                    //   01120
                    //   02230 <- m = 2   (height + top)=3
                    //   00000 <- m = 3


                    if(m < this->_top || (m >= (this->_height + this->_top)))
                    {
                        val     =   this->_paddingVal;
                    }
                    else
                    {
                        if(n < this->_left || (n >= (this->_width + this->_left)))
                        {
                            val     =   this->_paddingVal;
                        }
                        else
                        {
                            val     =   layerInput[ i*this->_channel*this->_height*this->_width + j*this->_height*this->_width + (m-this->_top)*this->_width + (n - this->_left)];
                        }
                    }


                    layerOutput[i*this->_outChannel*this->_outHeight*this->_outWidth + j*this->_outHeight*this->_outWidth + m*this->_outWidth + n] = val;

                }
            }
        }
    }
#endif

    this->_forwardTime = TimeUtil::getElapsedTime(st);
    // std::vector<float> aaa{this->output, this->output+this->outputNum};
}

#ifdef USE_GPU
void PaddingLayer::forwardGPU(NetworkState &netState)
{
    this->recordCudaStart();

    float* layerGpuInput   = netState.getGpuInput();
    float* layerGpuOutput  = nullptr;

    /* 输入 */
    if(this->_isBranchLayer) //分支
    {
        if(this->_isFirstBranch)//分支第一个
        {
            layerGpuInput      = netState.input;
        }
    }
    else
    {
        if(this->_layerIndex == 0) //第一层
        {
            layerGpuInput      = netState.input;
        }
        else //不是第一层
        {
            if(netState.net->layers[this->_layerIndex - 1]->getMemReUse() == 0)//上层内存不复用
            {
                layerGpuInput  = netState.input;
            }
        }
    }

    /* 输出 */
    if(this->_isBranchLayer) //分支,不管_memReUse
    {
        if(this->_isLastBranch)//分支第最后一个
        {
            layerGpuOutput     = this->_gpuOutput; //不复用
        }
        else //除最后一个branch层不复用外,其他层都复用
        {
            layerGpuOutput     = netState.getGpuOutput(); //此处的output将是下层的输入
            netState.shuffleGpuInOut();//input改output, output改input
        }
    }
    else
    {
        if(this->_memReUse==1) //本层复用
        {
            layerGpuOutput     = netState.getGpuOutput(); //此处的output将是下层的输入
            netState.shuffleGpuInOut();//input改output, output改input
        }
        else//本层不复用
        {
            layerGpuOutput     = this->_gpuOutput;
        }
    }

    PaddingLayerGPU::forwardNormalGPU(this->_batch, this->_outChannel, this->_outHeight, this->_outWidth,
                                      this->_height, this->_width, this->_channel,
                                      this->_top, this->_left,
                                      this->_paddingVal,
                                      layerGpuInput,
                                      layerGpuOutput
                                      );
    this->recordCudaStop();
}
#endif

int PaddingLayer::getTop() const
{
    return _top;
}

int PaddingLayer::getDown() const
{
    return _down;
}

int PaddingLayer::getLeft() const
{
    return _left;
}

int PaddingLayer::getRight() const
{
    return _right;
}

float PaddingLayer::getPaddingVal() const
{
    return _paddingVal;
}

}

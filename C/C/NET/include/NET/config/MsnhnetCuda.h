﻿#ifndef MSNHNETCUDA_H
#define MSNHNETCUDA_H

#include <cuda_runtime.h>
#include <cublas_v2.h>
#include <cuda.h>
#include "Msnhnet/config/MsnhnetCfg.h"
#include <string>
#include <iostream>
#include <algorithm>
#include <math.h>

#ifdef USE_CUDNN
//#include <cudnn_version.h>
// MsnhRobotStudio
#include "cudnn_version.h"
#include "cudnn_ops_infer.h"
#include "cudnn_adv_infer.h"
#include "cudnn_cnn_infer.h"
#include "cudnn_backend.h"

//commo
#include "cudnn.h"
#endif

//<<<>>>运算符对kernel函数完整的执行配置参数形式是<<<Dg, Db, Ns, S>>>
// 1、参数Dg用于定义整个grid的维度和尺寸，即一个grid有多少个block。为dim3类型。
//    Dim3 Dg(Dg.x, Dg.y, 1)表示grid中每行有Dg.x个block，每列有Dg.y个block，
//    第三维恒为1(目前一个核函数只有一个grid)。整个grid中共有Dg.x*Dg.y个block，其中Dg.x和Dg.y最大值为65535。
// 2、参数Db用于定义一个block的维度和尺寸，即一个block有多少个thread。为dim3类型。
//    Dim3 Db(Db.x, Db.y, Db.z)表示整个block中每行有Db.x个thread，每列有Db.y个thread，
//    高度为Db.z。Db.x和Db.y最大值为512，Db.z最大值为62。 一个block中共有Db.x*Db.y*Db.z个thread。
//    计算能力为1.0,1.1的硬件该乘积的最大值为768，计算能力为1.2,1.3的硬件支持的最大值为1024。
// 3、参数Ns是一个可选参数，用于设置每个block除了静态分配的shared Memory以外，最多能动态分配的shared memory大小，
//    单位为byte。不需要动态分配时该值为0或省略不写。
// 4、参数S是一个cudaStream_t类型的可选参数，初始值为零，表示该核函数处在哪个流之中。

// GPU硬件知识：Grid网格(每个显卡的个数)-》block-》线程
// 1、Grid：一个Grid代表一块GPU芯片，所有的线程共享显存数据；每个grid就相当于一块显卡。 grid 数量取决于你创建多少个 stream
// 2、Block：在每一个GPU芯片里面包含着多个block,每个block包含了512或者1024个线程。
//    每个线程的ID号可以通过一维0~1024索引，也可以通过二维dx*dy=1024索引，或者通过三维dx*dy*dz=1024。
//    这个就像图像opencv访问某个像素点一样，可以通过一维访问、或者二维访问：i+width*j。每个块里各自有一个
//    共享数据存储的区域，只有块内的线程可以访问；在一个块内，共享变量的修改，可能需要用到等待所有的线程处理
//    完毕，然后再修改共享变量，可以采用syncthreads（）函数用于等待。
// 3、Thread：每个block包含多个thread。

//GPU存储空间：
//1、block中的每个线程都有自己的寄存器和local memory；
//2、block中的所有线程共享一个shared memory；
//3、一个grid共享一个global memory（或者称之为显存）、常量存储器、纹理存储器。

//CUDA:
//1、GPU称之为设备device；device(0)表示设置显卡号码，多显卡，在CUDA程序中，
//   我们可以采用:cudaSetDevice(0)函数，表示选用第一块显卡进行计算
//2、CPU称之为主机host。host、device、global，三者分别表示定义的函数：在cpu调用执行、在gpu调用执行、cpu调用gpu执行。

//限定符:
//1、__global__  在设备端执行  可从主机端调用
//2、__device__  在设备端执行  仅能从设备端调用
//3、__host__    在主机端执行  一般省略此符号

namespace Msnhnet
{

class Cuda
{
public:
    static cudaStream_t stream;
    static bool streamInited;

    static cublasHandle_t handle;
    static bool handleInited;

    static int blockThread;

    static void cudaCheck(cudaError_t status, const char *fun, const char* file, const int &line);
#define CUDA_CHECK(X) Msnhnet::Cuda::cudaCheck(X,__FUNCTION__,__FILE__, __LINE__)

    static void cublasCheck(cublasStatus_t status, const char *fun, const char* file, const int &line);
#define CUBLAS_CHECK(X) Msnhnet::Cuda::cublasCheck(X,__FUNCTION__, __FILE__, __LINE__)

    static int getDevice();
    static void setBestGPU();
    static std::string getDeviceInfo();
    static dim3 getGrid(const size_t &n);
    static cudaStream_t getCudaStream();
    static cublasHandle_t getBlasHandle();
    static void deleteBlasHandle();
    static float *makeCudaArray(float *const &x, const size_t &n, const cudaMemcpyKind &copyType=cudaMemcpyHostToDevice);
    static float *mallocCudaArray(const size_t &n);

    static __half *makeFp16ArrayFromFp32(float * const &x, const size_t &n);
    // memcpy, host to device
    static void  pushCudaArray(float *const &gpuX, float *const &x, const size_t &n);
    // memcpy, device to host
    static void  pullCudaArray(float *const &gpuX, float *const &x, const size_t &n);
    static void  freeCuda(float *const &gpuX);

    static void  fp32ToFp16(float *const &fp32, const size_t &size, float * const &fp16);
    static void  fp16ToFp32(float * const &fp16, const size_t &size, float *const &fp32);

#ifdef USE_CUDNN
    static cudnnHandle_t cudnnHandle;
    static bool cudnnHandleInited;

    static void cudnnCheck(cudnnStatus_t status, const char *fun, const char* file, const int &line);
#define CUDNN_CHECK(X) Msnhnet::Cuda::cudnnCheck(X,__FUNCTION__, __FILE__, __LINE__)

    static cudnnHandle_t getCudnnHandle();
#endif

};
}



#endif // MSNHNETCUDA_H

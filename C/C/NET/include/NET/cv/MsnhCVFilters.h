﻿#ifndef MSNHCVFILTERS_H
#define MSNHCVFILTERS_H

#include <vector>
#include <list>
#include <algorithm>
#include <Msnhnet/cv/MsnhCVMat.h>

namespace Msnhnet
{
class MsnhNet_API Filter1D
{
public:
    //
    // 函数名称：AmplitudeLimiterFilter()-限幅滤波法
    // 优点：能有效克服因偶然因素引起的脉冲干扰
    // 缺点：无法抑制那种周期性的干扰，且平滑度差
    // 说明：
    //       根据经验判断，确定两次采样允许的最大偏差值（设为A），
    //       每次检测到新值时判断：
    //       如果本次值与上次值之差<=A，则本次值有效，
    //       如果本次值与上次值之差>A，则本次值无效，放弃本次值，用上次值代替本次值。
    //       AmpLimFilterLastVal:最近一次有效采样的值，该变量为全局变量
    //       currentVal:当前采样的值
    //       err:两次采样的最大误差值，该值需要使用者根据实际情况设置

    double ampLimLastVal=0;
    double ampFilter(double currentVal,double err);



    // 函数名称：MiddlevalueFilter()-中位值滤波法
    // 优点：能有效克服因偶然因素引起的波动干扰；对温度、液
    //       位等变化缓慢的被测参数有良好的滤波效果
    // 缺点：对流量，速度等快速变化的参数不宜
    // 说明：
    //       连续采样N次（N取奇数），把N次采样值按大小排列，
    //       取中间值为本次有效值。
    //       data:输入数据
    //       len:输入数据长度

    static double midFilter(double* data,int len);


    // 函数名称：aveFilter()-平均值滤波法
    // 优点：试用于对一般具有随机干扰的信号进行滤波。这种信号的特点是
    //       有一个平均值，信号在某一数值范围附近上下波动。
    // 缺点：对于测量速度较慢或要求数据计算较快的实时控制不适用。
    // 说明：连续取N个采样值进行算术平均运算
    //       data:输入数据
    //       len:输入数据长度

    static double aveFilter(double* data,int len);


    // 函数名称：listAveFilter()-递推平均滤波法（又称滑动平均滤波法）
    //
    // 优点：对周期性干扰有良好的抑制作用，平滑度高；试用于高频振荡的系统
    // 缺点：灵敏度低；对偶然出现的脉冲性干扰的抑制作用较差，不适于脉冲干
    //      扰较严重的场合
    // 说明：把连续N个采样值看成一个队列，队列长度固定为N。
    //      每次采样到一个新数据放入队尾，并扔掉队首的一
    //      次数据。把队列中的N各数据进行平均运算，既获得
    //      新的滤波结果。
    //      N值的选取：流量，N=12；压力，N=4；液面，N=4-12；温度，N=1-4。
    //      listAveSize:队列尺寸
    //      listAve:队列缓存
    //      currentVal:当前数值

    int listAveSize=0;
    std::list<double> listAve;
    double listAveFilter(double currentVal);



     // 函数名称：midAveFilter()-中位值平均滤波法（又称防脉冲干扰平均滤波法）
     // 优点：融合了两种滤波的优点。对于偶然出现的脉冲性干扰，可消
     //      除有其引起的采样值偏差。对周期干扰有良好的抑制作用，
     //      平滑度高，适于高频振荡的系统。
     //       N值的选取：3-14。
     // 缺点：测量速度慢
     // 说明：采一组队列去掉最大值和最小值
     //      data:输入数据
     //      len:输入数据长度

    static double midAveFilter(double* data,int len);


     // 函数名称：ampAveFilter()-限幅平均滤波法
     // 优点：融合了两种滤波法的优点；
     //       对于偶然出现的脉冲性干扰，可消除由于脉冲干扰所引起的采样值偏差。
     // 缺点：测量速度慢
     // 说明：相当于“限幅滤波法”+“递推平均滤波法”；
     //       每次采样到的新数据先进行限幅处理，
     //       再送入队列进行递推平均滤波处理。
     //      data:输入数据
     //      len:输入数据长度
     //      err:两次采样的最大误差值，该值需要使用者根据实际情况设置

    double ampAveLastVal=0;
    double ampAveFilter(double* data, int len, double err);


     // 函数名称：firstOrderLagFilter() 一阶滞后滤波
     // 优点：对周期性干扰具有良好的抑制作用；
     //       适用于波动频率较高的场合。
     // 缺点：相位滞后，灵敏度低；
     //       滞后程度取决于a值大小；
     //       不能消除滤波频率高于采样频率1/2的干扰信号。
     // 说明：取a=0-1，本次滤波结果=(1-a)*本次采样值+a*上次滤波结果。
     //      val:输入数据
     //      a: 权重

    double lastLagVal=0;
    double firstOrderLagFilter(double val, double a);


     // 函数名称：avoidWiggleFilter() 消抖滤波法
     // 优点：对于变化缓慢的被测参数有较好的滤波效果；
     //       可避免在临界值附近控制器的反复开/关跳动或显示器上数值抖动。
     // 缺点：对于快速变化的参数不宜；
     //       如果在计数器溢出的那一次采样到的值恰好是干扰值,则会将干扰值当作有效值导入系统。
     // 说明：设置一个滤波计数器，将每次采样值与当前有效值比较：
     //       如果采样值=当前有效值，则计数器清零；
     //       如果采样值<>当前有效值，则计数器+1，并判断计数器是否>=上限N（溢出）；
     //       如果计数器溢出，则将本次值替换当前有效值，并清计数器。

    double avoidWLastVal=0;
    double avoidWCnt=0;
    double avoidWiggleFilter(double val,int N);


     // 函数名称：ampAWFilter() 限幅消抖滤波法
     // 优点：继承了“限幅”和“消抖”的优点；
     //       改进了“消抖滤波法”中的某些缺陷，避免将干扰值导入系统。
     // 缺点：对于快速变化的参数不宜；
     // 说明：相当于“限幅滤波法”+“消抖滤波法”；
     //       先限幅，后消抖。
     //       val:参数；
     //       err:限幅误差
     //       N:计数上限

    double ampAWLastVal=0;
    double ampAWCnt=0;
    double ampAWFilter(double val,double err, int N);

};

class MsnhNet_API KalmanFilter
{
public:
     KalmanFilter(const Mat &x, const Mat &P, const Mat &Q, const Mat &H, const Mat &R);
     KalmanFilter(){}
     ~KalmanFilter();

    Mat getX() const;

    void setX(const Mat &x);

    Mat getF() const;

    void setF(const Mat &F);

    Mat getP() const;

    void setP(const Mat &P);

    Mat getQ() const;

    void setQ(const Mat &Q);

    Mat getH() const;

    void setH(const Mat &H);

    Mat getR() const;

    void setR(const Mat &R);

    void predict();

    void update(const Mat &z);

private:
    //      [ x  ]       [ 1 0 dt 0  ]       [ 1 0 0   0  ]      [ 1 0 0 0]      [ 1 0 0 0 ]      [ x ]
    //  x = [ y  ]   F = [ 0 1 0  dt ]  P =  [ 0 1 0   0  ]  Q = [ 0 1 0 0]  H = [ 0 1 0 0 ]  z = [ y ]
    //      [ vx ]       [ 0 0 1  0  ]       [ 0 0 100 0  ]      [ 0 0 1 0]
    //      [ vy ]       [ 0 0 0  1  ]       [ 0 0 0   100]      [ 0 0 0 1]

    // H矩阵为了把z变成x, 方便计算
    // K为卡尔曼增益

    //预测部分
    //1, x' = Fx + u    x预测
    //2, P' = FPF^t + Q

    //观测部分
    //1, y  = z - Hx'   z为实际测量值
    //2, K  = P'H^tS^-1 , S  = HP'H^t + R  K为卡尔曼增益
    //3, x  = x' + Ky
    //4, P  = (I-KH)P'

    //状态向量x
    Mat _x;

    //状态转移矩阵
    Mat _F;

    //状态协方差矩阵,对于普通测量模型,如雷达，只有位置可测，加速度不可测，
    //[1, 0  ]  位置可信度高
    //[0, 100]  加速度可信度低
    Mat _P;

    //过程噪声, 一般为单位阵
    Mat _Q;

    //测量矩阵
    Mat _H;

    //测量噪声矩阵,一般由测量公司给出
    Mat _R;

};

class MsnhNet_API SimpleKF1D
{
public:
    SimpleKF1D();

    void setF(const float &dt=0.01);
    void setX(const float &x = 0);
    void setP(const float &x = 1, const float &v = 100);
    void setQ();
    void setH();
    void setR(const float &err = 1000);
    void initKF();

    float update(const float &val);

private:
    KalmanFilter _kf;
};

}

#endif

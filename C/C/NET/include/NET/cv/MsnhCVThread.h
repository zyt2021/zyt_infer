﻿#ifndef MSNHCVTHREAD_H
#define MSNHCVTHREAD_H
#include <thread>

// abstract thread which contains the inner workings of the thread model
class Thread {

public:
    void start()
    {
        uthread = new std::thread(Thread::exec, this);
    }

    void join() {
        uthread->join();
        delete uthread;
        uthread = NULL;
    }

    Thread(){}

    virtual ~Thread()
    {
        join();
        if (uthread) {
            delete uthread;
        }
    }

protected:
    // is implemented by its ancestors
    virtual void run() = 0;

private:
    std::thread* uthread = NULL;

    // static function which points back to the class
    static void exec(Thread* cppThread)
    {
        cppThread->run();
    }
};
#endif // MSNHCVTHREAD_H

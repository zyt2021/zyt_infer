﻿#ifndef MSNHNETWORK_H
#define MSNHNETWORK_H
#include "Msnhnet/layers/MsnhBaseLayer.h"
#include "Msnhnet/utils/MsnhExport.h"
#include "Msnhnet/core/MsnhMemoryManager.h"

namespace Msnhnet
{
class BaseLayer;
class NetworkState;
class Network
{
public:
    Network(){}
    ~Network(){}
    // layers由NetWorkBuilder动态分配, 在NetWorkBuilder中释放
    std::vector<BaseLayer*>   layers;

    int             batch           =   0;       //一批图片
    int             height          =   0;
    int             width           =   0;
    int             channels        =   0;
    int             inputNum        =   0;

    float          *input           =  nullptr; // 中间变量，用来暂存某层网络的输入
    float          *output          =  nullptr; // 存储该层所有的输出，

    void forward(NetworkState &netState);
};

class MsnhNet_API NetworkState
{
public:
    // 指针都是临时变量, 不用不存在动态分配
    float           *input          =  nullptr; // 下一层的输入
    int             inputNum        =  0;
    Network         *net            =  nullptr;
    int             fixNan          =  0;


    // workspace 为NetwokState动态分配
    float           *workspace      =  nullptr; // 工作空间,占用运算空间最大的那个层的, 单位时间只有一个层进行forward
    float           *memPool2       =  nullptr;
    float           *memPool1       =  nullptr;
#ifdef USE_GPU
    float           *gpuWorkspace   =  nullptr; // 工作空间,占用运算空间最大的那个层的, 单位时间只有一个层进行forward
    float           *gpuMemPool1    =  nullptr;
    float           *gpuMemPool2    =  nullptr;

    float           *gpuInputFp16   =  nullptr;
#endif

    template<typename T>
    inline void releaseArr(T *& value)
    {
//        if(value!=nullptr)
//        {
//            delete[] value;
//            value = nullptr;
//        }

        MemoryManager::effcientDelete<T>(value);
    }

    ~NetworkState();

    inline void shuffleInOut()
    {
        uint8_t temp        = _inputWorkSpace;
        _inputWorkSpace     = _outputWorkSpace;
        _outputWorkSpace    = temp;
    }

    uint8_t getInputWorkSpace() const;

    uint8_t getOutputWorkSpace() const;

    //上一层的输出
    float *getInput() const;

    //下一层的输出
    float *getOutput() const;

#ifdef USE_GPU
    float *getGpuInput() const;

    //下一层的输出
    float *getGpuOutput() const;

    uint8_t getGpuInputWorkSpace() const;

    uint8_t getGpuOutputWorkSpace() const;

    inline void shuffleGpuInOut()
    {
        uint8_t temp            = _gpuInputWorkSpace;
        _gpuInputWorkSpace      = _gpuOutputWorkSpace;
        _gpuOutputWorkSpace     = temp;
    }
#endif


private:
    uint8_t _inputWorkSpace   = 0;
    uint8_t _outputWorkSpace  = 1;
#ifdef USE_GPU
    uint8_t _gpuInputWorkSpace   = 0;
    uint8_t _gpuOutputWorkSpace  = 1;
#endif

};
}
#endif // MSNHNETWORK_H

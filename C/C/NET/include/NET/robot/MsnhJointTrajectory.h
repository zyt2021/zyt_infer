﻿#ifndef MSNHJOINTTRAJECTORY_H
#define MSNHJOINTTRAJECTORY_H

#include <Msnhnet/robot/MsnhJointPath.h>
#include <Msnhnet/robot/MsnhVelocityProfile.h>

namespace Msnhnet
{

class MsnhNet_API JointTrajectory
{
public:
    JointTrajectory(){}

    virtual ~JointTrajectory(){}

    virtual double getDuration() const = 0;

    virtual VectorXSDS getPos(double time) const = 0;

    virtual VectorXSDS getVel(double time) const = 0;

    virtual VectorXSDS getAcc(double time) const = 0;
};

class MsnhNet_API JointTrajectorySegment : public JointTrajectory
{
public:
    JointTrajectorySegment(JointPath *geom, VelocityProfile *velProfile, bool aggregate = true);

    JointTrajectorySegment(JointPath *geom, VelocityProfile *velProfile, double duration, bool aggregate = true);

    virtual ~JointTrajectorySegment();

    virtual double getDuration() const ;

    virtual VectorXSDS getPos(double time) const;

    virtual VectorXSDS getVel(double time) const;

    virtual VectorXSDS getAcc(double time) const;

private:
    JointPath *_geom;
    VelocityProfile * _velProfile;
    bool _aggregate;

};

class MsnhNet_API JointTrajectoryComposite : public JointTrajectory
{
public:
    JointTrajectoryComposite(){}
    ~JointTrajectoryComposite();

    virtual double getDuration() const ;

    virtual VectorXSDS getPos(double time) const;

    virtual VectorXSDS getVel(double time) const;

    virtual VectorXSDS getAcc(double time) const;

    virtual void add(JointTrajectory* traj);

private:
    std::vector<JointTrajectory*> _trajVec;
    std::vector<double>      _dVec;
    double                   _duration;
};


}

#endif // MSNHJOINTTRAJECTORY_H

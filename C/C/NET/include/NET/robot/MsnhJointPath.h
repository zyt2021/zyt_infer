﻿#ifndef MSNHJOINTPATH_H
#define MSNHJOINTPATH_H

#include "Msnhnet/math/MsnhMath.h"

namespace Msnhnet
{

class MsnhNet_API JointPath
{
public:
    JointPath(){}

    virtual ~JointPath(){}

    /// \brief pathLength 规划的轨迹长度
    virtual double getPathLength() = 0;

    /// \brief getPos 位置
    virtual VectorXSDS getPos(double s) const = 0;

    /// \brief getVel 速度
    virtual VectorXSDS getVel(double s, double sd) const = 0;

    /// \brief getAcc 加速度
    virtual VectorXSDS getAcc(double s, double sd, double sdd) const = 0;

};

class MsnhNet_API SingleJointPath : public JointPath
{
public:
    SingleJointPath(const VectorXSDS& startJoints, const VectorXSDS& endJoints);
    virtual ~SingleJointPath(){}

    /// \brief pathLength 规划的轨迹长度
    virtual double getPathLength();

    /// \brief getPos 位置
    virtual VectorXSDS getPos(double s) const;

    /// \brief getVel 速度
    virtual VectorXSDS getVel(double s, double sd) const;

    /// \brief getAcc 加速度
    virtual VectorXSDS getAcc(double s, double sd, double sdd) const;

private:
    VectorXSDS _startJoints;
    VectorXSDS _endJoints;
    VectorXSDS _start2EndJoints;

    double     _length;
    std::vector<double> _jointsRatio;
};

class MsnhNet_API JointPathComposite : public JointPath
{
public:
    JointPathComposite();
    virtual ~JointPathComposite();

    void add(JointPath* geom, bool aggregate = true);

    virtual double getPathLength();

    virtual VectorXSDS getPos(double s) const;

    virtual VectorXSDS getVel(double s, double sd) const;

    virtual VectorXSDS getAcc(double s, double sd, double sdd) const;

private:
    std::vector<std::pair<JointPath*,bool>>  _geoVec;
    std::vector<double> _dVec;
    double _pathLength;


    //查找机制
    mutable double _cachedStarts;
    mutable double _cachedEnds;
    mutable size_t _cachedIndex;
    double lookUp(double s) const;
};

}


#endif // MSNHJOINTPATH_H

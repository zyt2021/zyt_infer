﻿#ifndef MSNHCARTESIANTRAJECTORY_H
#define MSNHCARTESIANTRAJECTORY_H

#include <Msnhnet/robot/MsnhCartesianPath.h>
#include <Msnhnet/robot/MsnhVelocityProfile.h>

namespace Msnhnet
{

class MsnhNet_API CartesianTrajectory
{
public:
    CartesianTrajectory(){}

    virtual ~CartesianTrajectory(){}

    virtual double getDuration() const = 0;

    virtual Frame getPos(double time) const = 0;

    virtual Twist getVel(double time) const = 0;

    virtual Twist getAcc(double time) const = 0;
};

class MsnhNet_API CartesianTrajectorySegment : public CartesianTrajectory
{
public:
    CartesianTrajectorySegment(CartesianPath *geom, VelocityProfile *velProfile, bool aggregate = true);

    CartesianTrajectorySegment(CartesianPath *geom, VelocityProfile *velProfile, double duration, bool aggregate = true);

    ~CartesianTrajectorySegment();

    virtual double getDuration() const ;

    virtual Frame getPos(double time) const;

    virtual Twist getVel(double time) const;

    virtual Twist getAcc(double time) const;

private:
    CartesianPath *_geom;
    VelocityProfile * _velProfile;
    bool _aggregate;

};

class MsnhNet_API CartesianTrajectoryComposite : public CartesianTrajectory
{
public:
    CartesianTrajectoryComposite(){}
    ~CartesianTrajectoryComposite();

    virtual double getDuration() const ;

    virtual Frame getPos(double time) const;

    virtual Twist getVel(double time) const;

    virtual Twist getAcc(double time) const;

    virtual void add(CartesianTrajectory* traj);

private:
    std::vector<CartesianTrajectory*> _trajVec;
    std::vector<double>      _dVec;
    double                   _duration;
};



}


#endif // MSNHCARTESIANTRAJECTORY_H

﻿#ifndef SPATIALMATH_H
#define SPATIALMATH_H

#include <Msnhnet/cv/MsnhCVMat.h>
#include <Msnhnet/cv/MsnhCVGeometry.h>
#include <Msnhnet/math/MsnhRotationMatS.h>
#include <Msnhnet/math/MsnhHomTransMatS.h>

namespace Msnhnet
{

///*此处允许w和v不为单位向量*/
//struct ScrewD
//{
//    ScrewD(){}
//    ScrewD(Vector<3,double> v, Vector<3,double> w)
//    {
//        this->v = v;
//        this->w = w;
//    }
//    Vector<3,double> v;
//    Vector<3,double> w;

//    void print()
//    {
//        v.print();
//        w.print();
//    }
//};

///*此处允许w和v不为单位向量*/
//struct ScrewF
//{
//    ScrewF(){}
//    ScrewF(Vector<3,float> v, Vector<3,float> w)
//    {
//        this->v = v;
//        this->w = w;
//    }
//    Vector<3,float> v;
//    Vector<3,float> w;

//    void print()
//    {
//        v.print();
//        w.print();
//    }

//};

template<typename T>
class Screw
{
public:
    Screw(){}
    Screw(Vector<3,T> v, Vector<3,T> w)
    {
        this->v = v;
        this->w = w;
    }

    Screw(Vector<6,T> list)
    {
        fromVector(list);
    }

    Vector<3,T> v;
    Vector<3,T> w;

    void print()
    {
        v.print();
        w.print();
    }

    friend Screw operator *(const Screw& screw, T a)
    {
        Screw tmp = screw;
        tmp.v = tmp.v*a;
        tmp.w = tmp.w*a;
        return tmp;
    }

    friend Screw operator *( T a, const Screw& screw)
    {
        Screw tmp = screw;
        tmp.v = tmp.v*a;
        tmp.w = tmp.w*a;
        return tmp;
    }

    Mat toMat()
    {
        Mat_<1,6,T> tmp;
        tmp.setVal({v[0],v[1],v[2],w[0],w[1],w[2]});
        return tmp;
    }

    Vector<6,T> toVector()
    {
        return Vector<6,double>({v[0],v[1],v[2],
                                 w[0],w[1],w[2]});
    }

    void fromVector(const Vector<6,T>& list)
    {
        v[0] = list[0];
        v[1] = list[1];
        v[2] = list[2];

        w[0] = list[3];
        w[1] = list[4];
        w[2] = list[5];
    }
};

class ScrewDS
{
public:
    ScrewDS(){}
    ScrewDS(const Vector3DS& v, const Vector3DS& w)
    {
        this->v = v;
        this->w = w;
    }

    ScrewDS(Vector6DS list)
    {
        fromVector(list);
    }

    Vector3DS v;
    Vector3DS w;

    void print()
    {
        v.print();
        w.print();
    }

    friend ScrewDS operator *(const ScrewDS& screw, double a)
    {
        ScrewDS tmp = screw;
        tmp.v = tmp.v*a;
        tmp.w = tmp.w*a;
        return tmp;
    }

    friend ScrewDS operator *(double a, const ScrewDS& screw)
    {
        ScrewDS tmp = screw;
        tmp.v = tmp.v*a;
        tmp.w = tmp.w*a;
        return tmp;
    }

    Vector6DS toVector()
    {
        return Vector6DS({v[0],v[1],v[2],w[0],w[1],w[2]});
    }

    void fromVector(const Vector6DS& list)
    {
        v[0] = list[0];
        v[1] = list[1];
        v[2] = list[2];

        w[0] = list[3];
        w[1] = list[4];
        w[2] = list[5];
    }
};

class ScrewFS
{
public:
    ScrewFS(){}
    ScrewFS(const Vector3FS& v, const Vector3FS& w)
    {
        this->v = v;
        this->w = w;
    }

    ScrewFS(Vector6FS list)
    {
        fromVector(list);
    }

    Vector3FS v;
    Vector3FS w;

    void print()
    {
        v.print();
        w.print();
    }

    friend ScrewFS operator *(const ScrewFS& screw, float a)
    {
        ScrewFS tmp = screw;
        tmp.v = tmp.v*a;
        tmp.w = tmp.w*a;
        return tmp;
    }

    friend ScrewFS operator *(float a, const ScrewFS& screw)
    {
        ScrewFS tmp = screw;
        tmp.v = tmp.v*a;
        tmp.w = tmp.w*a;
        return tmp;
    }

    Vector6FS toVector()
    {
        return Vector6FS({v[0],v[1],v[2],w[0],w[1],w[2]});
    }

    void fromVector(const Vector6FS& list)
    {
        v[0] = list[0];
        v[1] = list[1];
        v[2] = list[2];

        w[0] = list[3];
        w[1] = list[4];
        w[2] = list[5];
    }
};

typedef Screw<float> ScrewF;
typedef Screw<double> ScrewD;

class MsnhNet_API SO3D:public RotationMatD
{
public:
    SO3D(){}
    //return 会调用拷贝构造函数给返回值赋值
    //SO3D a,b; SO3D c = a + b; (此时a+b返回的是mat, 此时调用的是一般构造函数,因为a+b没有引用)
    SO3D(const Mat &mat); //一般构造函数 不同
    SO3D(Mat&& mat);
    SO3D(const SO3D& mat); //拷贝构造函数 Mat a(b) Mat a = b， 拷贝自己类的
    SO3D(SO3D&& mat);
    SO3D &operator= (const Mat &mat);
    SO3D &operator= (Mat&& mat);
    SO3D &operator= (const SO3D &mat);
    SO3D &operator= (SO3D&& mat);

    //即是本身
    RotationMatD &toRotMat();
    QuaternionD  toQuaternion();
    EulerD       toEuler(const RotSequence &rotSeq);
    RotationVecD toRotVector();

    //伴随矩阵
    SO3D adjoint();

    static SO3D rotX(double angleInRad);
    static SO3D rotY(double angleInRad);
    static SO3D rotZ(double angleInRad);
    static SO3D fromRotMat(const RotationMatD &rotMat);
    static SO3D fromQuaternion(const QuaternionD &quat);
    static SO3D fromEuler(const EulerD &euler, const RotSequence &rotSeq);
    static SO3D fromRotVec(const RotationVecD &rotVec);

    SO3D fastInvert();

    /// \brief wedge 反对称矩阵
    /// \param omg
    /// \param needCalUnit 是否需要计算omg的单位矩阵
    /// \return
    static Matrix3x3D wedge(const Vector3D &omg, bool needCalUnit=false);

    /// \brief vee 反对称矩阵逆变换
    /// \param mat3x3
    /// \param needCalUnit 是否需要计算omg的单位矩阵
    /// \return
    static Vector3D vee(const Matrix3x3D &mat3x3,bool needCalUnit=false);

    //指数映射
    static SO3D exp(const Vector3D &omg);
    static SO3D exp(const Vector3D &omg, double theta);

    //对数映射
    Vector3D log();

    static bool isSO3(const Mat &mat);

    static bool forceCheckSO3;
};

class MsnhNet_API SO3F:public RotationMatF
{
public:
    SO3F(){}
    //return 会调用拷贝构造函数给返回值赋值
    //SO3F a,b; SO3F c = a + b; (此时a+b返回的是mat, 此时调用的是一般构造函数,因为a+b没有引用)
    SO3F(const Mat &mat); //一般构造函数 不同
    SO3F(Mat&& mat);
    SO3F(const SO3F& mat); //拷贝构造函数 Mat a(b) Mat a = b， 拷贝自己类的
    SO3F(SO3F&& mat);
    SO3F &operator= (const Mat &mat);
    SO3F &operator= (Mat&& mat);
    SO3F &operator= (const SO3F &mat);
    SO3F &operator= (SO3F&& mat);

    //即是本身
    RotationMatF &toRotMat();
    QuaternionF  toQuaternion();
    EulerF       toEuler(const RotSequence &rotSeq);
    RotationVecF toRotVector();

    //伴随矩阵
    SO3F adjoint();

    static SO3F rotX(float angleInRad);
    static SO3F rotY(float angleInRad);
    static SO3F rotZ(float angleInRad);
    static SO3F fromRotMat(const RotationMatF &rotMat);
    static SO3F fromQuaternion(const QuaternionF &quat);
    static SO3F fromEuler(const EulerF &euler, const RotSequence &rotSeq);
    static SO3F fromRotVec(const RotationVecF &rotVec);

    SO3F fastInvert();

    /// \brief wedge 反对称矩阵
    /// \param omg
    /// \param needCalUnit 是否需要计算omg的单位矩阵
    /// \return
    static Matrix3x3F wedge(const Vector3F &omg, bool needCalUnit=false);

    /// \brief vee 反对称矩阵逆变换
    /// \param mat3x3
    /// \param needCalUnit 是否需要计算omg的单位矩阵
    /// \return
    static Vector3F vee(const Matrix3x3F &mat3x3, bool needCalUnit=false);

    //指数映射
    static SO3F exp(const Vector3F &omg);
    static SO3F exp(const Vector3F &omg,float theta);

    //对数映射
    Vector3F log();

    static bool isSO3(const Mat &mat);

    static bool forceCheckSO3;
};

class MsnhNet_API SE3D:public Matrix4x4D
{
public:
    //return 会调用拷贝构造函数给返回值赋值
    //SE3D a,b; SE3D c = a + b; (此时a+b返回的是mat, 此时调用的是一般构造函数,因为a+b没有引用)
    SE3D(){}
    SE3D(const Mat &mat); //一般构造函数 不同
    SE3D(Mat&& mat);
    SE3D(const SE3D& mat); //拷贝构造函数 Mat a(b) Mat a = b， 拷贝自己类的
    SE3D(SE3D&& mat);
    SE3D &operator= (const Mat &mat);
    SE3D &operator= (Mat&& mat);
    SE3D &operator= (const SE3D &mat);
    SE3D &operator= (SE3D&& mat);

    SE3D(const SO3D &rotMat, const Vector3D &trans);

    Matrix4x4D &toMatrix4x4();

    //伴随矩阵
    Mat adjoint();

    SE3D fastInvert();

    /// \brief wedge 反对称矩阵
    /// \param screw
    /// \param needCalUnit 是否需要计算screw的单位向量, screw.w!=0时对w单位化，scew.w==0时对v单位化
    /// \return
    static Matrix4x4D wedge(const ScrewD &screw, bool needCalUnit=false);

    /// \brief vee 反对称矩阵逆变换
    /// \param wed
    /// \param needCalUnit 是否需要计算返回值的单位向量, screw.w!=0时对w单位化，scew.w==0时对v单位化
    /// \return
    static ScrewD vee(const Matrix4x4D &wed, bool needCalUnit=false);

    ScrewD log();

    static SE3D exp(const ScrewD &screw, double theta);

    static SE3D exp(const ScrewD &screw);

    static bool isSE3(const Mat &mat);

    static bool forceCheckSE3;
};

class MsnhNet_API SE3F:public Matrix4x4F
{
public:

    SE3F(){}
    SE3F(const Mat &mat);
    SE3F(Mat&& mat);
    SE3F(const SE3F& mat);
    SE3F(SE3F&& mat);
    SE3F &operator= (const Mat &mat);
    SE3F &operator= (Mat&& mat);
    SE3F &operator= (const SE3F &mat);
    SE3F &operator= (SE3F&& mat);

    SE3F(const SO3F &rotMat, const Vector3F &trans);

    Matrix4x4F &toMatrix4x4();

    Mat adjoint();

    SE3F fastInvert();

    static Matrix4x4F wedge(const ScrewF &screw, bool needCalUnit=false);

    static ScrewF vee(const Matrix4x4F &wed, bool needCalUnit=false);

    ScrewF log();

    static SE3F exp(const ScrewF &screw, float theta);

    static SE3F exp(const ScrewF &screw);

    static bool isSE3(const Mat &mat);

    static bool forceCheckSE3;
};

class MsnhNet_API SO3DS:public RotationMatDS
{
public:
    SO3DS(){}
    SO3DS(const SO3DS& so3ds);
    SO3DS(const RotationMatDS& rotMat);
    SO3DS &operator= (const SO3DS &so3ds);
    SO3DS &operator= (const RotationMatDS &so3ds);

    QuaternionDS  toQuaternion();
    EulerDS       toEuler(const RotSequence &rotSeq);
    RotationVecDS toRotVector();

    //伴随矩阵
    SO3DS adjoint();

    static SO3DS rotX(double angleInRad);
    static SO3DS rotY(double angleInRad);
    static SO3DS rotZ(double angleInRad);
    static SO3DS fromRotMat(const RotationMatDS &rotMat);
    static SO3DS fromQuaternion(const QuaternionDS &quat);
    static SO3DS fromEuler(const EulerDS &euler, const RotSequence &rotSeq);
    static SO3DS fromRotVec(const RotationVecDS &rotVec);

    /// \brief wedge 反对称矩阵
    /// \param omg
    /// \param needCalUnit 是否需要计算omg的单位矩阵
    /// \return
    static SO3DS wedge(const Vector3DS &omg, bool needCalUnit=false);

    /// \brief vee 反对称矩阵逆变换
    /// \param mat3x3
    /// \param needCalUnit 是否需要计算omg的单位矩阵
    /// \return
    static Vector3DS vee(const SO3DS &so3ds,bool needCalUnit=false);

    //指数映射
    static SO3DS exp(const Vector3DS &omg);
    static SO3DS exp(const Vector3DS &omg, double theta);

    //对数映射
    Vector3DS log();
};

class MsnhNet_API SO3FS:public RotationMatFS
{
public:
    SO3FS(){}
    SO3FS(const SO3FS& so3ds);
    SO3FS(const RotationMatFS& rotMat);
    SO3FS &operator= (const SO3FS &so3fs);
    SO3FS &operator= (const RotationMatFS &so3fs);
    QuaternionFS  toQuaternion();
    EulerFS       toEuler(const RotSequence &rotSeq);
    RotationVecFS toRotVector();

    //伴随矩阵
    SO3FS adjoint();

    static SO3FS rotX(float angleInRad);
    static SO3FS rotY(float angleInRad);
    static SO3FS rotZ(float angleInRad);
    static SO3FS fromRotMat(const RotationMatFS &rotMat);
    static SO3FS fromQuaternion(const QuaternionFS &quat);
    static SO3FS fromEuler(const EulerFS &euler, const RotSequence &rotSeq);
    static SO3FS fromRotVec(const RotationVecFS &rotVec);

    /// \brief wedge 反对称矩阵
    /// \param omg
    /// \param needCalUnit 是否需要计算omg的单位矩阵
    /// \return
    static SO3FS wedge(const Vector3FS &omg, bool needCalUnit=false);

    /// \brief vee 反对称矩阵逆变换
    /// \param mat3x3
    /// \param needCalUnit 是否需要计算omg的单位矩阵
    /// \return
    static Vector3FS vee(const SO3FS &so3ds,bool needCalUnit=false);

    //指数映射
    static SO3FS exp(const Vector3FS &omg);
    static SO3FS exp(const Vector3FS &omg, float theta);

    //对数映射
    Vector3FS log();
};

class MsnhNet_API SE3DS:public HomTransMatDS
{
public:
    SE3DS(){}
    SE3DS(const SE3DS& se3d);
    SE3DS(const HomTransMatDS& homDS);
    SE3DS &operator= (const SE3DS &se3d);
    SE3DS &operator= (const HomTransMatDS &homDS);

    SE3DS(const SO3DS &rotMat, const Vector3DS &trans);

    //伴随矩阵
    Mat adjoint();

    /// \brief wedge 反对称矩阵
    /// \param screw
    /// \param needCalUnit 是否需要计算screw的单位向量, screw.w!=0时对w单位化，scew.w==0时对v单位化
    /// \return
    static HomTransMatDS wedge(const ScrewDS &screw, bool needCalUnit=false);

    /// \brief vee 反对称矩阵逆变换
    /// \param wed
    /// \param needCalUnit 是否需要计算返回值的单位向量, screw.w!=0时对w单位化，scew.w==0时对v单位化
    /// \return
    static ScrewDS vee(const HomTransMatDS &wed, bool needCalUnit=false);

    ScrewDS log();

    static SE3DS exp(const ScrewDS &screw, double theta);

    static SE3DS exp(const ScrewDS &screw);

};

class MsnhNet_API SE3FS:public HomTransMatFS
{
public:
    SE3FS(){}
    SE3FS(const SE3FS& se3d);
    SE3FS(const HomTransMatFS& homFS);
    SE3FS &operator= (const SE3FS &se3d);
    SE3FS &operator= (const HomTransMatFS &homFS);

    SE3FS(const SO3FS &rotMat, const Vector3FS &trans);

    //伴随矩阵
    Mat adjoint();

    /// \brief wedge 反对称矩阵
    /// \param screw
    /// \param needCalUnit 是否需要计算screw的单位向量, screw.w!=0时对w单位化，scew.w==0时对v单位化
    /// \return
    static HomTransMatFS wedge(const ScrewFS &screw, bool needCalUnit=false);

    /// \brief vee 反对称矩阵逆变换
    /// \param wed
    /// \param needCalUnit 是否需要计算返回值的单位向量, screw.w!=0时对w单位化，scew.w==0时对v单位化
    /// \return
    static ScrewFS vee(const HomTransMatFS &wed, bool needCalUnit=false);

    ScrewFS log();

    static SE3FS exp(const ScrewFS &screw, double theta);

    static SE3FS exp(const ScrewFS &screw);

};
}



#endif // SPATIALMATH_H

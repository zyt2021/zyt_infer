﻿#ifndef MSNHVELPROFILE_H
#define MSNHVELPROFILE_H

#include "Msnhnet/config/MsnhnetCfg.h"
#include "Msnhnet/utils/MsnhMathUtils.h"

namespace Msnhnet
{

// 速度曲线配置
class MsnhNet_API VelocityProfile
{
public:
    VelocityProfile(){}

    //子类如果赋值给基类，如果不声明virtual析构, 则在释放时，子类不会被释放
    virtual ~VelocityProfile(){}

    //此处的pos指的是所有的步长

    //以最快的速度从pos1到pos2
    virtual void setProfile(double pos1, double pos2) = 0;

    //经过多长时间从pos1到pos2
    virtual void setProfileDuration(double pos1, double pos2, double duration) = 0;

    //返回整个运动的持续时间（以秒为单位）
    virtual double getDuration() const = 0;

    //返回在<time>处的位置，单位为子类的构造函数的输入单位。
    virtual double getPos(double time) const = 0;

    //返回在<time>处的速度，单位为子类的构造函数的输入单位。
    virtual double getVel(double time) const = 0;

    //返回在<time>处的加速度，单位为子类的构造函数的输入单位。
    virtual double getAcc(double time) const = 0;
};

class MsnhNet_API VelocityProfileTrap : public VelocityProfile
{
public:
    VelocityProfileTrap(double maxVel, double maxAcc);

    virtual ~VelocityProfileTrap(){}

    virtual void setProfile(double pos1, double pos2);

    virtual void setProfileDuration(double pos1, double pos2, double duration);

    void setMax(double maxVel, double maxAcc);

    virtual double getDuration() const;

    virtual double getPos(double time) const;

    virtual double getVel(double time) const;

    virtual double getAcc(double time) const;

private:

    //3次系数 y = a0*x^0 + a1*x^1 + a2*x^2 + a3*x^3
    double _a1;
    double _a2;
    double _a3;

    double _b1;
    double _b2;
    double _b3;

    double _c1;
    double _c2;
    double _c3;
    double _duration;
    double _t1;
    double _t2;

    //运动配置
    double _maxVel;
    double _maxAcc;
    double _startPos;
    double _endPos;
};


//匀速
class MsnhNet_API VelocityProfileConst : public VelocityProfile
{
public:
    VelocityProfileConst(double maxVel = 0);

    virtual ~VelocityProfileConst(){}

    virtual void setProfile(double pos1, double pos2);

    virtual void setProfileDuration(double pos1, double pos2, double duration);

    void setMax(double maxVel);

    virtual double getDuration() const;

    virtual double getPos(double time) const;

    virtual double getVel(double time) const;

    virtual double getAcc(double time) const;

private:
    double _duration;
    double _posStart;
    double _velocity;
    double _maxVel;
};

/// ^
/// |      -
/// |    / |
/// |   /  |    isStartPart
/// |  /dS |
/// | /    |
///  ------------------------ >
///
/// ^
/// | -
/// | | \
/// | |  \      !isStartPart
/// | |   \
/// | |    \
///  ------------------------ >
class MsnhNet_API VelocityProfileTrapHalf : public VelocityProfile
{
public:
    VelocityProfileTrapHalf(double maxVel, double maxAcc, bool isStartPart);

    virtual ~VelocityProfileTrapHalf(){}

    virtual void setProfile(double pos1, double pos2);

    virtual void setProfileDuration(double pos1, double pos2, double newDuration);

    void setMax(double maxVel, double maxAcc, bool isStartPart);

    virtual double getDuration() const;

    virtual double getPos(double time) const;

    virtual double getVel(double time) const;

    virtual double getAcc(double time) const;

private:
    void planProfile1(double velocity, double acc);

    void planProfile2(double velocity, double acc);

    //3次系数 y = a0*x^0 + a1*x^1 + a2*x^2 + a3*x^3
    double _a1;
    double _a2;
    double _a3;

    double _b1;
    double _b2;
    double _b3;

    double _c1;
    double _c2;
    double _c3;
    double _duration;
    double _t1;
    double _t2;

    //运动配置
    double _maxVel;
    double _maxAcc;
    double _startPos;
    double _endPos;

    bool _isStartPart = false;
};


}

#endif // MSNHVELPROFILE_H

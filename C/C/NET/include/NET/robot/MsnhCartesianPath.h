﻿#ifndef MSNHCARTESIANPATH_H
#define MSNHCARTESIANPATH_H

#include <Msnhnet/robot/MsnhRotationInterp.h>
#include <Msnhnet/robot/MsnhFrame.h>

namespace Msnhnet
{

class MsnhNet_API CartesianPath
{
public:
    CartesianPath(){}

    //子类如果赋值给基类，如果不声明virtual析构, 则在释放时，子类不会被释放
    virtual ~CartesianPath(){}

    /// <- length ->
    /// ------------x---      scale |
    /// ---------------x----     <- |
    /// <- lengthToS ->
    /// \brief lengthToS
    virtual double getLengthToScale(double length) = 0;

    /// \brief pathLength 规划的轨迹长度
    virtual double getPathLength() = 0;

    /// \brief getPos 位置
    virtual Frame getPos(double s) const = 0;

    /// \brief getVel 速度
    virtual Twist getVel(double s, double sd) const = 0;

    /// \brief getAcc 加速度
    virtual Twist getAcc(double s, double sd, double sdd) const = 0;

};

class MsnhNet_API CartesianPathLine : public CartesianPath
{
public:
    /// 现在有了位置位移的总长度（或圆弧的弧长）和姿态变换的角度，需要对两者进行插值，插值的核心在于求取长度/角度的位置pos、
    /// 速度v和加速度a这三者分别与时间的数学关系式，如第2部分所述，有了距离，最大速度和加速度这三个参数，
    /// 位置/速度/加速度与时间的关系即可生成。很自然地，可以对位移和角度分别求取数学关系式，
    /// 但是这样会造成位移和角度的速度规律不一样，也就是说，两者无法在同一时刻达到最大的速度。
    /// KDL期望实现两者能够同时达到相应的最大的速度，然后保持运行相同的时间，然后在同一时刻都开始减速。'
    ///
    /// KDL里方法是这样的，它通过一个等效半径将角度与其想乘转化为等效长度，表示姿态的变换的幅度，而质点的位移的路径长度（线段加圆弧长）
    /// 表示位移的变化的幅度。通过比较出这两者的大小，来确定将谁作为pathlength（程序里的s）。具体的处理方式如下，
    ///
    /// alpha*eqradius > L ---> S = alpha*eqradius ----> L(t) = S(t)*scaleLinear = S(t)*L/S
    ///                                                  Alpha(t) = S(t)*scaleRotation = S(t)*1/eqradius
    ///
    /// alpha*eqradius < L ---> S = L  ----> L(t) = S(t)*scaleLinear = S(t)*1
    ///                                      Alpha(t) = S(t)*scaleRotation = S(t)*alpha/eqradius
    ///
    CartesianPathLine(const Frame& FBaseStart,
                      const Frame& FBaseEnd,
                      RotationInterp* orient,
                      double eqradius,
                      bool aggregate = true);

    virtual ~CartesianPathLine();

    virtual double getLengthToScale(double length);

    virtual double getPathLength();

    virtual Frame getPos(double s) const;

    virtual Twist getVel(double s, double sd) const;

    virtual Twist getAcc(double s, double sd, double sdd) const;

private:
    RotationInterp* _orient;

    Vector3DS _VBase2Start;
    Vector3DS _VBase2End;
    Vector3DS _VStart2End;

    //等效半径
    double _eqradius;

    double _pathLength;
    double _scaleLinear;
    double _scaleRotation;

    bool _aggregate;
};

class MsnhNet_API CartesianPathCircle : public CartesianPath
{
public:
    ///    (1,0,0)和(-1,0,0)无法确定平面
    ///     base2start
    ///    |
    ///    |
    ///    |     ─>┐ 方向
    ///    |
    ///    |╮ 角度(alpha)
    ///    -------------- base2End(用于指示方向,具体按alpha给的角度最终确定)，末端姿态为end的姿态
    ///    |
    ///    |
    ///    |
    ///    |
    ///    |
    ///
    ///
    ///
    ///
    ///
    CartesianPathCircle(const Frame& fBase2Start,
                        const Vector3DS& vBase2Center,
                        const Frame& fBase2End,
                        double alpha,
                        RotationInterp* orient,
                        double eqradius,
                        bool negWay = false,
                        bool aggregate=true);

    CartesianPathCircle(const Frame& frame1,
                        const Frame& frame2,
                        const Frame& frame3,
                        RotationInterp* orient,
                        double eqradius,
                        bool fullCircle = false,
                        bool aggregate=true);

    virtual ~CartesianPathCircle();

    virtual double getLengthToScale(double length);

    virtual double getPathLength();

    virtual Frame getPos(double s) const;

    virtual Twist getVel(double s, double sd) const;

    virtual Twist getAcc(double s, double sd, double sdd) const;

private:
    RotationInterp* _orient;

    double _radius;
    Frame _fBase2Center;
    bool  _negWay;

    //等效半径
    double _eqradius;

    double _pathLength;
    double _scaleLinear;
    double _scaleRotation;

    bool _aggregate;
};

class MsnhNet_API CartesianPathComposite : public CartesianPath
{
public:
    CartesianPathComposite();
    virtual ~CartesianPathComposite();

    void add(CartesianPath* geom, bool aggregate = true);

    virtual double getLengthToScale(double length);

    virtual double getPathLength();

    virtual Frame getPos(double s) const;

    virtual Twist getVel(double s, double sd) const;

    virtual Twist getAcc(double s, double sd, double sdd) const;

private:
    std::vector<std::pair<CartesianPath*,bool>>  _geoVec;
    std::vector<double> _dVec;
    double _pathLength;


    //查找机制
    mutable double _cachedStarts;
    mutable double _cachedEnds;
    mutable size_t _cachedIndex;
    double lookUp(double s) const;
};

class MsnhNet_API CartesianPathRoundComposite : public CartesianPath
{
public:
    CartesianPathRoundComposite(double radius,double eqradius, RotationInterp* orient, bool aggregate);
    virtual ~CartesianPathRoundComposite();

    //将一个点添加到这个圆角组合中，在相邻点之间将创建一条路径线，在两条线之间将创建一条路径线
    void add(const Frame& fBase2Point);

    void finish();

    virtual double getLengthToScale(double length);

    virtual double getPathLength();

    virtual Frame getPos(double s) const;

    virtual Twist getVel(double s, double sd) const;

    virtual Twist getAcc(double s, double sd, double sdd) const;

private:
    CartesianPathComposite *_comp;

    double _radius;     //round半径
    double _eqradius;   //等效半径

    RotationInterp* _orient;

    Frame _fBase2Start;
    Frame _fBase2Via;

    int _numOfPoints;
    bool _aggregate;
};

}
#endif // MSNHCARTESIANPATH_H

﻿#ifndef MSNHROBOT_H
#define MSNHROBOT_H

#include "Msnhnet/robot/MsnhChain.h"
#include "Msnhnet/robot/MsnhFrame.h"
#include "Msnhnet/robot/MsnhJoint.h"
#include "Msnhnet/robot/MsnhSegment.h"
#include "Msnhnet/robot/MsnhSpatialMath.h"
#include "Msnhnet/robot/MsnhModernRobot.h"
#include "Msnhnet/robot/MsnhURDF.h"
#include "Msnhnet/robot/MsnhCartesianTrajectory.h"
#include "Msnhnet/robot/MsnhCartesianPath.h"
#include "Msnhnet/robot/MsnhJointTrajectory.h"
#include "Msnhnet/robot/MsnhJointPath.h"
#include "Msnhnet/robot/MsnhVelocityProfile.h"

namespace Msnhnet
{

enum PlanType
{
    PLAN_JOINT,
    PLAN_JOINT_GROUP,
    PLAN_LINE,
    PLAN_LINE_GROUP,
    PLAN_ROUND_LINE_GROUP,
    PLAN_CIRCLE
};

enum RotationInterpType
{
    ROT_INTERP_AXIS_ANGLE,
    ROT_INTERP_AXIS_QUAT
};

}
#endif // MSNHROBOT_H

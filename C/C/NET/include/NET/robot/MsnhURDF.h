﻿#ifndef MSNHURDF_H
#define MSNHURDF_H
#include <Msnhnet/robot/MsnhChain.h>
#include <Msnhnet/utils/MsnhExString.h>
#include <Msnhnet/3rdparty/tinyxml2/tinyxml2.h>
#include <map>
// Instead of C++17 std::optional
#include <Msnhnet/3rdparty/optional/optional.h>
#include <fstream>
#include <Msnhnet/robot/MsnhTree.h>

#ifdef WIN32
#include <windows.h>
#endif


namespace Msnhnet
{

struct Color {
    float r;
    float g;
    float b;
    float a;

    void clear()
    {
        r = 0.;
        g = 0.;
        b = 0.;
        a = 1.;
    }

    Color() : r(0.), g(0.), b(0.), a(1.) {}
    Color(float r, float g, float b, float a) : r(r), g(g), b(b), a(a) {}
    Color(const Color& other) : r(other.r), g(other.g), b(other.b), a(other.a) {}
};

// ================  Geometry3D  ================
enum URDFGeometryType
{
    URDF_SPHERE,
    URDF_BOX,
    URDF_CYLINDER,
    URDF_CONE,
    URDF_MESH
};

class URDFGeometry
{
public:
    URDFGeometryType type;
    virtual ~URDFGeometry(void){}
    URDFGeometry(URDFGeometryType type): type(type) {}
};

class URDFSphere : public URDFGeometry
{
public:
    double radius;

    void clear()
    {
        radius = 0;
    }

    URDFSphere() :URDFGeometry(URDFGeometryType::URDF_SPHERE), radius(0.){}
};

class URDFBox : public URDFGeometry
{
public:
    Vector3DS dim;

    void clear()
    {
        dim[0] = 0;
        dim[1] = 0;
        dim[2] = 0;
    }

    URDFBox() : URDFGeometry(URDFGeometryType::URDF_BOX) {}
};

class URDFCylinder : public URDFGeometry
{
public:
    double length;
    double radius;

    void clear()
    {
        length = 0;
        radius = 0;
    }

    URDFCylinder() : URDFGeometry(URDFGeometryType::URDF_CYLINDER),length(0.), radius(0.) {}
};

class URDFCone : public URDFGeometry
{
public:
    double length;
    double radius;

    void clear()
    {
        length = 0;
        radius = 0;
    }

    URDFCone() : URDFGeometry(URDFGeometryType::URDF_CONE),length(0.), radius(0.) {}
};

class URDFMesh : public URDFGeometry
{
public:
    std::string filename;
    Vector3DS scale;

    void clear()
    {
        filename.clear();

        scale[0] = 1;
        scale[1] = 1;
        scale[2] = 1;
    }

    URDFMesh() : URDFGeometry(URDFGeometryType::URDF_MESH),scale(Vector3DS(1., 1., 1.)){}
};
// =============================================

// ==================  URDFJoint  ==================
enum URDFJointType
{
    URDF_UNKNOWN,
    URDF_REVOLUTE,		//rotation axis
    URDF_CONTINUOUS,
    URDF_PRISMATIC,		 //translation axis
    URDF_FLOATING,
    URDF_PLANAR,		//plane normal axis
    URDF_FIXED
};

struct URDFJointDynamics
{
    double damping;
    double friction;

    void clear()
    {
        damping = 0;
        friction = 0;
    }

    URDFJointDynamics() : damping(0.), friction(0.) {}
    URDFJointDynamics(const URDFJointDynamics& jd) : damping(jd.damping), friction(jd.friction) {}
};

struct URDFJointLimits
{
    double lower;
    double upper;
    double effort;
    double velocity;

    void clear()
    {
        lower = 0;
        upper = 0;
        effort = 0;
        velocity = 0;
    }

    URDFJointLimits() : lower(0.), upper(0.), effort(0.), velocity(0.) {}
    URDFJointLimits(const URDFJointLimits& jl) : lower(jl.lower), upper(jl.upper),
        effort(jl.effort), velocity(jl.velocity) {}
};

struct URDFJointSafety
{
    double upperLimit;
    double lowerLimit;
    double kPosition;
    double kVelocity;

    void clear()
    {
        upperLimit = 0;
        lowerLimit = 0;
        kPosition = 0;
        kVelocity = 0;
    }

    URDFJointSafety() : upperLimit(0.), lowerLimit(0.), kPosition(0.), kVelocity(0.) {}
    URDFJointSafety(const URDFJointSafety& js) : upperLimit(js.upperLimit), lowerLimit(js.lowerLimit),kPosition(js.kPosition), kVelocity(js.kVelocity) {}
};

struct URDFJointCalibration
{
    tl::optional<double> rising;
    tl::optional<double> falling;

    void clear()
    {
        rising.reset();
        falling.reset();
    }

    URDFJointCalibration() { clear(); }
    URDFJointCalibration(const URDFJointCalibration& jc): rising(jc.rising), falling(jc.falling) {}
};

struct URDFJointMimic
{
    std::string jointName;
    double offset;
    double multiplier;

    void clear()
    {
        jointName = "";
        offset = 0.;
        multiplier = 0.;
    }

    URDFJointMimic() : jointName(""), offset(0.), multiplier(0.) {}
    URDFJointMimic(const URDFJointMimic& mimic): jointName(mimic.jointName), offset(mimic.offset), multiplier(mimic.multiplier) {}
};

struct URDFJoint
{
    std::string name;
    URDFJointType type;
    Vector3DS axis;
    std::string childLinkName;
    std::string parentLinkName;
    Frame parentToJointTransform;

    tl::optional<std::shared_ptr<URDFJointDynamics>> dynamics;
    tl::optional<std::shared_ptr<URDFJointLimits>> limits;
    tl::optional<std::shared_ptr<URDFJointSafety>> safety;
    tl::optional<std::shared_ptr<URDFJointCalibration>> calibration;
    tl::optional<std::shared_ptr<URDFJointMimic>> mimic;

    void clear()
    {
        this->axis.clear();
        this->childLinkName.clear();
        this->parentLinkName.clear();
        this->parentToJointTransform.clear();

        this->dynamics.reset();
        this->limits.reset();
        this->safety.reset();
        this->calibration.reset();
        this->type = URDFJointType::URDF_UNKNOWN;
    }

    URDFJoint() : type(URDFJointType::URDF_UNKNOWN) { clear(); }
    URDFJoint(const URDFJoint& joint): name(joint.name), type(joint.type),
        axis(joint.axis), childLinkName(joint.childLinkName),
        parentLinkName(joint.parentLinkName),
        parentToJointTransform(joint.parentToJointTransform),
        dynamics(joint.dynamics), limits(joint.limits), safety(joint.safety),
        calibration(joint.calibration), mimic(joint.mimic) {}

};
// =============================================

// ==================  Link  ===================
struct URDFMaterial
{
    std::string name;
    std::string textureFilename;
    Color color;

    void clear() {
        name.clear();
        textureFilename.clear();
        color.clear();
    }

    URDFMaterial() { clear(); }
    URDFMaterial(const URDFMaterial& m): name(m.name), textureFilename(m.textureFilename),color(m.color) {}
};

struct URDFInertial
{
    Frame origin;
    double ixx,ixy,ixz,iyy,iyz,izz;
    double mass;

    void clear() {
        origin.clear();
        mass = 0.;
        ixx = 0.;
        ixy = 0.;
        ixz = 0.;
        iyy = 0.;
        iyz = 0.;
        izz = 0.;
    }

    URDFInertial() :ixx(0.), ixy(0.), ixz(0.), iyy(0.), iyz(0.), izz(0.),  mass(0.) {}
    URDFInertial(const URDFInertial& i) : origin(i.origin), ixx(i.ixx), ixy(i.ixy), ixz(i.ixz),iyy(i.iyy), iyz(i.iyz), izz(i.izz), mass(i.mass) {}
};

struct URDFVisual
{
    std::string name;
    std::string materialName;
    Frame origin;

    tl::optional<std::shared_ptr<URDFGeometry>> geometry;
    tl::optional<std::shared_ptr<URDFMaterial>> material;

    void clear()
    {
        origin.clear();
        name.clear();
        materialName.clear();

        material.reset();
        geometry.reset();
    }

    URDFVisual() { this->clear(); }
    URDFVisual(const URDFVisual& v) : name(v.name), materialName(v.materialName),origin(v.origin), geometry(v.geometry), material(v.material) {}
};

struct URDFCollision
{
    std::string name;
    Frame origin;
    tl::optional<std::shared_ptr<URDFGeometry>> geometry;

    void clear() {
        name.clear();
        origin.clear();

        geometry.reset();
    }

    URDFCollision() { this->clear(); }
    URDFCollision(const URDFCollision& c) : name(c.name), origin(c.origin), geometry(c.geometry) {}
};

struct URDFLink
{
    std::string name;

    tl::optional<URDFInertial> inertial;

    std::vector<std::shared_ptr<URDFCollision>>  collisions;
    std::vector<std::shared_ptr<URDFVisual>>  visuals;

    std::shared_ptr<URDFJoint> parentJoint;
    std::shared_ptr<URDFLink> parentLink;

    std::vector<std::shared_ptr<URDFJoint>> childJoints;
    std::vector<std::shared_ptr<URDFLink>> childLinks;

    int linkIndex;

    std::shared_ptr<URDFLink> getParent() const
    {
        return parentLink;
    }

    void setParentLink(std::shared_ptr<URDFLink> parent)
    {
        parentLink = parent;
    }

    void setParentJoint(std::shared_ptr<URDFJoint> parent)
    {
        parentJoint = parent;
    }

    void clear() {
        name.clear();
        linkIndex=-1;

        childJoints.clear();
        childLinks.clear();
        collisions.clear();
        visuals.clear();

        inertial.reset();

        parentJoint = nullptr;
        parentLink = nullptr;
    }

    URDFLink() { this->clear(); }
    URDFLink(const URDFLink& l) : name(l.name), inertial(l.inertial), collisions(l.collisions),
        visuals(l.visuals), parentJoint(l.parentJoint),
        parentLink(l.parentLink), childJoints(l.childJoints),
        childLinks(l.childLinks), linkIndex(l.linkIndex) {}

};



//struct URDFModelVisual
//{
//    HomTransMatFS root;       // root坐标轴
//    std::string   rootName;   // root名字

//    URDFJointVisual jointVisual;
//};

struct URDFModel
{
    string rootPath;
    string name;
    std::shared_ptr<URDFLink> rootLink;

    std::map<string, std::shared_ptr<URDFLink>> linkMap;
    std::map<string, std::shared_ptr<URDFJoint>> jointMap;
    std::map<string, std::shared_ptr<URDFMaterial>> materialMap;

    const string& getName() const { return name; }
    std::shared_ptr<URDFLink> getRoot() const { return rootLink; }

    std::shared_ptr<URDFLink> getLink(const string& name);
    std::shared_ptr<URDFJoint> getJoint(const string& name);
    std::shared_ptr<URDFMaterial> getMaterial(const string& name);

    void getLinks(vector<std::shared_ptr<URDFLink>>& linklist) const;

    void clear()
    {
        name.clear();

        linkMap.clear();

        jointMap.clear();

        materialMap.clear();

        rootLink = nullptr;
    }


    void initLinkTree(map<string, string>& parentLinkTree);
    void findRoot(const map<string, string> &parentLinkTree);

    URDFModel() { clear(); }
};

//auto aaa = std::dynamic_pointer_cast<std::shared_ptr<URDFJointVisual>>(visual.geometry.value());
///
///   VisualTree
///       |
///       +--WorldJoint---WorldVisual
///                     |
///                     +-- Joint(TF)--Visual(+TF)
///                     |     |
///                     |     +-- Joint(TF)--Visual(+TF)
///                     |             |
///                     |             +-- Joint(TF)--Visual(+TF)
///                     |
///                     +-- Joint(TF)--Visual(+TF)
///                           |
///                           +-- Joint(TF)--Visual(+TF)
///                                   |
///                                   +-- Joint(TF)--Visual(+TF)
class MsnhNet_API URDFLinkTree
{
public:
    tl::optional<std::shared_ptr<URDFJoint>> joint;      //Joint不存在即为BaseLink
    std::vector<std::shared_ptr<URDFVisual>>    visuals;     //连杆可视化,若不存在就为(0,0,0,0,0,0)
    std::vector<std::shared_ptr<URDFCollision>> collisions;  //碰撞组件

    std::vector<std::shared_ptr<URDFLinkTree>> nextLink; //下一个坐标加连杆
};

class MsnhNet_API ParseFromXML
{
public:
    static Vector3DS parseVec3(const std::string & str);
    static RotationMatDS parseRotMatFromRpy(const std::string & str);
    static Color parseColor(const std::string& str);
    static Frame parseTransFormFromXML(tinyxml2::XMLElement *xml);

    static std::shared_ptr<URDFSphere> parseSphereFromXML(tinyxml2::XMLElement *xml);
    static std::shared_ptr<URDFBox> parseBoxFromXML(tinyxml2::XMLElement *xml);
    static std::shared_ptr<URDFCylinder> parseCylinderFromXML(tinyxml2::XMLElement *xml);
    static std::shared_ptr<URDFCone> parseConeFromXML(tinyxml2::XMLElement *xml);
    static std::shared_ptr<URDFMesh> parseMeshFromXML(tinyxml2::XMLElement *xml);
    static std::shared_ptr<URDFGeometry> parseGeomFromXML(tinyxml2::XMLElement *xml);

    static std::shared_ptr<URDFJointDynamics> parseJointDynamicsFromXML(tinyxml2::XMLElement *xml);
    static std::shared_ptr<URDFJointLimits> parseJointLimitsFromXML(tinyxml2::XMLElement *xml);
    static std::shared_ptr<URDFJointSafety> parseJointSafetyFromXML(tinyxml2::XMLElement *xml);
    static std::shared_ptr<URDFJointCalibration> parseJointCalibrationFromXML(tinyxml2::XMLElement *xml);
    static std::shared_ptr<URDFJointMimic> parseJointMimicFromXML(tinyxml2::XMLElement *xml);
    static std::shared_ptr<URDFJoint> parseJointFromXML(tinyxml2::XMLElement *xml);

    static std::shared_ptr<URDFMaterial> parseMaterialFromXML(tinyxml2::XMLElement *xml, bool onlyNameIsOk);
    static URDFInertial parseInertialFromXML(tinyxml2::XMLElement *xml);
    static std::shared_ptr<URDFVisual> parseVisualFromXML(tinyxml2::XMLElement *xml);
    static std::shared_ptr<URDFCollision> parseCollisionFromXML(tinyxml2::XMLElement *xml);
    static std::shared_ptr<URDFLink> parseLinkFromXML(tinyxml2::XMLElement *xml);
};

class MsnhNet_API URDF
{
public:
    static std::shared_ptr<URDFModel> parseURDF(const std::string& msg, bool isPath = true, const std::string& rootPath="");
    static std::shared_ptr<URDFModel> parseFromUrdfStr(const std::string& xmlString, const std::string& rootPath);
    static std::shared_ptr<URDFModel> parseFromUrdfFile(const std::string& path);
    static std::string getURDFStr(const std::string& msg);

    static std::shared_ptr<URDFLinkTree> getLinkTree(const std::shared_ptr<URDFLink> &link);
    static std::shared_ptr<URDFLinkTree> getLinkTree(const std::string& msg, bool isPath = true, const std::string& rootPath="");
    static std::shared_ptr<StringTree> getStringTree(std::shared_ptr<URDFLinkTree> &link);
    static Chain getChain(const std::shared_ptr<URDFModel>& model, const std::shared_ptr<StringTree>& strTree, const std::string& start, const std::string& end);
    static std::vector<std::string> getChainNames(const std::shared_ptr<StringTree>& strTree, const std::string& start, const std::string& end);
};

MsnhNet_API const char* getParentLinkName(tinyxml2::XMLElement* xml);
MsnhNet_API const char* getParentJointName(tinyxml2::XMLElement* xml);


}

#endif // MSNHURDF_H

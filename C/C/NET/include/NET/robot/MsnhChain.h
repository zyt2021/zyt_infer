﻿#ifndef MSNHCHAIN_H
#define MSNHCHAIN_H

#include "Msnhnet/robot/MsnhSegment.h"
#include "Msnhnet/3rdparty/nlopt/nlopt_cpp.h"
#include <thread> //NONFREE
#include <atomic> //NONFREE
#include <mutex>

namespace Msnhnet
{

class MsnhNet_API Chain
{
public:
    std::vector<Segment> segments;
    Chain();
    Chain(const Chain& chain);
    Chain& operator= (const Chain &chain);

    void initOpt();

    void addSegments(const Segment &segment);

    uint32_t getNumOfJoints() const;

    uint32_t getNumOfSegments() const;

    const Segment& getSegment(uint32_t idx) const;

    void changeRefPoint(MatSDS &src, const Vector3DS& baseAB) const;

    std::vector<double> getMinJoints() const;

    std::vector<double> getMaxJoints() const;

    std::vector<MoveType> getJointMoveTypes() const;

    /// \brief jacobi 计算雅可比矩阵
    /// \param joints 输入关节角度
    /// \param segNum 计算到第几个Seg，默认-1为全部Seg
    /// \return
    void jacobi(MatSDS &jac, const VectorXSDS &joints, int segNum = -1) const;

    /// \brief jacobi 计算雅可比矩阵
    /// \param joints 输入关节角度
    /// \param segNum 计算到第几个Seg，默认-1为全部Seg
    /// \return
    void jacobi(MatSDS &jac, const VectorXSDS &joints, const std::vector<Frame> &jointRoot,           //NONFREE
                const std::vector<Frame> &jointTip, const Frame &finalFrame, int segNum = -1) const;   //NONFREE

    /// \brief fk      正向运动学
    /// \param chain   单链机器人模型
    /// \param joints  关节角度
    /// \param segNum  计算到第几个Seg，默认-1为全部Seg
    /// \return
    Frame fk(const VectorXSDS &joints, int segNum = -1);

    /// \brief fk      正向运动学
    /// \param chain   单链机器人模型
    /// \param joints  关节角度
    /// \param segNum  计算到第几个Seg，默认-1为全部Seg
    /// \return
    Frame fk(const VectorXSDS &joints, std::vector<Frame> &jointRoot, std::vector<Frame> &jointTip, int segNum = -1); //NONFREE

    /// \brief ikNewton     基于牛顿拉普森法的IK
    /// \param desireFrame  目标位姿
    /// \param outJoints    结果
    /// \param maxIter      最大迭代次数
    /// \param eps          最小误差
    /// \return
    int ikNewton(const Frame &desireFrame, VectorXSDS &outJoints, int maxIter=100, double epsilon = 1e-6, double maxTime = 0.001);

    /// \brief ikNewton     基于牛顿拉普森法角度限制的IK
    /// \param desireFrame  目标位姿
    /// \param outJoints    结果
    /// \param maxIter      最大迭代次数
    /// \param eps          最小误差
    /// \return
    int ikNewtonJL(const Frame &desireFrame, VectorXSDS &outJoints, int maxIter=100, double epsilon = 1e-6);

    ///
    /// \brief ikNewtonRR   基于牛顿拉普森法随机角度的IK
    /// \param desireFrame  目标位姿
    /// \param outJoints    结果
    /// \param bounds       边界
    /// \param randomStart  是否随机开始
    /// \param wrap         是否对q进行限制
    /// \param maxIter      最大迭代数
    /// \param eps          最小误差
    /// \param maxTime      最大迭代时间
    /// \return
    ///
    int ikNewtonRR(const Frame &desireFrame, VectorXSDS &outJoints, const Twist& bounds = Twist(),
                    const bool &randomStart = true, const bool &wrap = true, int maxIter=100, double epsilon = 1e-6, double maxTime = 0.001);

    /// \brief cartSumSquaredErr 计算欧氏距离误差的实际函数。这将使用KDL正向运动学解算器来计算当前关节配置的笛卡尔姿势，并将其与IK解算所需的笛卡尔姿势进行比较
    /// \param x
    /// \param error
    void cartSumSquaredErr(const std::vector<double>& x, double error[]);

    ///
    /// \brief ikSQPSumSqr  基于SQP和sum squared的方法求解IK
    /// \param desireFrame  目标位姿
    /// \param outJoints    结果
    /// \param bounds       边界
    /// \param maxIter      最大迭代数
    /// \param eps          最小误差
    /// \param maxTime      最大迭代时间
    /// \return
    ///
    int ikSQPSumSqr(const Frame &desireFrame, VectorXSDS &outJoints, const Twist& bounds = Twist(),
                     int maxIter=500, double epsilon = 1e-6, double maxTime = 0.001);



    /// \brief ikLM         基于Levenberg–Marquardt的方法求解IK
    /// \param desireFrame  目标位姿
    /// \param outJoints    结果
    /// \param maxIter      最大迭代数
    /// \param eps          最小误差
    /// \param epsJoint     指定当计算的关节角度增量小于eps关节时，算法必须停止。这是为了避免在关节角度增量非常小的情况下（以浮点形式）在达到最大值时进行不必要的计算，从而有效地不再改变关节角度。默认值是高于数字精度的几个数字。
    /// \param maxTime      最大迭代时间
    /// \return
    int ikLM(const Frame &desireFrame, VectorXSDS &outJoints, int maxIter=100,   //NONFREE
             double epsilon = 1e-6, double epsJoint = 1e-15, double maxTime = 0.001);   //NONFREE



    void initIKFast(int maxIter=100,   //NONFREE
                    const Twist& bounds = Twist(), const bool &randomStart = true, const bool &wrap = true,//NONFREE
                    double epsilon = 1e-6, double epsJoint= 1e-15, double maxTime = 0.001);//NONFREE

    void stopIKFast();//NONFREE

    int ikFast(const Frame &desireFrame,  VectorXSDS &outJoints);//NONFREE1

    static void runIKNetwon(Chain *const &data, int maxIter=100, double epsilon = 1e-6, double maxTime = 0.001);//NONFREE

    static void runIKNetwonRR(Chain *const &data,const Twist& bounds = Twist(),const bool &randomStart = true,//NONFREE
                               const bool &wrap = true, int maxIter=100, double epsilon = 1e-6, double maxTime = 0.001);//NONFREE

    static void runIKLM(Chain *const &data, int maxIter=100,   //NONFREE
                        double epsilon = 1e-6, double epsJoint = 1e-15, double maxTime = 0.001);//NONFREE

    bool getIkFastInited() const; //NONFREE
    bool getSQPInited() const;

private:
    nlopt::opt _opt;
    bool _optInited       = false;
    int  _optStatus       = -1;
    int _numOfJoints;
    int _numOfSegments;
    std::vector<double> _minJoints;
    std::vector<double> _maxJoints;
    std::vector<MoveType> _jointMoveTypes;
    double _epsSQP = 0.0001;

    //跨函数调用
    Frame _desireFrameSQP;
    std::vector<double> _bestXSQP;
    Twist _boundsSQP;

    //临界区域
    atomic_bool _stopImmediately ;  //NONFREE
    atomic_bool _th1Working      ;  //NONFREE
    atomic_bool _th2Working      ;  //NONFREE
    atomic_bool _th3Working      ;  //NONFREE

    Frame _desireFrame          ;   //NONFREE
    VectorXSDS _outJoints       ;   //NONFREE
    atomic_int _res             ;   //NONFREE


    bool _th1Running           = false; //NONFREE
    bool _th2Running           = false; //NONFREE
    bool _th3Running           = false; //NONFREE
    bool _ikFastInited         = false; //NONFREE
    std::thread th1;                    //NONFREE
    std::thread th2;                    //NONFREE
    std::thread th3;                    //NONFREE
};


}

#endif // MSNHCHAIN_H

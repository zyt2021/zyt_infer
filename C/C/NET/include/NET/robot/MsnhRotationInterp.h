﻿#ifndef MSNHROTATIONINTERP_H
#define MSNHROTATIONINTERP_H
#include "Msnhnet/math/MsnhGeometryS.h"
namespace Msnhnet
{

class MsnhNet_API RotationInterp
{
public:
    RotationInterp(){}

    virtual ~RotationInterp(){}

    /// \brief setStartEnd 设置起始和末端插值
    /// \param start       起始Rotation
    /// \param end         末端Rotation
    virtual void setStartEnd(const RotationMatDS& start, const RotationMatDS& end) = 0;

    /// \brief getAngle    获取从start到end, 绕axis的旋转角度
    virtual double getAngle() = 0;

    /// \brief getPos      获取在旋转theta角时的旋转矩阵
    virtual RotationMatDS getPos(double theta) const = 0;

    /// \brief getVel      返回theta角处的旋转速度，theta的导数==thetad
    virtual Vector3DS getVel(double theta, double thetad) const = 0;

    /// \brief getAcc      返回theta角处的旋转加速度，theta的导数==thetad，theta的二阶导数==thetadd
    virtual Vector3DS getAcc(double theta,double thetad,double thetadd) const = 0;

    virtual RotationInterp* clone() = 0;

};

class MsnhNet_API RotationInterpSingleAxis : public RotationInterp
{
public:
    RotationInterpSingleAxis(){}
    virtual ~RotationInterpSingleAxis(){}

    virtual void setStartEnd(const RotationMatDS& start, const RotationMatDS& end);
    virtual double getAngle();
    virtual RotationMatDS getPos(double theta) const;
    virtual Vector3DS getVel(double theta, double thetad) const;
    virtual Vector3DS getAcc(double theta, double thetad, double thetadd) const;
    virtual RotationInterpSingleAxis* clone();
private:
    RotationMatDS _rotBase2Start;
    RotationMatDS _rotBase2End;
    Vector3DS     _rotStart2EndAxis;
    double        _angle;
};

//slerp 球面路径最短
class MsnhNet_API RotationInterpQuat : public RotationInterp
{
public:
   RotationInterpQuat(){}

   virtual ~RotationInterpQuat(){}
   virtual void setStartEnd(const RotationMatDS& start, const RotationMatDS& end);
   virtual double getAngle();
   virtual RotationMatDS getPos(double theta) const;
   virtual Vector3DS getVel(double theta, double thetad) const;
   virtual Vector3DS getAcc(double theta, double thetad, double thetadd) const;
   virtual RotationInterpQuat* clone();
private:
   RotationMatDS _rotBase2Start;
   RotationMatDS _rotBase2End;
   QuaternionDS  _quatBase2Start;
   QuaternionDS  _quatBase2End;
   double        _angle;
   double        _cosHalfTheta;
   double        _sinHalfTheta;
   double        _halfTheta;
   double        _halfThetaDivSinHalfTheta;
   double        _dHalfThetaDivSinHalfTheta;
};
}
#endif // MSNHROTATIONINTERP_H

﻿#ifndef MSNHJOINT_H
#define MSNHJOINT_H

#include <string>
#include "Msnhnet/robot/MsnhFrame.h"
namespace Msnhnet
{
enum JointType
{
    JOINT_FIXED,
    JOINT_ROT_AXIS,  //TODO: 多轴
    JOINT_ROT_X,
    JOINT_ROT_Y,
    JOINT_ROT_Z,
    JOINT_TRANS_AXIS,  //TODO: 多轴
    JOINT_TRANS_X,
    JOINT_TRANS_Y,
    JOINT_TRANS_Z
};

enum MoveType
{
    MOVE_CAN_NOT,
    MOVE_ROT_LIMIT,
    MOVE_ROT_CONTINUOUS,
    MOVE_TRANS
};

class MsnhNet_API Joint
{
public:
    Joint(const std::string &name, const JointType &type, const double &scale=1, const double &offset=0,
          const double& inertia=0, const double& damping=0, const double& stiffness=0);

    Joint(const JointType &type, const double &scale=1, const double &offset=0,
          const double &inertia=0, const double &damping=0, const double &stiffness=0);

    Joint(const std::string &name, const Vector3DS& origin, const Vector3DS& axis, const JointType &type, const double &scale=1, const double &offset=0,
          const double& inertia=0, const double& damping=0, const double& stiffness=0);

    Joint(const Vector3DS& origin, const Vector3DS& axis, const JointType &type, const double &scale=1, const double &offset=0,
          const double& inertia=0, const double& damping=0, const double& stiffness=0);

    ~Joint(){}

    const std::string  &getName() const;

    const JointType &getType() const;

    const std::string getTypeName() const;

    Vector3DS getJointAxis() const;

    const Vector3DS getOrigin() const;

    /// \brief getPos 获取在关节位置q时，关节初始到末端位姿
    /// \param q
    /// \return
    Frame getPos(const double &q) const;

    /// \brief getTwist 获取关节速度为qdot时的速度twist[v,w]
    /// \param qdot
    /// \return
    Twist getTwist(const double& qdot)const;

private:
    std::string _name;
    JointType   _type;

    double      _scale      = 1;//(默认：1)输入参数和实际几何之间的缩放关系
    double      _offset     = 0;//(默认：0)输入参数和实际几何之间灯偏移
    double      _interia    = 0;//(默认：0)关节惯性
    double      _damping    = 0;//(默认：0)关节阻尼
    double      _stiffness  = 0;//(默认：0)关节刚度

    // 同时存在多个轴方向运动的参数 //TODO: 完善
    Vector3DS    _axis;
    Vector3DS    _origin;

    mutable Frame        _jointPos;
    mutable double       _qPrev     = 0;
};

}

#endif // MSNHJOINT_H

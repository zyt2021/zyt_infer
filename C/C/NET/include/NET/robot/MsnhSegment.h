﻿#ifndef MSNHSEGMENT_H
#define MSNHSEGMENT_H

#include "Msnhnet/robot/MsnhFrame.h"
#include "Msnhnet/robot/MsnhJoint.h"

namespace Msnhnet
{

class MsnhNet_API Segment
{
public:
    Segment(const std::string &name, const Joint &joint=Joint(JOINT_FIXED),
            const Frame& endToTip=Frame(), const double jointMin = -DBL_MAX, const double jointMax = DBL_MAX);
    Segment(const Joint &joint=Joint(JOINT_FIXED), const Frame& endToTip=Frame(),
            const double jointMin = -DBL_MAX, const double jointMax = DBL_MAX);

    Segment(const Segment& in);
    Segment& operator=(const Segment& in);

    std::string getName() const;

    Joint getJoint() const;

    Frame getEndToTip() const;

    ///
    /// \brief getPos 根据给出关节转角，计算当前Segment的位姿{以基坐标为参考} 参考Robot解析P5
    /// \param q 角度
    /// \return
    ///
    Frame getPos(const double &q) const;

    ///
    /// \brief getTwist 根据给出关节转角和关节速度获取Segment的Tip速度(v和w){以基坐标为参考}, 参考Robot解析P8
    /// \param q  角度
    /// \param qdot 速度
    /// \return
    ///
    Twist getTwist(const double &q, const double &qdot) const;

    double getJointMin() const;

    double getJointMax() const;

    MoveType getMoveType() const;

private:
    std::string _name;
    Joint       _joint;
    Frame       _endToTip;
    double      _jointMin    =   -DBL_MAX;
    double      _jointMax    =   DBL_MAX;
    MoveType    _moveType;
};

}
#endif // MSNHSEGMENT_H

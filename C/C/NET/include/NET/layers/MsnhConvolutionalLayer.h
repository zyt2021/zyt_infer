﻿#ifndef MSNHCONVOLUTIONALLAYER_H
#define MSNHCONVOLUTIONALLAYER_H
#include "Msnhnet/core/MsnhBlas.h"
#include "Msnhnet/core/MsnhGemm.h"
#include "Msnhnet/layers/MsnhBaseLayer.h"
#include "Msnhnet/layers/MsnhActivations.h"
#include "Msnhnet/layers/MsnhBatchNormLayer.h"
#include "Msnhnet/utils/MsnhExport.h"

#ifdef USE_GPU
#include "Msnhnet/layers/cuda/MsnhConvolutionalLayerGPU.h"
#endif

#ifdef USE_ARM
#include "Msnhnet/layers/arm/armv7a/MsnhConvolution3x3s1.h"
#include "Msnhnet/layers/arm/armv7a/MsnhConvolution3x3s2.h"
#include "Msnhnet/layers/arm/armv7a/MsnhConvolutionSgemm.h"
#include "Msnhnet/layers/arm/armv7a/MsnhConvolution3x3s1Winograd.h"
#endif

#ifdef USE_X86
#include "Msnhnet/layers/x86/MsnhConvolution3x3LayerX86.h"
#endif

namespace Msnhnet
{
class MsnhNet_API ConvolutionalLayer:public BaseLayer
{
public:

    /// \brief ConvlutionalLayer    卷积层
    /// \param batch                batch
    /// \param steps
    /// \param height               高
    /// \param width                宽
    /// \param channel              通道
    /// \param num                  核个数
    /// \param groups               分组卷积 (分组数)
    /// \param kSize                卷积核尺寸
    /// \param strideX              步长X
    /// \param strideY              步长Y
    /// \param dilation             空洞卷积率
    /// \param padding              padding 0外围个数
    /// \param actType              激活函数
    /// \param batchNorm            是否进行BN
    /// \param binary               是否对权重二值化
    /// \param xnor                 是否对权重和输入都进行二值化
    /// \param adam                 优化方式
    /// \param useBinOutput
    /// \param groupIndex           分组卷积索引
    /// \param antialiasing         是否抗锯齿, 如果抗锯齿，强制所有的步长都为1
    /// \param shareLayer           这个层是否和其他层共享
    /// \param assistedExcitation   辅助学习策略
    /// \param deform               GPU
    ConvolutionalLayer(const int &batch, const int &steps, const int &height, const int &width, const int &channel, const int &num, const int &groups,
                       const int &kSizeX, const int &kSizeY, const int &strideX, const int &strideY, const int &dilationX, const int &dilationY, const int &paddingX, const int &paddingY, ActivationType _activation, const std::vector<float> &actParams,
                       const int &batchNorm, const float &bnEps,  const int &useBias, const int &binary, const int &xnor, const int &useBinOutput, const int &groupIndex,
                       const int &antialiasing, ConvolutionalLayer *const &shareLayer, const int &assistedExcitation, const int &deform);
    ~ConvolutionalLayer();

    //计算输出特征图的大小
    int convOutHeight();
    int convOutWidth();

    int getConvWorkSpaceSize();
    int getWorkSpaceSize32();
    int getWorkSpaceSize16();

    static void  addBias(float *const &output, float *const &biases, const int &batch, const int &num, const int &whSize);
    static void  scaleBias(float *const &output, float *const &scales, const int &batch, const int &num, const int &whSize);

    void binarizeWeights(float *const &weights, const int &num, const int &wtSize, float *const &binary);
    void cpuBinarize(float *const &x, const int &xNum, float *const &binary);
    void swapBinary();

    virtual void mallocMemory();
    void forward(NetworkState &netState);
#ifdef USE_GPU
    void forwardGPU(NetworkState &netState);
#endif
    void loadAllWeigths(std::vector<float> &weights);

    virtual void saveAllWeights(const int &mainIdx, const int &branchIdx=-1, const int &branchIdx1=-1);

    void loadScales(float *const &weights, const int& len);
    void loadBias(float *const &bias, const int& len);
    void loadWeights(float *const &weights, const int& len);
    void loadRollMean(float * const &rollMean, const int &len);
    void loadRollVariance(float * const &rollVariance, const int &len);
    void loadPreluWeights(float *const &weights, const int& len); //add Prelu 2020/11/14

    float *getWeights() const;

    float *getBiases() const;

    float *getScales() const;

    float *getRollMean() const;

    float *getRollVariance() const;

    char *getCWeights() const;

    float *getBinaryInputs() const;

    float *getBinaryWeights() const;

    float *getMeanArr() const;

    uint32_t *getBinRePackedIn() const;

    char *getTBitInput() const;

    char *getAlignBitWeights() const;

    int getBitAlign() const;

    int getLdaAlign() const;

    int getUseBias() const;

    int getNScales() const;

    int getNRollMean() const;

    int getNRollVariance() const;

    int getNBiases() const;

    int getNWeights() const;

    int getGroups() const;

    int getGroupIndex() const;

    int getXnor() const;

    int getBinary() const;

    int getUseBinOutput() const;

    int getSteps() const;

    int getAntialiasing() const;

    int getAssistedExcite() const;

    int getKSizeX() const;

    int getKSizeY() const;

    int getStride() const;

    int getStrideX() const;

    int getStrideY() const;

    int getPaddingX() const;

    int getPaddingY() const;

    int getDilationX() const;

    int getDilationY() const;

    int getBatchNorm() const;

    int getNPreluWeights() const;

protected:

#ifdef USE_ARM
    void selectArmConv(); //如果新增arm的卷积方法，注意新增

    float       *_sgemmWeightsPack4  =   nullptr;
    float       *_winogradWeights1   =   nullptr;
    float       *_winogradWeights2   =   nullptr;

    int         _winogradOutWidth    =   0;
    int         _winogradOutHeight   =   0;

    bool        useWinograd3x3S1     =   false;
    bool        useIm2ColSgemm       =   false;
    bool        use3x3S1             =   false;
    bool        use3x3S2             =   false;
#endif

    void selectX86Conv();//如果新增x86的卷积方法，注意新增

#ifdef USE_X86
    bool        use3x3S1             =   false;
    bool        use3x3S2             =   false;
#endif

    float       *_weights            =   nullptr;
    float       *_biases             =   nullptr;
    ConvolutionalLayer* _shareLayer  =   nullptr;

    float       *_scales             =   nullptr;
    float       *_rollMean           =   nullptr;
    float       *_rollVariance       =   nullptr;
    float       *_preluWeights       =   nullptr;

    char        *_cWeights           =   nullptr;
    float       *_binaryInputs       =   nullptr;
    float       *_binaryWeights      =   nullptr;
    float       *_meanArr            =   nullptr;
    uint32_t    *_binRePackedIn      =   nullptr;
    char        *_tBitInput          =   nullptr;
    char        *_alignBitWeights    =   nullptr;

#ifdef USE_GPU
#ifdef USE_CUDNN
    // 卷积算法的描述
    // cudnn_tion_fwd_algo_gemm——将卷积建模为显式矩阵乘法，
    // cudnn_tion_fwd_algo_fft——它使用快速傅立叶变换(FFT)进行卷积或
    // cudnn_tion_fwd_algo_winograd——它使用Winograd算法执行卷积
    cudnnConvolutionFwdAlgo_t       _fwAlgo;

    //卷积描述符
    cudnnConvolutionDescriptor_t    _convDesc;

    //输入输出张量描述符
    cudnnTensorDescriptor_t         _inputDesc;
    cudnnTensorDescriptor_t         _outputDesc;

    //权重描述符
    cudnnFilterDescriptor_t         _weightDesc;


    cudnnConvolutionFwdAlgo_t       _fwAlgo16;

    //输入输出张量描述符
    cudnnTensorDescriptor_t         _inputDesc16;
    cudnnTensorDescriptor_t         _outputDesc16;

    //权重描述符
    cudnnFilterDescriptor_t         _weightDesc16;

    float        *_gpuWeightsFp16    =   nullptr;
    float        *_gpuOutputFp16     =   nullptr;

#endif

    float       *_gpuWeights         =   nullptr;
    float       *_gpuBiases          =   nullptr;

    float       *_gpuScales          =   nullptr;
    float       *_gpuRollMean        =   nullptr;
    float       *_gpuRollVariance    =   nullptr;
    float       *_gpuPreluWeights    =   nullptr;

    // TODO: XOR and Bin GPU
    //    char        *_cWeights           =   nullptr;
    //    float       *_binaryInputs       =   nullptr;
    //    float       *_binaryWeights      =   nullptr;
    //    float       *_activationInput    =   nullptr;
    //    float       *_meanArr            =   nullptr;
    //    uint32_t    *_binRePackedIn      =   nullptr;
    //    char        *_tBitInput          =   nullptr;
    //    char        *_alignBitWeights    =   nullptr;
#endif

    int         _bitAlign            =   0;
    int         _ldaAlign            =   0;

    int         _useBias             =   1;

    int         _nPreluWeights       =   0;
    int         _nScales             =   0;
    int         _nRollMean           =   0;
    int         _nRollVariance       =   0;
    float       _bnEps               =   0.00001f;

    int         _nBiases             =   0;
    int         _nWeights            =   0;
    int         _groups              =   0;
    int         _groupIndex          =   0;

    int         _xnor                =   0;
    int         _binary              =   0;
    int         _useBinOutput        =   0;
    int         _steps               =   0;

    int         _antialiasing        =   0;
    int         _assistedExcite      =   0;

    int         _kSizeX              =   0;
    int         _kSizeY              =   0;
    int         _stride              =   0;
    int         _strideX             =   0;
    int         _strideY             =   0;
    int         _paddingX            =   0;
    int         _paddingY            =   0;
    int         _dilationX           =   0;
    int         _dilationY           =   0;
    int         _batchNorm           =   0;
};
}

#endif // MSNHCONVOLUTIONALLAYER_H

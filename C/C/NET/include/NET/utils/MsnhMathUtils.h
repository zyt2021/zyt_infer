﻿#ifndef MSNHMATHUTILS_H
#define MSNHMATHUTILS_H
#include "Msnhnet/config/MsnhnetCfg.h"
#include "Msnhnet/utils/MsnhExport.h"
#include <Msnhnet/math/MsnhGeometryS.h>
#include <Msnhnet/robot/MsnhFrame.h>

#include <math.h>
#ifndef M_PI
#define M_PI       3.14159265358979323846   // pi
#endif

namespace Msnhnet
{
class MsnhNet_API MathUtils
{
public:
    static float sumArray(float *const &x, const int &xNum);
    static float meanArray(float *const &x, const int &xNum );
    // 范围随机
    static float randUniform(float min, float max);
    //正态分布的random
    static float randNorm();

    static unsigned int randomGen();

    template<typename T>
    static inline void swap(T &a, T &b)
    {
        T tmp;
        tmp = a;
        a   = b;
        b   = tmp;
    }

    static double sign(double val);

    static void threePoint2Circle(const Frame& frame1,const Frame& frame2,const Frame& frame3, TranslationDS& center, double& angle, double& radius, bool& neg);
};
}

#endif // MSNHMATHUTILS_H

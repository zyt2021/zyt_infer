﻿#ifndef MSNHEXSTRING_H
#define MSNHEXSTRING_H
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>
#include <regex>
#include "Msnhnet/utils/MsnhException.h"
#include "Msnhnet/utils/MsnhExport.h"

#ifndef WIN32
#include <string.h>
#endif

namespace Msnhnet
{

class MsnhNet_API ExString
{
public:
    /// \brief split      按符号拆分
    /// \param result     返回
    /// \param str        输入
    /// \param delimiter  标志符号
    static void split(std::vector<std::string> &result, const std::string& str, const std::string& delimiter);
    /// \brief 全部大写
    static void toUpper(std::string &s);
    /// \brief 是否是大写
    static bool isUpper(std::string &s);
    /// \brief 是否是小写
    static bool isLower(std::string &s);
    /// \brief 全部小写
    static void toLower(std::string &s);
    /// \brief 左侧去除空格
    static void leftTrim(std::string &s);
    /// \brief 右侧除空格
    static void rightTrim(std::string &s);
    /// \brief 去除所有空格
    static void trim(std::string &s);
    /// \brief 删除字符
    static void deleteMark(std::string& s, const std::string& mark);
    /// \brief 是否是空
    static bool isEmpty(const std::string &s);
    /// \brief 是否是数字,包含小数点
    static bool isNum(const std::string &s);
    /// \brief 是否相等
    static bool isEqual(const std::string &a, const std::string &b);
    static bool isEqual(const char *a, const char *b);
    /// \brief 字符串转int
    static bool strToInt(const std::string &s,int &i);
    /// \brief 字符串转float
    static bool strToFloat(const std::string &s, float &i);
    /// \brief 字符串转double
    static bool strToDouble(const std::string &s, double &i);
    /// \brief 是否是email格式
    static bool isEmail(const std::string& str);
    /// \brief 是否全是中文
    static bool isChinese(const std::string& str);
    /// \brief 是否是数字或字母
    static bool isNumAndChar(const std::string& str);
    /// \brief 是否是字母
    static bool isChar(const std::string& str);
    /// \brief 是否是16进制字符
    static bool isHex(const std::string& str);
    /// \brief 左边取字符串
    static std::string left(const std::string& str, const int& n);
    /// \brief 右边取字符串
    static std::string right(const std::string& str, const int& n);
    /// \brief 中间取字符串
    static std::string mid(const std::string& str, const size_t &start, const size_t &offset);
    /// \brief 16进制字符串拆分
    static std::vector<std::string> splitHex(const std::string& str);
    /// \brief 字符串替换
    static bool replace(std::string& str, const std::string& from, const std::string& to);
};

namespace detail {

const std::string m_sTrue = "true";
const std::string m_sFalse = "false";

template<typename Target, typename Source>
struct Converter
{
    static Target convert(const Source& arg, const bool bThrow) {
        std::stringstream interpreter;
        Target result;

        if (!(interpreter << arg && interpreter >> result) && bThrow)
            throw Exception(1,"String convert error!",__FILE__, __LINE__, __FUNCTION__);

        return result;
    }
};

// 模板特化，支持bool
template<typename Source>
struct Converter<bool, Source> {

    static bool convert(const Source& arg, const bool bThrow) {
        return !!arg;
    }
};

inline bool convert(const char* from, const bool bThrow) {
    const size_t len = strlen(from);
    if (len != 4 && len != 5 && bThrow)
        throw Exception(1,"Argument is invalid!",__FILE__, __LINE__, __FUNCTION__);

    std::string str = std::string(from);

    ExString::toLower(str);

    bool r = true;
    if (len == 4) {
        r = (m_sTrue==str);

        if (r)
            return true;
    } else if (len == 5) {
        r = (m_sFalse==str);

        if (r)
            return false;
    }
    if (bThrow)
        throw Exception(1,"Argument is invalid!",__FILE__, __LINE__, __FUNCTION__);
    else
        return false;
}

template<>
struct Converter<bool, std::string> {
    static bool convert(const std::string& source, const bool bThrow = false) {
        return detail::convert(source.c_str(), bThrow);
    }
};

template<>
struct Converter<bool, const char*> {
    static bool convert(const char* source, const bool bThrow = false) {
        return detail::convert(source, bThrow);
    }
};

template<>
struct Converter<bool, char*> {
    static bool convert(char* source, const bool bThrow = false) {
        return detail::convert(source, bThrow);
    }
};

template<unsigned N>
struct Converter<bool, const char[N]> {
    static bool convert(const char (&source)[N], const bool bThrow = false) {
        return detail::convert(source, bThrow);
    }
};

template<unsigned N>
struct Converter<bool, char[N]> {
    static bool convert(const char (&source)[N], const bool bThrow = false) {
        return detail::convert(source, bThrow);
    }
};
}

template<typename Target, typename Source>
Target lexical_cast(const Source& arg, const bool bThrow = true)
{
    return detail::Converter<Target, Source>::convert(arg, bThrow);
}

}

#endif // MSNHEXSTRING_H

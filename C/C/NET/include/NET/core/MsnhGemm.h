﻿#ifndef MSNHGEMM_H
#define MSNHGEMM_H
#include "Msnhnet/config/MsnhnetCfg.h"
#include "Msnhnet/core/MsnhSimd.h"
#include "Msnhnet/utils/MsnhExport.h"
#ifdef USE_GPU
#include "Msnhnet/core/cuda/MsnhGemmGPU.h"
#endif

namespace Msnhnet
{
class MsnhNet_API Gemm
{
public:
    //  32 chs  -> 1 ch
    // 258 chs  -> 8 ch
    // 二值网络用
    static void repackInput(float *const &input, float *const &rePackedInput,
                            const int &width, const int &height, const int &channel);

    // float转bit >0?1:0, 用8bit存, uint8_t 84 => 0101 0101
    static void float2Bit(float *const &input, uint8_t *const &output, size_t size);


    /// \brief cpuIm2col    类caffe的加速卷积方式转换
    /// \param input        输入
    /// \param channelNum   通道数
    /// \param height       高(行)
    /// \param width        宽(列)
    /// \param kSize        核size
    /// \param stride       stride
    /// \param padding      padding
    /// \param output       输出
    static void cpuIm2col(float *const &input, const int &channelNum, const int &height, const int &width,
                          const int &kSize,const int &stride, const int &padding, float *const &output);

    /// \brief cpuCol2Im    类caffe的加速卷积方式转换
    /// \param input        输入
    /// \param channelNum   通道数
    /// \param height       高(行)
    /// \param width        宽(列)
    /// \param kSizeX       核sizeX
    /// \param kSizeY       核sizeY
    /// \param strideX      步长X
    /// \param strideY      步长Y
    /// \param paddingX     pad x
    /// \param paddingY     pad y
    /// \param output       输出
    static void cpuCol2Im(float *const &input, const int &channelNum, const int &height, const int &width,
                           const int &kSizeX, const int &kSizeY, const int &strideX, const int &strideY,
                           const int &paddingX, const int &paddingY, float *const &output);

    // 函数使用从int到unsigned的强制转换来比较参数a的值是否大于或等于零且小于参数b的值。b参数的类型为signed且始终为正，
    // 因此其值始终小于0x800。。。当强制转换参数的负值时，将其转换为大于0x800的值。。。
    inline static int is_a_ge_zero_and_a_lt_b(const int &a, const int &b)
    {
        return static_cast<unsigned>(a) < static_cast<unsigned>(b);
    }

    /// \brief cpuIm2colEx   真caffe的加速卷积方式转换
    /// \param input         输入
    /// \param channelNum    通道数
    /// \param height        高(行)
    /// \param width         宽(列)
    /// \param kernelH       核高
    /// \param kernelW       核宽
    /// \param padH          padding H
    /// \param padW          padding W
    /// \param strideH       步幅 h
    /// \param strideW       步幅 w
    /// \param dilationH     空洞卷积 h
    /// \param dilationW     空洞卷积 w
    /// \param output        输出
    static void cpuIm2colEx(float *input, const int &channelNum, const int &height, const int &width,
                            const int &kernelH, const int &kernelW, const int &padH, const int &padW,
                            const int &strideH,  const int &strideW, const int &dilationH, const int &dilationW,
                            float *output);

    /// \brief cpuCol2ImEx   真caffe的加速卷积方式转换
    /// \param input         输入
    /// \param channelNum    通道数
    /// \param height        高(行)
    /// \param width         宽(列)
    /// \param kernelH       核高
    /// \param kernelW       核宽
    /// \param padH          padding H
    /// \param padW          padding W
    /// \param strideH       步幅 h
    /// \param strideW       步幅 w
    /// \param dilationH     空洞卷积 h
    /// \param dilationW     空洞卷积 w
    /// \param output        输出
    static void cpuCol2ImEx(float *input, const int &channelNum, const int &height, const int &width,
                            const int &kernelH, const int &kernelW, const int &padH, const int &padW,
                            const int &strideH,  const int &strideW, const int &dilationH, const int &dilationW,
                            float *output);

    /// \brief cpuIm2colWithAvx     同上,加入了Avx加速
    /// \param input                输入
    /// \param channelNum           通道数
    /// \param height               高(行)
    /// \param width                宽(列)
    /// \param kSize                核size
    /// \param stride               stride
    /// \param padding              padding
    /// \param output               输出
    /// \param supportAvxAndFma     是否支持AVX和FMA
    static void cpuIm2colWithAvx(float * const &input, const int &channelNum, const int &height, const int &width,const int &kSize,
                                 const int &stride, const int &padding, float * const &output,const bool &supportAvxAndFma);

    /// \brief cpuIm2colBinWithAvx  加速卷积方式,二值化
    /// \param input                输入
    /// \param channelNum           通道数
    /// \param height               高(行)
    /// \param width                宽(列)
    /// \param kSize                核size
    /// \param stride               stride
    /// \param padding              padding
    /// \param output               输出
    /// \param bitAlign             字节对齐
    /// \param supportAvxAndFma     是否支持AVX和FMA
    static void cpuIm2colBinWithAvx(float * const &input, const int &channelNum, const int &height, const int &width,const int &kSize,
                                    const int &stride, const int &padding, float * const &output, const int &bitAlign, const bool &supportAvxAndFma);


    /// \brief img2ColGetPixel  通过行,列,通道获取pixel数据
    /// \param input            输入 {(ch1 12,33,44...)(ch2 13,34,45..)(ch3 14,44,66)} 数据排布
    /// \param height           输入的高
    /// \param width            输入的宽
    /// \param channelNum       通道数
    /// \param row              padding之后, 提取的行
    /// \param col              padding之后, 提取的列
    /// \param channel          提取的通道
    /// \param padding          padding
    static float img2ColGetPixel(float *const &input, const int &height, const int &width, const int &row,
                                 const int &col,const int &channel, const int &padding);

    // 基础操作 ============================================================

    /// \brief setBit
    /// \param dst    数据
    /// \param index  index
    /// dst = 00000000 00000000 index=1 ==>00000010 00000000 index=8 ==> 00000000 00000001
    static inline void setBit(uint8_t *const &dst, const int &index)
    {
        int dstI  = index / 8;
        int dstShift = index % 8;
        dst[dstI]   |= 1 << dstShift;
    }

    /// \brief getBit
    /// \param src    数据
    /// \param index  index
    /// \return       0/1
    /// dst = 1(00000001),2(00000010)  index=0 ==> 1 index=1 ==>0  index=9 ==>1
    static inline uint8_t getBit(const uint8_t *const &src, int index)
    {
        int srcI      = index / 8;
        int srcShift  = index % 8;
        uint8_t val   = (src[srcI] & (1 << srcShift)) > 0;
        return val;
    }

    // 矩阵运算=======================================================================

    ///M(4) lda(2)  N(4) ldb(4)    K(4) ldb(4)
    ///  4 * 2                         4*4               4*4
    /// { 1, 2 }      2 * 4       { 1, 0, 2, 1}    {15, 17, 22, 24}
    /// { 3, 4 } \/ {2,3,4,5} _|_ { 0, 1,-1, 3} -- {30, 38, 43, 54}
    /// { 5, 6 } /\ {6,7,8,9}  |  {-1, 2, 1, 1} -- {45, 59, 69, 80}
    /// { 7, 8 }                  { 1, 3, 2,-8}    {63, 80, 94, 99}
    /// \brief cpuGemm  C = ALPHA * A * B + BETA * C
    /// \param TA       是否需要对A进行转置   M*A N*A => M*A X A*N =>M*N
    /// \param TB       是否需要对B进行转置
    /// \param M        (TA) ? A'的行和C的行 : A的行和C的行 {M}
    /// \param N        (TB) ? B'的列和C的列 : B的列和C的列 {N}
    /// \param K        {(TA) ? A'的列 : A的列} == { (TB) ? B'的列 : B的列}
    /// \param ALPHA    系数
    /// \param A        A(内存一行排布)
    /// \param lda      (TA) ? A'的列 : A的列
    /// \param B        B(内存一行排布)
    /// \param ldb      (TB) ? B'的列 : B的列
    /// \param BETA     系数
    /// \param C        C(内存一行排布)
    /// \param ldc      C的列
    static void cpuGemm(const int &TA,   const int &TB, const int &M, const int &N, const int &K, const float &ALPHA,
                        float *const &A, const int &lda,
                        float *const &B, const int &ldb,
                        const float &BETA,
                        float *const &C, const int &ldc,
                        const bool &supportAvxAndFma);

    static void cpuGemm(const int &TA,   const int &TB, const int &M, const int &N, const int &K, const double &ALPHA,
                        double *const &A, const int &lda,
                        double *const &B, const int &ldb,
                        const float &BETA,
                        double *const &C, const int &ldc,
                        const bool &supportAvxAndFma);

    /// \brief cpuGemmNN     C = ALPHA*A*B +C //参数见cpuGemm
    /// \param M
    /// \param N
    /// \param K
    /// \param ALPHA
    /// \param A
    /// \param lda
    /// \param B
    /// \param ldb
    /// \param C
    /// \param ldc
    /// \param supportAvxAndFma
    static void cpuGemmNN(const int &M, const int &N, const int &K, const float &ALPHA,
                          float *const &A, const int &lda,
                          float *const &B, const int &ldb,
                          float *const &C, const int &ldc,
                          const bool &supportAvxAndFma);

    static void cpuGemmNN(const int &M, const int &N, const int &K, const double &ALPHA,
                          double *const &A, const int &lda,
                          double *const &B, const int &ldb,
                          double *const &C, const int &ldc,
                          const bool &supportAvxAndFma);

    /// \brief cpuGemmTN     C = ALPHA*A'*B +C //参数见cpuGemm
    /// \param M
    /// \param N
    /// \param K
    /// \param ALPHA
    /// \param A
    /// \param lda
    /// \param B
    /// \param ldb
    /// \param C
    /// \param ldc
    /// \param supportAvxAndFma
    static void cpuGemmTN(const int &M, const int &N, const int &K, const float &ALPHA,
                          float *const &A, const int &lda,
                          float *const &B, const int &ldb,
                          float *const &C, const int &ldc,
                          const bool &supportAvxAndFma);

    static void cpuGemmTN(const int &M, const int &N, const int &K, const double &ALPHA,
                          double *const &A, const int &lda,
                          double *const &B, const int &ldb,
                          double *const &C, const int &ldc,
                          const bool &supportAvxAndFma);

    /// \brief cpuGemmNT     C = ALPHA*A*B' +C //参数见cpuGemm
    /// \param M
    /// \param N
    /// \param K
    /// \param ALPHA
    /// \param A
    /// \param lda
    /// \param B
    /// \param ldb
    /// \param C
    /// \param ldc
    /// \param supportAvxAndFma
    static void cpuGemmNT(const int &M, const int &N, const int &K, const float &ALPHA,
                          float *const &A, const int &lda,
                          float *const &B, const int &ldb,
                          float *const &C, const int &ldc,
                          const bool &supportAvxAndFma);

    static void cpuGemmNT(const int &M, const int &N, const int &K, const double &ALPHA,
                          double *const &A, const int &lda,
                          double *const &B, const int &ldb,
                          double *const &C, const int &ldc,
                          const bool &supportAvxAndFma);

    /// \brief cpuGemmTT     C = ALPHA*A'*B' +C //参数见cpuGemm
    /// \param M
    /// \param N
    /// \param K
    /// \param ALPHA
    /// \param A
    /// \param lda
    /// \param B
    /// \param ldb
    /// \param C
    /// \param ldc
    /// \param supportAvxAndFma
    static void cpuGemmTT(const int &M, const int &N, const int &K, const float &ALPHA,
                          float *const &A, const int &lda,
                          float *const &B, const int &ldb,
                          float *const &C, const int &ldc,
                          const bool &supportAvxAndFma);

    static void cpuGemmTT(const int &M, const int &N, const int &K, const double &ALPHA,
                          double *const &A, const int &lda,
                          double *const &B, const int &ldb,
                          double *const &C, const int &ldc,
                          const bool &supportAvxAndFma);


    static void cpuFastADotB(const int &n, float *const &A, float *const& B, float *const &C);


#define TILE_F32_M 4  // 4 ops
#define TILE_F32_N 16 // AVX2 = 2 ops * 8 floats
#define TILE_F32_K 16 // loop

#define TILE_F64_M 4  // 4 ops
#define TILE_F64_N 8 // AVX2 = 2 ops * 4 floats
#define TILE_F64_K 16 // loop

    /// \brief cpuGemmNNFast  快速计算C = ALPHA*A*B +C //参数见cpuGemm
    /// \param M
    /// \param N
    /// \param K
    /// \param ALPHA
    /// \param A
    /// \param lda
    /// \param B
    /// \param ldb
    /// \param C
    /// \param ldc
    static void cpuGemmNNFast(const int &M, const int &N, const int &K, const float &ALPHA,
                              float *const &A, const int &lda,
                              float *const &B, const int &ldb,
                              float *const &C, const int &ldc);

    static void cpuGemmNNFast(const int &M, const int &N, const int &K, const double &ALPHA,
                              double *const &A, const int &lda,
                              double *const &B, const int &ldb,
                              double *const &C, const int &ldc);

    static void cpuGemmTNFast(const int &M, const int &N, const int &K, const float &ALPHA,
                              float *const &A, const int &lda,
                              float *const &B, const int &ldb,
                              float *const &C, const int &ldc);

    static void cpuGemmTNFast(const int &M, const int &N, const int &K, const double &ALPHA,
                              double *const &A, const int &lda,
                              double *const &B, const int &ldb,
                              double *const &C, const int &ldc);

    // 转置==========================================================================

    static void swapVal(uint32_t &a0, uint32_t&a1, int &j, unsigned &m);

    static uint8_t lookup[16] ;
    // 10000000 => 00000001
    static inline uint8_t reverse8Bit(const uint8_t &a)
    {
        return ((a * 0x0802U & 0x22110U) | (a * 0x8020U & 0x88440U)) * 0x10101U >> 16;
    }

    static inline uint8_t reverseByte1(const uint8_t &a)
    {
        return static_cast<uint8_t>(((a & 0x1) << 7) | ((a & 0x2) << 5) |
                                    ((a & 0x4) << 3) | ((a & 0x8) << 1) |
                                    ((a & 0x10) >> 1) | ((a & 0x20) >> 3) |
                                    ((a & 0x40) >> 5) | ((a & 0x80) >> 7));
    }

    static inline uint8_t reverseByte3(const uint8_t &n)
    {
        // Reverse the top and bottom nibble then swap them.
        return static_cast<uint8_t>((lookup[n & 0b1111] << 4) | lookup[n >> 4]);
    }

    // 10000000 00000000 000000000 00000000 => 00000000 00000000 00000000 00000001
    static inline uint32_t reverse32Bit(const uint32_t &a)
    {
        return  static_cast<uint32_t>((reverse8Bit(static_cast<uint8_t>(a >> 24)) << 0) |
                                      (reverse8Bit(static_cast<uint8_t>(a >> 16)) << 8) |
                                      (reverse8Bit(static_cast<uint8_t>(a >> 8)) << 16) |
                                      (reverse8Bit(static_cast<uint8_t>(a >> 0)) << 24));
    }


    /// \brief transpose32Optimized
    /// \param A
    /// \param num
    static void transpose32Optimized(uint32_t *const& A, const int &num=32);


    /// \brief transpose32x32ReversedDiag
    /// \param A
    /// \param B
    /// \param m
    /// \param n
    static void transpose32x32ReversedDiag(uint32_t *const &A, uint32_t *const &B, const int &m, const int &n);


    /// \brief transposeBinary 以二进制进行转置
    /// \param A               输入矩阵(一维格式)
    /// \param B               输出矩阵(一维格式)
    /// \param n               行
    /// \param m               列
    /// \param lda             a的列
    /// \param ldb             b的列
    /// \param blockSize
    static void transposeBinary(uint32_t *const &A, uint32_t *const &B, const int &n, const int &m,
                                const int &lda, const int &ldb, const int &blockSize);

    /// \brief binTransposeAlinInput 二值网络用
    /// \param k
    /// \param n
    /// \param b
    /// \param tBitInput
    /// \param ldbAlign
    /// \param bitAlign
    /// \return
    static int binTransposeAlinInput(int k, int n, float *b, char **tBitInput, int ldbAlign, int bitAlign);


    /// \brief trainsposeUint32 对uint32转置
    /// \param input            输出
    /// \param output           输出
    /// \param inH              宽
    /// \param inW              高
    /// \param inAlign          输入对齐
    /// \param outAlign         输出对齐
    static void transposeUint32(uint32_t *const &input, uint32_t *const &output, const int &inH,
                                const int &inW, const int &inAlign, const int &outAlign);


    // TODO: ===============================================================

#ifdef USE_X86
    // 暂时不研究
    static void gemmNNBinMeanTrans(int M, int N, int K, float ALPHA_UNUSED,
                                   unsigned char *A, int lda,
                                   unsigned char *B, int ldb,
                                   float *C, int ldc, float *mean_arr);

    // 暂时不研究
    static inline void xnorAvx2Popcnt(__m256i aBit256, __m256i bBit256, __m256i *countSum)
    {
        __m256i cBit256 = _mm256_set1_epi8(static_cast<char>(-1));
        __m256i xor256  = _mm256_xor_si256(aBit256, bBit256);  // xnor = not(xor(a,b))

        cBit256         = _mm256_andnot_si256(xor256, cBit256);       // can be optimized - we can do other NOT for wegihts once and do not do this NOT

        *countSum      = _mm256_add_epi64(count256(cBit256), *countSum);    //  1st part - popcnt Mula's algorithm
    }

    // 暂时不研究
    // 2nd part - popcnt Mula's algorithm

    static inline int getCountMula(__m256i countSum)
    {

        return  static_cast<int>(_mm256_extract_epi64(countSum, 0)
                                 + _mm256_extract_epi64(countSum, 1)
                                 + _mm256_extract_epi64(countSum, 2)
                                 + _mm256_extract_epi64(countSum, 3));

    }

    static inline int popcnt128(__m128i n)
    {
        const __m128i nHi = _mm_unpackhi_epi64(n, n);
#if defined(_MSC_VER)
        return static_cast<int>(__popcnt64(_mm_cvtsi128_si64(n)) + __popcnt64(_mm_cvtsi128_si64(nHi)));
#elif defined(__APPLE__) && defined(__clang__)
        return _mm_popcnt_u64(_mm_cvtsi128_si64(n)) + _mm_popcnt_u64(_mm_cvtsi128_si64(nHi));
#else
        return __popcntq(_mm_cvtsi128_si64(n)) + __popcntq(_mm_cvtsi128_si64(nHi));
#endif
    }

    static inline int popcnt256(__m256i n)
    {
        return popcnt128(_mm256_extractf128_si256(n, 0)) + popcnt128(_mm256_extractf128_si256(n, 1));
    }

    static inline __m256i count256(__m256i v)
    {
        __m256i lookup   = _mm256_setr_epi8(0, 1, 1, 2, 1, 2, 2, 3, 1, 2,
                                            2, 3, 2, 3, 3, 4, 0, 1, 1, 2, 1, 2, 2, 3,
                                            1, 2, 2, 3, 2, 3, 3, 4);

        __m256i low_mask = _mm256_set1_epi8(0x0f);

        __m256i lo = _mm256_and_si256(v, low_mask);
        __m256i hi = _mm256_and_si256(_mm256_srli_epi32(v, 4), low_mask);
        __m256i popcnt1 = _mm256_shuffle_epi8(lookup, lo);
        __m256i popcnt2 = _mm256_shuffle_epi8(lookup, hi);
        __m256i total = _mm256_add_epi8(popcnt1, popcnt2);

        return _mm256_sad_epu8(total, _mm256_setzero_si256());
    }

    static inline int popcnt256_custom(__m256i n)
    {
        __m256i val = count256(n);

        //return val.m256i_i64[0] +
        //val.m256i_i64[1] +
        //val.m256i_i64[2] +
        //val.m256i_i64[3];
        return  static_cast<int>( _mm256_extract_epi64(val, 0)
                                  + _mm256_extract_epi64(val, 1)
                                  + _mm256_extract_epi64(val, 2)
                                  + _mm256_extract_epi64(val, 3));
    }
#endif

};

}

#endif // MSNHGEMM_H

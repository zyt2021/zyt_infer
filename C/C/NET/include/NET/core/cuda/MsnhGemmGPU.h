﻿#ifndef MSNHGEMMGPU_H
#define MSNHGEMMGPU_H

#include "Msnhnet/utils/MsnhExport.h"
#include "Msnhnet/config/MsnhnetCuda.h"

namespace Msnhnet
{
class MsnhNet_API GemmGPU
{
public:
    /// \brief gpuGemm  C = ALPHA * A * B + BETA * C
    /// \param TA       是否需要对A进行转置   M*A N*A => M*A X A*N =>M*N
    /// \param TB       是否需要对B进行转置
    /// \param M        (TA) ? A'的行和C的行 : A的行和C的行 {M}
    /// \param N        (TB) ? B'的列和C的列 : B的列和C的列 {N}
    /// \param K        {(TA) ? A'的列 : A的列} == { (TB) ? B'的列 : B的列}
    /// \param ALPHA    系数
    /// \param A        A(内存一行排布)
    /// \param lda      (TA) ? A'的列 : A的列
    /// \param B        B(内存一行排布)
    /// \param ldb      (TB) ? B'的列 : B的列
    /// \param BETA     系数
    /// \param C        C(内存一行排布)
    /// \param ldc      C的列
    static void gpuGemm(const int &TA,   const int &TB, const int &M, const int &N, const int &K, const float &ALPHA,
                        float *const &A, const int &lda,
                        float *const &B, const int &ldb,
                        const float &BETA,
                        float *const &C, const int &ldc);

    /// \brief gpuCol2ImEx   真caffe的加速卷积方式转换
    /// \param input         输入
    /// \param channelNum    通道数
    /// \param height        高(行)
    /// \param width         宽(列)
    /// \param kernelH       核高
    /// \param kernelW       核宽
    /// \param padH          padding H
    /// \param padW          padding W
    /// \param strideH       步幅 h
    /// \param strideW       步幅 w
    /// \param dilationH     空洞卷积 h
    /// \param dilationW     空洞卷积 w
    /// \param output        输出
    static void gpuIm2ColEx(float *input, const int &channelNum, const int &height, const int &width,
                            const int &kernelH, const int &kernelW, const int &padH, const int &padW,
                            const int &strideH,  const int &strideW, const int &dilationH, const int &dilationW,
                            float *output);

    /// \brief gpuIm2col    类caffe的加速卷积方式转换
    /// \param input        输入
    /// \param channelNum   通道数
    /// \param height       高(行)
    /// \param width        宽(列)
    /// \param kSize        核size
    /// \param stride       stride
    /// \param padding      padding
    /// \param output       输出
    static void gpuIm2col(float *const &input, const int &channelNum, const int &height, const int &width,
                          const int &kSize,const int &stride, const int &padding, float *const &output);

};

}

#endif // MSNHGEMM_H

﻿/**
* @projectName   Msnhnet210319
* @brief         摘要
* @author        Msnh
* @date          2021-03-23
*/

#include <iostream>
#include "Msnhnet/net/MsnhNetBuilder.h"
#include "Msnhnet/utils/MsnhExVector.h"
#include "Msnhnet/utils/MsnhOpencvUtil.h"
#include "Msnhnet/utils/MsnhCVUtil.h"
#include "Msnhnet/io/MsnhIO.h"
#include "Msnhnet/config/MsnhnetCfg.h"
#include <thread>
#include "../test/testActivationsAvx.h"
#include "Msnhnet/layers/MsnhDeConvolutionalLayer.h"
#include "Msnhnet/io/MsnhIO.h"
#include "Msnhnet/config/MsnhnetCuda.h"
#include <atomic>
#include <Msnhnet/core/cuda/MsnhBlasGPU.h>
#include <QtOpenGL/QtOpenGL>

#include <Msnhnet/cv/MsnhCVDraw.h>
#include <Msnhnet/cv/MsnhCVMatOp.h>
#include <Msnhnet/cv/MsnhCVVideo.h>
#include <Msnhnet/cv/MsnhCV.h>
#include <Msnhnet/robot/MsnhFrame.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>


#include <Msnhnet/robot/MsnhRobot.h>
#include <Msnhnet/Msnhnet.h>
#include <Msnhnet/cv/MsnhCVThread.h>
#include <Msnhnet/hardware/MsnhSerialPort.h>
#include <Msnhnet/3rdparty/simdb/simdb.h>

#include <opencv2/opencv.hpp>
#include <Msnhnet/3rdparty/imgui/implot.h>

#include <QThread>

#ifdef WIN32
#pragma execution_character_set("utf-8")
#endif

//#include <vld.h>


std::string root = "E:/works/qt/casia/projects/";
//std::string root = "/media/msnh/Files/works/qt/casia/projects/";

//std::string root = "G:/casia/projects/Msnhnet/Msnhnet0712/";
using namespace std;
using namespace Msnhnet;


//class NRThread: public Thread
//{
//public:

//    Chain puma560;
//    Chain kuka;
//    Chain puma4;

//    std::vector<uint64_t> success;
//    std::vector<uint64_t> failed;
//    std::vector<uint64_t> all;
//    std::vector<double>  f;
//    uint32_t dt = 0;
//    double mss = 0;
//    uint64_t cnt = 0;
//    int max = 0;

//    NRThread()
//    {
//        success.resize(OMP_THREAD);
//        failed.resize(OMP_THREAD);
//        all.resize(100);
//        puma560.addSegments(Segment("link1",Joint(JOINT_ROT_Z),Frame::SDH(0     ,MSNH_PI_2 ,0     ,0) , -MSNH_PI, MSNH_PI));
//        puma560.addSegments(Segment("link2",Joint(JOINT_ROT_Z),Frame::SDH(0.4318,0         ,0     ,0) , -MSNH_PI, MSNH_PI));
//        puma560.addSegments(Segment("link3",Joint(JOINT_ROT_Z),Frame::SDH(0.0203,-MSNH_PI_2,0.1500,0) , -MSNH_PI, MSNH_PI));
//        puma560.addSegments(Segment("link4",Joint(JOINT_ROT_Z),Frame::SDH(0     ,MSNH_PI_2 ,0.4318,0) , -MSNH_PI, MSNH_PI));
//        puma560.addSegments(Segment("link5",Joint(JOINT_ROT_Z),Frame::SDH(0     ,-MSNH_PI_2,0     ,0) , -MSNH_PI, MSNH_PI));
//        puma560.addSegments(Segment("link6",Joint(JOINT_ROT_Z),Frame::SDH(0     ,0         ,0     ,0) , -MSNH_PI, MSNH_PI));

//        puma4.addSegments(Segment("link1",Joint(JOINT_ROT_Z),Frame::SDH(0     ,MSNH_PI_2 ,0     ,0) , -MSNH_PI, MSNH_PI));
//        puma4.addSegments(Segment("link2",Joint(JOINT_ROT_Z),Frame::SDH(0.4318,0         ,0     ,0) , -MSNH_PI, MSNH_PI));
//        puma4.addSegments(Segment("link3",Joint(JOINT_ROT_Z),Frame::SDH(0.0203,-MSNH_PI_2,0.1500,0) , -MSNH_PI, MSNH_PI));
//        puma4.addSegments(Segment("link4",Joint(JOINT_ROT_Z),Frame::SDH(0     ,MSNH_PI_2 ,0.4318,0) , -MSNH_PI, MSNH_PI));
//        puma4.addSegments(Segment("link5",Joint(JOINT_ROT_Z),Frame::SDH(0     ,-MSNH_PI_2,0     ,0) , -MSNH_PI, MSNH_PI));


//        kuka.addSegments(Segment("link1",Joint(JOINT_ROT_Z),Frame::SDH(0.0, 0.0, 0.31, 0.0) , -MSNH_PI, MSNH_PI));
//        kuka.addSegments(Segment("link2",Joint(JOINT_ROT_Z),Frame::SDH(0.0, 1.5707963, 0.0, 0.0) , -MSNH_PI, MSNH_PI));
//        kuka.addSegments(Segment("link3",Joint(JOINT_ROT_Z),Frame::SDH(0.0, -1.5707963, 0.4, 0.0) , -MSNH_PI, MSNH_PI));
//        kuka.addSegments(Segment("link4",Joint(JOINT_ROT_Z),Frame::SDH(0.0, -1.5707963, 0.0, 0.0) , -MSNH_PI, MSNH_PI));
//        kuka.addSegments(Segment("link5",Joint(JOINT_ROT_Z),Frame::SDH(0.0, 1.5707963, 0.39, 0.0) , -MSNH_PI, MSNH_PI));
//        kuka.addSegments(Segment("link6",Joint(JOINT_ROT_Z),Frame::SDH(0.0, 1.5707963, 0.0, 0.0) , -MSNH_PI, MSNH_PI));
//        kuka.addSegments(Segment("link7",Joint(JOINT_ROT_Z),Frame::SDH(0.0, -1.5707963, 0.0, 0.0) , -MSNH_PI, MSNH_PI));
//        kuka.addSegments(Segment("link8"));
//    }

//protected:
//    void run()
//    {
//        puma560.initOpt();
//        puma560.initIKFast();
//#define FIVE
//#ifdef FIVE
//        VectorXSDS qlast(5);
//        puma4.initOpt();
//#endif
//#ifdef SIX
//        VectorXSDS qlast(6);
//#endif
//#ifdef SEVEN
//        VectorXSDS qlast(7);
//#endif
//        auto st = std::chrono::high_resolution_clock::now();

//#ifdef FIVE
//        for (int q1 = -180; q1 < 180; q1+=60)
//        {
//            for (int q2 = -180; q2 < 180; q2+=60)
//            {
//                for (int q3 = -180; q3 < 180; q3+=60)
//                {
//                    for (int q4 = -180; q4 < 180; q4+=60)
//                    {
//                        for (int q5 = -180; q5 < 180; q5+=60)
//                        {

//                            VectorXSDS q({deg2radd(q1),
//                                          deg2radd(q2),
//                                          deg2radd(q3),
//                                          deg2radd(q4),
//                                          deg2radd(q5)});

//                            Frame fk = puma4.fk(q);

//                            VectorXSDS qin = VectorXSDS({0,
//                                                         0,
//                                                         0,
//                                                         0,
//                                                         0});
//                            int res1=puma4.ikSQPSumSqr(fk, qin);

//                            //                                Frame ikfk = puma560.fk(qin);


//                            //                                f.push_back(Frame::diff(fk,ikfk).length());



//                            //                                if(res1 < 0)
//                            //                                {
//                            //                                    q.print();
//                            //                                    return;
//                            //                                }

//                            //                                auto ikfk = puma560.fk(qin);


//                            //                                std::cout<<((fk.trans-ikfk.trans).length()<1e-6)<<std::endl;

//                            //                                std::cout<<res1<<std::endl;

//                            //                                qin = VectorXSDS({0,
//                            //                                                  0,
//                            //                                                  0,
//                            //                                                  0,
//                            //                                                  0,
//                            //                                                  0});
//                            //                                int res3=puma560.ikNewtonRR(fk, qin);


//                            //std::cout<<res1<<std::endl;

//                            if(res1 >= 0/*|| res3>0*/)
//                            {
//                                success[omp_get_thread_num()] ++;
//                                int min = res1/*<res3?res1:res3*/;

//                                //all[min] +=1;

//                                if(max<min)
//                                    max = min;

//                                cnt += res1/*<res3?res1:res3*/;
//                            }
//                            else
//                            {
//                                failed[omp_get_thread_num()] ++;
//                            }

//                        }
//                    }
//                }
//            }
//        }
//#endif

//#ifdef SIX
//        for (int q1 = -180; q1 < 180; q1+=30)
//        {
//            for (int q2 = -180; q2 < 180; q2+=30)
//            {
//                for (int q3 = -180; q3 < 180; q3+=60)
//                {
//                    for (int q4 = -180; q4 < 180; q4+=60)
//                    {
//                        for (int q5 = -180; q5 < 180; q5+=60)
//                        {
//                            for (int q6 = -180; q6 < 180; q6+=60)
//                            {

//                                VectorXSDS q({deg2radd(q1),
//                                              deg2radd(q2),
//                                              deg2radd(q3),
//                                              deg2radd(q4),
//                                              deg2radd(q5),
//                                              deg2radd(q6)});

//                                Frame fk = puma560.fk(q);

//                                VectorXSDS qin = VectorXSDS({0,
//                                                             0,
//                                                             0,
//                                                             0,
//                                                             0,
//                                                             0});
//                                int res1=puma560.ikLM(fk, qin);

//                                //                                Frame ikfk = puma560.fk(qin);


//                                //                                f.push_back(Frame::diff(fk,ikfk).length());



//                                //                                if(res1 < 0)
//                                //                                {
//                                //                                    q.print();
//                                //                                    return;
//                                //                                }

//                                //                                auto ikfk = puma560.fk(qin);


//                                //                                std::cout<<((fk.trans-ikfk.trans).length()<1e-6)<<std::endl;

//                                //                                std::cout<<res1<<std::endl;

//                                //                                qin = VectorXSDS({0,
//                                //                                                  0,
//                                //                                                  0,
//                                //                                                  0,
//                                //                                                  0,
//                                //                                                  0});
//                                //                                int res3=puma560.ikNewtonRR(fk, qin);


//                                //std::cout<<res1<<std::endl;

//                                if(res1 >= 0/*|| res3>0*/)
//                                {
//                                    success[omp_get_thread_num()] ++;
//                                    int min = res1/*<res3?res1:res3*/;

//                                    //all[min] +=1;

//                                    if(max<min)
//                                        max = min;

//                                    cnt += res1/*<res3?res1:res3*/;
//                                }
//                                else
//                                {
//                                    failed[omp_get_thread_num()] ++;
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }

//#endif

//#ifdef SEVEN
//        for (int q1 = -180; q1 < 180; q1+=60)
//        {
//            for (int q2 = -180; q2 < 180; q2+=60)
//            {
//                for (int q3 = -180; q3 < 180; q3+=60)
//                {
//                    for (int q4 = -180; q4 < 180; q4+=60)
//                    {
//                        for (int q5 = -180; q5 < 180; q5+=60)
//                        {
//                            for (int q6 = -180; q6 < 180; q6+=60)
//                            {
//                                for (int q7 = -180; q7 < 180; q7+=60)
//                                {
//                                    VectorXSDS q({deg2radd(q1),
//                                                  deg2radd(q2),
//                                                  deg2radd(q3),
//                                                  deg2radd(q4),
//                                                  deg2radd(q5),
//                                                  deg2radd(q6),
//                                                  deg2radd(q7),
//                                                 });

//                                    Frame fk = kuka.fk(q);

//                                    VectorXSDS qin = VectorXSDS({0,
//                                                                 0,
//                                                                 0,
//                                                                 0,
//                                                                 0,
//                                                                 0,
//                                                                 0});
//                                    int res1=kuka.ikLM(fk, qin);

//                                    //                                Frame ikfk = puma560.fk(qin);


//                                    //                                f.push_back(Frame::diff(fk,ikfk).length());



//                                    //                                if(res1 < 0)
//                                    //                                {
//                                    //                                    q.print();
//                                    //                                    return;
//                                    //                                }

//                                    //                                auto ikfk = puma560.fk(qin);


//                                    //                                std::cout<<((fk.trans-ikfk.trans).length()<1e-6)<<std::endl;

//                                    //                                std::cout<<res1<<std::endl;

//                                    //                                qin = VectorXSDS({0,
//                                    //                                                  0,
//                                    //                                                  0,
//                                    //                                                  0,
//                                    //                                                  0,
//                                    //                                                  0});
//                                    //                                int res3=puma560.ikNewtonRR(fk, qin);


//                                    //std::cout<<res1<<std::endl;

//                                    if(res1 >= 0/*|| res3>0*/)
//                                    {
//                                        success[omp_get_thread_num()] ++;
//                                        int min = res1/*<res3?res1:res3*/;

//                                        //all[min] +=1;

//                                        if(max<min)
//                                            max = min;

//                                        cnt += res1/*<res3?res1:res3*/;
//                                    }
//                                    else
//                                    {
//                                        failed[omp_get_thread_num()] ++;
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//#endif
//        puma560.stopIKFast();
//        auto so = std::chrono::high_resolution_clock::now();
//        std::cout<<std::chrono::duration <double,std::milli> ((so-st)).count()/(179202+7422)<< "------Msnhnet" << std::endl;

//    }

//};

//int main(){
//    Chain puma560;
//    // a alpah d theta
//    puma560.addSegments(Segment("link1",Joint(Joint::JOINT_ROT_Z),Frame::SDH(0     ,MSNH_PI_2 ,0     ,0)));
//    puma560.addSegments(Segment("link2",Joint(Joint::JOINT_ROT_Z),Frame::SDH(0.4318,0         ,0     ,0)));
//    puma560.addSegments(Segment("link3",Joint(Joint::JOINT_ROT_Z),Frame::SDH(0.0203,-MSNH_PI_2,0.1500,0)));
//    puma560.addSegments(Segment("link4",Joint(Joint::JOINT_ROT_Z),Frame::SDH(0     ,MSNH_PI_2 ,0.4318,0)));
//    puma560.addSegments(Segment("link5",Joint(Joint::JOINT_ROT_Z),Frame::SDH(0     ,-MSNH_PI_2,0     ,0)));
//    puma560.addSegments(Segment("link6",Joint(Joint::JOINT_ROT_Z),Frame::SDH(0     ,0         ,0     ,0)));

//    Frame f = ChainFK::jointToCartesian(puma560,{MSNH_PI_2,0,0,0,0,0});
//    std::cout << "Final frame: \n";
//    f.print();
//    std::cout << "Euler(Rad):  \n";
//    Geometry::rotMat2Euler(f.getRotationMat(),ROT_ZYX).print();
//    std::cout << "\nPosition:  \n";
//    f.getTranslation().print();

//    return 1;
//}
#include <Eigen/Core>
#include <Eigen/Dense>

using namespace Eigen;


#ifdef USE_OPENCV
void test_yolo_video()
{
    std::string config_file_ = root + "models/yolov3/yolov3.msnhnet";
    Msnhnet::NetBuilder  msnhNet;
    msnhNet.buildNetFromMsnhNet(config_file_);
    std::cout<<msnhNet.getLayerDetail();
    msnhNet.loadWeightsFromMsnhBin(root + "models/yolov3/yolov3.msnhbin");
    std::vector<std::string> labels ;
    std::string labelPath   =   root + "models/yolov3/coco.names";
    Msnhnet::IO::readVectorStr(labels, labelPath.data(), "\n");

    cv::VideoCapture cap(0);
    cv::Mat mat;
    if(!cap.isOpened())
    {
        std::cout<< "cam err" <<std::endl;
    }

    while (1)
    {
        cap >> mat;
        cv::Mat org = mat.clone();
        std::vector<float> img = Msnhnet::OpencvUtil::getPaddingZeroF32C3(mat, cv::Size(416,416));
        std::vector<std::vector<Msnhnet::YoloBox>> result = msnhNet.runYoloGPU(img);
        Msnhnet::OpencvUtil::drawYoloBox(org,labels,result,Msnhnet::Point2I(416,416));
        cv::imshow("test",org);
        if(cv::waitKey(20) == 27)
        {
            break;
        }

    }
}

void test_yolo()
{
    std::string config_file_ = root + "models/yolov3/yolov3.msnhnet";
    //try
    {
        Msnhnet::NetBuilder  msnhNet;
        //msnhNet.setSaveLayerOutput(true);
        //Msnhnet::NetBuilder::setOnlyGpu(true);
        //msnhNet.setUseFp16(true);
        msnhNet.buildNetFromMsnhNet(config_file_);
        std::cout<<msnhNet.getLayerDetail();
        msnhNet.loadWeightsFromMsnhBin(root + "models/yolov3/yolov3.msnhbin");
        std::vector<std::string> labels ;
        std::string labelPath   =   root + "models/yolov3/coco.names";
        Msnhnet::IO::readVectorStr(labels, labelPath.data(), "\n");
        Msnhnet::Point2I inSize = msnhNet.getInputSize();

        cv::Mat org = cv::imread(root + "models/yolov3/bus.jpg");

        std::vector<std::vector<Msnhnet::YoloBox>> result ;
        std::vector<float> img = Msnhnet::OpencvUtil::getPaddingZeroF32C3(root + "models/yolov3/bus.jpg",cv::Size(inSize.x,inSize.y));

        for (int i = 0; i < 1; ++i)
        {
            auto st = std::chrono::high_resolution_clock::now();
            result = msnhNet.runYolo(img);
            auto so = std::chrono::high_resolution_clock::now();
            std::cout<<std::chrono::duration <double,std::milli> (so-st).count()<< "---------------------------------------" << std::endl;

        }

        Msnhnet::OpencvUtil::drawYoloBox(org,labels,result,inSize);
        std::cout<<msnhNet.getTimeDetail()<<" "<<inSize.x<<" "<<inSize.y<<std::endl;
        cv::pyrDown(org,org);
        cv::imshow("test",org);
        cv::waitKey();
    }
    //    catch (Msnhnet::Exception ex)
    //    {
    //        std::cout<<ex.what()<<" "<<ex.getErrFile() << " " <<ex.getErrLine()<< " "<<ex.getErrFun()<<std::endl;
    //    }
}

void test_lenet5()
{
    std::vector<float>  img{0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,0.f,0.f,0.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,0.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,0.f,0.f,0.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,0.f,1.f,1.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,1.f,1.f,1.f,1.f,1.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,1.f,1.f,1.f,1.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,1.f,1.f,1.f,1.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,1.f,1.f,1.f,1.f,1.f,1.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,1.f,1.f,1.f,1.f,1.f,1.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,1.f,1.f,1.f,1.f,1.f,1.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,1.f,1.f,1.f,1.f,1.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,1.f,1.f,1.f,1.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,0.f,0.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,1.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
        0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f};

    std::string config_file_ = root + "models/lenet5_bn/lenet5_bn.msnhnet";
    Msnhnet::NetBuilder  msnhNet;
    // ================================ check alexnet ================================
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/lenet5_bn/lenet5_bn.msnhbin");

    std::vector<float> result =  msnhNet.runClassify(img);

    std::cout<<msnhNet.getTimeDetail();

    int bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));
    std::cout<<"max   : pytorch[19.2447 ]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  5     ]  msnhnet: " << bestIndex<<std::endl;

}

void test_classfiy()
{
    std::vector<float> img = Msnhnet::OpencvUtil::getImgDataF32C3(root + "models/timg.jpg",cv::Size(227,227));
    std::string config_file_ = root + "models/alexnet/alexnet.msnhnet";
    Msnhnet::NetBuilder  msnhNet;
    //msnhNet.setSaveLayerOutput(true);
    //Msnhnet::NetBuilder::setOnlyGpu(true);
    //msnhNet.setUseFp16(true);

    //    std::vector<float> result; //单独测试
    //    int bestIndex; //单独测试

    // ================================ check alexnet ================================
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/alexnet/alexnet.msnhbin");
    std::vector<float> result =  msnhNet.runClassify(img);

    Msnhnet::IO::saveVector<float>(img, "cv.txt","\n");

    int bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));

    if( (std::abs(Msnhnet::ExVector::max<float>(result)-10.5764f) < 0.0001) && bestIndex==331)
    {
        std::cout<<"\n===== alexnet check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== alexnet check failed ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[10.57645]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  331   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;
    //==============================================================================

    // =============================== check darknet53 ==============================
    img = Msnhnet::OpencvUtil::getImgDataF32C3(root + "models/timg.jpg",cv::Size(224,224));
    config_file_ = root + "models/darknet53/darknet53.msnhnet";
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/darknet53/darknet53.msnhbin");
    result =  msnhNet.runClassify(img);
    bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));
    if( (std::abs(Msnhnet::ExVector::max<float>(result)-13.60f) < 0.01) && bestIndex==285)
    {
        std::cout<<"\n===== darknet53 check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== darknet53 check failed ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[13.60138]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  285   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;

    // ==============================================================================


    // =============================== check googLenet ==============================
    img = Msnhnet::OpencvUtil::getGoogLenetF32C3(root + "models/timg.jpg",cv::Size(224,224));
    config_file_ = root + "models/googLenet/googLenet.msnhnet";
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/googLenet/googLenet.msnhbin");
    result =  msnhNet.runClassify(img);
    bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));
    if( (std::abs(Msnhnet::ExVector::max<float>(result)-6.24f) < 0.05) && bestIndex==284)
    {
        std::cout<<"\n===== googLenet check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== googLenet check failed ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[6.26569 ]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  284   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;
    //==============================================================================
    //msnhNet.setForceUseCuda(false);
    // =============================== check mobilenetv2 =============================
    img = Msnhnet::OpencvUtil::getImgDataF32C3(root + "models/timg.jpg",cv::Size(224,224));
    config_file_ = root + "models/mobilenetv2/mobilenetv2.msnhnet";
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/mobilenetv2/mobilenetv2.msnhbin");
    result =  msnhNet.runClassify(img);
    bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));
    if( (std::abs(Msnhnet::ExVector::max<float>(result)-12.63f) < 1.f) && bestIndex==285) //12.63
    {
        std::cout<<"\n===== mobilenetv2 check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== mobilenetv2 check failed  ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[12.63113]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  285   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;
    // ==============================================================================


    // =============================== check resnet18 =============================
    img = Msnhnet::OpencvUtil::getImgDataF32C3(root + "models/timg.jpg",cv::Size(224,224));
    config_file_ = root + "models/resnet18/resnet18_pytorch.msnhnet";
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/resnet18/resnet18.msnhbin");
    result =  msnhNet.runClassify(img);
    bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));
    if( (std::abs(Msnhnet::ExVector::max<float>(result)-11.10f) < 0.01f) && bestIndex==285)
    {
        std::cout<<"\n===== resnet18 check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== resnet18 check failed  ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[11.10028]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  285   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;
    // ==============================================================================

    // =============================== check resnet34 =============================`
    img = Msnhnet::OpencvUtil::getImgDataF32C3(root + "models/timg.jpg",cv::Size(224,224));
    config_file_ = root + "models/resnet34/resnet34.msnhnet";
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/resnet34/resnet34.msnhbin");
    result =  msnhNet.runClassify(img);
    bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));
    if( (std::abs(Msnhnet::ExVector::max<float>(result)-12.080f) < 0.01f) && bestIndex==285)
    {
        std::cout<<"\n===== resnet34 check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== resnet34 check failed  ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[12.08040]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  285   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;
    // ==============================================================================

    // =============================== check resnet50 =============================
    img = Msnhnet::OpencvUtil::getImgDataF32C3(root + "models/timg.jpg",cv::Size(224,224));
    config_file_ = root + "models/resnet50/resnet50.msnhnet";
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/resnet50/resnet50.msnhbin");
    result =  msnhNet.runClassify(img);
    bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));
    if( (std::abs(Msnhnet::ExVector::max<float>(result)-13.036f) < 0.001f) && bestIndex==285)
    {
        std::cout<<"\n===== resnet50 check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== resnet50 check failed  ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[13.03649]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  285   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;
    // ==============================================================================

    // =============================== check resnet101 =============================
    img = Msnhnet::OpencvUtil::getImgDataF32C3(root + "models/timg.jpg",cv::Size(224,224));
    config_file_ = root + "models/resnet101/resnet101.msnhnet";
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/resnet101/resnet101.msnhbin");
    result =  msnhNet.runClassify(img);
    bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));
    if( (std::abs(Msnhnet::ExVector::max<float>(result)-9.273f) < 1.f) && bestIndex==285)
    {
        std::cout<<"\n===== resnet101 check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== resnet101 check failed  ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[9.273776]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  285   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;
    // ==============================================================================


    // =============================== check resnet152 =============================
    img = Msnhnet::OpencvUtil::getImgDataF32C3(root + "models/timg.jpg",cv::Size(224,224));
    config_file_ = root + "models/resnet152/resnet152.msnhnet";
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/resnet152/resnet152.msnhbin");
    result =  msnhNet.runClassify(img);
    bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));
    if( (std::abs(Msnhnet::ExVector::max<float>(result)-11.60f) < 1.f) && bestIndex==285)
    {
        std::cout<<"\n===== resnet152 check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== resnet152 check failed  ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[11.60315]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  285   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;
    // ==============================================================================

    // =========================== check resnet50 keras ==============================
    img = Msnhnet::OpencvUtil::getCaffeModeF32C3(root + "models/timg.jpg",cv::Size(224,224));
    config_file_ = root + "models/resnet50/resnet50_keras.msnhnet";
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/resnet50/resnet50_keras.msnhbin");
    result =  msnhNet.runClassify(img);
    bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));
    if( bestIndex==285)
    {
        std::cout<<"\n===== resnet50 keras check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== resnet50 keras check failed  ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[0.428268]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  285   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;
    // ==============================================================================


    // =============================== check vgg16 keras ===========================
    img = Msnhnet::OpencvUtil::getCaffeModeF32C3(root + "models/timg.jpg",cv::Size(224,224));
    config_file_ = root + "models/vgg16/vgg16_keras.msnhnet";
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/vgg16/vgg16_keras.msnhbin");
    result =  msnhNet.runClassify(img);
    bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));
    if( bestIndex==285)
    {
        std::cout<<"\n===== vgg16 keras check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== vgg16 keras check failed  ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[11.60315]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  285   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;
    // ==============================================================================


    // =============================== check vgg16 ==================================
    img = Msnhnet::OpencvUtil::getImgDataF32C3(root + "models/timg.jpg",cv::Size(224,224));
    config_file_ = root + "models/vgg16/vgg16.msnhnet";
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/vgg16/vgg16.msnhbin");
    result =  msnhNet.runClassify(img);
    bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));
    if( (std::abs(Msnhnet::ExVector::max<float>(result)-12.1254f) < 0.001f) && bestIndex==285)
    {
        std::cout<<"\n===== vgg16 check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== vgg16 check failed  ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[12.12543]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  285   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;
    // ==============================================================================

    // =============================== check vgg16_bn ================================
    img = Msnhnet::OpencvUtil::getImgDataF32C3(root + "models/timg.jpg",cv::Size(224,224));
    config_file_ = root + "models/vgg16_bn/vgg16_bn.msnhnet";
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/vgg16_bn/vgg16_bn.msnhbin");
    result =  msnhNet.runClassify(img);
    bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));
    if( (std::abs(Msnhnet::ExVector::max<float>(result)-9.224f) < 0.01f) && bestIndex==285)
    {
        std::cout<<"\n===== vgg16_bn check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== vgg16_bn check failed  ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[9.22434 ]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  285   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;
    // ==============================================================================

}

void test_fcns()
{
    std::string config_file_ = root + "models/fcns/fcns.msnhnet";
    Msnhnet::NetBuilder  msnhNet;
    // ================================ check alexnet ================================
    msnhNet.buildNetFromMsnhNet(config_file_);

    int netX  = msnhNet.getInputSize().x;
    int netY  = msnhNet.getInputSize().y;

    std::cout<<msnhNet.getLayerDetail();
    msnhNet.loadWeightsFromMsnhBin(root + "models/fcns/fcns.msnhbin");

    std::vector<float> img = Msnhnet::OpencvUtil::getTransformedF32C3(root + "models/fcns/fcns.jpg",{netX,netY},cv::Scalar(0.485, 0.456, 0.406),cv::Scalar(0.229, 0.224, 0.225));
    //Msnhnet::IO::readVector<float>(img,"G:/casia/pytorch/pytorch-FCN-easiest-demo/a.txt","\n");

    std::vector<float> result =  msnhNet.runClassify(img);

    cv::Mat mat = cv::imread(root + "models/fcns/fcns.jpg");

    //cv::resize(mat, mat, {netX,netY});

    cv::imshow("org",mat);

    cv::Mat mask(netX,netY,CV_8UC3,cv::Scalar(0,0,0));

    for (int i = 0; i < result.size()/2; ++i)
    {
        if(result[i] < result[i+msnhNet.getInputSize().x*msnhNet.getInputSize().y])
        {
            mask.data[i*3+2] += 120;
        }
    }

    cv::medianBlur(mask,mask,11);
    cv::resize(mask,mask,{mat.rows,mat.cols});
    std::cout<<msnhNet.getTimeDetail()<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;

    mat = mat + mask;

    cv::imshow("get",mat);
    cv::waitKey();

}

void test_unet()
{
    std::string config_file_ = root + "models/unet/unet.msnhnet";
    Msnhnet::NetBuilder  msnhNet;
    //Msnhnet::NetBuilder::setOnlyGpu(true);
    // ================================ check alexnet ================================
    //msnhNet.setUseFp16(true);
    msnhNet.buildNetFromMsnhNet(config_file_);

    int netX  = msnhNet.getInputSize().x;
    int netY  = msnhNet.getInputSize().y;

    std::cout<<msnhNet.getLayerDetail();
    msnhNet.loadWeightsFromMsnhBin(root + "models/unet/unet.msnhbin");

    std::vector<float> img = Msnhnet::OpencvUtil::getImgDataF32C3(root + "models/unet/unet.jpg",{netX,netY});
    //Msnhnet::IO::readVector<float>(img,"G:/casia/pytorch/pytorch-FCN-easiest-demo/a.txt","\n");

    std::vector<float> result =  msnhNet.runClassify(img);

    cv::Mat mat = cv::imread(root + "models/unet/unet.jpg");

    //cv::resize(mat, mat, {netX,netY});

    cv::imshow("org",mat);

    cv::Mat mask(netX,netY,CV_8UC3,cv::Scalar(0,0,0));

    for (int i = 0; i < result.size()/2; ++i)
    {
        if(result[i] < result[i+msnhNet.getInputSize().x*msnhNet.getInputSize().y])
        {
            mask.data[i*3+2] += 120;
        }
    }

    cv::medianBlur(mask,mask,11);
    cv::resize(mask,mask,{mat.rows,mat.cols});
    std::cout<<msnhNet.getTimeDetail()<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;

    mat = mat + mask;

    cv::imshow("get",mat);
    cv::waitKey();

}

#endif

void test_yolo_msnhcv()
{
    std::string config_file_ = root + "models/yolov3/yolov5s.msnhnet";
    //try
    {
        Msnhnet::NetBuilder  msnhNet;
        // msnhNet.setMemAlign(true);
        //msnhNet.setSaveLayerOutput(true);
        //msnhNet.setSaveLayerWeights(true);
        //Msnhnet::NetBuilder::setOnlyGpu(true);
        //msnhNet.setUseFp16(true);
        msnhNet.buildNetFromMsnhNet(config_file_);
        std::cout<<msnhNet.getLayerDetail();
        msnhNet.loadWeightsFromMsnhBin(root + "models/yolov3/yolov5s.msnhbin");
        std::vector<std::string> labels ;
        std::string labelPath   =   root + "models/yolov3/coco.names";
        Msnhnet::IO::readVectorStr(labels, labelPath.data(), "\n");
        Msnhnet::Point2I inSize = msnhNet.getInputSize();

        Msnhnet::Mat org(root + "models/yolov3/bus.jpg");

        std::vector<std::vector<Msnhnet::YoloBox>> result ;
        std::vector<float> img;// = Msnhnet::CVUtil::getPaddingZeroF32C3(root + "models/yolov3/bus.jpg",Msnhnet::Vec2I32(inSize.x,inSize.y));
        Msnhnet::IO::readVector<float>(img,"C:/Users/msnh/Desktop/qq/yolov5-master/img.txt","\n");
        for (int i = 0; i < 1; ++i)
        {
            //auto st = std::chrono::high_resolution_clock::now();
            result = msnhNet.runYolo(img);
            //auto so = std::chrono::high_resolution_clock::now();
            //std::cout<<std::chrono::duration <double,std::milli> (so-st).count()<< "---------------------------------------" << std::endl;

            std::cout<<msnhNet.getTimeDetail()<<" "<<inSize.x<<" "<<inSize.y<<std::endl;

        }

        Msnhnet::CVUtil::drawYoloBox(org,labels,result,inSize);

        Msnhnet::Gui::imShow("TEST",org);
        Msnhnet::Gui::wait();
        //        cv::pyrDown(org,org);
        //        cv::imshow("test",org);
        //        cv::waitKey();
    }
    //    catch (Msnhnet::Exception ex)
    //    {
    //        std::cout<<ex.what()<<" "<<ex.getErrFile() << " " <<ex.getErrLine()<< " "<<ex.getErrFun()<<std::endl;
    //    }
}

void test_yolo_msnhcv_video()
{

    try
    {
        std::string config_file_ = root + "models/yoloface500k/yoloface500k.msnhnet";

        Msnhnet::NetBuilder  msnhNet;
        msnhNet.buildNetFromMsnhNet(config_file_);
        std::cout<<msnhNet.getLayerDetail();
        msnhNet.loadWeightsFromMsnhBin(root + "models/yoloface500k/yoloface500k.msnhbin");
        std::vector<std::string> labels ;
        std::string labelPath   =   root + "models/yolov3/coco.names";
        Msnhnet::IO::readVectorStr(labels, labelPath.data(), "\n");
        Msnhnet::Point2I inSize = msnhNet.getInputSize();

        std::string config_file_1 = root + "models/landmark106/landmark106.msnhnet";

        Msnhnet::NetBuilder  msnhNet1;
        //msnhNet.setSaveLayerOutput(true);
        msnhNet1.buildNetFromMsnhNet(config_file_1);
        std::cout<<msnhNet1.getLayerDetail();
        msnhNet1.loadWeightsFromMsnhBin(root + "models/landmark106/landmark106.msnhbin");
        Msnhnet::Point2I inSize1 = msnhNet1.getInputSize();

        Msnhnet::VideoCapture cap;

        cap.openCamera(0);

        Msnhnet::Mat org;
        Msnhnet::Mat mat;

        while (1)
        {
            cap.getMat(mat);
            org = mat;

            std::vector<std::vector<Msnhnet::YoloBox>> result ;
            std::vector<float> img = Msnhnet::CVUtil::getPaddingZeroF32C3(org,Msnhnet::Vec2I32(inSize.x,inSize.y));

            auto st = std::chrono::high_resolution_clock::now();
            result = msnhNet.runYolo(img);
            auto so = std::chrono::high_resolution_clock::now();
            std::cout<<std::chrono::duration <double,std::milli> (so-st).count()<< "---------------------------------------" << std::endl;
            auto points = Msnhnet::CVUtil::drawYoloBox(mat,labels,result,inSize);


            Msnhnet::Mat face;

            Msnhnet::MatOp::getROI(mat,face, points[0],points[1]);

            std::vector<float> img1 = Msnhnet::CVUtil::getImgDataF32C3(face,Msnhnet::Vec2I32(inSize1.x,inSize1.y),true);
            std::vector<float>  result1 = msnhNet1.runClassify(img1);

            for (int i = 0; i < result1.size()/2; ++i)
            {
                Msnhnet::Vec2I32 vec;
                vec.x1 = (int)(1.0f*result1[i*2]*(abs(points[0].x1-points[1].x1)) + (points[0].x1+points[1].x1)/2.0f - 0.5f*abs(points[0].x1-points[1].x1));
                vec.x2 = (int)(1.0f*result1[i*2+1]*(abs(points[0].x2-points[1].x2))+ (points[0].x2+points[1].x2)/2.0f - 0.5f*abs(points[0].x2-points[1].x2));
                Msnhnet::Draw::fillEllipse(mat,vec,2,2,{255,0,0});
            }

            Msnhnet::Gui::imShow("TEST",mat);
            Msnhnet::Gui::waitEnterKey();
        }



        //        cv::pyrDown(org,org);
        //        cv::imshow("test",org);
        //        cv::waitKey();
    }
    catch (Msnhnet::Exception ex)
    {
        std::cout<<ex.what()<<" "<<ex.getErrFile() << " " <<ex.getErrLine()<< " "<<ex.getErrFun()<<std::endl;
    }
}

void test_classfiy_msnhcv()
{

    std::vector<float> img = Msnhnet::CVUtil::getImgDataF32C3(root + "models/timg.jpg",Msnhnet::Vec2I32(227,227));
    std::string config_file_ = root + "models/alexnet/alexnet.msnhnet";
    Msnhnet::NetBuilder  msnhNet;
    //msnhNet.setSaveLayerOutput(true);
    //Msnhnet::NetBuilder::setOnlyGpu(true);
    //msnhNet.setUseFp16(true);

    //    std::vector<float> result; //单独测试
    //    int bestIndex; //单独测试

    // ================================ check alexnet ================================
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/alexnet/alexnet.msnhbin");
    std::vector<float> result =  msnhNet.runClassify(img);

    Msnhnet::IO::saveVector<float>(img, "msnhcv.txt","\n");

    int bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));

    if( (std::abs(Msnhnet::ExVector::max<float>(result)-10.5764f) < 0.0001) && bestIndex==331)
    {
        std::cout<<"\n===== alexnet check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== alexnet check failed ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[10.57645]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  331   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;
    //==============================================================================

    // =============================== check darknet53 ==============================
    img = Msnhnet::CVUtil::getImgDataF32C3(root + "models/timg.jpg",Msnhnet::Vec2I32(224,224));
    config_file_ = root + "models/darknet53/darknet53.msnhnet";
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/darknet53/darknet53.msnhbin");
    result =  msnhNet.runClassify(img);
    bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));
    if( (std::abs(Msnhnet::ExVector::max<float>(result)-13.60f) < 0.01) && bestIndex==285)
    {
        std::cout<<"\n===== darknet53 check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== darknet53 check failed ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[13.60138]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  285   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;

    // ==============================================================================


    // =============================== check googLenet ==============================
    img = Msnhnet::CVUtil::getGoogLenetF32C3(root + "models/timg.jpg",Msnhnet::Vec2I32(224,224));
    config_file_ = root + "models/googLenet/googLenet.msnhnet";
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/googLenet/googLenet.msnhbin");
    result =  msnhNet.runClassify(img);
    bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));
    if( (std::abs(Msnhnet::ExVector::max<float>(result)-6.24f) < 0.05) && bestIndex==284)
    {
        std::cout<<"\n===== googLenet check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== googLenet check failed ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[6.26569 ]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  284   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;
    //==============================================================================
    //msnhNet.setForceUseCuda(false);
    // =============================== check mobilenetv2 =============================
    img = Msnhnet::CVUtil::getImgDataF32C3(root + "models/timg.jpg",Msnhnet::Vec2I32(224,224));
    config_file_ = root + "models/mobilenetv2/mobilenetv2.msnhnet";
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/mobilenetv2/mobilenetv2.msnhbin");
    result =  msnhNet.runClassify(img);
    bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));
    if( (std::abs(Msnhnet::ExVector::max<float>(result)-12.63f) < 1.f) && bestIndex==285) //12.63
    {
        std::cout<<"\n===== mobilenetv2 check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== mobilenetv2 check failed  ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[12.63113]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  285   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;
    // ==============================================================================


    // =============================== check resnet18 =============================
    img = Msnhnet::CVUtil::getImgDataF32C3(root + "models/timg.jpg",Msnhnet::Vec2I32(224,224));
    config_file_ = root + "models/resnet18/resnet18_pytorch.msnhnet";
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/resnet18/resnet18.msnhbin");
    result =  msnhNet.runClassify(img);
    bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));
    if( (std::abs(Msnhnet::ExVector::max<float>(result)-11.10f) < 0.01f) && bestIndex==285)
    {
        std::cout<<"\n===== resnet18 check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== resnet18 check failed  ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[11.10028]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  285   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;
    // ==============================================================================

    // =============================== check resnet34 =============================`
    img = Msnhnet::CVUtil::getImgDataF32C3(root + "models/timg.jpg",Msnhnet::Vec2I32(224,224));
    config_file_ = root + "models/resnet34/resnet34.msnhnet";
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/resnet34/resnet34.msnhbin");
    result =  msnhNet.runClassify(img);
    bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));
    if( (std::abs(Msnhnet::ExVector::max<float>(result)-12.080f) < 0.01f) && bestIndex==285)
    {
        std::cout<<"\n===== resnet34 check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== resnet34 check failed  ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[12.08040]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  285   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;
    // ==============================================================================

    // =============================== check resnet50 =============================
    img = Msnhnet::CVUtil::getImgDataF32C3(root + "models/timg.jpg",Msnhnet::Vec2I32(224,224));
    config_file_ = root + "models/resnet50/resnet50.msnhnet";
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/resnet50/resnet50.msnhbin");
    result =  msnhNet.runClassify(img);
    bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));
    if( (std::abs(Msnhnet::ExVector::max<float>(result)-13.036f) < 0.001f) && bestIndex==285)
    {
        std::cout<<"\n===== resnet50 check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== resnet50 check failed  ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[13.03649]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  285   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;
    // ==============================================================================

    // =============================== check resnet101 =============================
    img = Msnhnet::CVUtil::getImgDataF32C3(root + "models/timg.jpg",Msnhnet::Vec2I32(224,224));
    config_file_ = root + "models/resnet101/resnet101.msnhnet";
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/resnet101/resnet101.msnhbin");
    result =  msnhNet.runClassify(img);
    bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));
    if( (std::abs(Msnhnet::ExVector::max<float>(result)-9.273f) < 1.f) && bestIndex==285)
    {
        std::cout<<"\n===== resnet101 check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== resnet101 check failed  ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[9.273776]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  285   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;
    // ==============================================================================


    // =============================== check resnet152 =============================
    img = Msnhnet::CVUtil::getImgDataF32C3(root + "models/timg.jpg",Msnhnet::Vec2I32(224,224));
    config_file_ = root + "models/resnet152/resnet152.msnhnet";
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/resnet152/resnet152.msnhbin");
    result =  msnhNet.runClassify(img);
    bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));
    if( (std::abs(Msnhnet::ExVector::max<float>(result)-11.60f) < 1.f) && bestIndex==285)
    {
        std::cout<<"\n===== resnet152 check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== resnet152 check failed  ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[11.60315]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  285   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;
    // ==============================================================================

    // =========================== check resnet50 keras ==============================
    img = Msnhnet::CVUtil::getCaffeModeF32C3(root + "models/timg.jpg",Msnhnet::Vec2I32(224,224));
    config_file_ = root + "models/resnet50/resnet50_keras.msnhnet";
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/resnet50/resnet50_keras.msnhbin");
    result =  msnhNet.runClassify(img);
    bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));
    if( bestIndex==285)
    {
        std::cout<<"\n===== resnet50 keras check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== resnet50 keras check failed  ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[0.428268]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  285   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;
    // ==============================================================================


    // =============================== check vgg16 keras ===========================
    img = Msnhnet::CVUtil::getCaffeModeF32C3(root + "models/timg.jpg",Msnhnet::Vec2I32(224,224));
    config_file_ = root + "models/vgg16/vgg16_keras.msnhnet";
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/vgg16/vgg16_keras.msnhbin");
    result =  msnhNet.runClassify(img);
    bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));
    if( bestIndex==285)
    {
        std::cout<<"\n===== vgg16 keras check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== vgg16 keras check failed  ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[11.60315]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  285   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;
    // ==============================================================================


    // =============================== check vgg16 ==================================
    img = Msnhnet::CVUtil::getImgDataF32C3(root + "models/timg.jpg",Msnhnet::Vec2I32(224,224));
    config_file_ = root + "models/vgg16/vgg16.msnhnet";
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/vgg16/vgg16.msnhbin");
    result =  msnhNet.runClassify(img);
    bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));
    if( (std::abs(Msnhnet::ExVector::max<float>(result)-12.1254f) < 0.001f) && bestIndex==285)
    {
        std::cout<<"\n===== vgg16 check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== vgg16 check failed  ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[12.12543]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  285   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;
    // ==============================================================================

    // =============================== check vgg16_bn ================================
    img = Msnhnet::CVUtil::getImgDataF32C3(root + "models/timg.jpg",Msnhnet::Vec2I32(224,224));
    config_file_ = root + "models/vgg16_bn/vgg16_bn.msnhnet";
    msnhNet.buildNetFromMsnhNet(config_file_);
    msnhNet.loadWeightsFromMsnhBin(root + "models/vgg16_bn/vgg16_bn.msnhbin");
    result =  msnhNet.runClassify(img);
    bestIndex = static_cast<int>(Msnhnet::ExVector::maxIndex(result));
    if( (std::abs(Msnhnet::ExVector::max<float>(result)-9.224f) < 0.01f) && bestIndex==285)
    {
        std::cout<<"\n===== vgg16_bn check success ====="<<std::endl;
    }
    else
    {
        std::cout<<"\n===== vgg16_bn check failed  ====="<<std::endl;
    }
    std::cout<<"max   : pytorch[9.22434 ]  msnhnet: " << Msnhnet::ExVector::max<float>(result)<<std::endl;
    std::cout<<"index : pytorch[  285   ]  msnhnet: " << bestIndex<<std::endl;
    std::cout<<"time  : " << msnhNet.getInferenceTime()<<"ms"<<std::endl;
    // ==============================================================================

}

void testlandmark()
{
    std::string config_file_ = root + "models/landmark106/landmark106.msnhnet";

    Msnhnet::NetBuilder  msnhNet;
    //msnhNet.setSaveLayerOutput(true);
    msnhNet.buildNetFromMsnhNet(config_file_);
    std::cout<<msnhNet.getLayerDetail();
    msnhNet.loadWeightsFromMsnhBin(root + "models/landmark106/landmark106.msnhbin");
    Msnhnet::Point2I inSize = msnhNet.getInputSize();

    Msnhnet::Mat mat(root + "models/landmark106/landmark106.bmp");

    Msnhnet::Mat org = mat;

    //    if(mat.getChannel()==4)
    //    {
    //        Msnhnet::MatOp::cvtColor(mat,mat,Msnhnet::CVT_RGBA2RGB);
    //    }

    int w = org.getWidth();
    int h = org.getHeight();

    std::vector<float> img = Msnhnet::CVUtil::getImgDataF32C3(mat,{inSize.x,inSize.y},true,true);
    std::vector<float> result ;

    auto st = std::chrono::high_resolution_clock::now();
    result = msnhNet.runClassify(img);
    auto so = std::chrono::high_resolution_clock::now();
    std::cout<<std::chrono::duration <double,std::milli> (so-st).count()<< "---------------------------------------" << std::endl;

    for (int i = 0; i < result.size()/2; ++i)
    {
        Msnhnet::Vec2I32 vec;
        vec.x1 = result[i*2]*w;
        vec.x2 = result[i*2+1]*h;
        Msnhnet::Draw::fillEllipse(org,vec,2,2,{255,0,0});
    }

    Msnhnet::Gui::imShow("show",org);
    Msnhnet::Gui::wait();

}

void unetMsnhCV()
{

    //    std::string msnhnetPath = root + "models/unet/unet.msnhnet";
    //    std::string msnhbinPath = root + "models/unet/unet.msnhbin";
    //    std::string imgPath = root + "models/unet/unet.jpg";
    std::string msnhnetPath = "F:/models/unet/unet.msnhnet";
    std::string msnhbinPath = "F:/models/unet/unet.msnhbin";
    std::string imgPath = "F:/models/unet/unet.jpg";
    try
    {
        Msnhnet::NetBuilder  msnhNet;
        msnhNet.buildNetFromMsnhNet(msnhnetPath);
        std::cout<<msnhNet.getLayerDetail();
        msnhNet.loadWeightsFromMsnhBin(msnhbinPath);

        int netX  = msnhNet.getInputSize().x;
        int netY  = msnhNet.getInputSize().y;

        std::vector<float> img = Msnhnet::CVUtil::getImgDataF32C3(imgPath,{netX,netY},false);
        std::vector<float> result =  msnhNet.runClassify(img);
        Msnhnet::Mat mat(imgPath);

        Msnhnet::Mat mask(netX,netY,Msnhnet::MatType::MAT_RGB_U8);

        for (int i = 0; i < result.size()/2; ++i)
        {
            if(result[i] < result[i+msnhNet.getInputSize().x*msnhNet.getInputSize().y])
            {
                mask.getData().u8[i*3+0] += 120;
            }
        }

        Msnhnet::MatOp::resize(mask,mask,{mat.getWidth(),mat.getHeight()});
        std::cout<<msnhNet.getTimeDetail()<<std::endl;
        //Msnhnet::MatOp::cvtColor(mat, mat, Msnhnet::CVT_GRAY2RGB);
        mat = mat+mask;

#ifdef USE_MSNHCV_GUI1
        Msnhnet::Gui::imShow("mask",mask);
        Msnhnet::Gui::imShow("unet",mat);
        Msnhnet::Gui::wait();
#else
        mat.saveImage("unet.jpg");
#endif
    }
    catch (Msnhnet::Exception ex)
    {
        std::cout<<ex.what()<<" "<<ex.getErrFile() << " " <<ex.getErrLine()<< " "<<ex.getErrFun()<<std::endl;
    }
}

using namespace Msnhnet;

int main3(){

    Chain singleLink;
    // a alpah d theta
    singleLink.addSegments(Segment("link1",Joint(JOINT_ROT_Z),Frame::SDH(0     ,MSNH_PI_2 ,0     ,0)));
    singleLink.addSegments(Segment("link2",Joint(JOINT_ROT_Z),Frame::SDH(0.4318,0         ,0     ,0)));
    singleLink.addSegments(Segment("link3",Joint(JOINT_ROT_Z),Frame::SDH(0.0203,-MSNH_PI_2,0.1500,0)));
    singleLink.addSegments(Segment("link4",Joint(JOINT_ROT_Z),Frame::SDH(0     ,MSNH_PI_2 ,0.4318,0)));
    singleLink.addSegments(Segment("link5",Joint(JOINT_ROT_Z),Frame::SDH(0     ,-MSNH_PI_2,0     ,0)));
    singleLink.addSegments(Segment("link6",Joint(JOINT_ROT_Z),Frame::SDH(0     ,0         ,0     ,0)));
    Frame f = singleLink.fk(VectorXSDS({MSNH_PI_2,0,-MSNH_PI_2,0,0,0}));
    f.print();
    GeometryS::rotMat2Euler(f.rotMat,ROT_ZYX).print();
    f.trans.print();

    return 1;
    //    const int inw = 5;
    //    const int inh = 5;
    //    const int inch = 3;
    //    const int kw = 3;
    //    const int kh = 3;
    //    const int outw = inw - kw + 1;
    //    const int outh = inh - kh + 1;
    //    const int outch = 4;

    //    //5x5x3
    //    float *src = new float[inw * inh * inch];
    //    //3x3x4
    //    float *kernel = new float[kw * kh * inch * outch];
    //    //3x3x4
    //    float *dest = new float[outw * outh * outch]();

    //    //赋值
    //    for(int i = 0; i < inw * inh * inch; i++){
    //        src[i] = a[i];
    //    }

    //    for(int i = 0; i < kw * kh * inch * outch; i++){
    //        kernel[i] = b[i];
    //    }

    //    for(int i = 0; i < outh * outw * outch; i++){
    //        dest[i] = 0.f;
    //    }

    //    Msnhnet::Convolution3x3LayerX86::convolution3x3S1(src, inh, inw,inch, dest, outh,outw,outch, kernel, true);

    //    for(int i = 0; i < outw * outh * outch ; i++){
    //        bool flag = cmp(dest[i], c[i]);
    //        if(flag == false){
    //            printf("WA: %d\n", i);
    //            printf("Expected: %.4f, ConvOutput: %.4f\n", c[i], dest[i]);
    //        }
    //    }

    //    for(int i = 0; i < outw * outh * outch; i++){
    //        std::cout<<dest[i]<<std::endl;
    //    }


    //    float a[] = {1,2,3,
    //                 4,5,6,
    //                 7,8,9,

    //                 1,2,3,
    //                 4,5,6,
    //                 7,8,9,

    //                 1,2,3,
    //                 4,5,6,
    //                 7,8,9,

    //                 1,2,3,
    //                 4,5,6,
    //                 7,8,9,

    //                 1,2,3,
    //                 4,5,6,
    //                 7,8,9,

    //                 1,2,3,
    //                 4,5,6,
    //                 7,8,9,

    //                 1,2,3,
    //                 4,5,6,
    //                 7,8,9,

    //                 1,2,3,
    //                 4,5,6,
    //                 7,8,9,

    //                 1,2,3,
    //                 4,5,6,
    //                 7,8,9,
    //                };

    //    float b[300] = {99};
    //    float c[300] = {0};

    //    Msnhnet::BlasNCHW4::cpuNCHWToNCHW4(a, 3,3,5,1,b);
    //    Msnhnet::BlasNCHW4::cpuNCHW4ToNCHW(b,12,3,1,3,1,c);


    Msnhnet::RotationMatD rotMat;

    rotMat.setVal({
        1,0,0,
        0,1,0,
        0,0,1
    });

    rotMat.setValAtRowCol(0,0,2);

    Msnhnet::TranslationD trans;
    trans.setVal({
        2,
        3,
        5
    });

    Msnhnet::Matrix4x4D mat;
    mat.setRotationMat(rotMat);
    mat.setTranslation(trans);
    mat.print();

    mat.getRotationMat().print();
    mat.getTranslation().print();

    Msnhnet::Matrix4x4D mat1 = Msnhnet::Mat::eye(4, Msnhnet::MatType::MAT_GRAY_F64);

    glm::mat4 transform = glm::mat4(1.0f); // make sure to initialize matrix to identity matrix first
    transform = glm::translate(transform, glm::vec3(0.5f, -0.5f, 0.0f));
    std::cout<<glm::to_string(transform);

    //std::cout<<ddd.distanceToPoint(ccc)<<std::end;


    //  std::cout<<ddd.length();


    //m.getRow(1).print();
    return 0;
}

int ma11in(int argc, char** argv)
{


    //    Msnhnet::Mat ey = Msnhnet::Mat::random(2,3,Msnhnet::MAT_GRAY_F32);
    //    ey.printMat();
    //    Msnhnet::Mat ma = ey.transpose();
    //    ma.printMat();
    //    ey = Msnhnet::Mat::mul(ey,ma);
    //    ey.printMat();

    //try
    {

        //unetMsnhCV();
        //        Msnhnet::Mat mat("F:/timg.jpg");
        //        std::vector<Msnhnet::Mat> splits;

        //        Msnhnet::MatOp::split(mat,splits);

        //        Msnhnet::Mat merge1;

        //        Msnhnet::MatOp::merge(splits,merge1);

        //        Msnhnet::Mat gray;

        //        Msnhnet::MatOp::cvtColor(mat,gray,Msnhnet::CVT_RGB2GRAY);

        //        Msnhnet::Gui::imShow("A",gray);

        //        Msnhnet::MatOp::threshold(gray,gray,100,255,Msnhnet::ThresholdType::THRESH_BINARY|Msnhnet::ThresholdType::THRESH_OTSU);

        //        Msnhnet::Gui::imShow("B",gray);

        //        Msnhnet::Gui::imShow("RGBA",merge1);
        //        Msnhnet::Gui::wait();

        //        Msnhnet::RotationVec rotV;
        //        rotV.setVal({1.209199,-1.209199,1.209199});
        //        Msnhnet::Geometry::rotVec2Euler(rotV,Msnhnet::ROT_ZYX).print();

        return 0;

        //        Msnhnet::VideoCapture cap;
        //        cap.openCamera(0,1280,720);

        //        Msnhnet::Mat mat;
        //        while (1)
        //        {
        //            cap.getMat(mat);
        //            Msnhnet::Gui::imShow("DSS",mat);
        //        }


        //        Msnhnet::Mat matc;

        //        Msnhnet::Mat3x3F f32;

        //        f32.setVal({2,2,3,4,5,6,5,8,9});

        //        Msnhnet::Mat3x3F f31;

        //        f31.setVal({3,3,5,5,3,3,1,1,7});

        //        Msnhnet::RotationMat rot;

        //        rot.setVal({0.707106,-0.707106,0,
        //                    0.707106,0.707106,0,
        //                    0,0,1
        //                   });

        //        rot.getVal(0);
        //        std::cout<< Msnhnet::Geometry::isRealRotMat(rot);

        //std::cout<<f32.getVal()[0];

        //        cam.openCamera(0,1280,720,false);
        //        Msnhnet::Mat mat;
        //        while (1)
        //        {
        //            //Msnhnet::MatOp::cvtColor(mat,mat,Msnhnet::CVT_RGB2RGBA);
        //            //auto st = Msnhnet::TimeUtil::startRecord();
        //            //cam.getMat(mat);
        //            //Msnhnet::Gui::imShow("test",mat);
        //            //std::cout<<Msnhnet::TimeUtil::getElapsedTime(st)<<std::endl;
        //            //Msnhnet::Gui::wait();
        //        }

        //Kalman filter
        //        {
        //            std::vector<float> val;
        //            std::vector<float> after;

        //            for (int i = 0; i < 1000; ++i)
        //            {
        //                val.push_back(sinf(i/50)+Msnhnet::Mat::randUniform<float>(-1,1)/2);
        //            }

        //            for (int i = 0; i < 100; ++i)
        //            {
        //                val.push_back(0);
        //            }

        //            Msnhnet::SimpleKF1D simpKF1D;

        //            //                float x1[] = {val[0],0};

        //            //                Msnhnet::Mat x(1,2,Msnhnet::MAT_GRAY_F32,x1);

        //            //                float P1[] = {1,0,
        //            //                              0,100};
        //            //                Msnhnet::Mat P(2,2,Msnhnet::MAT_GRAY_F32,P1);

        //            //                Msnhnet::Mat Q = Msnhnet::Mat::eye(2,Msnhnet::MAT_GRAY_F32);

        //            //                float H1[] = {1,0};
        //            //                Msnhnet::Mat H(2,1,Msnhnet::MAT_GRAY_F32,H1);

        //            //                Msnhnet::Mat R = Msnhnet::Mat::diag(1,Msnhnet::MAT_GRAY_F32,1000);

        //            //                Msnhnet::KalmanFilter kf(x,P,Q,H,R);

        //            //                float F1[] = {1,0.01,0,1};
        //            //                Msnhnet::Mat F(2,2,Msnhnet::MAT_GRAY_F32,F1);
        //            //                kf.setF(F);

        //            for (int i = 0; i < 1100; ++i)
        //            {
        //                after.push_back(simpKF1D.update(val[i]));
        //                //                    kf.predict();
        //                //                    Msnhnet::Mat z = Msnhnet::Mat::diag(1,Msnhnet::MAT_GRAY_F32, val[i]);
        //                //                    kf.update(z);
        //                //                    after.push_back(kf.getX().getData().f32[0]);
        //            }
        //            std::vector<std::vector<float>> val1 = {val,after};
        //            Msnhnet::IO::saveVector<float>(val1,"a.csv","\n");
        //        }

        //                float aaa[] = {
        //                    4,12,-16,
        //                    12,37,-43,
        //                    -16,-43,98
        //                };

        //                Msnhnet::Mat x(3,3,Msnhnet::MAT_GRAY_F32,aaa);
        //                std::vector<Msnhnet::Mat> mat = x.CholeskyDeComp(true);
        //                mat[0].printMat();
        //                mat[1].printMat();

        //                x.invert().printMat();



        //                Msnhnet::Mat mat("C:/Users/msnh/Desktop/timg.jpg");
        //                Msnhnet::Gui::imShow("test1",mat);
        //                Msnhnet::Gui::wait();
        //        Msnhnet::MatOp::cvtColor(mat,mat,Msnhnet::CVT_RGB2BGR);
        //        Msnhnet::Gui::imShow("test2",mat);
        //        Msnhnet::MatOp::cvtColor(mat,mat,Msnhnet::CVT_RGB2GRAY);
        //        Msnhnet::Gui::imShow("test3",mat);
        //        Msnhnet::Gui::wait();
        //        Msnhnet::VideoEncoder encode("a.mpeg",960,544,Msnhnet::VideoEncoder::FPS_60);
        //        std::vector<std::string> imgs;
        //        Msnhnet::IO::readVectorStr(imgs,"C:/Users/msnh-pc/Desktop/test/a.txt","\n");
        //        for (int i = 0; i < imgs.size(); ++i)
        //        {
        //                Msnhnet::Mat mat(imgs[i]);
        //                encode.writeMat(mat);
        //        }
        //        encode.close();
        //        std::vector<std::string> imgs;
        //        Msnhnet::IO::readVectorStr(imgs,"C:/Users/msnh/Desktop/avi/1.txt","\n");
        //        Msnhnet::VideoEncoder encoder;
        //        encoder.open("a.avi",1280,720,Msnhnet::VIDEO_MJPG,100,Msnhnet::VIDEO_FPS_30);
        //        for (int i = 0; i < imgs.size(); ++i)
        //        {
        //            Msnhnet::Mat mat(imgs[i]);
        //            encoder.writeMat(mat);
        //            //                Msnhnet::Gui::imShow("test",mat);
        //            //                Msnhnet::Gui::wait(20);
        //        }
        //        encoder.close();

        //        Msnhnet::VideoDecoder decoder;
        //        decoder.open("C:/Users/msnh/Desktop/output.mpg");
        //        Msnhnet::Mat mat;
        //        Msnhnet::GifEncoder encoder;
        //        encoder.open("a.gif",640,480);

        //        while (decoder.getMat(mat))
        //        {
        //            //Msnhnet::MatOp::cvtColor(mat,mat,Msnhnet::CVT_RGB2RGBA);
        //            encoder.writeMat(mat);
        //            Msnhnet::Gui::imShow("test",mat);
        //            //Msnhnet::Gui::wait(20);
        //        }
        //        encoder.close();
        //        Msnhnet::Gui::stopIt();

        //        Msnhnet::GifDecoder gifDecode;
        //        gifDecode.open("a.gif");

        //        Msnhnet::Mat mat;

        //        while (gifDecode.getNextFrame(mat))
        //        {
        //            Msnhnet::Gui::imShow("test",mat);
        //            Msnhnet::Gui::wait(20);
        //        }
        //        gifDecode.close();
        //        Msnhnet::Gui::stopIt();
    }
    //    catch (Msnhnet::Exception ex)
    //    {
    //        std::cout<<ex.what();
    //    }


        //    int x =0;
        //    int y =0;
        //    int z = 0;

        //    uint8_t* data1 = img.data;

        //    uint8_t* data = stbi_load("C:/Users/msnh/Desktop/a.jpg",&x,&y,&z,0);
        //    std::cout<<cv::DataType<cv::Vec2b>::channels;

        //    if(data == nullptr)
        //    {
        //        std::cout<<"fuck";
        //    }
        //    stbi_image_free(data);

        //    std::vector<float> da = {1.2f,3.3f,4.4f,5.5f,6.6f,1.2f,3.4f,3.2f,3.5f};

        //    Msnhnet::Mat mat(3,3,Msnhnet::MatType::MAT_RGB_F32,da.data());

        //    mat.setPixel<Msnhnet::Vec3F32>(2,0,Msnhnet::Vec3F32(9.9f,7.7f,6.4f));

        //    Msnhnet::Vec4F32 val = mat.getPixel<Msnhnet::Vec4F32>(2,0);

        //    uint8_t * vala = mat.getData().u8;

        //    std::cout<<std::endl;
        //    std::cout<< val.x1 <<std::endl;
        //    std::cout<< val.x2 <<std::endl;
        //    std::cout<< val.x3 <<std::endl;
        //    //std::cout<< val.x4 <<std::endl;

        //    //try
        //    {
        //        Msnhnet::Mat mat2;

        //        mat2.readImage("C:/Users/msnh/Desktop/timg.jpg");
        //        //Msnhnet::MatOp::fillPixel<Msnhnet::Vec3U8>(mat2,Msnhnet::Vec3U8(100,0,0));
        //        mat2.saveImage("C:/Users/msnh/Desktop/dd.jpg", Msnhnet::SaveImageType::MAT_SAVE_JPG,100);

        //        Msnhnet::Mat mat3;

        //        mat2.copyTo(mat3);

        //        // mat2.clearMat();

        //        mat2.saveImage("C:/Users/msnh/Desktop/ddc.jpg", Msnhnet::SaveImageType::MAT_SAVE_JPG,100);

        //        Msnhnet::Mat R(600,600,Msnhnet::MatType::MAT_RGB_U8);
        //        std::cout<<R.getWidth();
        //        //        Msnhnet::MatOp::fillPixel<Vec3U8>(R,);
        //        R.fillPixel<Vec3U8>({200,200,0});
        //        R.getPixel<Vec3U8>(Msnhnet::Vec2I32(1,1));

        //        //        for (int i = 0; i < 10; ++i)
        //        //        {
        //        //            for (int j = 0; j < 10; ++j)
        //        //            {
        //        //                R.setPixel(Msnhnet::Vec2U32(i,j),Vec3U8(0,0,0));
        //        //            }
        //        //        }

        //        Msnhnet::Draw::fillRect(R,Msnhnet::Vec2I32(0,0),Msnhnet::Vec2I32(16,32),Msnhnet::Vec3U8(100,0,255));

        //        std::vector<Msnhnet::Vec2I32> points {Msnhnet::Vec2I32(0,10),Msnhnet::Vec2I32(5,20),Msnhnet::Vec2I32(10,40),Msnhnet::Vec2I32(50,2),
        //                    Msnhnet::Vec2I32(50,10),Msnhnet::Vec2I32(50,20),Msnhnet::Vec2I32(100,40),Msnhnet::Vec2I32(10,20)
        //                                             };

        //        Msnhnet::Draw::drawPoly(R,points, Msnhnet::Vec3U8(255,0,0),4);

        //        Msnhnet::Draw::drawLine(R,{0,0},{500,10},{0,0,0},20);

        //        // Msnhnet::Draw::drawLine(R,{300,300},{300,100},{0,0,0},20);

        //        Msnhnet::Draw::drawEllipse(R,Msnhnet::Vec2I32(150,150), 100 ,100,Msnhnet::Vec3U8(0,0,255),40);

        //        Msnhnet::Draw::drawFont(R,"msnhnet TEST 99.9%",Msnhnet::Vec2I32(100,180),Msnhnet::Vec3U8(255,255,255));

        //        Msnhnet::Mat E;

        //        //        Msnhnet::MatOp::getROI(R,E,Msnhnet::Vec2I32(100,0),Msnhnet::Vec2I32(200,200));

        //        //        Msnhnet::MatOp::setROI(R,E,Msnhnet::Vec2I32(10,0));

        //        R.saveImage("C:/Users/msnh/Desktop/R.png", Msnhnet::SaveImageType::MAT_SAVE_PNG,100);

        //        Msnhnet::Mat Q = R;

        //        Msnhnet::Mat P;

        //        Msnhnet::MatOp::cvtColor(Q, P, Msnhnet::CVT_RGB2GRAY);
        //        Msnhnet::MatOp::resize(P, P, Msnhnet::Vec2I32(300,300), Msnhnet::RESIZE_BILINEAR);

        //        Msnhnet::MatOp::copyMakeBorder<Msnhnet::Vec3U8>(mat2,P,10,10,10,10,Msnhnet::Vec3U8(0,255,0));

        //        Q.saveImage("C:/Users/msnh/Desktop/aa.png", Msnhnet::SaveImageType::MAT_SAVE_PNG,100);
        //        P.saveImage("C:/Users/msnh/Desktop/qq.png", Msnhnet::SaveImageType::MAT_SAVE_PNG,100);

        //    }
        //    //    catch (Msnhnet::Exception ex)
        //    //    {
        //    //        std::cout<<ex.what()<<ex.getErrFile()<<ex.getErrLine()<<ex.getErrFun()<<std::endl;
        //    //    }


        //    return 0;

        ////    float *x = new float[8]{-7.f,-.06f,-2.f,-1.f,0.f,1.f,2.f,3.f};
        ////    float *a = Msnhnet::Cuda::makeCudaArray(x,8);

        ////    Msnhnet::ActivationsGPU::gpuActivateArray(a,8,HARD_SWISH);
        ////    cudaMemcpy(x,a,8*4,cudaMemcpyDeviceToHost);

        ////    for(int i=0;i<8;i++)
        ////    {
        ////        std::cout<<x[i]<<std::endl;
        ////    }
        ////    return 0;

        std::string config_file_ = root + "models/varop/varop.msnhnet";
    Msnhnet::NetBuilder  msnhNet;
    msnhNet.buildNetFromMsnhNet(config_file_);
    //msnhNet.setForceUseCuda(true);

        std::cout<<msnhNet.getLayerDetail()<<std::endl;

        //    std::vector<float> val;
        //    for (int i = 0; i < 200*200*3; ++i) {
        //        val.push_back(i*1.0f);
        //    }


        std::vector<float> val{  0,1,2,
            3,4,5,
            6,7,8,

            0,1,2,
            3,4,5,
            6,7,8,

            0,1,2,
            3,4,5,
            6,7,8,

            0,1,2,
            3,4,5,
            6,7,8,
            };


    //        std::vector<float> val{
    //            1.4487,  0.7608,  0.3425,
    //            -0.1336,  0.6779,  0.1364,
    //            0.1736, -0.7252,  0.1173,

    //            1.2905, -1.4977, -0.0539,
    //            1.0562,  1.6240, -0.9961,
    //            1.0537, -1.1074, -2.6881,

    //            -0.3599, -1.8681,  1.2596,
    //            0.9898, -1.3962, -0.8761,
    //            2.1064,  0.5664,  0.1657,

    //            -0.0410, -1.5518,  0.1818,
    //            -0.5000,  1.0547,  0.4335,
    //            -2.2038,  0.6440, -0.4977
    //                               };

    ////    std::vector<float> val
    ////    {
    ////        1.0f,2.0f,
    ////        3.0f,4.0f,
    ////        5.0f,6.0f,

    ////        1.0f,2.0f,
    ////        3.0f,4.0f,
    ////        5.0f,6.0f,

    ////        1.0f,2.0f,
    ////        3.0f,4.0f,
    ////        5.0f,6.0f,

    ////        1.0f,2.0f,
    ////        3.0f,4.0f,
    ////        5.0f,6.0f,
    ////    };
    //    for (int i = 0; i < 10; ++i)
    //    {
    //    }


    return 1;


    //    std::thread t1(test_yolo);
    //    t1.join();
    //    try
    //    {
    //test_classfiy();
    //test_classfiy();


    //    Msnhnet::BaseLayer::initSimd();

    //    Msnhnet::DeConvolutionalLayer decon(1,2,2,1,1,3,3,1,1,0,0,1,ActivationType::NONE,{},1);

    //        std::vector<float> weights = {0.3035, -0.3141,  0.0802, -0.0463,  0.0624, -0.2996, -0.2612, -0.1909, -0.3000, -0.2322};

    //        Msnhnet::NetworkState state;

    //        float in[] = {2.2914, -0.1486, 0.3715,  1.5140};


    //    tensor([[[[ 0.4632, -0.9971, -0.0018, -0.2441],
    //              [-0.2255,  0.2604, -1.3738, -0.0663],
    //              [-0.8478, -0.6777, -0.9080, -0.6413],
    //              [-0.3292, -0.6985, -0.6327, -0.6864]]]],

    //        state.input = in;

    //        decon.loadAllWeigths(weights);

    //        decon.forward(state);

    //        std::vector<float> out{decon.getOutput(), decon.getOutput()+decon.getOutputNum()};

    //        Msnhnet::IO::printVector<float>(out);

    //        std::cout<<"end"<<std::endl;

    //decon.loadAllWeigths();

    //        test_yolo();
    //try
    {

        //test_classfiy();
        //test_fcns();
        // test_yolo();
        //        float a[16] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
        //        float a1[16] = {0};
        //        float *b;
        //        float *c;
        //        float d[36] = {0};
        //        cudaMalloc((void **)&b, 16*sizeof(float));
        //        cudaMalloc((void **)&c, 36*sizeof(float));
        //        Msnhnet::Cuda::pushCudaArray(b,a,16);
        //        cudaMemcpy(b,a,16*sizeof(float),cudaMemcpyDefault);
        //        cudaMemcpy(a1,b,16*sizeof(float),cudaMemcpyDefault);

        //        Msnhnet::Gemm::gpuIm2ColEx(b,1,4,4,3,3,0,0,1,1,1,1,c);
        //        Msnhnet::Cuda::pullCudaArray(c,d,36);
        //        //cudaMemcpy(d,c,36*sizeof(float),cudaMemcpyDefault);
        //        cudaFree(b);
        //        cudaFree(c);

        //test_lenet5();
        //cudaSetDevice(0);

        //Msnhnet::Cuda::getBlasHandle();
        //        try
        //        {
        //test_yolo();
        //        }
        //        catch (Msnhnet::Exception ex)
        //        {
        //            std::cout<<ex.what()<<std::endl;
        //        }
        //需要保存参数，设置全局变量即可,直接copy一份给output
        //Msnhnet::Cuda::deleteBlasHandle();

        //std::cout<<Msnhnet::Cuda::getDeviceInfo();
    }
    //    catch (Msnhnet::Exception ex)
    //    {
    //        std::cout<<ex.what()<<ex.getErrFile()<<ex.getErrLine();
    //    }


    //    std::vector<float> a = {-4,-3,-2, -0.1, 0 ,0.1 ,2,3,4};
    //    std::string config_file_ = "C:/Users/msnh/Desktop/a.msnhnet";
    //    Msnhnet::NetBuilder  msnhNet;
    //    // ================================ check alexnet ================================
    //    msnhNet.buildNetFromMsnhNet(config_file_);
    //    std::vector<float> fff = msnhNet.runClassify(a);
    //    Msnhnet::IO::printVector<float>(fff);



    //    Msnhnet::SoftMaxLayer soft(1,9,1);
    //    Msnhnet::BaseLayer::initSimd();
    //    Msnhnet::NetworkState net;
    //    net.input = a;

    //    soft.forward(net);

    //    std::vector<float> f(soft.output,soft.output+9);

    //    Msnhnet::IO::printVector<float>(f);


    //    auto st = std::chrono::high_resolution_clock::now();
    //    int z = 0;
    //    for (int i = 0; i < 1000000; ++i)
    //    {
    //        z+=i;
    //    }
    //    auto so = std::chrono::high_resolution_clock::now();
    //    float forwardTime =   1.f * (std::chrono::duration_cast<std::chrono::microseconds>(so - st)).count()* std::chrono::microseconds::period::num / std::chrono::microseconds::period::den;
    //    std::cout<<z<<" "<<forwardTime<<std::endl;

    //    int c = 0;
    //    st = std::chrono::high_resolution_clock::now();
    //#pragma omp parallel for reduction(+:c)
    //    for (int i = 0; i < 1000000; ++i)
    //    {
    //        c = c +i;
    //    }
    //    so = std::chrono::high_resolution_clock::now();
    //    forwardTime =   1.f * (std::chrono::duration_cast<std::chrono::microseconds>(so - st)).count()* std::chrono::microseconds::period::num / std::chrono::microseconds::period::den;
    //    std::cout<<c<<" "<<forwardTime<<std::endl;

    //return Catch::Session().run( argc, argv );
    //    }
    //    catch(Msnhnet::Exception ex)
    //    {
    //        std::cout<<"[Error]: "<< ex.what() <<" "<<ex.getErrFile()<<" line: "<<ex.getErrLine()<<std::endl;
    //    }


    //    float aa[] = {1,2,3,
    //                  4,5,6,
    //                  11,22,33,
    //                  44,55,66,
    //                  11,12,13,
    //                  14,15,16,
    //                 };

    //    Msnhnet::NetworkState net;
    //    net.input = aa;

    //    Msnhnet::PaddingLayer pad(1,2,3,3,1,1,1,1,0);
    //    pad.forward(net);

    //    net.inputNum    =   pad.outputNum;
    //    net.input       =   pad.output;

    //    std::cout<<pad.outHeight<<" "<<pad.outWidth<<" "<<std::endl;
    //    std::vector<float> val1{pad.output, pad.output + pad.outputNum};

    //    for (int c = 0; c < pad.outChannel; ++c)
    //    {
    //        for (int i = 0; i < pad.outHeight; ++i)
    //        {
    //            for (int j = 0; j < pad.outWidth; ++j)
    //            {
    //                std::cout<<val1[ c*pad.outHeight*pad.outWidth + i*pad.outWidth+j]<<" ";
    //            }
    //            std::cout<<std::endl;
    //        }
    //    }


    //    Msnhnet::MaxPoolLayer padd(1,2,3,3,2,2,1,1,1,1,0,0,1,0);
    //    padd.forward(net);

    //    std::vector<float> val{padd.output, padd.output + padd.outputNum};

    //    for (int c = 0; c < padd.outChannel; ++c)
    //    {
    //        for (int i = 0; i < padd.outHeight; ++i)
    //        {
    //            for (int j = 0; j < padd.outWidth; ++j)
    //            {
    //                std::cout<<val[ c*padd.outHeight*padd.outWidth + i*padd.outWidth+j]<<" ";
    //            }
    //            std::cout<<std::endl;
    //        }
    //    }

    // Msnhnet::IO::printVector(val,"\n");

}
#include <test/test3PointToCircle.h>
int main(int argc,char* argv[])
{
    test_yolo_video();
    return 1;
    //    Frame frame1;
    //    Frame frame2;
    //    Frame frame3;

    //    frame1.trans = Msnhnet::TranslationDS({0,-1,0});
    //    frame2.trans = Msnhnet::TranslationDS({-0.86602540,0.5,0});
    //    frame3.trans = Msnhnet::TranslationDS({1,0,0});

    //    getCenter(frame1, frame2, frame3).print();

    //    return 1;

    //    std::vector<float> a;
    //    std::vector<float> b;
    //    Msnhnet::IO::readVector<float>(a,"C:/Users/msnh/Desktop/qq/yolov5-master/max1.txt","\n");
    //    Msnhnet::IO::readVector<float>(b,"E:/works/qt/casia/projects/Msnhnet/build-Msnhnet2108030-Desktop_Qt_5_15_2_MSVC2019_64bit-Debug/83cpu.txt","\n");

    //    for(size_t i =0; i<a.size();i++)
    //    {
    //        if(abs(a[i] - b[i])>0.1)
    //        {
    //            std::cout<<i<<" "<<a[i]<<" "<<b[i]<<std::endl;
    //        }
    //    }
    //    std::cout<<"done"<<std::endl;
    //test_yolo_msnhcv();

    //Msnhnet::NetBuilder  msnhNet;
    //// msnhNet.setMemAlign(true);
    ////msnhNet.setSaveLayerOutput(true);
    ////msnhNet.setSaveLayerWeights(true);
    ////Msnhnet::NetBuilder::setOnlyGpu(true);
    ////msnhNet.setUseFp16(true);
    //msnhNet.buildNetFromMsnhNet("E:/works/qt/casia/projects/Msnhnet/Msnhnet210830/test/maxpool.msnhnet");
    //std::vector<float> a;
    //Msnhnet::IO::readVector<float>(a,"E:/works/qt/casia/projects/Msnhnet/Msnhnet210830/test/maxpool_k13x13_p6x6_s1x1_data.txt","\n");
    //std::vector<float>ress = msnhNet.runClassify(a);

    //    return 1;
    //{
    //        //    int   bar_data[11] = {1,2,3,4,5,6,7,8,9,10,11};
    //        //    ImGui::Begin("My Window");
    //        ////    if (ImPlot::BeginPlot("My Plot")) {
    //        ////        ImPlot::PlotBars("My Bar Plot", bar_data, 11);
    //        //////        ImPlot::PlotLine("My Line Plot", x_data, y_data, 1000);
    //        ////        ImPlot::EndPlot();
    //        ////    }
    //        //    ImGui::End();



    //            PathLine* path = new PathLine(Frame(GeometryS::euler2RotMat(Vector3DS(MSNH_PI,0,0),ROT_ZYX),Vector3DS(-1,0,0)),
    //                                          Frame(GeometryS::euler2RotMat(Vector3DS(MSNH_PI_2,0,0),ROT_ZYX),Vector3DS(-0.5,0,0.5)),
    //                                          new RotationInterpSingleAxis(),0.01
    //                                          );

    CartesianPathCircle* circle = new CartesianPathCircle(Frame(GeometryS::euler2RotMat(Vector3DS(MSNH_PI,0,0),ROT_ZYX),Vector3DS(0,1,0)),
        Frame(GeometryS::euler2RotMat(Vector3DS(MSNH_PI,0,0),ROT_ZYX),Vector3DS(1,0,0)),
        Frame(GeometryS::euler2RotMat(Vector3DS(MSNH_PI,0,0),ROT_ZYX),Vector3DS(0.7071067,0.7071067,0)),
        new RotationInterpSingleAxis(),0.01,false);

    CartesianPathCircle* circle1 = new CartesianPathCircle(Frame(GeometryS::euler2RotMat(Vector3DS(0,MSNH_PI,0),ROT_ZYX),Vector3DS(0,1,0)),
        Vector3DS(0,0,0),
        Frame(GeometryS::euler2RotMat(Vector3DS(0,0,MSNH_PI),ROT_ZYX),Vector3DS(1,0,0)),
        MSNH_PI_2,new RotationInterpSingleAxis(),0.01
        );

//    CartesianPathCircle* circle2 = new CartesianPathCircle(Frame(GeometryS::euler2RotMat(Vector3DS(0,MSNH_PI,0),ROT_ZYX),Vector3DS(0,0,1)),
//        Vector3DS(0,0,0),
//        Frame(GeometryS::euler2RotMat(Vector3DS(0,0,MSNH_PI),ROT_ZYX),Vector3DS(1,0,0)),
//        MSNH_PI_2,new RotationInterpSingleAxis(),0.01
//        );

//    CartesianPathCircle* circle3 = new CartesianPathCircle(Frame(GeometryS::euler2RotMat(Vector3DS(0,MSNH_PI,0),ROT_ZYX),Vector3DS(1,0,0)),
//        Vector3DS(0,0,0),
//        Frame(GeometryS::euler2RotMat(Vector3DS(0,0,MSNH_PI),ROT_ZYX),Vector3DS(0,-1,-1)),
//        MSNH_PI_2,new RotationInterpSingleAxis(),0.01
//        );

//    CartesianPathCircle* circle4 = new CartesianPathCircle(Frame(GeometryS::euler2RotMat(Vector3DS(0,MSNH_PI,0),ROT_ZYX),Vector3DS(0,-1,-1)),
//        Vector3DS(0,0,0),
//        Frame(GeometryS::euler2RotMat(Vector3DS(0,0,MSNH_PI),ROT_ZYX),Vector3DS(-1,0,0)),
//        MSNH_PI_2,new RotationInterpSingleAxis(),0.01
//        );

    CartesianPathComposite *path = new CartesianPathComposite();

    path->add(circle);
    //   path->add(circle2);
    //    path->add(circle3);
    //    path->add(circle4);


    CartesianPathRoundComposite* path1 = new CartesianPathRoundComposite(0.2,0.01,new RotationInterpQuat(),true);

    //    path->add(Frame(GeometryS::euler2RotMat(Vector3DS(MSNH_PI,0,0),ROT_ZYX),Vector3DS(-1,0,0)));
    //    path->add(Frame(GeometryS::euler2RotMat(Vector3DS(MSNH_PI_2,0,0),ROT_ZYX),Vector3DS(-0.5,0,0)));
    //    path->add(Frame(GeometryS::euler2RotMat(Vector3DS(0,0,0),ROT_ZYX),Vector3DS(0,0,0)));
    //    path->add(Frame(GeometryS::euler2RotMat(Vector3DS(0.7,0.7,0.7),ROT_ZYX),Vector3DS(1,1,1)));
    //    path->add(Frame(GeometryS::euler2RotMat(Vector3DS(0,0.7,0),ROT_ZYX),Vector3DS(1.5,0.3,0)));
    //    path->add(Frame(GeometryS::euler2RotMat(Vector3DS(0.7,0.7,0),ROT_ZYX),Vector3DS(1,1,0)));
    //    path->finish();

    VelocityProfile* velpref = new VelocityProfileTrap(0.6,0.1);

    velpref->setProfile(0,path->getPathLength());
    CartesianTrajectory* traject = new CartesianTrajectorySegment(path, velpref);

    std::cout<<traject->getDuration();
    double dt=0.01;

    std::ofstream of("a.csv");

    for (double t=0.0; t <= traject->getDuration(); t+= dt)
    {
        Frame pos = traject->getPos(t);
        //pos.trans.print();

        EulerDS euler = GeometryS::rotMat2Euler(pos.rotMat, ROT_ZYX);
        of << pos.trans[0]<<","<< pos.trans[1]<<","<< pos.trans[2] << "\n";
    }

    of.flush();
    of.close();
    //}


    //    {

    //        VectorXSDS start({0,0,0,0,0,0});
    //        VectorXSDS end({MSNH_PI_2,MSNH_PI_3,MSNH_PI_4,MSNH_PI_6,MSNH_PI,MSNH_PI});
    //        VectorXSDS start1({0,0,0,0,0,0});

    //        SingleJointPath* line = new SingleJointPath(start,end);
    //        SingleJointPath* line1 = new SingleJointPath(end,start1);

    //        JointPathComposite *path = new JointPathComposite();

    //        path->add(line);
    //        path->add(line1);

    //        VelocityProfile* velpref = new VelocityProfileTrap(0.6,0.1);

    //        velpref->setProfile(0,path->getPathLength());
    //        JointTrajectory* traject = new JointTrajectorySegment(path, velpref);

    //        std::cout<<path->getPathLength();

    //        double dt=0.1;

    //        std::ofstream of("c.csv");

    //        for (double t=0.0; t <= traject->getDuration(); t+= dt)
    //        {
    //            VectorXSDS pos = traject->getPos(t);

    //            of << pos[0]<<","<< pos[1]<<","<< pos[2]<<","<< pos[3]<<","<< pos[4]<<","<< pos[5]<<"\n";
    //        }

    //        of.flush();
    //        of.close();
    //    }
    /*
    return 1;

    std::cout<<"Msnhnet IK Test: "<<std::endl;

    NRThread th;

    th.start();

    th.join();

    uint64_t suc = 0;
    uint64_t fal = 0;

    int q = OMP_THREAD;

    for (int i = 0; i < q; ++i)
    {
        suc = suc + th.success[i];
        fal = fal + th.failed[i];
    }

    std::cout<<th.dt<<std::endl;

    std::cout<<"succeed: "<<suc<<" \n"<<"failed: "<<fal<<"  \npercent: "<<(suc)*100.0/(suc+fal)<<"%  \niters: "<<th.cnt<<" \nmax iters/step: "<<th.max<<std::endl<<std::flush;

    return 1;
    */
    //IO::saveVector<double>(th.f,"D:/a.csv","\n");

    //    try
    //    {
    //        RotationVecDS rotV(1,0.5,0);
    //        rotV.normalized().print();
    //        GeometryS::rotVec2RotMat(rotV.normalized()*1.0471975333333334).print();

    //        return 1;
    //std::shared_ptr<URDFModel> model = URDF::parseFromUrdfFile("G:/Msnh/MsnhProject/MsnhRobotStudio/MsnhRobotStudioDummy/MsnhRobotStudio/resources/shapes/Dummy-URDF.urdf");
    ////std::shared_ptr<URDFModel> model = URDF::parseFromUrdfFile("G:/casia/Robot/petercorke_robot_data/rtb-data/rtbdata/xacro/yumi_description/urdf/yumi.urdf");

    //std::shared_ptr<URDFLink> link = model->getRoot();

    //std::cout<<model->rootPath<<std::endl;

    //std::shared_ptr<URDFLinkTree> clink = URDF::getLinkTree(link);

    //std::shared_ptr<StringTree> tree = URDF::getStringTree(clink);

    //tree->printT(tree);

    //std::cout<<link->visuals.size()<<std::endl;

    //Chain chain = URDF::getChain(model, tree, "Joint1","JointEffector");

    //auto seg = chain.segments;

    ////        for(int i=0; i<seg.size(); i++)
    ////        {
    ////            seg[i].getEndToTip().print();
    ////        }

    //Frame ffk = chain.fk(VectorXSDS({15/180.0*MSNH_PI,
    //                                 15/180.0*MSNH_PI,
    //                                 15/180.0*MSNH_PI,
    //                                 15/180.0*MSNH_PI,
    //                                 15/180.0*MSNH_PI,
    //                                 15/180.0*MSNH_PI,
    //                                }));//15/180.0*MSNH_PI}));

    //ffk.print();
    //auto childLink = link->childLinks;
    //auto childJoint = link->childJoints;

    //        while(childLink.size())
    //        {
    //            //            for (size_t i = 0; i < childLink.size(); ++i)
    //            //            {
    //            //                std::cout<<childLink[i]->name<<std::endl;
    //            //            }
    //            std::cout<<childLink.size()<<std::endl;
    //            std::cout<<childLink[0]->name<<std::endl;
    //            //std::cout<<childLink[0]->childJoints[0]->name<<std::endl;

    //            childLink = childLink[0]->childLinks;
    //        }

    //    }
    //    catch(Exception ex)
    //    {
    //        std::cout<<ex.what()<<std::endl;
    //    }

    //return 1;


    // IO::saveVector<uint64_t>(th.all,"D:/nr.csv","\n");

    //    MatSDS aa(3,3);
    //    aa.setVal({1,2,3,4,5,6,7,8,9});

    //    MatSDS cc(1,3);
    //    cc.setVal({2,4,6});

    //    (aa*cc).print();

    //    return 1;



    //Chain puma560;
    //// a alpah d theta
    //puma560.addSegments(Segment("link1",Joint(JOINT_ROT_Z),Frame::SDH(0     ,MSNH_PI_2 ,0     ,0) , -MSNH_PI, MSNH_PI));
    //puma560.addSegments(Segment("link2",Joint(JOINT_ROT_Z),Frame::SDH(0.4318,0         ,0     ,0) , -MSNH_PI, MSNH_PI));
    //puma560.addSegments(Segment("link3",Joint(JOINT_ROT_Z),Frame::SDH(0.0203,-MSNH_PI_2,0.1500,0) , -MSNH_PI, MSNH_PI));
    //puma560.addSegments(Segment("link4",Joint(JOINT_ROT_Z),Frame::SDH(0     ,MSNH_PI_2 ,0.4318,0) , -MSNH_PI, MSNH_PI));
    //puma560.addSegments(Segment("link5",Joint(JOINT_ROT_Z),Frame::SDH(0     ,-MSNH_PI_2,0     ,0) , -MSNH_PI, MSNH_PI));
    //puma560.addSegments(Segment("link6",Joint(JOINT_ROT_Z),Frame::SDH(0     ,0         ,0     ,0) , -MSNH_PI, MSNH_PI));

    //puma560.initIKFast();

    //Frame fk = puma560.fk(VectorXSDS({
    //                                     -3.14159265453,
    //                                     -3.14159265453,
    //                                     -1.04719755151,
    //                                     -2.09439510302,
    //                                     -1.04719755151,
    //                                     -3.14159265453
    //                                 }));

    //puma560.initOpt();

    //fk.print();

    //VectorXSDS qin = VectorXSDS({0,0,0,0,0,0});

    //double x[16] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
    //double y[16] = {0};

    ////        double*  x = new double[16]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};


    //int res = 0;

    //auto st = std::chrono::high_resolution_clock::now();

    //for (int i = 0; i < 10000; ++i)
    //{
    //    qin = VectorXSDS({0,
    //                      0,
    //                      0,
    //                      0,
    //                      0,
    //                      0});
    //    res=puma560.ikSQPSumSqr(fk, qin);
    //}

    //auto so = std::chrono::high_resolution_clock::now();
    //std::cout<<std::chrono::duration <double,std::milli> ((so-st)/10000).count()<< "------QQQ" << std::endl;

    //std::cout<<"res :"<<res<<std::endl;
    //qin.print();

    //fk = puma560.fk(qin);
    //fk.print();

    //return 1;

    //st = std::chrono::high_resolution_clock::now();

    //for (int i = 0; i < 10000; ++i)
    //{
    //    qin = VectorXSDS({0,
    //                      0,
    //                      0,
    //                      0,
    //                      0,
    //                      0});
    //    res=puma560.ikNewtonJL(fk, qin);
    //}
    //so = std::chrono::high_resolution_clock::now();
    //std::cout<<std::chrono::duration <double,std::milli> ((so-st)/10000).count()<< "------NR_JL" << std::endl;

    //std::cout<<"res :"<<res<<std::endl;


    //qin.print();

    //st = std::chrono::high_resolution_clock::now();

    //for (int i = 0; i < 10000; ++i)
    //{
    //    qin = VectorXSDS({0,
    //                      0,
    //                      0,
    //                      0,
    //                      0,
    //                      0});
    //    res= puma560.ikNewtonRR(fk, qin);
    //}

    //so = std::chrono::high_resolution_clock::now();
    //std::cout<<std::chrono::duration <double,std::milli> ((so-st)/10000).count()<< "------NR_RR" << std::endl;

    //std::cout<<"res :"<<res<<std::endl;

    //st = std::chrono::high_resolution_clock::now();

    //for (int i = 0; i < 10000; ++i)
    //{
    //    qin = VectorXSDS({0,
    //                      0,
    //                      0,
    //                      0,
    //                      0,
    //                      0});
    //    res= puma560.ikSQPSumSqr(fk, qin);
    //}
    //so = std::chrono::high_resolution_clock::now();
    //std::cout<<std::chrono::duration <double,std::milli> ((so-st)/10000).count()<< "------SQP" << std::endl;

    //std::cout<<"res :"<<res<<std::endl;


    //TODO: Mat_及其衍生拷贝构造和赋值构造release优化。

    //    //    try
    //    //    {

    //    std::vector<double> vec =  { 2,2,1,1};
    //    //     std::vector<double> vec =  {
    //    //    5.000000000000, 0.000000000000, 0.000000000000 ,
    //    //    3.000000000000, 3.000000000000, 0.000000000000 ,
    //    //    -1.000000000000, 1.000000000000, 3.000000000000 };

    //    cv::Mat mat(2,2,CV_64FC1);
    //    for (int i = 0; i < 4; ++i)
    //    {
    //        mat.at<double>(i) = vec[i];
    //    }


    //    Msnhnet::Mat_<2,2,double> b;
    //    b.setVal(vec);
    //    auto c = b.pseudoInvert();
    //    c.print();
    //    std::cout<<"--------------------------------"<<std::endl;

    //        Msnhnet::ScrewD screw;
    //        screw.v = Msnhnet::Vector3D({1,2,3});
    //        screw.w = Msnhnet::Vector3D({3,4,5});

    //        Msnhnet::SE3D::exp(screw).log().print();

    //        Msnhnet::Mat&& m = Msnhnet::Mat::random(128,128,Msnhnet::MAT_GRAY_F32);
    //        auto st = std::chrono::high_resolution_clock::now();
    //        for (int i = 0; i < 100000; ++i)
    //        {
    //            m = m*m;           //
    //        }
    //        auto so = std::chrono::high_resolution_clock::now();
    //        std::cout<<std::chrono::duration <double,std::milli> ((so-st)/100).count()<< "------Msnhnet" << std::endl;

    //        Eigen::MatrixXf A = Eigen::MatrixXf::Random(128, 128);
    //        st = std::chrono::high_resolution_clock::now();
    //        for (int i = 0; i < 100000; ++i)
    //        {
    //            A = A*A;           //
    //        }
    //        so = std::chrono::high_resolution_clock::now();
    //        std::cout<<std::chrono::duration <double,std::milli> ((so-st)/100).count()<< "------Eigen" << std::endl;

    //m.print();

    //    }
    //    catch(Msnhnet::Exception ex)
    //    {
    //        std::cout<<ex.what();
    //    }

    //return 0;
}

QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        CameraDS.cpp \
        VideoCapture.cpp \
        main.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

INCLUDEPATH+=$$quote(D:/Program Files (x86)/Visual Leak Detector/include)

LIBS += $$quote(D:/Program Files (x86)/Visual Leak Detector/lib/Win64/*)

HEADERS += \
    CameraDS.h \
    VideoCapture.h \
    qedit.h

LIBS += -lgdi32 -lole32 -lstrmiids -loleaut32 -luser32 -lshlwapi

﻿#include <iostream>
#include <vector>

class Tree
{
public:
    Tree(const std::string& name):mName(name){}
    std::string mName;
    std::vector<std::shared_ptr<Tree>> childs;
    void addChild(const std::shared_ptr<Tree>& tree)
    {
        childs.push_back(tree);
    }

    static bool deepFirstSearch(const std::shared_ptr<Tree>& root, const std::string& dest, std::vector<std::string> &result)
    {
        result.push_back(root->mName);

        if(root->mName == dest)
        {
            return true;
        }

        if(root->childs.empty())
        {
            return false;
        }

        for(size_t i = 0; i < root->childs.size(); i++)
        {
            std::vector<std::string> pt = result;
            bool res = deepFirstSearch(root->childs[i], dest, pt);

            if(!res)
            {
                continue;
            }
            else
            {
                result = pt;
                return res;
            }
        }

        return false;
    }

    static bool getShortestPath(const std::shared_ptr<Tree>& root,const std::string& start, const std::string& end, std::vector<std::string>& result)
    {
        std::vector<std::string> way1;
        std::vector<std::string> way2;
        bool search1 = deepFirstSearch(root,start,way1);
        bool search2 = deepFirstSearch(root,end,way2);

        if(!search1||!search2)
        {
            return false;
        }

        for (int i = 0; i < way1.size(); ++i)
        {
            for (int j = 0; j < way2.size(); ++j)
            {
                size_t ii = way1.size() - 1 - i;
                size_t jj = way2.size() - 1 - j;
                if(way1[ii]==way2[jj])
                {
                    std::vector<std::string> res;
                    for (size_t m = ii; m < way1.size(); ++m)
                    {
                        res.push_back(way1[m]);
                    }

                    std::reverse(res.begin(), res.end());

                    for (size_t n = jj+1; n < way2.size(); ++n)
                    {
                        res.push_back(way2[n]);
                    }

                    result = res;
                    return true;
                }
            }
        }
        return false;
    }

    void printT(std::shared_ptr<Tree> tree)
    {
        std::cout<<tree->mName.data()<<" ";

        for(size_t i=0; i<tree->childs.size(); i++)
        {
            printT(tree->childs[i]);
        }
    }
};

///
///           A
///      B    C    D
///    E F G     H I J
///
///
int main()
{
    std::shared_ptr<Tree> root = std::make_shared<Tree>("A");
    std::shared_ptr<Tree> B    = std::make_shared<Tree>("B");
    root->addChild(B);
    root->addChild(std::make_shared<Tree>("C"));
    std::shared_ptr<Tree> D    = std::make_shared<Tree>("D");
    root->addChild(D);
    B->addChild(std::make_shared<Tree>("E"));
    B->addChild(std::make_shared<Tree>("F"));
    B->addChild(std::make_shared<Tree>("G"));
    D->addChild(std::make_shared<Tree>("H"));
    D->addChild(std::make_shared<Tree>("I"));
    D->addChild(std::make_shared<Tree>("J"));

    root->printT(root);

    std::vector<std::string> ways;

    //Tree::deepFirstSearch(root,"H",ways);
    Tree::getShortestPath(root,"D","H",ways);

    for(size_t i=0; i<ways.size(); i++)
    {
        std::cout<<ways[i].data()<<"->";
    }

}

﻿//src conv kernel
#include <vector>
#include <iostream>
#define USE_NEON 0
#define USE_OMP 0
using namespace std;

void conv3x3s1_neon(float *const &src, const int &inw, const int &inh,  const int &inch, float *const &kernel, const int &kw, 
                        const int &kh, float* &dest, const int &outw, const int &outh, const int &outch){
    int cc_outch = outch >> 1;
    int cc_remain_outch = cc_outch << 1;
    printf("%d %d\n", cc_outch, cc_remain_outch);
    const int in_size = inw * inh;
    const int out_size = outw * outh;

    ///                                         ------
    ///                                         1 2 3 | (w3)
    ///                                      ------ 6 |                         1 2  (o3)
    ///                                      1 2 3 |9 |                         3 4
    ///                                      4 5 6 |
    ///                                      7 8 9 |
    ///
    ///                                ------
    ///       -------------            1 2 3 | (w2)
    ///       1  2  3   4  |        ------ 6 |                            1 2  (o2)
    ///    -----------  8  |        1 2 3 |9 |                            3 4
    ///    1  2  3  4 | 12 |        4 5 6 |
    ///    5  6  7  8 | 16 |        7 8 9 |
    ///    9 10 11 12 |
    ///   13 14 15 16 |        ------
    ///                        1 2 3 | (w1)
    ///                     ------ 6 |                                1 2  (o1)
    ///                     1 2 3 |9 |                                3 4
    ///                     4 5 6 |
    ///                     7 8 9 |



    //                      3
    for(int cc = 0; cc < cc_outch; cc++)
    {
        int c = cc << 1;
        //get two conv output in same time
        float *dest0 = dest + c * out_size;         //(o1)
        float *dest1 =  dest + (c + 1) * out_size;  //(o2)

        for(int j = 0; j < out_size; j++) dest0[j] = 0.f;
        for(int j = 0; j < out_size; j++) dest1[j] = 0.f;

        //two output rely on two kernel
        //                        2
        float *k0 = kernel + c * inch * 3 * 3;  //w1
        //                              2
        float *k1 = kernel + (c + 1) * inch * 3 * 3; //w2

        //                  2
        for(int q = 0; q < inch; q++){
            //一次处理两个-----------------------------------
            float* destptr0 = dest0;                     //|(O0)
            float* destptr1 = dest1;                     //|(O1)
            //一次处理两行-------------------------------    |
            float* destptr0_next = destptr0 + outw;  //| //|(O0+下一行)
            float* destptr1_next = destptr1 + outw;  //| //|(O1+下一行)

            //                      inch  inW*inH
            const float* src0 = src + q * in_size;
            //deal four lines and get two outputs in a feature map
            //处理src的四行
            const float* r0 = src0;
            const float* r1 = src0 + inw;
            const float* r2 = src0 + inw * 2;
            const float* r3 = src0 + inw * 3;

            int i = 0;
            for(; i + 1 < outh; i += 2) //out 一次两行
            {

                int remain = outw;

                for(; remain > 0; remain--){ //每列


                    float sum0 = 0.f;       //(O0)
                    float sum1 = 0.f;       //(O1)
                    float sum0next = 0.f;   //(O0+下一行)
                    float sum1next = 0.f;   //(O1+下一行)

                    //conv output1->chanel q output1
                                            //  src      kernel1
                                            // 第一行     kernel
                    sum0 += r0[0] * k0[0];  //   0         0     -|      ---
                    sum0 += r0[1] * k0[1];  //   1         1      |     |x x|
                    sum0 += r0[2] * k0[2];  //   2         2      |     |x x|
                                            // 第二行              |      ---
                    sum0 += r1[0] * k0[3];  //   0         3      |    ---
                    sum0 += r1[1] * k0[4];  //   1         4      |-- |0 x|
                    sum0 += r1[2] * k0[5];  //   2         5      |   |x x|
                                            // 第三行              |    ---
                    sum0 += r2[0] * k0[6];  //   0         6      |
                    sum0 += r2[1] * k0[7];  //   1         7      |
                    sum0 += r2[2] * k0[8];  //   2         8     -|

                    //conv output1->channel q output2
                                            //  src      kernel2
                                            // 第一行     kernel
                    sum1 += r0[0] * k1[0];  //   0         0      -|      ---
                    sum1 += r0[1] * k1[1];  //   1         1       |     |0 x|
                    sum1 += r0[2] * k1[2];  //   2         2       |     |x x|
                                            // 第二行               |      ---
                    sum1 += r1[0] * k1[3];  //   0         3       |    ---
                    sum1 += r1[1] * k1[4];  //   1         4       |-- |x x|
                    sum1 += r1[2] * k1[5];  //   2         5       |   |x x|
                                            // 第三行               |    ---
                    sum1 += r2[0] * k1[6];  //   0         6       |
                    sum1 += r2[1] * k1[7];  //   1         7       |
                    sum1 += r2[2] * k1[8];  //   2         8      -|

                    //conv output2->channel q output1
                                                 //  src      kernel1
                                                 // 第二行     kernel
                    sum0next += r1[0] * k0[0];   //   0         0     -|      ---
                    sum0next += r1[1] * k0[1];   //   1         1      |     |x x|
                    sum0next += r1[2] * k0[2];   //   2         2      |     |x x|
                                                 // 第三行              |      ---
                    sum0next += r2[0] * k0[3];   //   0         3      |    ---
                    sum0next += r2[1] * k0[4];   //   1         4      |-- |x x|
                    sum0next += r2[2] * k0[5];   //   2         5      |   |0 x|
                                                 // 第四行              |    ---
                    sum0next += r3[0] * k0[6];   //   0         6      |
                    sum0next += r3[1] * k0[7];   //   1         7      |
                    sum0next += r3[2] * k0[8];   //   2         8     -|

                    //conv output2->channel q output2
                                                 //  src      kernel1
                                                 // 第二行     kernel
                    sum1next += r1[0] * k1[0];   //   0         0     -|      ---
                    sum1next += r1[1] * k1[1];   //   1         1      |     |x x|
                    sum1next += r1[2] * k1[2];   //   2         2      |     |0 x|
                                                 // 第三行              |      ---
                    sum1next += r2[0] * k1[3];   //   0         3      |    ---
                    sum1next += r2[1] * k1[4];   //   1         4      |-- |x x|
                    sum1next += r2[2] * k1[5];   //   2         5      |   |x x|
                                                 // 第四行              |    ---
                    sum1next += r3[0] * k1[6];   //   0         6      |
                    sum1next += r3[1] * k1[7];   //   1         7      |
                    sum1next += r3[2] * k1[8];   //   2         8     -|

                    //sum to dest
                    *destptr0 += sum0;
                    *destptr1 += sum1;
                    *destptr0_next += sum0next;
                    *destptr1_next += sum1next;
                    //update point address
                    r0++;              //src下一列
                    r1++;              //src下一列
                    r2++;              //src下一列
                    r3++;              //src下一列
                    destptr0++;        //ptr下一列
                    destptr1++;        //ptr下一列
                    destptr0_next++;   //ptr下一列
                    destptr1_next++;   //ptr下一列
                }

                r0 += 2 + inw;
                r1 += 2 + inw;
                r2 += 2 + inw;
                r3 += 2 + inw;
                
                destptr0 += outw;
                destptr1 += outw;
                destptr0_next += outw;
                destptr1_next += outw;
            }
            
            //deal three lines and get one output in a feature map
            for(; i < outh; i++){  //out最后一行
                int remain = outw;

                for(; remain > 0; remain--){

                    float sum0 = 0.f;
                    float sum1 = 0.f;

                    //conv output1->chanel q output1
                    sum0 += r0[0] * k0[0];
                    sum0 += r0[1] * k0[1];
                    sum0 += r0[2] * k0[2];
                    sum0 += r1[0] * k0[3];
                    sum0 += r1[1] * k0[4];
                    sum0 += r1[2] * k0[5];
                    sum0 += r2[0] * k0[6];
                    sum0 += r2[1] * k0[7];
                    sum0 += r2[2] * k0[8];

                    //conv output2->channel q output1
                    sum1 += r0[0] * k1[0];
                    sum1 += r0[1] * k1[1];
                    sum1 += r0[2] * k1[2];
                    sum1 += r1[0] * k1[3];
                    sum1 += r1[1] * k1[4];
                    sum1 += r1[2] * k1[5];
                    sum1 += r2[0] * k1[6];
                    sum1 += r2[1] * k1[7];
                    sum1 += r2[2] * k1[8];

                    //sum to dest
                    *destptr0 += sum0;
                    *destptr1 += sum1;
                    //update point address
                    r0++;
                    r1++;
                    r2++;
                    destptr0++;
                    destptr1++;
                }

                r0 += 2;
                r1 += 2;
                r2 += 2;
            }
            
            //mov conv kernel
            k0 += 9; //下一通道的核
            k1 += 9; //下一通道的核
        }
    }
}

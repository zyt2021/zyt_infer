QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        camera.cpp \
        main.cpp \
        point_cloud.cpp \
        program.cpp \
        renderer.cpp \
        shader.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


INCLUDEPATH += D:/libs/GLFW/include\
D:/libs/glew-2.1.0/include\
D:/libs/glm

LIBS+= D:/libs/GLFW/lib/glfw3dll.lib\
D:/libs/glew-2.1.0/lib/Release/x64/glew32.lib\
$$quote(D:/Windows Kits/10/Lib/10.0.18362.0/um/x64/OpenGL32.Lib)

DISTFILES += \
    untitled.pro.user

HEADERS += \
    camera.hpp \
    main.hpp \
    point_cloud.hpp \
    program.hpp \
    renderer.hpp \
    shader.hpp

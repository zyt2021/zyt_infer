QT += gui

CONFIG += c++11 console
CONFIG -= app_bundle

HEADERS += \
    src/lodepng.h

SOURCES += \
    src/lodepng.cpp \
    src/main.cpp

INCLUDEPATH += D:/VulkanSDK/1.2.176.1/Include

LIBS += D:/VulkanSDK/1.2.176.1/Lib/*.lib

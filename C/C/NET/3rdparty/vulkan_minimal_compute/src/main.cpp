﻿
#include <vulkan/vulkan.h>

#include <vector>
#include <string.h>
#include <assert.h>
#include <stdexcept>
#include <cmath>
#include <chrono>
#include <iostream>

#include "lodepng.h" //Used for png encoding.

//分形图案尺寸
const int WIDTH = 3200; // Size of rendered mandelbrot set.
const int HEIGHT = 2400; // Size of renderered mandelbrot set.
const int WORKGROUP_SIZE = 32; // Workgroup size in compute shader.

#ifdef NDEBUG
const bool enableValidationLayers = false;
#else
const bool enableValidationLayers = true;
#endif

// 用于验证Vulkan API调用的返回值。
#define VK_CHECK_RESULT(f) 																				\
{																										\
    VkResult res = (f);																					\
    if (res != VK_SUCCESS)																				\
    {																									\
        printf("Fatal : VkResult is %d in %s at line %d\n", res,  __FILE__, __LINE__); \
        assert(res == VK_SUCCESS);																		\
    }																									\
}

/*
应用程序启动一个计算着色器来渲染mandelbrot集，
通过将其呈现到存储缓冲区中。
然后从GPU读取存储缓冲区，并保存为.png。
*/
class ComputeApplication {
private:
    //  渲染的mandelbrot集的像素采用以下格式：
    struct Pixel {
        float r, g, b, a;
    };
    
    /*
    为了使用Vulkan，必须创建一个实例。
    */
    VkInstance instance;

    VkDebugReportCallbackEXT debugReportCallback;
    /*
    物理设备是系统上支持使用Vulkan的某些设备。通常，它只是一个支持Vulkan的图形卡。
    */
    VkPhysicalDevice physicalDevice;
    /*
    然后是逻辑设备VkDevice，它基本上允许我们与物理设备交互。
    */
    VkDevice device;

    /*
    管道指定了所有图形和计算命令在Vulkan中传递的管道。我们将在这个应用程序中创建一个简单的计算管道。
    */
    VkPipeline pipeline;
    VkPipelineLayout pipelineLayout;
    VkShaderModule computeShaderModule;

    /*
    命令缓冲区用于记录将提交到队列的命令。要分配这样的命令缓冲区，我们使用命令池。
    */
    VkCommandPool commandPool;
    VkCommandBuffer commandBuffer;

    /*

    描述符表示着色器中的资源。它们允许我们在GLSL中使用统一缓冲区、存储缓冲区和图像。
    单个描述符表示单个资源，多个描述符被组织成描述符集，这些描述符集基本上只是描述符的集合。
    */
    VkDescriptorPool descriptorPool;
    VkDescriptorSet descriptorSet;
    VkDescriptorSetLayout descriptorSetLayout;

    /*
    mandelbrot集将被渲染到此缓冲区。
    支持缓冲区的内存是bufferMemory。
    */
    VkBuffer buffer;
    VkDeviceMemory bufferMemory;
        
    uint32_t bufferSize; // size of `buffer` in bytes.

    std::vector<const char *> enabledLayers;

    /*
    为了在设备（GPU）上执行命令，命令必须提交到队列。命令存储在一个命令缓冲区中，这个命令缓冲区被提供给队列。
    设备上将有不同类型的队列。例如，并非所有队列都支持图形操作。对于这个应用程序，我们至少需要一个支持计算操作的队列。
    */
    VkQueue queue; // 支持计算操作的队列。

    /*
    具有相同功能的队列组（例如，它们都支持图形和计算机操作）被分组到队列族中。
    提交命令缓冲区时，必须指定要提交到的族中的哪个队列。此变量跟踪队列族中该队列的索引。
    */
    uint32_t queueFamilyIndex;

public:
    void run() {
        // 缓冲区将包含渲染mandelbrot集的存储缓冲区的大小。
        bufferSize = sizeof(Pixel) * WIDTH * HEIGHT;

        // Initialize vulkan:
        createInstance();
        findPhysicalDevice();
        createDevice();
        createBuffer();
        createDescriptorSetLayout();
        createDescriptorSet();
        createComputePipeline();
        createCommandBuffer();

        // 最后，运行记录的命令缓冲区。
        runCommandBuffer();

        // 前一个命令将mandelbrot设置为缓冲区。
        // 将该缓冲区另存为磁盘上的png。
        saveRenderedImage();

        // 清理所有Vulkan的资源。
        cleanup();
    }

    void saveRenderedImage() {
        void* mappedMemory = NULL;
        // 映射缓冲内存，这样我们就可以在CPU上读取它。
        vkMapMemory(device, bufferMemory, 0, bufferSize, 0, &mappedMemory);
        Pixel* pmappedMemory = (Pixel *)mappedMemory;

        // 从缓冲区获取颜色数据，并将其转换为字节。
        // 我们把数据保存到向量中。
        std::vector<unsigned char> image;
        image.reserve(WIDTH * HEIGHT * 4);
        for (int i = 0; i < WIDTH*HEIGHT; i += 1) {
            image.push_back((unsigned char)(255.0f * (pmappedMemory[i].r)));
            image.push_back((unsigned char)(255.0f * (pmappedMemory[i].g)));
            image.push_back((unsigned char)(255.0f * (pmappedMemory[i].b)));
            image.push_back((unsigned char)(255.0f * (pmappedMemory[i].a)));
        }
        // 读完了，所以取消映射。
        vkUnmapMemory(device, bufferMemory);

        // 现在我们将获取的颜色数据保存到.png中。
        unsigned error = lodepng::encode("mandelbrot.png", image, WIDTH, HEIGHT);
        if (error) printf("encoder error %d: %s", error, lodepng_error_text(error));
    }

    static VKAPI_ATTR VkBool32 VKAPI_CALL debugReportCallbackFn(
        VkDebugReportFlagsEXT                       flags,
        VkDebugReportObjectTypeEXT                  objectType,
        uint64_t                                    object,
        size_t                                      location,
        int32_t                                     messageCode,
        const char*                                 pLayerPrefix,
        const char*                                 pMessage,
        void*                                       pUserData) {

        printf("Debug Report: %s: %s\n", pLayerPrefix, pMessage);

        return VK_FALSE;
     }

    void createInstance() {
        std::vector<const char *> enabledExtensions;

        /*
        通过启用验证层，如果API使用不当，Vulkan将发出警告。
        我们将启用层VK_LAYER_LUNARG_standard_validation,验证，它基本上是几个有用的验证层的集合。
        */
        if (enableValidationLayers) {
            /*
            我们使用vkEnumerateInstanceLayerProperties获得所有受支持的层。
            */
            uint32_t layerCount;
            vkEnumerateInstanceLayerProperties(&layerCount, NULL);

            std::vector<VkLayerProperties> layerProperties(layerCount);
            vkEnumerateInstanceLayerProperties(&layerCount, layerProperties.data());

            /*
             * 然后我们只需检查VK_LAYER_LUNARG_standard_validation是否在支持的层中。
            */
            bool foundLayer = false;
            for (VkLayerProperties prop : layerProperties) {
                
                if (strcmp("VK_LAYER_LUNARG_standard_validation", prop.layerName) == 0) {
                    foundLayer = true;
                    break;
                }

            }
            
            if (!foundLayer) {
                throw std::runtime_error("Layer VK_LAYER_LUNARG_standard_validation not supported\n");
            }
            enabledLayers.push_back("VK_LAYER_LUNARG_standard_validation"); // Alright, we can use this layer.

            /*
            我们需要启用一个名为VK_EXT_DEBUG_REPORT_EXTENSION_NAME的扩展，以便能够打印验证层发出的警告。
            同样，我们只需检查扩展是否在支持的扩展中。
            */
            
            uint32_t extensionCount;
            
            vkEnumerateInstanceExtensionProperties(NULL, &extensionCount, NULL);
            std::vector<VkExtensionProperties> extensionProperties(extensionCount);
            vkEnumerateInstanceExtensionProperties(NULL, &extensionCount, extensionProperties.data());

            bool foundExtension = false;
            for (VkExtensionProperties prop : extensionProperties) {
                if (strcmp(VK_EXT_DEBUG_REPORT_EXTENSION_NAME, prop.extensionName) == 0) {
                    foundExtension = true;
                    break;
                }

            }

            if (!foundExtension) {
                throw std::runtime_error("Extension VK_EXT_DEBUG_REPORT_EXTENSION_NAME not supported\n");
            }
            enabledExtensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
        }		

        /*
        接下来，我们实际创建实例。
        
        */
        
        /*
        包含应用程序信息。这其实没那么重要。唯一真正重要的字段是apiVersion。
        */
        VkApplicationInfo applicationInfo = {};
        applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        applicationInfo.pApplicationName = "Hello world app";
        applicationInfo.applicationVersion = 0;
        applicationInfo.pEngineName = "awesomeengine";
        applicationInfo.engineVersion = 0;
        applicationInfo.apiVersion = VK_API_VERSION_1_0;;
        
        VkInstanceCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        createInfo.flags = 0;
        createInfo.pApplicationInfo = &applicationInfo;
        
        // 给我们想要的层和扩展到vulkan。
        createInfo.enabledLayerCount = enabledLayers.size();
        createInfo.ppEnabledLayerNames = enabledLayers.data();
        createInfo.enabledExtensionCount = enabledExtensions.size();
        createInfo.ppEnabledExtensionNames = enabledExtensions.data();
    
        /*
        实际创建实例。
        创建了实例之后，我们就可以开始使用vulkan了。
        */
        VK_CHECK_RESULT(vkCreateInstance(
            &createInfo,
            NULL,
            &instance));

        /*
        为扩展名VK_EXT_DEBUG_REPORT_EXTENSION_NAME注册回调函数，以便从验证中发出警告
        图层实际上是打印出来的。
        */
        if (enableValidationLayers) {
            VkDebugReportCallbackCreateInfoEXT createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
            createInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT | VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT;
            createInfo.pfnCallback = &debugReportCallbackFn;

            // 我们必须显式地加载这个函数。
            auto vkCreateDebugReportCallbackEXT = (PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT");
            if (vkCreateDebugReportCallbackEXT == nullptr) {
                throw std::runtime_error("Could not load vkCreateDebugReportCallbackEXT");
            }

            //创建并注册回调。
            VK_CHECK_RESULT(vkCreateDebugReportCallbackEXT(instance, &createInfo, NULL, &debugReportCallback));
        }
    
    }

    void findPhysicalDevice() {
        /*
        在这个函数中，我们找到了一个可以与Vulkan一起使用的物理设备。
        */

        /*
        因此，首先我们将使用vkEnumeratePhysicalDevices列出系统上的所有物理设备。
        */
        uint32_t deviceCount;
        vkEnumeratePhysicalDevices(instance, &deviceCount, NULL);
        if (deviceCount == 0) {
            throw std::runtime_error("could not find a device with vulkan support");
        }

        std::vector<VkPhysicalDevice> devices(deviceCount);
        vkEnumeratePhysicalDevices(instance, &deviceCount, devices.data());

        VkPhysicalDeviceProperties props;
        vkGetPhysicalDeviceProperties(devices[0], &props);
        std::cout<<"device ID: "<<props.deviceID<<std::endl;
        std::cout<<"device Name: "<<props.deviceName<<std::endl;


        /*
        接下来，我们选择一个可以用于我们的目的的设备。

        通过VkPhysicalDeviceFeatures（），我们可以检索设备支持的物理功能的细粒度列表。

        然而，在这个演示中，我们只是启动一个简单的计算着色器，没有

        此任务所需的特殊物理特征。

        使用VkPhysicalDeviceProperties（），我们可以获得物理设备属性的列表。最重要的是，

        我们得到了一个物理设备限制列表。对于这个应用程序，我们启动一个计算着色器，

        工作组的最大大小和计算着色器调用的总数受物理设备的限制，

        我们应该确保名为maxComputeWorkGroupCount、maxComputeWorkGroupInvocations和

        我们的应用程序不会超出maxComputeWorkGroupSize。此外，我们在计算着色器中使用了一个存储缓冲区，

        我们应该通过检查maxStorageBufferRange的限制来确保它不超过设备可以处理的范围。

        但是，在我们的应用程序中，工作组的大小和着色器调用的总数相对较小，并且存储缓冲区也较小

        因此，为了保持简单和干净，我们将不在这里执行任何此类检查，而只选择第一个实体

        列表中的设备。但在实际和严肃的应用程序中，这些限制肯定应该得到考虑。

        */
        for (VkPhysicalDevice device : devices) {
            if (true) { //如上所述，我们不做功能检查，所以只接受。
                physicalDevice = device;
                break;
            }
        }
    }

    // 返回支持计算操作的队列族的索引。
    uint32_t getComputeQueueFamilyIndex() {
        uint32_t queueFamilyCount;

        vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, NULL);

        // 检索所有队列族。
        std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
        vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, queueFamilies.data());

        // 现在找一个支持计算的族。
        uint32_t i = 0;
        for (; i < queueFamilies.size(); ++i) {
            VkQueueFamilyProperties props = queueFamilies[i];

            if (props.queueCount > 0 && (props.queueFlags & VK_QUEUE_COMPUTE_BIT)) {
                // 找到一个包含compute的队列。我们完了！
                break;
            }
        }

        if (i == queueFamilies.size()) {
            throw std::runtime_error("could not find a queue family that supports operations");
        }

        return i;
    }

    void createDevice() {
        /*
        我们在这个函数中创建逻辑设备。
        */

        /*
        在创建设备时，我们还指定它有哪些队列。
        */
        VkDeviceQueueCreateInfo queueCreateInfo = {};
        queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueFamilyIndex = getComputeQueueFamilyIndex(); // 查找具有计算功能的队列族。
        queueCreateInfo.queueFamilyIndex = queueFamilyIndex;
        queueCreateInfo.queueCount = 1; // 在此族中创建一个队列。我们不需要更多。
        float queuePriorities = 1.0;  // 我们只有一个队列，所以这没那么重要。
        queueCreateInfo.pQueuePriorities = &queuePriorities;

        /*
        现在我们创建逻辑设备。逻辑设备允许我们与物理设备进行交互设备。
        */
        VkDeviceCreateInfo deviceCreateInfo = {};

        // 在此处指定任何所需的设备功能。不过，我们不需要任何应用程序。
        VkPhysicalDeviceFeatures deviceFeatures = {};

        deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        deviceCreateInfo.enabledLayerCount = enabledLayers.size();  // 这里也需要指定验证层。
        deviceCreateInfo.ppEnabledLayerNames = enabledLayers.data();
        deviceCreateInfo.pQueueCreateInfos = &queueCreateInfo; // 在创建逻辑设备时，我们还指定它有哪些队列。
        deviceCreateInfo.queueCreateInfoCount = 1;
        deviceCreateInfo.pEnabledFeatures = &deviceFeatures;

        VK_CHECK_RESULT(vkCreateDevice(physicalDevice, &deviceCreateInfo, NULL, &device)); // 创建逻辑设备。

        //获取队列族中唯一成员的句柄。
        vkGetDeviceQueue(device, queueFamilyIndex, 0, &queue);
    }

    // 查找具有所需属性的内存类型。
    uint32_t findMemoryType(uint32_t memoryTypeBits, VkMemoryPropertyFlags properties) {
        VkPhysicalDeviceMemoryProperties memoryProperties;

        vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memoryProperties);

        /*
        这个搜索是如何工作的？
        有关详细说明，请参阅VkPhysicalDeviceMemoryProperties的文档。
        */
        for (uint32_t i = 0; i < memoryProperties.memoryTypeCount; ++i) {
            if ((memoryTypeBits & (1 << i)) &&
                ((memoryProperties.memoryTypes[i].propertyFlags & properties) == properties))
                return i;
        }
        return -1;
    }

    void createBuffer() {
        /*
        我们现在将创建一个缓冲区。稍后，我们将在计算机阴影中将mandelbrot集渲染到此缓冲区中。
        */
        
        VkBufferCreateInfo bufferCreateInfo = {};
        bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        bufferCreateInfo.size = bufferSize; // 缓冲区大小（字节）。
        bufferCreateInfo.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT; // 缓冲区用作存储缓冲区。
        bufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE; // 缓冲区一次只能用于单个队列族。

        VK_CHECK_RESULT(vkCreateBuffer(device, &bufferCreateInfo, NULL, &buffer)); // 创建缓冲区。

        /*
        但是缓冲区不会为自己分配内存，所以我们必须手动完成。
        */
    
        /*
        首先，我们找到缓冲区的内存需求。
        */
        VkMemoryRequirements memoryRequirements;
        vkGetBufferMemoryRequirements(device, buffer, &memoryRequirements);
        
        /*
        现在使用获得的内存需求信息为缓冲区分配内存。
        */
        VkMemoryAllocateInfo allocateInfo = {};
        allocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        allocateInfo.allocationSize = memoryRequirements.size; // 指定所需内存。
        /*
        有几种类型的内存可以分配，我们必须选择一种内存类型：

        1) 满足内存要求（memoryRequirements.memoryTypeBits）。
        2) 满足我们自己的使用要求。我们希望能够通过vkMapMemory将缓冲内存从GPU读取到CPU，因此我们设置了VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT.
        另外，通过设置VK_MEMORY_PROPERTY_HOST_COHERENT_BIT，设备（GPU）写入的内存将很容易被主机（CPU）看到，而无需调用任何额外的刷新命令。所以主要是为了方便，我们设置了这个标志。
        */
        allocateInfo.memoryTypeIndex = findMemoryType(
            memoryRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);

        VK_CHECK_RESULT(vkAllocateMemory(device, &allocateInfo, NULL, &bufferMemory)); // allocate memory on device.
        
        // 现在将分配的内存与缓冲区相关联。这样，缓冲区就得到了实际内存的支持。
        VK_CHECK_RESULT(vkBindBufferMemory(device, buffer, bufferMemory, 0));
    }

    void createDescriptorSetLayout() {
        /*
        这里我们指定一个描述符集布局。这允许我们将描述符绑定到着色器中的资源。

        */

        /*
        在这里，我们指定一个类型为VK_DESCRIPTOR_TYPE_STORAGE_BUFFER的绑定到绑定点0
          layout(std140, binding = 0) buffer buf
        in the compute shader.
        */
        VkDescriptorSetLayoutBinding descriptorSetLayoutBinding = {};
        descriptorSetLayoutBinding.binding = 0; // binding = 0
        descriptorSetLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        descriptorSetLayoutBinding.descriptorCount = 1;
        descriptorSetLayoutBinding.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;

        VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo = {};
        descriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        descriptorSetLayoutCreateInfo.bindingCount = 1; // 此描述符集布局中只有一个绑定。
        descriptorSetLayoutCreateInfo.pBindings = &descriptorSetLayoutBinding; 

        // 创建描述符集布局。
        VK_CHECK_RESULT(vkCreateDescriptorSetLayout(device, &descriptorSetLayoutCreateInfo, NULL, &descriptorSetLayout));
    }

    void createDescriptorSet() {
        /*
        所以我们将在这里分配一个描述符集。
        但我们需要首先创建一个描述符池来完成这项工作。
        */

        /*
        我们的描述符池只能分配一个存储缓冲区。
        */
        VkDescriptorPoolSize descriptorPoolSize = {};
        descriptorPoolSize.type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        descriptorPoolSize.descriptorCount = 1;

        VkDescriptorPoolCreateInfo descriptorPoolCreateInfo = {};
        descriptorPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        descriptorPoolCreateInfo.maxSets = 1; // 我们只需要从池中分配一个描述符集。
        descriptorPoolCreateInfo.poolSizeCount = 1;
        descriptorPoolCreateInfo.pPoolSizes = &descriptorPoolSize;

        // 创建描述符池。
        VK_CHECK_RESULT(vkCreateDescriptorPool(device, &descriptorPoolCreateInfo, NULL, &descriptorPool));

        /*
        分配了池之后，我们现在可以分配描述符集。
        */
        VkDescriptorSetAllocateInfo descriptorSetAllocateInfo = {};
        descriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO; 
        descriptorSetAllocateInfo.descriptorPool = descriptorPool; // 要从中分配的池。
        descriptorSetAllocateInfo.descriptorSetCount = 1; // 分配单个描述符集.
        descriptorSetAllocateInfo.pSetLayouts = &descriptorSetLayout;

        // 分配描述符集。
        VK_CHECK_RESULT(vkAllocateDescriptorSets(device, &descriptorSetAllocateInfo, &descriptorSet));

        /*
        接下来，我们需要将实际的存储缓冲区与descrptor连接起来。
        我们使用vkUpdateDescriptorSets（）来更新描述符集。
        */

        // 指定要绑定到描述符的缓冲区。
        VkDescriptorBufferInfo descriptorBufferInfo = {};
        descriptorBufferInfo.buffer = buffer;
        descriptorBufferInfo.offset = 0;
        descriptorBufferInfo.range = bufferSize;

        VkWriteDescriptorSet writeDescriptorSet = {};
        writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writeDescriptorSet.dstSet = descriptorSet; // 写入此描述符集。
        writeDescriptorSet.dstBinding = 0; // 先写，再绑定。
        writeDescriptorSet.descriptorCount = 1; // 更新单个描述符。
        writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER; // storage buffer.
        writeDescriptorSet.pBufferInfo = &descriptorBufferInfo;

        // 执行描述符集的更新。
        vkUpdateDescriptorSets(device, 1, &writeDescriptorSet, 0, NULL);
    }

    // 将文件读入字节数组，并转换为uint32\u t*，然后返回。
    // 数据已被填充，因此可以放入数组uint32\t中。
    uint32_t* readFile(uint32_t& length, const char* filename) {

        FILE* fp = fopen(filename, "rb");
        if (fp == NULL) {
            printf("Could not find or open file: %s\n", filename);
        }

        // get file size.
        fseek(fp, 0, SEEK_END);
        long filesize = ftell(fp);
        fseek(fp, 0, SEEK_SET);

        long filesizepadded = long(ceil(filesize / 4.0)) * 4;

        // read file contents.
        char *str = new char[filesizepadded];
        fread(str, filesize, sizeof(char), fp);
        fclose(fp);

        // data padding. 
        for (int i = filesize; i < filesizepadded; i++) {
            str[i] = 0;
        }

        length = filesizepadded;
        return (uint32_t *)str;
    }

    void createComputePipeline() {
        /*
        我们在这里创建一个计算管道。
        */

        /*
        创建着色器模块。着色器模块基本上只是封装了一些着色器代码。
        */
        uint32_t filelength;
        // the code in comp.spv was created by running the command:
        // glslangValidator.exe -V shader.comp
        uint32_t* code = readFile(filelength, "shaders/comp.spv");
        VkShaderModuleCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        createInfo.pCode = code;
        createInfo.codeSize = filelength;
        
        VK_CHECK_RESULT(vkCreateShaderModule(device, &createInfo, NULL, &computeShaderModule));
        delete[] code;

        /*
        现在让我们实际创建计算管道。
        与图形管道相比，计算管道非常简单。
        它只包含一个带有计算着色器的单个阶段。

        所以首先我们指定compute shader stage，它是入口点（main）。
        */
        VkPipelineShaderStageCreateInfo shaderStageCreateInfo = {};
        shaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        shaderStageCreateInfo.stage = VK_SHADER_STAGE_COMPUTE_BIT;
        shaderStageCreateInfo.module = computeShaderModule;
        shaderStageCreateInfo.pName = "main";

        /*
        管道布局允许管道访问描述符集。
        因此，我们只需指定前面创建的描述符集布局。
        */
        VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = {};
        pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        pipelineLayoutCreateInfo.setLayoutCount = 1;
        pipelineLayoutCreateInfo.pSetLayouts = &descriptorSetLayout; 
        VK_CHECK_RESULT(vkCreatePipelineLayout(device, &pipelineLayoutCreateInfo, NULL, &pipelineLayout));

        VkComputePipelineCreateInfo pipelineCreateInfo = {};
        pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
        pipelineCreateInfo.stage = shaderStageCreateInfo;
        pipelineCreateInfo.layout = pipelineLayout;

        /*
        现在，我们终于创建了计算管道。
        */
        VK_CHECK_RESULT(vkCreateComputePipelines(
            device, VK_NULL_HANDLE,
            1, &pipelineCreateInfo,
            NULL, &pipeline));
    }

    void createCommandBuffer() {
        /*
        我们离终点越来越近了。为了向设备（GPU）发送命令，
        我们必须首先将命令记录到命令缓冲区中。
        要分配命令缓冲区，必须首先创建一个命令池。所以让我们这样做。
        */
        VkCommandPoolCreateInfo commandPoolCreateInfo = {};
        commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        commandPoolCreateInfo.flags = 0;
        // 此命令池的队列族。从该命令池分配的所有命令缓冲区，
        // 必须仅提交到此族的队列。
        commandPoolCreateInfo.queueFamilyIndex = queueFamilyIndex;
        VK_CHECK_RESULT(vkCreateCommandPool(device, &commandPoolCreateInfo, NULL, &commandPool));

        /*
        现在从命令池中分配一个命令缓冲区。
        */
        VkCommandBufferAllocateInfo commandBufferAllocateInfo = {};
        commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        commandBufferAllocateInfo.commandPool = commandPool; // 指定要从中分配的命令池
        // 如果命令缓冲区是主的，则可以直接提交到队列。
        // 辅助缓冲区必须从某个主命令缓冲区调用，不能直接调用
        // 已提交到队列。为了简单起见，我们使用一个主命令缓冲区。
        commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        commandBufferAllocateInfo.commandBufferCount = 1; // 分配单个命令缓冲区。
        VK_CHECK_RESULT(vkAllocateCommandBuffers(device, &commandBufferAllocateInfo, &commandBuffer)); // 分配命令缓冲区。.

        /*
        现在我们将开始将命令记录到新分配的命令缓冲区中。
        */
        VkCommandBufferBeginInfo beginInfo = {};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT; // 缓冲区在此应用程序中仅提交和使用一次。
        VK_CHECK_RESULT(vkBeginCommandBuffer(commandBuffer, &beginInfo)); // 开始录制命令。

        /*
        在分派之前，我们需要绑定一个管道和一个描述符集。
        如果您忘记了这些，验证层将不会给出警告，因此请非常小心不要忘记它们。
        */
        vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline);
        vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipelineLayout, 0, 1, &descriptorSet, 0, NULL);

        /*
        调用vkCmdDispatch基本上启动计算管道，并执行计算着色器。
        工作组的数目在参数中指定。
        如果您已经熟悉OpenGL中的计算着色器，那么这对您来说应该不是什么新鲜事。
        */
        vkCmdDispatch(commandBuffer, (uint32_t)ceil(WIDTH / float(WORKGROUP_SIZE)), (uint32_t)ceil(HEIGHT / float(WORKGROUP_SIZE)), 1);

        VK_CHECK_RESULT(vkEndCommandBuffer(commandBuffer)); // end recording commands.
    }

    void runCommandBuffer() {
        /*
        现在，我们将最终将记录的命令缓冲区提交到队列。
        */

        VkSubmitInfo submitInfo = {};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.commandBufferCount = 1; // 提交单个命令缓冲区
        submitInfo.pCommandBuffers = &commandBuffer; // 要提交的命令缓冲区。

        /*
         我们建了一个篱笆。
        */
        VkFence fence;
        VkFenceCreateInfo fenceCreateInfo = {};
        fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        fenceCreateInfo.flags = 0;
        VK_CHECK_RESULT(vkCreateFence(device, &fenceCreateInfo, NULL, &fence));

        /*
        我们在队列上提交命令缓冲区，同时给出一个围栏。
        */
        VK_CHECK_RESULT(vkQueueSubmit(queue, 1, &submitInfo, fence));
        /*
        在围栏发出信号之前，命令不会完成执行。
        所以我们在这里等着。
        我们将直接从GPU读取缓冲区，
        我们也不能确定命令是否已经执行完毕，除非我们等待围栏。
        因此，我们在这里使用围栏。
        */
        VK_CHECK_RESULT(vkWaitForFences(device, 1, &fence, VK_TRUE, 100000000000));

        vkDestroyFence(device, fence, NULL);
    }

    void cleanup() {
        /*
        Clean up all Vulkan Resources. 
        */

        if (enableValidationLayers) {
            // destroy callback.
            auto func = (PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT");
            if (func == nullptr) {
                throw std::runtime_error("Could not load vkDestroyDebugReportCallbackEXT");
            }
            func(instance, debugReportCallback, NULL);
        }

        vkFreeMemory(device, bufferMemory, NULL);
        vkDestroyBuffer(device, buffer, NULL);	
        vkDestroyShaderModule(device, computeShaderModule, NULL);
        vkDestroyDescriptorPool(device, descriptorPool, NULL);
        vkDestroyDescriptorSetLayout(device, descriptorSetLayout, NULL);
        vkDestroyPipelineLayout(device, pipelineLayout, NULL);
        vkDestroyPipeline(device, pipeline, NULL);
        vkDestroyCommandPool(device, commandPool, NULL);	
        vkDestroyDevice(device, NULL);
        vkDestroyInstance(instance, NULL);		
    }
};

int main() {


    ComputeApplication app;

    try {
        auto st = std::chrono::high_resolution_clock::now();
        app.run();
        auto so = std::chrono::high_resolution_clock::now();
        std::cout<<std::chrono::duration<float,std::milli>(so - st).count();
    }
    catch (const std::runtime_error& e) {
        printf("%s\n", e.what());
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}

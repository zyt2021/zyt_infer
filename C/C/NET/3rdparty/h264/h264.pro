QT += gui

CONFIG += c++11 console
CONFIG -= app_bundle

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES +=  main.cpp \
    QAviWriter.cpp \
    gwavi.cpp
# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


INCLUDEPATH+=D:/libs/opencv/include
INCLUDEPATH+=D:/Msnhnet/include
LIBS += D:/Msnhnet/lib/Msnhnet.lib
LIBS +=  D:/libs/opencv/x64/vc15/lib/opencv_*430_d.lib

HEADERS += \
    QAviWriter.h \
    gwavi.h \
    pl_mpeg.h

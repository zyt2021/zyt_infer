﻿#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
//#include <Msnhnet/io/MsnhIO.h>

#define PL_MPEG_IMPLEMENTATION
#include "pl_mpeg.h"
#include <QAviWriter.h>
#include <QDebug>

int main(int argc, char* argv[]) {

    plm_t *plm = plm_create_with_filename("C:/Users/msnh/Desktop/output.mpg");
    if (!plm) {
        printf("Couldn't open file");
        return 1;
    }

    plm_set_audio_enabled(plm, FALSE);

    int w = plm_get_width(plm);
    int h = plm_get_height(plm);

    uint8_t *rgb_buffer = (uint8_t *)malloc(w * h * 3);

    char png_name[16];
    plm_frame_t *frame = NULL;
    int hh = plm_buffer_get_remaining(plm->video_buffer);
    for (int i = 0; frame = plm_decode_video(plm); i++)
    {

        plm_frame_to_rgb(frame, rgb_buffer, w * 3);
        cv::Mat mat(h,w,CV_8UC3,rgb_buffer);
        cv::cvtColor(mat,mat,cv::COLOR_RGB2BGR);
        cv::imshow("test",mat);
        cv::waitKey(30);
    }

//    QAviWriter avi;
//    avi.setFileName("a.avi");
//    avi.setFps(30);
//    avi.setSize({960,544});
//    avi.open();

//    std::vector<std::string> imgs;
//    Msnhnet::IO::readVectorStr(imgs,"C:/Users/msnh-pc/Desktop/test/a.txt","\n");
//    //qDebug()<<imgs.size();
//    for (int i = 0; i < imgs.size(); ++i)
//    {
//        QImage img(QString::fromStdString(imgs[i]));
//        avi.addFrame(img);
//        qDebug()<<i;
//    }
//    avi.close();


    return 0;
}

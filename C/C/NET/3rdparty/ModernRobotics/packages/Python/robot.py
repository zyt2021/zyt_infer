import numpy as np
from modern_robotics.core import *

R = np.array(((1,0,0),(0,1,0),(0,0,1)))
omg = np.array((1,0,0))
print(so3ToVec(VecToso3(omg)))
print(AxisAng3(omg*10))

T = np.array(((1,0,0,0),(0,0,-1,0),(0,1,0,3),(0,0,0,1)))
print(np.linalg.pinv(T))
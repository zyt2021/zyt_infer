﻿//#ifndef TESTACTIVATIONS_H
//#define TESTACTIVATIONS_H
//#ifdef USE_X86
//#include "Msnhnet/layers/MsnhActivationsAvx.h"
//#include "Msnhnet/layers/MsnhActivations.h"
//#define CATCH_CONFIG_MAIN
//#include "../3rdparty/catch2/catch.hpp"
//#include <iostream>

//TEST_CASE ("logistic","[single-file]")
//{
//    float a[] = {-3.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    float b[] = {-3.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    Msnhnet::Activations::activateArray(a,8,ActivationType::LOGISTIC,false);
//    Msnhnet::ActivationsAvx::logisticActivateSize8(b);
//    REQUIRE((a[0]-b[0])<0.000001);
//    REQUIRE((a[1]-b[1])<0.000001);
//    REQUIRE((a[2]-b[2])<0.000001);
//    REQUIRE((a[3]-b[3])<0.000001);
//    REQUIRE((a[4]-b[4])<0.000001);
//    REQUIRE((a[5]-b[5])<0.000001);
//    REQUIRE((a[6]-b[6])<0.000001);
//    REQUIRE((a[7]-b[7])<0.000001);
//    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<a[4]<<" "<<a[5]<<" "<<a[6]<<" "<<a[7]<<" "<<std::endl<<std::flush;
//    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<b[4]<<" "<<b[5]<<" "<<b[6]<<" "<<b[7]<<" "<<std::endl<<std::flush;
//}

//TEST_CASE ("loggy","[single-file]")
//{
//    float a[] = {-3.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    float b[] = {-3.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    Msnhnet::Activations::activateArray(a,8,ActivationType::LOGGY,false);
//    Msnhnet::ActivationsAvx::loggyActivateSize8(b);
//    REQUIRE((a[0]-b[0])<0.000001);
//    REQUIRE((a[1]-b[1])<0.000001);
//    REQUIRE((a[2]-b[2])<0.000001);
//    REQUIRE((a[3]-b[3])<0.000001);
//    REQUIRE((a[4]-b[4])<0.000001);
//    REQUIRE((a[5]-b[5])<0.000001);
//    REQUIRE((a[6]-b[6])<0.000001);
//    REQUIRE((a[7]-b[7])<0.000001);
//    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<a[4]<<" "<<a[5]<<" "<<a[6]<<" "<<a[7]<<" "<<std::endl<<std::flush;
//    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<b[4]<<" "<<b[5]<<" "<<b[6]<<" "<<b[7]<<" "<<std::endl<<std::flush;
//}

//TEST_CASE ("relu","[single-file]")
//{
//    float a[] = {-3.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    float b[] = {-3.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    Msnhnet::Activations::activateArray(a,8,ActivationType::RELU,false);
//    Msnhnet::ActivationsAvx::reluActivateSize8(b);
//    REQUIRE((a[0]-b[0])<0.000001);
//    REQUIRE((a[1]-b[1])<0.000001);
//    REQUIRE((a[2]-b[2])<0.000001);
//    REQUIRE((a[3]-b[3])<0.000001);
//    REQUIRE((a[4]-b[4])<0.000001);
//    REQUIRE((a[5]-b[5])<0.000001);
//    REQUIRE((a[6]-b[6])<0.000001);
//    REQUIRE((a[7]-b[7])<0.000001);
//    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<a[4]<<" "<<a[5]<<" "<<a[6]<<" "<<a[7]<<" "<<std::endl<<std::flush;
//    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<b[4]<<" "<<b[5]<<" "<<b[6]<<" "<<b[7]<<" "<<std::endl<<std::flush;
//}
//TEST_CASE ("relu6","[single-file]")
//{
//    float a[] = {-3.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    float b[] = {-3.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    Msnhnet::Activations::activateArray(a,8,ActivationType::RELU6,false);
//    Msnhnet::ActivationsAvx::relu6ActivateSize8(b);
//    REQUIRE((a[0]-b[0])<0.000001);
//    REQUIRE((a[1]-b[1])<0.000001);
//    REQUIRE((a[2]-b[2])<0.000001);
//    REQUIRE((a[3]-b[3])<0.000001);
//    REQUIRE((a[4]-b[4])<0.000001);
//    REQUIRE((a[5]-b[5])<0.000001);
//    REQUIRE((a[6]-b[6])<0.000001);
//    REQUIRE((a[7]-b[7])<0.000001);
//    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<a[4]<<" "<<a[5]<<" "<<a[6]<<" "<<a[7]<<" "<<std::endl<<std::flush;
//    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<b[4]<<" "<<b[5]<<" "<<b[6]<<" "<<b[7]<<" "<<std::endl<<std::flush;
//}

//TEST_CASE ("elu","[single-file]")
//{
//    float a[] = {-3.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    float b[] = {-3.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    Msnhnet::Activations::activateArray(a,8,ActivationType::ELU,false);
//    Msnhnet::ActivationsAvx::eluActivateSize8(b);
//    REQUIRE((a[0]-b[0])<0.000001);
//    REQUIRE((a[1]-b[1])<0.000001);
//    REQUIRE((a[2]-b[2])<0.000001);
//    REQUIRE((a[3]-b[3])<0.000001);
//    REQUIRE((a[4]-b[4])<0.000001);
//    REQUIRE((a[5]-b[5])<0.000001);
//    REQUIRE((a[6]-b[6])<0.000001);
//    REQUIRE((a[7]-b[7])<0.000001);
//    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<a[4]<<" "<<a[5]<<" "<<a[6]<<" "<<a[7]<<" "<<std::endl<<std::flush;
//    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<b[4]<<" "<<b[5]<<" "<<b[6]<<" "<<b[7]<<" "<<std::endl<<std::flush;
//}

//TEST_CASE ("selu","[single-file]")
//{
//    float a[] = {-3.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    float b[] = {-3.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    Msnhnet::Activations::activateArray(a,8,ActivationType::SELU,false);
//    Msnhnet::ActivationsAvx::seluActivateSize8(b);
//    REQUIRE((a[0]-b[0])<0.000001);
//    REQUIRE((a[1]-b[1])<0.000001);
//    REQUIRE((a[2]-b[2])<0.000001);
//    REQUIRE((a[3]-b[3])<0.000001);
//    REQUIRE((a[4]-b[4])<0.000001);
//    REQUIRE((a[5]-b[5])<0.000001);
//    REQUIRE((a[6]-b[6])<0.000001);
//    REQUIRE((a[7]-b[7])<0.000001);
//    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<a[4]<<" "<<a[5]<<" "<<a[6]<<" "<<a[7]<<" "<<std::endl<<std::flush;
//    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<b[4]<<" "<<b[5]<<" "<<b[6]<<" "<<b[7]<<" "<<std::endl<<std::flush;
//}

//TEST_CASE ("relie","[single-file]")
//{
//    float a[] = {-3.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    float b[] = {-3.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    Msnhnet::Activations::activateArray(a,8,ActivationType::RELIE,false);
//    Msnhnet::ActivationsAvx::relieActivateSize8(b);
//    REQUIRE((a[0]-b[0])<0.000001);
//    REQUIRE((a[1]-b[1])<0.000001);
//    REQUIRE((a[2]-b[2])<0.000001);
//    REQUIRE((a[3]-b[3])<0.000001);
//    REQUIRE((a[4]-b[4])<0.000001);
//    REQUIRE((a[5]-b[5])<0.000001);
//    REQUIRE((a[6]-b[6])<0.000001);
//    REQUIRE((a[7]-b[7])<0.000001);
//    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<a[4]<<" "<<a[5]<<" "<<a[6]<<" "<<a[7]<<" "<<std::endl<<std::flush;
//    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<b[4]<<" "<<b[5]<<" "<<b[6]<<" "<<b[7]<<" "<<std::endl<<std::flush;
//}

//TEST_CASE ("ramp","[single-file]")
//{
//    float a[] = {-3.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    float b[] = {-3.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    Msnhnet::Activations::activateArray(a,8,ActivationType::RAMP,false);
//    Msnhnet::ActivationsAvx::rampActivateSize8(b);
//    REQUIRE((a[0]-b[0])<0.000001);
//    REQUIRE((a[1]-b[1])<0.000001);
//    REQUIRE((a[2]-b[2])<0.000001);
//    REQUIRE((a[3]-b[3])<0.000001);
//    REQUIRE((a[4]-b[4])<0.000001);
//    REQUIRE((a[5]-b[5])<0.000001);
//    REQUIRE((a[6]-b[6])<0.000001);
//    REQUIRE((a[7]-b[7])<0.000001);
//    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<a[4]<<" "<<a[5]<<" "<<a[6]<<" "<<a[7]<<" "<<std::endl<<std::flush;
//    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<b[4]<<" "<<b[5]<<" "<<b[6]<<" "<<b[7]<<" "<<std::endl<<std::flush;
//}

//TEST_CASE ("leaky","[single-file]")
//{
//    float a[] = {-3.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    float b[] = {-3.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    Msnhnet::Activations::activateArray(a,8,ActivationType::LEAKY,false);
//    Msnhnet::ActivationsAvx::leakyActivateSize8(b);
//    REQUIRE((a[0]-b[0])<0.000001);
//    REQUIRE((a[1]-b[1])<0.000001);
//    REQUIRE((a[2]-b[2])<0.000001);
//    REQUIRE((a[3]-b[3])<0.000001);
//    REQUIRE((a[4]-b[4])<0.000001);
//    REQUIRE((a[5]-b[5])<0.000001);
//    REQUIRE((a[6]-b[6])<0.000001);
//    REQUIRE((a[7]-b[7])<0.000001);
//    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<a[4]<<" "<<a[5]<<" "<<a[6]<<" "<<a[7]<<" "<<std::endl<<std::flush;
//    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<b[4]<<" "<<b[5]<<" "<<b[6]<<" "<<b[7]<<" "<<std::endl<<std::flush;
//}

//TEST_CASE ("tanh","[single-file]")
//{
//    float a[] = {-3.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    float b[] = {-3.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    Msnhnet::Activations::activateArray(a,8,ActivationType::TANH,false);
//    Msnhnet::ActivationsAvx::tanhActivateSize8(b);
//    REQUIRE((a[0]-b[0])<0.000001);
//    REQUIRE((a[1]-b[1])<0.000001);
//    REQUIRE((a[2]-b[2])<0.000001);
//    REQUIRE((a[3]-b[3])<0.000001);
//    REQUIRE((a[4]-b[4])<0.000001);
//    REQUIRE((a[5]-b[5])<0.000001);
//    REQUIRE((a[6]-b[6])<0.000001);
//    REQUIRE((a[7]-b[7])<0.000001);
//    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<a[4]<<" "<<a[5]<<" "<<a[6]<<" "<<a[7]<<" "<<std::endl<<std::flush;
//    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<b[4]<<" "<<b[5]<<" "<<b[6]<<" "<<b[7]<<" "<<std::endl<<std::flush;
//}

//TEST_CASE ("softplus","[single-file]")
//{
//    float a[] = {-3.f, -2.f, -1.f, 0 ,0.1f, 2.f, 3.f, 4.f};
//    float b[] = {-3.f, -2.f, -1.f, 0 ,0.1f, 2.f, 3.f, 4.f};
//    Msnhnet::Activations::activateArray(a,8,ActivationType::SOFT_PLUS,false);
//    Msnhnet::ActivationsAvx::softplusActivateSize8(b,0.1f);
//    REQUIRE((a[0]-b[0])<0.000001);
//    REQUIRE((a[1]-b[1])<0.000001);
//    REQUIRE((a[2]-b[2])<0.000001);
//    REQUIRE((a[3]-b[3])<0.000001);
//    REQUIRE((a[4]-b[4])<0.000001);
//    REQUIRE((a[5]-b[5])<0.000001);
//    REQUIRE((a[6]-b[6])<0.000001);
//    REQUIRE((a[7]-b[7])<0.000001);
//    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<a[4]<<" "<<a[5]<<" "<<a[6]<<" "<<a[7]<<" "<<std::endl<<std::flush;
//    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<b[4]<<" "<<b[5]<<" "<<b[6]<<" "<<b[7]<<" "<<std::endl<<std::flush;
//}

//TEST_CASE ("plse","[single-file]")
//{
//    float a[] = {-4.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    float b[] = {-4.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    Msnhnet::Activations::activateArray(a,8,ActivationType::PLSE,false);
//    Msnhnet::ActivationsAvx::plseActivateSize8(b);
//    REQUIRE((a[0]-b[0])<0.000001);
//    REQUIRE((a[1]-b[1])<0.000001);
//    REQUIRE((a[2]-b[2])<0.000001);
//    REQUIRE((a[3]-b[3])<0.000001);
//    REQUIRE((a[4]-b[4])<0.000001);
//    REQUIRE((a[5]-b[5])<0.000001);
//    REQUIRE((a[6]-b[6])<0.000001);
//    REQUIRE((a[7]-b[7])<0.000001);
//    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<a[4]<<" "<<a[5]<<" "<<a[6]<<" "<<a[7]<<" "<<std::endl<<std::flush;
//    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<b[4]<<" "<<b[5]<<" "<<b[6]<<" "<<b[7]<<" "<<std::endl<<std::flush;
//}

//TEST_CASE ("lhtan","[single-file]")
//{
//    float a[] = {-4.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    float b[] = {-4.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    Msnhnet::Activations::activateArray(a,8,ActivationType::LHTAN,false);
//    Msnhnet::ActivationsAvx::lhtanActivateSize8(b);
//    REQUIRE((a[0]-b[0])<0.000001);
//    REQUIRE((a[1]-b[1])<0.000001);
//    REQUIRE((a[2]-b[2])<0.000001);
//    REQUIRE((a[3]-b[3])<0.000001);
//    REQUIRE((a[4]-b[4])<0.000001);
//    REQUIRE((a[5]-b[5])<0.000001);
//    REQUIRE((a[6]-b[6])<0.000001);
//    REQUIRE((a[7]-b[7])<0.000001);
//    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<a[4]<<" "<<a[5]<<" "<<a[6]<<" "<<a[7]<<" "<<std::endl<<std::flush;
//    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<b[4]<<" "<<b[5]<<" "<<b[6]<<" "<<b[7]<<" "<<std::endl<<std::flush;
//}

//TEST_CASE ("mish","[single-file]")
//{
//    float a[] = {-4.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    float b[] = {-4.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    Msnhnet::Activations::activateArray(a,8,ActivationType::MISH,false);
//    Msnhnet::ActivationsAvx::mishActivateSize8(b);
//    REQUIRE((a[0]-b[0])<0.000001);
//    REQUIRE((a[1]-b[1])<0.000001);
//    REQUIRE((a[2]-b[2])<0.000001);
//    REQUIRE((a[3]-b[3])<0.000001);
//    REQUIRE((a[4]-b[4])<0.000001);
//    REQUIRE((a[5]-b[5])<0.000001);
//    REQUIRE((a[6]-b[6])<0.000001);
//    REQUIRE((a[7]-b[7])<0.000001);
//    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<a[4]<<" "<<a[5]<<" "<<a[6]<<" "<<a[7]<<" "<<std::endl<<std::flush;
//    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<b[4]<<" "<<b[5]<<" "<<b[6]<<" "<<b[7]<<" "<<std::endl<<std::flush;
//}

//TEST_CASE ("smish","[single-file]")
//{
//    float a[] = {-4.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    float b[] = {-4.f, -2.f, -1.f, 0 ,1.f, 2.f, 3.f, 4.f};
//    Msnhnet::Activations::activateArray(a,8,ActivationType::SWISH,false);
//    Msnhnet::ActivationsAvx::swishActivateSize8(b);
//    REQUIRE((a[0]-b[0])<0.000001);
//    REQUIRE((a[1]-b[1])<0.000001);
//    REQUIRE((a[2]-b[2])<0.000001);
//    REQUIRE((a[3]-b[3])<0.000001);
//    REQUIRE((a[4]-b[4])<0.000001);
//    REQUIRE((a[5]-b[5])<0.000001);
//    REQUIRE((a[6]-b[6])<0.000001);
//    REQUIRE((a[7]-b[7])<0.000001);
//    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<a[4]<<" "<<a[5]<<" "<<a[6]<<" "<<a[7]<<" "<<std::endl<<std::flush;
//    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<b[4]<<" "<<b[5]<<" "<<b[6]<<" "<<b[7]<<" "<<std::endl<<std::flush;
//}
//#endif
//#endif // TESTACTIVATIONS_H

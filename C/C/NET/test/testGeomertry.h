﻿#include <Msnhnet/cv/MsnhCV.h>
#include "Msnhnet/math/MsnhMath.h"

void testGeomertry()
{

    /*  [ x: 1.0471976, y: 0.5235988, z: 0.7853982 ]
     *
     *  旋转矩阵
     *  [  0.6123725, -0.0473672,  0.7891491;
     *     0.6123725,  0.6597396, -0.4355958;
     *    -0.5000000,  0.7500000,  0.4330127 ]
     *  四元数
     *  [ 0.3604234, 0.3919038, 0.2005621, 0.8223632 ]
     *
     *  旋转矢量
     *  [ 0.7668133, 0.8337891, 0.4267029 ]
     */
    Msnhnet::EulerD eulerD({deg2radd(60.0),deg2radd(30.0),deg2radd(45.0)});
    Msnhnet::RotationMatD rotMatD = Msnhnet::Geometry::euler2RotMat(eulerD,Msnhnet::RotSequence::ROT_ZYX);
    Msnhnet::QuaternionD  quatD   = Msnhnet::Geometry::euler2Quaternion(eulerD,Msnhnet::RotSequence::ROT_ZYX);
    Msnhnet::RotationVecD rotVecD = Msnhnet::Geometry::euler2RotVec(eulerD,Msnhnet::RotSequence::ROT_ZYX);
    rotMatD.print();
    quatD.print();
    rotVecD.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::Geometry::rotVec2Euler(rotVecD,Msnhnet::RotSequence::ROT_ZYX).print();
    eulerD.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::Geometry::rotVec2RotMat(rotVecD).print();
    rotMatD.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::Geometry::rotVec2Quaternion(rotVecD).print();
    quatD.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::Geometry::rotMat2Euler(rotMatD,Msnhnet::RotSequence::ROT_ZYX).print();
    eulerD.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::Geometry::rotMat2Quaternion(rotMatD).print();
    quatD.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::Geometry::rotMat2RotVec(rotMatD).print();
    rotVecD.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::Geometry::quaternion2Euler(quatD,Msnhnet::RotSequence::ROT_ZYX).print();
    eulerD.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::Geometry::quaternion2RotVec(quatD).print();
    rotVecD.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::Geometry::quaternion2RotMat(quatD).print();
    rotMatD.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    std::cout<<std::endl<<std::endl;
    Msnhnet::EulerF eulerF({deg2radf(60.0f),deg2radf(30.0f),deg2radf(45.0f)});
    Msnhnet::RotationMatF rotMatF =Msnhnet::Geometry::euler2RotMat(eulerF,Msnhnet::RotSequence::ROT_ZYX);
    Msnhnet::QuaternionF  quatF   = Msnhnet::Geometry::euler2Quaternion(eulerF,Msnhnet::RotSequence::ROT_ZYX);
    Msnhnet::RotationVecF rotVecF = Msnhnet::Geometry::euler2RotVec(eulerF,Msnhnet::RotSequence::ROT_ZYX);
    rotMatF.print();
    quatF.print();
    rotVecF.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::Geometry::rotVec2Euler(rotVecF,Msnhnet::RotSequence::ROT_ZYX).print();
    eulerF.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::Geometry::rotVec2RotMat(rotVecF).print();
    rotMatF.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::Geometry::rotVec2Quaternion(rotVecF).print();
    quatF.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::Geometry::rotMat2Euler(rotMatF,Msnhnet::RotSequence::ROT_ZYX).print();
    eulerF.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::Geometry::rotMat2Quaternion(rotMatF).print();
    quatF.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::Geometry::rotMat2RotVec(rotMatF).print();
    rotVecF.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::Geometry::quaternion2Euler(quatF,Msnhnet::RotSequence::ROT_ZYX).print();
    eulerF.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::Geometry::quaternion2RotVec(quatF).print();
    rotVecF.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::Geometry::quaternion2RotMat(quatF).print();
    rotMatF.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;
}

extern void testGeomertryS()
{

    /*  [ x: 1.0471976, y: 0.5235988, z: 0.7853982 ]
     *
     *  旋转矩阵
     *  [  0.6123725, -0.0473672,  0.7891491;
     *     0.6123725,  0.6597396, -0.4355958;
     *    -0.5000000,  0.7500000,  0.4330127 ]
     *  四元数
     *  [ 0.3604234, 0.3919038, 0.2005621, 0.8223632 ]
     *
     *  旋转矢量
     *  [ 0.7668133, 0.8337891, 0.4267029 ]
     */
    Msnhnet::EulerDS eulerD(deg2radd(60.0),deg2radd(30.0),deg2radd(45.0));
    Msnhnet::RotationMatDS rotMatD = Msnhnet::GeometryS::euler2RotMat(eulerD,Msnhnet::RotSequence::ROT_ZYX);
    Msnhnet::QuaternionDS  quatD   = Msnhnet::GeometryS::euler2Quaternion(eulerD,Msnhnet::RotSequence::ROT_ZYX);
    Msnhnet::RotationVecDS rotVecD = Msnhnet::GeometryS::euler2RotVec(eulerD,Msnhnet::RotSequence::ROT_ZYX);
    rotMatD.print();
    quatD.print();
    rotVecD.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::GeometryS::rotVec2Euler(rotVecD,Msnhnet::RotSequence::ROT_ZYX).print();
    eulerD.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::GeometryS::rotVec2RotMat(rotVecD).print();
    rotMatD.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::GeometryS::rotVec2Quaternion(rotVecD).print();
    quatD.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::GeometryS::rotMat2Euler(rotMatD,Msnhnet::RotSequence::ROT_ZYX).print();
    eulerD.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::GeometryS::rotMat2Quaternion(rotMatD).print();
    quatD.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::GeometryS::rotMat2RotVec(rotMatD).print();
    rotVecD.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::GeometryS::quaternion2Euler(quatD,Msnhnet::RotSequence::ROT_ZYX).print();
    eulerD.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::GeometryS::quaternion2RotVec(quatD).print();
    rotVecD.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::GeometryS::quaternion2RotMat(quatD).print();
    rotMatD.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    std::cout<<std::endl<<std::endl;
    Msnhnet::EulerFS eulerF(deg2radf(60.0f),deg2radf(30.0f),deg2radf(45.0f));
    Msnhnet::RotationMatFS rotMatF =Msnhnet::GeometryS::euler2RotMat(eulerF,Msnhnet::RotSequence::ROT_ZYX);
    Msnhnet::QuaternionFS  quatF   = Msnhnet::GeometryS::euler2Quaternion(eulerF,Msnhnet::RotSequence::ROT_ZYX);
    Msnhnet::RotationVecFS rotVecF = Msnhnet::GeometryS::euler2RotVec(eulerF,Msnhnet::RotSequence::ROT_ZYX);
    rotMatF.print();
    quatF.print();
    rotVecF.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::GeometryS::rotVec2Euler(rotVecF,Msnhnet::RotSequence::ROT_ZYX).print();
    eulerF.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::GeometryS::rotVec2RotMat(rotVecF).print();
    rotMatF.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::GeometryS::rotVec2Quaternion(rotVecF).print();
    quatF.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::GeometryS::rotMat2Euler(rotMatF,Msnhnet::RotSequence::ROT_ZYX).print();
    eulerF.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::GeometryS::rotMat2Quaternion(rotMatF).print();
    quatF.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::GeometryS::rotMat2RotVec(rotMatF).print();
    rotVecF.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::GeometryS::quaternion2Euler(quatF,Msnhnet::RotSequence::ROT_ZYX).print();
    eulerF.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::GeometryS::quaternion2RotVec(quatF).print();
    rotVecF.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;

    Msnhnet::GeometryS::quaternion2RotMat(quatF).print();
    rotMatF.print();
    std::cout<<std::endl<<"-----------------------------------------"<<std::endl;
}

﻿#ifndef TESTACTIVATIONSNEON_H
#define TESTACTIVATIONSNEON_H
#ifdef USE_NEON
#include "Msnhnet/layers/MsnhActivationsNeon.h"
#include "Msnhnet/layers/MsnhActivations.h"
#define CATCH_CONFIG_RUNNER
#include "../3rdparty/catch2/catch.hpp"
#include <iostream>

TEST_CASE ("logistic","[single-file]")
{
    float a[] = {-2.f, -1.f, 0 ,1.f};
    float b[] = {-2.f, -1.f, 0 ,1.f};
    Msnhnet::Activations::activateArray(a,4,ActivationType::LOGISTIC,false);
    Msnhnet::ActivationsNeon::logisticActivateSize4(b);
    REQUIRE((a[0]-b[0])<0.00001);
    REQUIRE((a[1]-b[1])<0.00001);
    REQUIRE((a[2]-b[2])<0.00001);
    REQUIRE((a[3]-b[3])<0.00001);
    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<std::endl<<std::flush;
    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<std::endl<<std::flush;
}

TEST_CASE ("loggy","[single-file]")
{
    float a[] = {-2.f, -1.f, 0 ,1.f};
    float b[] = {-2.f, -1.f, 0 ,1.f};
    Msnhnet::Activations::activateArray(a,4,ActivationType::LOGGY,false);
    Msnhnet::ActivationsNeon::loggyActivateSize4(b);
    REQUIRE((a[0]-b[0])<0.00001);
    REQUIRE((a[1]-b[1])<0.00001);
    REQUIRE((a[2]-b[2])<0.00001);
    REQUIRE((a[3]-b[3])<0.00001);
    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<std::endl<<std::flush;
    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<std::endl<<std::flush;
}

TEST_CASE ("relu","[single-file]")
{
    float a[] = {-2.f, -1.f, 0 ,1.f};
    float b[] = {-2.f, -1.f, 0 ,1.f};
    Msnhnet::Activations::activateArray(a,4,ActivationType::RELU,false);
    Msnhnet::ActivationsNeon::reluActivateSize4(b);
    REQUIRE((a[0]-b[0])<0.00001);
    REQUIRE((a[1]-b[1])<0.00001);
    REQUIRE((a[2]-b[2])<0.00001);
    REQUIRE((a[3]-b[3])<0.00001);
    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<std::endl<<std::flush;
    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<std::endl<<std::flush;
}

TEST_CASE ("relu6","[single-file]")
{
    float a[] = {-2.f, -1.f, 0 ,1.f};
    float b[] = {-2.f, -1.f, 0 ,1.f};
    Msnhnet::Activations::activateArray(a,4,ActivationType::RELU6,false);
    Msnhnet::ActivationsNeon::relu6ActivateSize4(b);
    REQUIRE((a[0]-b[0])<0.00001);
    REQUIRE((a[1]-b[1])<0.00001);
    REQUIRE((a[2]-b[2])<0.00001);
    REQUIRE((a[3]-b[3])<0.00001);
    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<std::endl<<std::flush;
    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<std::endl<<std::flush;
}


TEST_CASE ("elu","[single-file]")
{
    float a[] = {-2.f, -1.f, 0 ,1.f};
    float b[] = {-2.f, -1.f, 0 ,1.f};
    Msnhnet::Activations::activateArray(a,4,ActivationType::ELU,false);
    Msnhnet::ActivationsNeon::eluActivateSize4(b);
    REQUIRE((a[0]-b[0])<0.00001);
    REQUIRE((a[1]-b[1])<0.00001);
    REQUIRE((a[2]-b[2])<0.00001);
    REQUIRE((a[3]-b[3])<0.00001);
    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<std::endl<<std::flush;
    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<std::endl<<std::flush;
}

TEST_CASE ("selu","[single-file]")
{
    float a[] = {-2.f, -1.f, 0 ,1.f};
    float b[] = {-2.f, -1.f, 0 ,1.f};
    Msnhnet::Activations::activateArray(a,4,ActivationType::SELU,false);
    Msnhnet::ActivationsNeon::seluActivateSize4(b);
    REQUIRE((a[0]-b[0])<0.00001);
    REQUIRE((a[1]-b[1])<0.00001);
    REQUIRE((a[2]-b[2])<0.00001);
    REQUIRE((a[3]-b[3])<0.00001);
    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<std::endl<<std::flush;
    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<std::endl<<std::flush;
}

TEST_CASE ("relie","[single-file]")
{
    float a[] = {-2.f, -1.f, 0 ,1.f};
    float b[] = {-2.f, -1.f, 0 ,1.f};
    Msnhnet::Activations::activateArray(a,4,ActivationType::RELIE,false);
    Msnhnet::ActivationsNeon::relieActivateSize4(b);
    REQUIRE((a[0]-b[0])<0.00001);
    REQUIRE((a[1]-b[1])<0.00001);
    REQUIRE((a[2]-b[2])<0.00001);
    REQUIRE((a[3]-b[3])<0.00001);
    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<std::endl<<std::flush;
    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<std::endl<<std::flush;
}

TEST_CASE ("ramp","[single-file]")
{
    float a[] = {-2.f, -1.f, 0 ,1.f};
    float b[] = {-2.f, -1.f, 0 ,1.f};
    Msnhnet::Activations::activateArray(a,4,ActivationType::RAMP,false);
    Msnhnet::ActivationsNeon::rampActivateSize4(b);
    REQUIRE((a[0]-b[0])<0.00001);
    REQUIRE((a[1]-b[1])<0.00001);
    REQUIRE((a[2]-b[2])<0.00001);
    REQUIRE((a[3]-b[3])<0.00001);
    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<std::endl<<std::flush;
    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<std::endl<<std::flush;
}

TEST_CASE ("leaky","[single-file]")
{
    float a[] = {-2.f, -1.f, 0 ,1.f};
    float b[] = {-2.f, -1.f, 0 ,1.f};
    Msnhnet::Activations::activateArray(a,4,ActivationType::LEAKY,false);
    Msnhnet::ActivationsNeon::leakyActivateSize4(b);
    REQUIRE((a[0]-b[0])<0.00001);
    REQUIRE((a[1]-b[1])<0.00001);
    REQUIRE((a[2]-b[2])<0.00001);
    REQUIRE((a[3]-b[3])<0.00001);
    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<std::endl<<std::flush;
    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<std::endl<<std::flush;
}

TEST_CASE ("tanh","[single-file]")
{
    float a[] = {-2.f, -1.f, 0 ,1.f};
    float b[] = {-2.f, -1.f, 0 ,1.f};
    Msnhnet::Activations::activateArray(a,4,ActivationType::TANH,false);
    Msnhnet::ActivationsNeon::tanhActivateSize4(b);
    REQUIRE((a[0]-b[0])<0.00001);
    REQUIRE((a[1]-b[1])<0.00001);
    REQUIRE((a[2]-b[2])<0.00001);
    REQUIRE((a[3]-b[3])<0.00001);
    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<std::endl<<std::flush;
    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<std::endl<<std::flush;
}

TEST_CASE ("stair","[single-file]")
{
    float a[] = {-2.f, -1.f, 0 ,1.f};
    float b[] = {-2.f, -1.f, 0 ,1.f};
    Msnhnet::Activations::activateArray(a,4,ActivationType::STAIR,false);
    Msnhnet::ActivationsNeon::stairActivateSize4(b);
    REQUIRE((a[0]-b[0])<0.00001);
    REQUIRE((a[1]-b[1])<0.00001);
    REQUIRE((a[2]-b[2])<0.00001);
    REQUIRE((a[3]-b[3])<0.00001);
    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<std::endl<<std::flush;
    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<std::endl<<std::flush;
}

TEST_CASE ("hardtan","[single-file]")
{
    float a[] = {-2.f, -1.f, 0 ,1.f};
    float b[] = {-2.f, -1.f, 0 ,1.f};
    Msnhnet::Activations::activateArray(a,4,ActivationType::HARDTAN,false);
    Msnhnet::ActivationsNeon::hardtanActivateSize4(b);
    REQUIRE((a[0]-b[0])<0.00001);
    REQUIRE((a[1]-b[1])<0.00001);
    REQUIRE((a[2]-b[2])<0.00001);
    REQUIRE((a[3]-b[3])<0.00001);
    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<std::endl<<std::flush;
    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<std::endl<<std::flush;
}

TEST_CASE ("softplus","[single-file]")
{
    float a[] = {-2.f, -0.1f, 0 ,0.1f};
    float b[] = {-2.f, -0.1f, 0 ,0.1f};
    Msnhnet::Activations::activateArray(a,4,ActivationType::SOFT_PLUS,false);
    Msnhnet::ActivationsNeon::softplusActivateSize4(b,0.1);
    REQUIRE((a[0]-b[0])<0.00001);
    REQUIRE((a[1]-b[1])<0.00001);
    REQUIRE((a[2]-b[2])<0.00001);
    REQUIRE((a[3]-b[3])<0.00001);
    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<std::endl<<std::flush;
    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<std::endl<<std::flush;
}

TEST_CASE ("plse","[single-file]")
{
    float a[] = {-4.f, -0.1f, 0 ,4.f};
    float b[] = {-4.f, -0.1f, 0 ,4.f};
    Msnhnet::Activations::activateArray(a,4,ActivationType::PLSE,false);
    Msnhnet::ActivationsNeon::plseActivateSize4(b);
    REQUIRE((a[0]-b[0])<0.00001);
    REQUIRE((a[1]-b[1])<0.00001);
    REQUIRE((a[2]-b[2])<0.00001);
    REQUIRE((a[3]-b[3])<0.00001);
    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<std::endl<<std::flush;
    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<std::endl<<std::flush;
}

TEST_CASE ("ltan","[single-file]")
{
    float a[] = {-4.f, -0.1f, 0 ,1.f};
    float b[] = {-4.f, -0.1f, 0 ,1.f};
    Msnhnet::Activations::activateArray(a,4,ActivationType::LHTAN,false);
    Msnhnet::ActivationsNeon::lhtanActivateSize4(b);
    REQUIRE((a[0]-b[0])<0.00001);
    REQUIRE((a[1]-b[1])<0.00001);
    REQUIRE((a[2]-b[2])<0.00001);
    REQUIRE((a[3]-b[3])<0.00001);
    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<std::endl<<std::flush;
    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<std::endl<<std::flush;
}

TEST_CASE ("mish","[single-file]")
{
    float a[] = {-4.f, -0.1f, 0 ,1.f};
    float b[] = {-4.f, -0.1f, 0 ,1.f};
    Msnhnet::Activations::activateArray(a,4,ActivationType::MISH,false);
    Msnhnet::ActivationsNeon::mishActivateSize4(b);
    REQUIRE((a[0]-b[0])<0.00001);
    REQUIRE((a[1]-b[1])<0.00001);
    REQUIRE((a[2]-b[2])<0.00001);
    REQUIRE((a[3]-b[3])<0.00001);
    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<std::endl<<std::flush;
    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<std::endl<<std::flush;
}

TEST_CASE ("swish","[single-file]")
{
    float a[] = {-4.f, -0.1f, 0 ,1.f};
    float b[] = {-4.f, -0.1f, 0 ,1.f};
    Msnhnet::Activations::activateArray(a,4,ActivationType::SWISH,false);
    Msnhnet::ActivationsNeon::swishActivateSize4(b);
    REQUIRE((a[0]-b[0])<0.00001);
    REQUIRE((a[1]-b[1])<0.00001);
    REQUIRE((a[2]-b[2])<0.00001);
    REQUIRE((a[3]-b[3])<0.00001);
    std::cout<<"\na: "<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<" "<<std::endl<<std::flush;
    std::cout<<"b: "<<b[0]<<" "<<b[1]<<" "<<b[2]<<" "<<b[3]<<" "<<std::endl<<std::flush;
}
#endif
#endif // TESTACTIVATIONSNEON_H

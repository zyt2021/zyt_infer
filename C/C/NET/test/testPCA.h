﻿#ifndef TESTPCA_H
#define TESTPCA_H

void pca()
{

string filename = "C:/Users/msnh/Desktop/zidane.jpg";
Mat img = imread(filename, IMREAD_GRAYSCALE);
imshow("a", img);
img.convertTo(img, CV_32FC1);//转换为float类型
string tile = "压缩比率";
Mat U, W, VT;
//    SVD svd;
//    svd.compute(img, W, U, VT);

//    std::cout<<W<<std::endl;
//    // std::cout<<U<<std::endl;
//    //std::cout<<VT<<std::endl;
//    //将矩阵进行压缩
//    //原始数据进行压缩之后是 m*m m*n n*n
//    //由于特征值在前几行中占有的比率是比较大的，所以，仅仅选择前几行(r)作为好的特征值
//    //那么分解的结果就变为 m*r r*r r*n 这样的形式
//    double radio = 0.01;//压缩比率
//    int rows = radio*img.rows;
//    Mat WROI = Mat::zeros(rows, rows, CV_32FC1);//W矩阵的大小
//    //填充举着WROI;
//    for (int i = 0; i < rows;++i)
//    {
//        WROI.at<float>(i, i) = W.at<float>(i);
//    }
//    Mat UROI = U.colRange(0, rows);//主要注意的是，colrange中不包含End
//    Mat VTROI = VT.rowRange(0, rows);
//    Mat Result = UROI*WROI*VTROI;
//    //将结果转换为灰度影像
//    Result.convertTo(Result, CV_8UC1);
//    namedWindow(tile);
//    imshow(tile, Result);
//    waitKey(0);
//return 0;

Msnhnet::Mat mat1(img.rows,img.cols,Msnhnet::MAT_GRAY_F32);

for (int i = 0; i < img.cols; ++i)
{
    for (int j = 0; j < img.rows; ++j)
    {
        mat1.getFloat32()[i*img.rows + j] = img.at<float>(j,i);
    }
}

std::vector<Msnhnet::Mat> aaa = mat1.svd();

float rate = 0.01f;

int r = (int)(mat1.getHeight()*rate);

Msnhnet::Mat AA = aaa[0].colRange(0,r);
Msnhnet::Mat BB(r,r,AA.getMatType());
Msnhnet::Mat CC = aaa[2].rowRange(0,r);

AA.print();
BB.print();
CC.print();

for (int i = 0; i < r; ++i)
{
    BB.getFloat32()[i*r+i] = aaa[1].getFloat32()[i];
}

Msnhnet::Mat final = AA*BB*CC;


for (int i = 0; i < img.cols; ++i)
{
    for (int j = 0; j < img.rows; ++j)
    {
        img.at<float>(j,i) = final.getFloat32()[i*img.rows + j];
    }
}

img.convertTo(img, CV_8UC1);
tile = "fuck";
namedWindow(tile);
imshow(tile, img);
waitKey(0);
}
//aaa.print();

#endif // TESTPCA_H

﻿#ifndef TEST3POINTTOCIRCLE_H
#define TEST3POINTTOCIRCLE_H
#include <Msnhnet/math/MsnhGeometryS.h>
#include <Msnhnet/robot/MsnhFrame.h>
Msnhnet::Vector5DS getCenter(const Msnhnet::Frame& frame1,const Msnhnet::Frame& frame2,const Msnhnet::Frame& frame3)
{
    Msnhnet::TranslationDS p1 = frame1.trans;
    Msnhnet::TranslationDS p2 = frame2.trans;
    Msnhnet::TranslationDS p3 = frame3.trans;

    Msnhnet::TranslationDS pa = p1-p2;
    Msnhnet::TranslationDS pb = p3-p2;

    Msnhnet::TranslationDS pc = Msnhnet::TranslationDS::crossProduct(pa,pb);

    if(pa.length()<MSNH_F32_EPS || pb.length()<MSNH_F32_EPS || pc.length()<MSNH_F32_EPS)
    {
        throw Msnhnet::Exception(1,"3 points on line!",__FILE__,__LINE__,__FUNCTION__);
    }
    double x1 = p1[0];
    double y1 = p1[1];
    double z1 = p1[2];

    double x2 = p2[0];
    double y2 = p2[1];
    double z2 = p2[2];

    double x3 = p3[0];
    double y3 = p3[1];
    double z3 = p3[2];

    double l1 = x1*x1 + y1*y1 + z1*z1;
    double l2 = x2*x2 + y2*y2 + z2*z2;
    double l3 = x3*x3 + y3*y3 + z3*z3;

    double m = y1*(z2 - z3) + y2*(z3 - z1) + y3*(z1 - z2);
    double n = -(x1*(z2 - z3) + x2*(z3 - z1) + x3*(z1 - z2));
    double p = x1*(y2 - y3) - x2*(y1 - y3) - x3*(y2 - y1);
    double q = -(x1*(y2*z3 - y3*z2) - x2*y1*z3 + x2*y3*z1 + x3*(y1*z2 - y2*z1));


    Msnhnet::MatSDS mat(4,4);

    mat.setVal({
        x1,y1,z1,1,
        x2,y2,z2,1,
        x3,y3,z3,1,
        -m,-n,-p,0
    });

    double A = mat.det();

    mat.setVal({
        l1,y1,z1,1,
        l2,y2,z2,1,
        l3,y3,z3,1,
        2*q,-n,-p,0
    });

    double B= -mat.det();

    mat.setVal({
        l1,x1,z1,1,
        l2,x2,z2,1,
        l3,x3,z3,1,
        2*q,-m,-p,0
    });

    double C= mat.det();

    mat.setVal({
        l1,x1,y1,1,
        l2,x2,y2,1,
        l3,x3,y3,1,
        2*q,-m,-n,0
    });

    double D= -mat.det();

    Msnhnet::TranslationDS center(-0.5*B/A,-0.5*C/A,-0.5*D/A);

    Msnhnet::TranslationDS OA = frame1.trans - center;
    Msnhnet::TranslationDS OB = frame2.trans - center;
    Msnhnet::TranslationDS OC = frame3.trans - center;

    double angAOB = acos((OA[0]*OB[0]+ OA[1]*OB[1]+OA[2]*OB[2])/(OA.length()*OB.length()));
    double angBOC = acos((OB[0]*OC[0]+ OB[1]*OC[1]+OB[2]*OC[2])/(OB.length()*OC.length()));
    double angAOC = acos((OA[0]*OC[0]+ OA[1]*OC[1]+OA[2]*OC[2])/(OA.length()*OC.length()));


    if(angAOB >= angAOC || angBOC >= angAOC)
    {
        angAOC = MSNH_2_PI - angAOC; //取补角
    }

    return Msnhnet::Vector5DS({center[0],center[1],center[2],angAOC,OA.length()});
}

#endif // TEST3POINTTOCIRCLE_H

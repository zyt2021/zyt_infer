﻿#include "Msnhnet/Msnhnet.h"
int main(void)
{
    Msnhnet::PermuteLayer a(1,2,3,4,2,1,0);

    Msnhnet::NetworkState netstate;
    Msnhnet::NetworkState netstategpu;

    float va[] = {1.0f,2.0f,
                  3.0f,4.0f,
                  5.0f,6.0f,

                  1.0f,2.0f,
                  3.0f,4.0f,
                  5.0f,6.0f,

                  1.0f,2.0f,
                  3.0f,4.0f,
                  5.0f,6.0f,

                  1.0f,2.0f,
                  3.0f,4.0f,
                  5.0f,6.0f,
                 };

    Msnhnet::Cuda::getBlasHandle();
    float* cudaVal = Msnhnet::Cuda::makeCudaArray(va, 24, cudaMemcpyHostToDevice);

    netstate.input = va;
    netstate.inputNum = 24;

    netstategpu.input = cudaVal;
    netstategpu.inputNum = 24;

    a.forward(netstate);

    a.forwardGPU(netstategpu);

    float* out = a.getOutput();

    float* outc = new float[24]();
    Msnhnet::Cuda::pullCudaArray(a.getGpuOutput(), outc,24);
    for (int i = 0; i < 24; ++i)
    {
        std::cout<<out[i]<<" : "<<outc[i]<<std::endl;
    }
    Msnhnet::Cuda::freeCuda(cudaVal);
    Msnhnet::Cuda::deleteBlasHandle();

    delete[] outc;
    outc = nullptr;

    return 1;

}
